function activePropertyCheckFailModal(cancelFunction, proceedFunction, cancelTransactionFunction, warningMessage)
{
    swal({
        type: "warning",
        html:   '<div class="row">'+
            '<div class="col-lg-12 col-md-12 col-sm-12">'+warningMessage+'</div>'+
            '<div class="col-lg-4 col-md-4 col-sm-6"><button id="cancelOp"  class="btn-v2 btn-red">Cancel</button></div>'+
            '<div class="col-lg-4 col-md-4 col-sm-6"><button id="removeOp"  class="btn-v2 btn-red">Cancel Transaction</button></div>'+
            '<div class="col-lg-4 col-md-4 col-sm-6"><button id="proceedOp" class="btn-v2 btn-teal">Proceed</button></div>'+
            '</div>',
        showConfirmButton: false,
        allowOutsideClick: false,
        allowEscapeKey: false,
        onBeforeOpen: () => {
            const content = swal.getContent();
            const s$ = content.querySelector.bind(content);
            const cancel    = s$('#cancelOp');
            const proceed   = s$('#proceedOp');
            const remove    = s$('#removeOp');

            cancel.addEventListener('click', () => {
                cancelFunction();
                swal.close();
            });
            proceed.addEventListener('click', () => {
                proceedFunction();
                swal.close();
            });
            remove.addEventListener('click', () => {
                cancelTransactionFunction();
                swal.close();
            });
        },
    });
}

function addressOverride(route, token, address, transactionID)
{
    $.ajax({
        url : route,
        type: "post",
        tryCount : 0,
        retryLimit : 3,
        data: {"_token":token, "address":address, "transaction_id":transactionID},
        success: function(data){

        },
        error : function(xhr, textStatus, errorThrown ) {
            if (textStatus === 'timeout') {
                this.tryCount++;
                if (this.tryCount <= this.retryLimit) {
                    $.ajax(this);
                }
            }
        },
        timeout: 0
    });

}

function correctVuexRouteURL(routeObject)
{
    if (routeObject[0])
    {
        let route = routeObject[0].route;
        if (window.location.host == 'localhost' || window.location.host == '127.0.0.1') {
            return window.location.protocol + '//' + window.location.host + '/OfferToClose/public' + route;
        }
        return window.location.protocol + '//' + window.location.host + route;
    }
    return false;
}

/**
 * Returns index of item that first matches value. Only works with arrays of objects.
 * @param arr
 * @param property
 * @param val
 * @returns {*}
 */
function searchArrayOfObjectsForFirstValue(arr,property,val) {
    let totalLength = arr.length;
    for (var i=0; i<totalLength; i++)
        if (arr[i][property] === val)
            return i;
    return false;
}

/**
 *
 * @param className
 * @param url
 * @param redirectURL
 * This function switches the role of the user based on the data attached to the element clicked.
 * Note that you will have to attach what role you want to change into as a data attribute.
 * You can also attach the URL and RedirectURL as data attributes or pass them in as arguments.
 * The URL: the url of the controller for ajax to call
 * The redirectURL: the url to redirect the user after ajax success.
 */
function switchRoles(className,url = null,redirectURL = null)
{
    var self            = $('.'+className);
    var clickedRole     = self.data('role');
    if(url === null) url                        = self.data('url');
    if(redirectURL === null) redirectURL        = self.data('redirect-url');
    if(clickedRole)
    {
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                role: clickedRole,
            },
            error: function(err){

            },
            success: function(response){
                if(response.status === 'success')
                {
                    window.location.href = redirectURL;
                }
            },
        });
    }
}

/**
 * This function takes a value, such as 1000
 * and returns it in currency format with the correct commas.
 * @param value
 */
function currencyFormat(value)
{
    return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
 * Adds the offset to the date. Does not take into account business days or holidays.
 * @param date
 * @param offset
 * @returns {string}
 */
function addOffset(date, offset){
    let days = parseInt(offset);
    date = date.split(' ');
    date = date[0];
    let newdate = new Date(date);
    newdate.setDate(newdate.getDate()+parseInt(days)+1);
    return newdate.getFullYear()+'-'+(newdate.getMonth()+1)+'-'+newdate.getDate();
}

/**
 * This function brings up the dialog needed to advise the user of the tasks that will be affected if they change a specific
 * timeline date.
 *
 * Finishing function can be false and it will simply close the dialog once it is done.
 * @param milestoneData
 * @param routes
 * @param transactionID
 * @param finishingFunction
 * @param token
 * @param headerText
 * @param locked
 * @param mustBeBusinessDay
 * @param notesEnabled
 */
function timelineDateChangeDialog(milestoneData, routes, transactionID, finishingFunction, token, headerText = '', locked = false, mustBeBusinessDay = false, notesEnabled = false)
{
    let approveTaskChangeRoute      = routes.approveTaskChangeRoute  || null;
    let listAffectedTasksRoute      = routes.listAffectedTasksRoute  || null;
    let updateMilestoneLengthRoute  = routes.updateMilestoneLengthRoute || null;
    milestoneData.ID            = parseInt(milestoneData.ID);
    let chainSteps              = [];
    let chainStepDetails        = [];
    let steps                   = [];
    let pendingTaskListChanges  = [];
    let disabled                = '';
    if(locked) disabled         = 'disabled';
    let modalHTML = '' +
        '<div class="row">' +
        '    <h4>'+headerText+'</h4>' +
        '    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin: auto;">' +
        '        <input type="date" id="datePicker" class="form-control" value="'+milestoneData.DateDue+'" autofocus '+disabled+'>' +
        '    </div>';

    if (notesEnabled) modalHTML +=
        '   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin: auto;">' +
        '       <textarea id="notes" placeholder="Notes">'+(milestoneData.Notes || '')+'</textarea>' +
        '   </div>';

    modalHTML +=
        '    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="margin:auto;"><button class="btn-v2 btn-teal" id="saveNewDate">Save</button></div>' +
        '</div>';
    Swal2({
        title: milestoneData.Description,
        html: modalHTML,
        showCloseButton: true,
        showConfirmButton: false,
        showCancelButton: true,
        onBeforeOpen: () => {
            const content           = Swal2.getContent();
            const $s                = content.querySelector.bind(content);
            let saveNewDate         = $s('#saveNewDate');
            let dateInput;
            let notes;
            saveNewDate.addEventListener('click', () => {
                dateInput = $s('#datePicker').value+' 00:00';

                let checkForAffectedTasks = true;
                if(dateInput == milestoneData.DateDue + ' 00:00') checkForAffectedTasks = false;

                if(notesEnabled) notes = $s('#notes').value;
                else notes = milestoneData.Notes;

                if(mustBeBusinessDay)
                {
                    if (!isBusinessDayFunction(dateInput))
                    {
                        dateInput = moveToBusinessDay(dateInput);
                        dateInput = dateInput.toLocaleString('en-US');
                    }
                }

                if(checkForAffectedTasks)
                {
                    $.ajax({
                        url: listAffectedTasksRoute,
                        method: 'post',
                        data: {
                            milestoneID: milestoneData.ID,
                            transactionID: transactionID,
                        },
                        success: function(r){
                            let taskList = r.list;
                            if(taskList.length === 0 || !taskList)
                            {
                                $.ajax({
                                    url: approveTaskChangeRoute,
                                    type: "post",
                                    data: {
                                        milestoneID:    milestoneData.ID,
                                        transactionID:  transactionID,
                                        status:         milestoneData.Description,
                                        newDate:        dateInput,
                                        Notes:          notes,
                                    },
                                    success: function(){
                                        Swal2({
                                            title: 'Saved',
                                            type: 'success',
                                            showConfirmButton: false,
                                            timer: 2000,
                                        });
                                        if(
                                            milestoneData.Description === 'Close Escrow' ||
                                            milestoneData.Description == 'Appraisal Completed' ||       //  Deprecated
                                            milestoneData.Description == 'Appraisal Contingency Completed' ||
                                            milestoneData.Description == 'Loan Contingency' ||
                                            milestoneData.Description == 'Inspection Contingency'
                                        )
                                        {
                                            if(updateMilestoneLengthRoute)
                                            {
                                                $.ajax({
                                                    url: updateMilestoneLengthRoute,
                                                    type: 'post',
                                                    data:{
                                                        transactionID: transactionID,
                                                        newDate: dateInput,
                                                        milestone: milestoneData.Description,
                                                    },
                                                    success: function (r) {
                                                        if(!r.status === 'success')
                                                        {
                                                            genericError('Error updating escrow length.');
                                                        }
                                                    },
                                                    error: function (err) {
                                                        genericError('Error updating escrow length.');
                                                    }
                                                });
                                            }
                                        }
                                        if(!finishingFunction)
                                        {
                                            Swal2.close();
                                        }
                                        else
                                        {
                                            finishingFunction();
                                        }
                                    },
                                    error: function(){
                                        Swal2({
                                            title: 'Error',
                                            text: 'An unknown error has occurred. Please try again.',
                                            timer: 2000,
                                            showConfirmButton: false,
                                            type: 'error',
                                        });
                                    },
                                })
                            }
                            else
                            {
                                let modalHTML =
                                    '<div class="row">' +
                                    '<h3>' +
                                    '    Changing the date of <i>'+milestoneData.Description+'</i> impacts the following tasks. Do you wish to proceed?' +
                                    '</h3>' +
                                    '<div class="col-xs-12">';
                                $.each(taskList, (i,obj) => {
                                    modalHTML += '' +
                                        '<div style="margin-bottom: 10px; padding: 20px; border-bottom:solid 2px #21d7d1;">' +
                                        '    <div>' +
                                        '        <div style="text-align: left;">'+(parseInt(i)+1)+'. '+obj.Task+'.</div>' +
                                        '    </div>' +
                                        '</div>';
                                });
                                modalHTML += '' +
                                    '</div>' +
                                    '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
                                    '    <button id="proceedWithChanges" class="btn-v2 btn-teal">Proceed</button>' +
                                    '</div>' +
                                    '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
                                    '    <button id="cancelChanges" class="btn-v2 btn-red">Cancel</button>' +
                                    '</div>' +
                                    '</div>';
                                Swal2({
                                    html: modalHTML,
                                    showConfirmButton: false,
                                    allowOutsideClick: false,
                                    onBeforeOpen: () => {
                                        const content   = Swal2.getContent();
                                        const $s        = content.querySelector.bind(content);
                                        let cancel      = $s('#cancelChanges');
                                        let proceed     = $s('#proceedWithChanges');
                                        cancel.addEventListener('click',()=>{
                                            Swal2.close();
                                        });
                                        proceed.addEventListener('click',()=>{
                                            if(
                                                milestoneData.Description === 'Close Escrow' ||
                                                milestoneData.Description == 'Appraisal Completed' ||       //  Deprecated
                                                milestoneData.Description == 'Appraisal Contingency Completed' ||
                                                milestoneData.Description == 'Loan Contingency' ||
                                                milestoneData.Description == 'Inspection Contingency'
                                            )
                                            {
                                                if(updateMilestoneLengthRoute)
                                                {
                                                    $.ajax({
                                                        url: updateMilestoneLengthRoute,
                                                        type: 'post',
                                                        data:{
                                                            transactionID: transactionID,
                                                            newDate: dateInput,
                                                            milestone: milestoneData.Description,
                                                        },
                                                        success: function (r) {
                                                            if(!r.status === 'success') genericError('Error updating escrow length.');
                                                        },
                                                        error: function (err) {
                                                            genericError('Error updating escrow length.');
                                                        }
                                                    });
                                                }
                                            }
                                            $.ajax({
                                                url: approveTaskChangeRoute,
                                                type: "post",
                                                data: {
                                                    milestoneID:    milestoneData.ID,
                                                    transactionID:  transactionID,
                                                    status:         milestoneData.Description,
                                                    newDate:        dateInput,
                                                    Notes:          notes,
                                                },
                                                success: function (r){
                                                    let editedTaskList      = r.list;
                                                    let listLength          = r.list.length;
                                                    let proposedTaskDate;
                                                    $.each(editedTaskList, (i,obj) => {
                                                        proposedTaskDate = addOffset(dateInput, obj.DateOffset);
                                                        let modalHTML =
                                                            '<div class="row">' +
                                                            '<h3>Task '+(i+1)+': '+obj.Task+'</h3>' +
                                                            '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">' +
                                                            '    <p>' +
                                                            '    Changing '+obj.TimelineName+'\'s date, will change this task\'s due date' +
                                                            '    from '+moment(obj.TaskDateDue.substring(0,10)).format("dddd, MMMM Do YYYY")+' to '+moment(proposedTaskDate).format("dddd, MMMM Do YYYY")+
                                                            '    </p>' +
                                                            '</div>' +
                                                            '<div>' +
                                                            '    <p>You can approve this change or skip the change to leave the task as it is.</p>' +
                                                            '</div>' +
                                                            '    ' +
                                                            '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
                                                            '    <button id="approveChange" class="btn-v2 btn-teal" data-id="'+obj.ID+'" data-proposedDate="'+proposedTaskDate+'">Approve Change</button>' +
                                                            '</div>' +
                                                            '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
                                                            '    <button id="skipChange" class="btn-v2 btn-red">Skip Change</button>' +
                                                            '</div>' +
                                                            '</div>';
                                                        chainSteps.push(modalHTML);
                                                        chainStepDetails.push({
                                                            TaskID: obj.ID,
                                                            ProposedDate: proposedTaskDate,
                                                        });
                                                        steps.push((i+1));
                                                    });
                                                    function showAlertNTimes(n) {
                                                        if (n > 0) {
                                                            Swal2({
                                                                html: chainSteps[chainSteps.length - n],
                                                                showConfirmButton: false,
                                                                allowOutsideClick: false,
                                                                onBeforeOpen: () => {
                                                                    const content       = Swal2.getContent();
                                                                    const $s            = content.querySelector.bind(content);
                                                                    let approve         = $s('#approveChange');
                                                                    let skip            = $s('#skipChange');
                                                                    approve.addEventListener('click', function(){
                                                                        let taskID          = chainStepDetails[chainStepDetails.length-n].TaskID;
                                                                        let proposedDate    = chainStepDetails[chainStepDetails.length-n].ProposedDate;
                                                                        pendingTaskListChanges.push({ID: taskID, NewDate: proposedDate});
                                                                        showAlertNTimes(n - 1);
                                                                    });
                                                                    skip.addEventListener('click', function(){
                                                                        showAlertNTimes(n - 1);
                                                                    });
                                                                },
                                                            });
                                                        }
                                                        else
                                                        {
                                                            $.ajax({
                                                                url: approveTaskChangeRoute,
                                                                type: "post",
                                                                data: {
                                                                    list: pendingTaskListChanges,
                                                                    transactionID: transactionID,
                                                                    pendingTasks: true,
                                                                    _token: token
                                                                },
                                                                success: function (data) {
                                                                    Swal2({
                                                                        title: 'Saved',
                                                                        type: 'success',
                                                                        showConfirmButton: false,
                                                                        timer: 2000,
                                                                    });
                                                                    if(!finishingFunction)
                                                                    {
                                                                        Swal2.close();
                                                                    }
                                                                    else
                                                                    {
                                                                        finishingFunction();
                                                                    }
                                                                },
                                                                error: function(){
                                                                    Swal2({
                                                                        title: 'Error',
                                                                        text: 'An unknown error has occurred. Please try again.',
                                                                        timer: 2000,
                                                                        showConfirmButton: false,
                                                                        type: 'error',
                                                                    });
                                                                },
                                                            });
                                                        }
                                                    }

                                                    showAlertNTimes(chainSteps.length);
                                                },
                                                error: function(){

                                                },
                                            }); //end ajax
                                        }); //end of proceed function
                                    },
                                });
                            }
                        },
                        error: function(err){
                            //console.log(err);
                        }
                    });
                }
                else
                {
                    $.ajax({
                        url: approveTaskChangeRoute,
                        type: "post",
                        data: {
                            milestoneID:    milestoneData.ID,
                            transactionID:  transactionID,
                            status:         milestoneData.Description,
                            newDate:        dateInput,
                            Notes:          notes,
                        },
                        success: function(){
                            Swal2({
                                title: 'Saved',
                                type: 'success',
                                showConfirmButton: false,
                                timer: 2000,
                            });
                            if(
                                milestoneData.Description === 'Close Escrow' ||
                                milestoneData.Description == 'Appraisal Completed' ||        //  Deprecated
                                milestoneData.Description == 'Appraisal Contingency Completed' ||
                                milestoneData.Description == 'Loan Contingency' ||
                                milestoneData.Description == 'Inspection Contingency'
                            )
                            {
                                if(updateMilestoneLengthRoute)
                                {
                                    $.ajax({
                                        url: updateMilestoneLengthRoute,
                                        type: 'post',
                                        data:{
                                            transactionID: transactionID,
                                            newDate: dateInput,
                                            milestone: milestoneData.Description
                                        },
                                        success: function (r) {
                                            if(!r.status === 'success')
                                            {
                                                genericError('Error updating escrow length.');
                                            }
                                        },
                                        error: function (err) {
                                            genericError('Error updating escrow length.');
                                        }
                                    });
                                }
                            }
                            if(!finishingFunction)
                            {
                                Swal2.close();
                            }
                            else
                            {
                                finishingFunction();
                            }
                        },
                        error: function(){
                            Swal2({
                                title: 'Error',
                                text: 'An unknown error has occurred. Please try again.',
                                timer: 2000,
                                showConfirmButton: false,
                                type: 'error',
                            });
                        },
                    })
                }

            });
        },
    });
}


function businessDaysFromDate(date,businessDays)
{
    var counter = 0, tmp = new Date(date);
    while( businessDays>=0 ) {
        tmp.setTime( date.getTime() + counter * 86400000 );
        if(isBusinessDayFunction (tmp)) {
            --businessDays;
        }
        ++counter;
    }
    return tmp;
}

function isBusinessDayFunction (date)
{
    if (date)
    {
        date = new Date(date);
        let dayOfWeek = date.getDay();
        if (dayOfWeek === 0 || dayOfWeek === 6) {
            // Weekend
            return false;
        }

        let holidays = [
            '12/31+5', // New Year's Day on a saturday celebrated on previous friday
            '1/1',     // New Year's Day
            '1/2+1',   // New Year's Day on a sunday celebrated on next monday
            '1-3/1',   // Birthday of Martin Luther King, third Monday in January
            '2-3/1',   // Washington's Birthday, third Monday in February
            '5~1/1',   // Memorial Day, last Monday in May
            '7/3+5',   // Independence Day
            '7/4',     // Independence Day
            '7/5+1',   // Independence Day
            '9-1/1',   // Labor Day, first Monday in September
            '10-2/1',  // Columbus Day, second Monday in October
            '11/10+5', // Veterans Day
            '11/11',   // Veterans Day
            '11/12+1', // Veterans Day
            '11-4/4',  // Thanksgiving Day, fourth Thursday in November
            '12/24+5', // Christmas Day
            '12/25',   // Christmas Day
            '12/26+1',  // Christmas Day
        ];

        let dayOfMonth = date.getDate(),
            month = date.getMonth() + 1,
            monthDay = month + '/' + dayOfMonth;

        if (holidays.indexOf(monthDay) > -1) {
            return false;
        }

        let monthDayDay = monthDay + '+' + dayOfWeek;
        if (holidays.indexOf(monthDayDay) > -1) {
            return false;
        }

        let weekOfMonth = Math.floor((dayOfMonth - 1) / 7) + 1,
            monthWeekDay = month + '-' + weekOfMonth + '/' + dayOfWeek;
        if (holidays.indexOf(monthWeekDay) > -1) {
            return false;
        }
        let lastDayOfMonth = new Date(date);
        lastDayOfMonth.setMonth(lastDayOfMonth.getMonth() + 1);
        lastDayOfMonth.setDate(0);
        let negWeekOfMonth = Math.floor((lastDayOfMonth.getDate() - dayOfMonth - 1) / 7) + 1,
            monthNegWeekDay = month + '~' + negWeekOfMonth + '/' + dayOfWeek;

        if(holidays.indexOf(monthNegWeekDay)>-1){
            return false;
        }

        return true;
    }
}

/**
 * Accepts a date string and moves it to the next business day.
 * @param date
 * @returns {Date}
 */
function moveToBusinessDay(date)
{
    if(date)
    {
        date = new Date(date);
        let dayOfWeek = date.getDay();
        if (dayOfWeek === 0) {
            date.setDate(date.getDate()+1);
        }
        else if(dayOfWeek === 6)
        {
            date.setDate(date.getDate()+2);
        }

        if(!isBusinessDayFunction(date))
        {
            date.setDate(date.getDate()+1);
            moveToBusinessDay(date);
        }
        return date;
    }
}

/**
 * Used to calculate the days in between two dates.
 * @param dateOne
 * @param dateTwo
 * @returns {number}
 */
function daysBetweenTwoDates(dateOne,dateTwo)
{
    let oneDay      = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    let firstDate   = new Date(dateOne);
    let secondDate  = new Date(dateTwo);

    return Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
}

/**
 * Used to recalculate timeline and task dates when date of acceptance changes.
 * @param recalculateTimelineRoute
 * @param transactionID
 * @param dateAcceptance
 * @param successFunction
 */
function recalculateTimeline(recalculateTimelineRoute,transactionID, dateAcceptance, successFunction)
{
    $.ajax({
        url:recalculateTimelineRoute,
        method: 'post',
        data: {
            transactionID: transactionID,
            dateAcceptance: dateAcceptance,
        },
        success: function(r){
            if(r.status)
            {
                if (r.status === 'success')
                {
                    Swal2({
                        title: 'Success',
                        type: 'success',
                        timer: 2000,
                    });
                    successFunction();
                }
                else if (r.status === 'fail')
                {
                    genericError(r.message);
                }
                else
                {
                    genericError();
                }
            }
        },
        error: function(err){
            genericError();
        },
    });
}

/**
 * Generic modals, you can pass in text.
 * @param text
 */
function genericError(text = null)
{
    Swal2({
        title: 'Error',
        type: 'error',
        text: text || 'An unknown error has occurred. Please try again.',
        showConfirmButton: true,
        allowOutsideClick: false,
    });
}

function genericSuccess(text = null, timer = 1500)
{
    Swal2({
        title: 'Success',
        type: 'success',
        text: text || 'Data successfully processed.',
        showConfirmButton: false,
        allowOutsideClick: false,
        timer: timer,
    });
}

function genericSuccessToast(text = null)
{
    Swal2({
        title: 'Success',
        type: 'success',
        toast: true,
        position: 'top-end',
        text: text || 'Data successfully processed.',
        showConfirmButton: false,
        allowOutsideClick: false,
        timer: 2000,
    });
}

function genericInfo(text, title = null)
{
    Swal2({
        title: title,
        type: 'info',
        text: text,
        showConfirmButton: true,
        confirmButtonText: 'Got it!',
        allowOutsideClick: false,
    });
}

function genericWarning(text,confirmButtonText = null, title = null)
{
    Swal2({
        title: title || 'Warning',
        type: 'warning',
        text: text,
        showConfirmButton: true,
        confirmButtonText: confirmButtonText || 'Okay',
        allowOutsideClick: false,
    });
}