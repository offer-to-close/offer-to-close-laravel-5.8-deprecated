/**
 * When passing in the route, put ':side' as the parameter that takes the side.
 * @param transactionCreateRoute
 */
function addNewTransactionModal(transactionCreateRoute)
{
    let modalHTML = '<div>' +
        '<div>Please select a side for the transaction.</div>' +
        '<div class="row" style="text-align:center;">' +
        '   <div class="col-sm-12 col-md-2 col-lg-2"></div>' +
        '       <div class="col-sm-12 col-md-8 col-lg-8" style="display:inline-block;">' +
        '           <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 10px;">' +
        '               <button class="btn-v2 btn-teal" id="buyerTransaction">Buyer</button>' +
        '           </div>' +
        '           <div class="col-sm-12 col-md-6 col-lg-6" style="padding: 10px;">' +
        '               <button class="btn-v2 btn-teal" id="sellerTransaction">Seller</button>' +
        '           </div>\n' +
        '           <div class="col-sm-12 col-md-12 col-lg-12" style="padding: 10px;">' +
        '               <button class="btn-v2 btn-teal" id="bothTransaction">Both</button>' +
        '           </div>\n' +
        '       </div>\n' +
        '   <div class="col-sm-12 col-md-2 col-lg-2"></div>' +
        '</div>' +
        '</div>';
    Swal2({
        title: 'Select Side',
        html: modalHTML,
        showConfirmButton: false,
        showCancelButton: true,
        showCloseButton: true,
        onBeforeOpen: () => {
            const content       = Swal2.getContent();
            const s$            = content.querySelector.bind(content);
            const b             = s$('#buyerTransaction');
            const s             = s$('#sellerTransaction');
            const bs            = s$('#bothTransaction');
            b.addEventListener('click', () => {
                window.location.href = transactionCreateRoute.replace(':side', 'b');
            });
            s.addEventListener('click', () => {
                window.location.href = transactionCreateRoute.replace(':side', 's');
            });
            bs.addEventListener('click', () => {
                window.location.href = transactionCreateRoute.replace(':side', 'bs');
            });
        }
    });
}

function assignDataToModal(routes, HTMLContainerID, fillInElements)
{
    let limit               = 50;
    let offset              = 0;
    let searchPerson        = $('.search_person');
    let searchList          = $('.search_list');
    let id                  = 0;
    let searchRoute         = routes.searchRoute;
    let receiveDataRoute    = routes.receiveDataRoute;

    searchPerson.keyup(_.debounce(function () {
        if(searchPerson.val().length > 2)
        {
            function fetchRecords()
            {
                $.ajax({
                    url: searchRoute,
                    method: 'POST',
                    data: {
                        _table      : $('#'+HTMLContainerID+' input[name="_table"]').val(),
                        _model      : $('#'+HTMLContainerID+' input[name="_model"]').val(),
                        _roleCode   : $('#'+HTMLContainerID+' input[name="_roleCode"]').val(),
                        _token      : $('#'+HTMLContainerID+' input[name="_token"]').val(),
                        search_query: searchPerson.val(),
                        limit       : limit,
                        offset      : offset,
                    },
                    success: function (r) {
                        for (let i in r)
                        {
                            if (r.hasOwnProperty(i))
                            {
                                let license =   r[i]['License'] === null || r[i]['License'] === undefined ? '' : '#'+r[i]['License'];
                                let result  =   r[i]["NameFull"] ? r[i]["NameFull"] : r[i]["NameFirst"] && r[i]["NameLast"] ?
                                    r[i]["NameFirst"] + ' ' + r[i]["NameLast"] : r[i]['Company'] ? r[i]['Company'] : '';
                                let output  =   result + ' ' + license;
                                searchList.append('<li data-id=' + r[i]["ID"] + '> ' + output + '</li>');
                            }
                        }
                        searchList.show();
                    },
                    error: function () {
                        genericError('Error code: data-modal-001');
                    }
                }).then(()=>{

                    searchList.on('scroll', function () {
                        var el = $(this);
                        var scrollTop = el.scrollTop();
                        var scrollHeight = this.scrollHeight;
                        if (Math.round(scrollTop + $(this).innerHeight(), 10) >= Math.round(scrollHeight, 10)) {
                            limit += 50;
                            offset += 50;
                            fetchRecords();
                        }
                    });

                    searchPerson.on('click mouseenter',function () {
                        searchList.show();
                    });

                    searchList.mouseleave(function () {
                        searchList.hide();
                    });

                    searchList.scroll(_.debounce(function () {
                        searchList.hide();
                    }, 2000));

                    $('.search_list li').bind('click').on('click', function ()
                    {
                        id =  $(this).data('id');
                        searchList.hide();
                        if(receiveDataRoute)
                        {
                            $.ajax({
                                url: receiveDataRoute,
                                method: 'POST',
                                data: {
                                    ID: id,
                                    _model: $('#'+HTMLContainerID+' input[name="_model"]').val(),
                                    _token: $('#'+HTMLContainerID+' input[name="_token"]').val(),
                                },
                                success: function (r) {
                                    if(r.data[0])
                                    {
                                        if(r.data[0].Address)
                                        {
                                            let address = r.data[0].Address;
                                            address = address.replace('  ,',',');
                                            address = address.replace(' ,',',');
                                            address = address.replace(',,',' ');
                                            r.data[0].Address = address;
                                        }
                                        $.each(fillInElements, (i,obj) => {
                                            $('#'+HTMLContainerID+' input[name="'+obj+'"]').val(r.data[0][obj]);
                                        });
                                    }
                                    else
                                    {
                                        genericError('Error code: data-modal-003');
                                    }
                                },
                                error: function (err) {
                                    genericError('Error code: data-modal-002');
                                },
                            })
                        }
                    });
                });
            }

            fetchRecords();
        }
    }, 1000));
}

/**
 * The html that defines the search container inside of the modal.
 * It will only draw the search container if table and modal are not falsy.
 * Returns the HTML which can be used inside of modals.
 * @param table
 * @param model
 * @returns {string}
 */
function searchObjectHTML(table ='',model = '')
{
    if (table && model)
    {
        let searchObject = {
            _table: table,
            _model: model,
            _pickedID: '',
        };
        let html =
            '<div class="col-md-12 search-container col-xs-12 col-sm-12 text-left">' +
            '    <div class="col-md-12 col-xs-12 field-box">';
        $.each(searchObject, function (i, obj) {
            html += '<input type="hidden" name="' + i + '" value="' + obj + '">';
        });
        html += '    <input name="search_query" class="search_person" autocomplete="off" placeholder="Search our Database" type="text">' +
            '        <ul class="col-md-12 col-sm-12 col-xs-12 search_list"></ul>' +
            '    </div>' +
            '</div>';
        return html;
    }

    return '';
}

/**
 * modalID: the id of the modal wrapper div.
 * successFunction: a function to execute once the user clicks save and success is returned from
 * the server.
 * @param dataObject
 * @param modalID
 * @param successFunction
 */
function openBuyerSellerModal(dataObject = {}, modalID, successFunction)
{
    /**
     * Search object must of type object.
     * example: dataObject = {
     *              table: //table to search,
     *              model: //model attached to table,
     *              roleCode: //for example, ba stands for buyers agent, e for escrow, etc.
     *              token: csrf_token()
     *              searchRoute: //route to use to search, standard is the search person route.
     *              receiveDataRoute: //the route to fetch information based on record ID once selected.
     *              saveDataRoute: //where the data is sent to be saved
     *              transactionID: //transactionID
     *          }
     *
     * If searchRoute is a falsy statement, search will not be added.
     * If searchRoute is a route, and table and model are not defined, search will malfunction in unknown ways.
     *
     * After defining the HTML in the modal, you must call the function buildModal, which will actually
     * display the modal and take actions whether the user cancels,saves, or makes errors on the form.
     */


    let title = dataObject.roleCode === 'b' ? 'Buyer':'Seller';
    let html = '' +
        '<div id="'+modalID+'" class="row field-row" style="width: 100%">' +
        '    <input type="hidden" name="Transactions_ID" value="'+(dataObject.transactionID || '')+'">' +
        '    <input type="hidden" name="ID" value="">' +
        '    <input type="hidden" name="_roleCode" value="'+dataObject.roleCode+'">' +
        '    <input type="hidden" name="_token" value="'+dataObject.token+'">';
    if(dataObject.searchRoute) html += searchObjectHTML(dataObject.table,dataObject.model);
    html +=
        '    <div class="field-row agent-details overflow-hide col-lg-12">' +
        '        <div class="col-md-3 col-xs-12 form-group with-label">' +
        '            <label for="NameFirst" class="label-control">FIRST NAME <span class="important">*</span></label>\n' +
        '            <input name="NameFirst" class="padding-left-0 text-field" placeholder="Enter First Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="NameLast" class="label-control">LAST NAME <span class="important">*</span></label>\n' +
        '            <input name="NameLast" class="text-field padding-left-0" placeholder="Enter Last Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="" class="label-control">NAME ON DOCUMENTS <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="NameFull" class="text-field padding-left-0" placeholder="Enter Full Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '    </div>' +
        '    <div class="field-row agent-details overflow-hide col-lg-12">\n' +
        '        <div class=" col-md-4 form-group with-label ">\n' +
        '            <label for="PrimaryPhone" class="label-control">CELL # <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="PrimaryPhone" class="padding-left-0 text-field" placeholder="### ### ####">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-4 form-group with-label">\n' +
        '            <label for="SecondaryPhone" class="label-control">OFFICE # <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="SecondaryPhone" class="text-field padding-left-0" placeholder="### ### ####">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-4 form-group with-label">\n' +
        '            <label for="Email" class="label-control">EMAIL <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="Email" class="text-field padding-left-0" placeholder="example@mail.com">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-6 col-xs-12 form-group with-label ">\n' +
        '            <label for="Address" class="label-control">ADDRESS <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="Address" class="padding-left-0 text-field" placeholder="Enter Current Address"/>\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class="col-md-3 form-group with-label ">\n' +
        '            <label for="Unit" class="label-control" style="text-transform: uppercase;">UNIT <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="Unit" class="padding-left-0 text-field" placeholder="Enter Unit #">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        ' +
        '    </div>\n' +
        '    <div class="col-lg-12 row">' +
        '        <div class="col-lg-6" style="margin: auto;">' +
        '            <button id="cancelButton" class="btn btn-red">Cancel</button>' +
        '            <button id="saveButton" class="btn btn-teal">Save</button>' +
        '        </div>' +
        '    </div>' +
        '</div>';

    buildModal(html,title,modalID,dataObject,successFunction);
}

/**
 * modalID: the id of the modal wrapper div.
 * successFunction: a function to execute once the user clicks save and success is returned from
 * the server.
 * @param dataObject
 * @param modalID
 * @param successFunction
 */
function openAgentModal(dataObject = {}, modalID, successFunction)
{
    /**
     * Search object must of type object.
     * example: dataObject = {
     *              table: //table to search,
     *              model: //model attached to table,
     *              roleCode: //for example, ba stands for buyers agent, e for escrow, etc.
     *              token: csrf_token()
     *              searchRoute: //route to use to search, standard is the search person route.
     *              receiveDataRoute: //the route to fetch information based on record ID once selected.
     *              saveDataRoute: //where the data is sent to be saved
     *              transactionID: //transactionID
     *          }
     *
     * If searchRoute is a falsy statement, search will not be added.
     * If searchRoute is a route, and table and model are not defined, search will malfunction in unknown ways.
     *
     * After defining the HTML in the modal, you must call the function buildModal, which will actually
     * display the modal and take actions whether the user cancels,saves, or makes errors on the form.
     */

    let title = dataObject.roleCode === 'ba' ? 'Buyer\'s Agent':'Seller\'s Agent';
    let html = '' +
        '<div id="'+modalID+'" class="row field-row" style="width: 100%">' +
        '    <input type="hidden" name="Transactions_ID" value="'+(dataObject.transactionID || '')+'">' +
        '    <input type="hidden" name="ID" value="">' +
        '    <input type="hidden" name="_roleCode" value="'+dataObject.roleCode+'">' +
        '    <input type="hidden" name="_token" value="'+dataObject.token+'">';
    if(dataObject.searchRoute) html += searchObjectHTML(dataObject.table,dataObject.model);
    html +=
        '    <div class="field-row agent-details overflow-hide col-lg-12">' +
        '        <div class="col-md-3 col-xs-12 form-group with-label">' +
        '            <label for="NameFirst" class="label-control">FIRST NAME <span class="important">*</span></label>\n' +
        '            <input name="NameFirst" class="padding-left-0 text-field" placeholder="Enter First Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="NameLast" class="label-control">LAST NAME <span class="important">*</span></label>\n' +
        '            <input name="NameLast" class="text-field padding-left-0" placeholder="Enter Last Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="" class="label-control">NAME ON LICENSE<span class="important" style="display: none;"></span></label>\n' +
        '            <input name="NameFull" class="text-field padding-left-0" placeholder="Enter Full Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="NameFull" class="label-control">LICENSE #<span class="important" style="display: none;"></span></label>\n' +
        '            <input name="License" class="text-field padding-left-0" placeholder="Enter License">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '    </div>' +
        '    <div class="field-row agent-details overflow-hide col-lg-12">\n' +
        '        <div class=" col-md-4 form-group with-label ">\n' +
        '            <label for="PrimaryPhone" class="label-control">CELL # <span class="important">*</span></label>\n' +
        '            <input name="PrimaryPhone" class="padding-left-0 text-field" placeholder="### ### ####">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-4 form-group with-label">\n' +
        '            <label for="SecondaryPhone" class="label-control">OFFICE # <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="SecondaryPhone" class="text-field padding-left-0" placeholder="### ### ####">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-4 form-group with-label">\n' +
        '            <label for="Email" class="label-control">EMAIL <span class="important">*</span></label>\n' +
        '            <input name="Email" class="text-field padding-left-0" placeholder="example@mail.com">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-6 col-xs-12 form-group with-label ">\n' +
        '            <label for="Address" class="label-control">ADDRESS <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="Address" class="padding-left-0 text-field" placeholder="Enter Current Address"/>\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '    <div class="col-lg-12 row">' +
        '        <div class="col-lg-6" style="margin: auto;">' +
        '            <button id="cancelButton" class="btn btn-red">Cancel</button>' +
        '            <button id="saveButton" class="btn btn-teal">Save</button>' +
        '        </div>' +
        '    </div>' +
        '</div>';

    buildModal(html,title,modalID,dataObject,successFunction);
}

function openTCModal(dataObject = {}, modalID, successFunction)
{
    /**
     * Search object must of type object.
     * example: dataObject = {
     *              table: //table to search,
     *              model: //model attached to table,
     *              roleCode: //for example, ba stands for buyers agent, e for escrow, etc.
     *              token: csrf_token()
     *              searchRoute: //route to use to search, standard is the search person route.
     *              receiveDataRoute: //the route to fetch information based on record ID once selected.
     *              saveDataRoute: //where the data is sent to be saved
     *              transactionID: //transactionID
     *          }
     *
     * If searchRoute is a falsy statement, search will not be added.
     * If searchRoute is a route, and table and model are not defined, search will malfunction in unknown ways.
     *
     * After defining the HTML in the modal, you must call the function buildModal, which will actually
     * display the modal and take actions whether the user cancels,saves, or makes errors on the form.
     */

    let title = dataObject.roleCode === 'btc' ? 'Buyer\'s Transaction Coordinator':'Seller\'s Transaction Coordinator';
    let html = '' +
        '<div id="'+modalID+'" class="row field-row" style="width: 100%">' +
        '    <input type="hidden" name="Transactions_ID" value="'+(dataObject.transactionID || '')+'">' +
        '    <input type="hidden" name="ID" value="">' +
        '    <input type="hidden" name="_roleCode" value="'+dataObject.roleCode+'">' +
        '    <input type="hidden" name="_token" value="'+dataObject.token+'">';
    if(dataObject.searchRoute) html += searchObjectHTML(dataObject.table,dataObject.model);
    html +=
        '    <div class="field-row agent-details overflow-hide col-lg-12">' +
        '        <div class="col-md-3 col-xs-12 form-group with-label">' +
        '            <label for="NameFirst" class="label-control">FIRST NAME <span class="important">*</span></label>\n' +
        '            <input name="NameFirst" class="padding-left-0 text-field" placeholder="Enter First Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="NameLast" class="label-control">LAST NAME <span class="important">*</span></label>\n' +
        '            <input name="NameLast" class="text-field padding-left-0" placeholder="Enter Last Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="" class="label-control">COMPANY NAME <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="Company" class="text-field padding-left-0" placeholder="Enter Company Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="NameFull" class="label-control">LICENSE #<span class="important" style="display: none;"></span></label>\n' +
        '            <input name="License" class="text-field padding-left-0" placeholder="Enter License">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '    </div>' +
        '    <div class="field-row agent-details overflow-hide col-lg-12">\n' +
        '        <div class=" col-md-4 form-group with-label ">\n' +
        '            <label for="PrimaryPhone" class="label-control">CELL # <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="PrimaryPhone" class="padding-left-0 text-field" placeholder="### ### ####">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-4 form-group with-label">\n' +
        '            <label for="Email" class="label-control">EMAIL <span class="important">*</span></label>\n' +
        '            <input name="Email" class="text-field padding-left-0" placeholder="example@mail.com">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-12 col-xs-12 form-group with-label ">\n' +
        '            <label for="Address" class="label-control">ADDRESS <span class="important">*</span></label>\n' +
        '            <input name="Address" class="padding-left-0 text-field" placeholder="Enter Current Address"/>\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '    <div class="col-lg-12 row">' +
        '        <div class="col-lg-6" style="margin: auto;">' +
        '            <button id="cancelButton" class="btn btn-red">Cancel</button>' +
        '            <button id="saveButton" class="btn btn-teal">Save</button>' +
        '        </div>' +
        '    </div>' +
        '</div>';

    buildModal(html,title,modalID,dataObject,successFunction);
}

function openAuxModal(dataObject = {}, modalID, successFunction)
{
    /**
     * Search object must of type object.
     * example: dataObject = {
     *              table: //table to search,
     *              model: //model attached to table,
     *              roleCode: //for example, ba stands for buyers agent, e for escrow, etc.
     *              token: csrf_token()
     *              searchRoute: //route to use to search, standard is the search person route.
     *              receiveDataRoute: //the route to fetch information based on record ID once selected.
     *              saveDataRoute: //where the data is sent to be saved
     *              transactionID: //transactionID
     *          }
     *
     * If searchRoute is a falsy statement, search will not be added.
     * If searchRoute is a route, and table and model are not defined, search will malfunction in unknown ways.
     *
     * After defining the HTML in the modal, you must call the function buildModal, which will actually
     * display the modal and take actions whether the user cancels,saves, or makes errors on the form.
     */
    let title;
    switch (dataObject.roleCode)
    {
        case 'e':
            title = 'Escrow Agent';
            break;
        case 'l':
            title = 'Lender';
            break;
        case 't':
            title = 'Title Agent';
            break;
        default:
            genericError('Error code: aux-modal-001');
            break;

    }
    let html = '' +
        '<div id="'+modalID+'" class="row field-row" style="width: 100%">' +
        '    <input type="hidden" name="Transactions_ID" value="'+(dataObject.transactionID || '')+'">' +
        '    <input type="hidden" name="ID" value="">' +
        '    <input type="hidden" name="_roleCode" value="'+dataObject.roleCode+'">' +
        '    <input type="hidden" name="_token" value="'+dataObject.token+'">';
    if(dataObject.searchRoute) html += searchObjectHTML(dataObject.table,dataObject.model);
    html +=
        '    <div class="field-row agent-details overflow-hide col-lg-12">' +
        '        <div class="col-md-3 col-xs-12 form-group with-label">' +
        '            <label for="NameFirst" class="label-control">FIRST NAME <span class="important">*</span></label>\n' +
        '            <input name="NameFirst" class="padding-left-0 text-field" placeholder="Enter First Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="NameLast" class="label-control">LAST NAME <span class="important">*</span></label>\n' +
        '            <input name="NameLast" class="text-field padding-left-0" placeholder="Enter Last Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="" class="label-control">COMPANY NAME <span class="important" style="display: none;"></span></label>\n' +
        '            <input name="Company" class="text-field padding-left-0" placeholder="Enter Company Name">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-3 col-xs-12 form-group with-label">\n' +
        '            <label for="NameFull" class="label-control">LICENSE #<span class="important" style="display: none;"></span></label>\n' +
        '            <input name="License" class="text-field padding-left-0" placeholder="Enter License">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '    </div>' +
        '    <div class="field-row agent-details overflow-hide col-lg-12">\n' +
        '        <div class=" col-md-4 form-group with-label ">\n' +
        '            <label for="PrimaryPhone" class="label-control">CELL # <span class="important">*</span></label>\n' +
        '            <input name="PrimaryPhone" class="padding-left-0 text-field" placeholder="### ### ####">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-4 form-group with-label">\n' +
        '            <label for="Email" class="label-control">EMAIL <span class="important">*</span></label>\n' +
        '            <input name="Email" class="text-field padding-left-0" placeholder="example@mail.com">\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '        <div class=" col-md-12 col-xs-12 form-group with-label ">\n' +
        '            <label for="Address" class="label-control">ADDRESS <span class="important">*<span class="address-required"></span></span></label>\n' +
        '            <input name="Address" class="padding-left-0 text-field" placeholder="Enter Current Address"/>\n' +
        '            <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '    <div class="col-lg-12 row">' +
        '        <div class="col-lg-6" style="margin: auto;">' +
        '            <button id="cancelButton" class="btn btn-red">Cancel</button>' +
        '            <button id="saveButton" class="btn btn-teal">Save</button>' +
        '        </div>' +
        '    </div>' +
        '</div>';

    buildModal(html,title,modalID,dataObject,successFunction);
}

function buildModal(html, title, modalID, dataObject, successFunction = false,)
{
    Swal2({
        html: html,
        title: 'Add '+title,
        showConfirmButton: false,
        showCancelButton: false,
        showCloseButton:true,
        width: '80%',
        animation: false,
        customClass: 'animated fadeInDown faster',
        onBeforeOpen: () => {

            let inputs = [];
            $('#'+modalID+' input').each(function (i,el) {
                if(el.type !== 'hidden')
                {
                    inputs.push(el.name);
                }
                else if (el.type === 'hidden' && el.name === 'ID')
                {
                    inputs.push(el.name);
                }
            });

            assignDataToModal(dataObject, modalID, inputs);

            const content   = Swal2.getContent();
            const $s        = content.querySelector.bind(content);

            let save        = $s('#saveButton');
            let cancel      = $s('#cancelButton');

            save.addEventListener('click', function () {
                Swal2.showLoading();

                let formData = new FormData();
                formData.append('_token', dataObject.token);
                formData.append('Transactions_ID', dataObject.transactionID);
                formData.append('_roleCode',dataObject.roleCode);
                formData.append('ajax',true);
                $.each(inputs, (i,obj) => {
                    let input = $('#'+modalID+' input[name="'+obj+'"]');
                    if(obj === 'ID')
                    {
                        if(input.val())
                        {
                            formData.append(obj, input.val());
                        }
                    }
                    else
                    {
                        formData.append(obj, input.val());
                    }
                });

                $.ajax({
                    url: dataObject.saveDataRoute,
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (r) {
                        Swal2.hideLoading();
                        if(r.status === 'success')
                        {
                            Swal2.close();
                            if(successFunction)
                            {
                                successFunction();
                            }
                        }
                        else if (r.status === 'Failure')
                        {
                            genericError(r.message);
                        }
                    },
                    error: function (err) {
                        Swal2.hideLoading();
                        let errors;
                        if(err.responseJSON) errors = err.responseJSON.errors;
                        else
                        {
                            let responseText = err.responseText.split('.')[2];
                            errors = JSON.parse(responseText).errors;
                        }
                        if(Object.keys(errors).length > 0)
                        {
                            let identifier;
                            let sibling;
                            $.each(errors, function (i,error) {
                                identifier = '#'+modalID+' input[name="'+i+'"]';
                                sibling = $(identifier).siblings('.errors');
                                sibling.text(error[0]);
                                sibling.css('display','block');
                            });
                        }
                        else
                        {
                            genericError();
                        }
                    },
                });
            });

            cancel.addEventListener('click', function () {
                Swal2.close();
            });
        },

    });
}