<?php

namespace App\Exceptions;

use App\Library\Utilities\_LaravelTools;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\Debug\Exception\FlattenException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if($exception instanceof ReportException)
        {
            return $this->showReportErrorPage();
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $flattenedException = FlattenException::create($exception);
        $statusCode = $flattenedException->getStatusCode();
        if($statusCode == 500)
        {
            return response()->view(_LaravelTools::addVersionToViewName('errors.'.$statusCode),[
                'message' => $flattenedException->getMessage().' '.$flattenedException->getFile().' '.$flattenedException->getLine(),
            ],$statusCode);
        }
        return parent::render($request,$exception);
    }
    protected function showReportErrorPage()
    {
        return view()->make('2a.errors.Report');
    }
}
