<?php namespace App\Transformer;

use App\Library\Utilities\_Variables;
use League\Fractal\TransformerAbstract;

class AgentTransformer extends TransformerAbstract {

    public function transform($agent) {
//        ddd([$agent, __METHOD__=>__LINE__]);
        $rv = [];
        try
        {
            if (($count = $agent->count()) < 1) return ['id'=>null, 'agent'=>null, 'license'=>null];
            if (_Variables::getObjectName($agent) == 'Collection')
            {
                foreach ($agent as $rec)
                {
                    $rv[] = [
                        'id'      => $rec->ID,
                        'agent'   => $rec->NameFull,
                        'license' => $rec->License,
                    ];
                }
            }
            else $rv = [
                'id'      => $agent->ID,
                'agent'   => $agent->NameFull,
                'license' => $agent->License,
            ];
            return $rv;
        }
        catch (\Exception $e)
        {
            ddd(['EXCEPTION'=>$e, 'agent'=>$agent, __METHOD__=>__LINE__]);
        }
    }
}