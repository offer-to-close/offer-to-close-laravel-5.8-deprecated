<?php

namespace App;

use App\Models\Model_Parent;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Scopes\NoTestScope;
use App\Library\Utilities\_Convert;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'OtcUsers';
    protected $connection = 'mysql-admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'NameFirst','NameLast','name', 'email', 'password', 'ActivationCode', 'ActivationStatus',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected static function boot()
    {
        parent::boot();

        $fields = self::getTableFields();
        if (array_search('inTest', $fields) !== false)
        {
            Log::info(['isTest Column not found in table Users', $fields, __METHOD__=>__LINE__]);
            return;
        }
        static::addGlobalScope((new NoTestScope));
    }
    public static function getTableFields()
    {
        $tbl = 'OtcUsers';
        $schema = 'otcAdmin';
        $tableFields = [];
        $query = DB::table('information_schema.columns')
                   ->select('Table_Schema as Schema', 'Table_name as Table', 'column_name as Column')
                   ->where('table_schema', '=', $schema)
                   ->where('Table_Name', $tbl);

        $result = $query->get()->toArray();
        foreach ($result as $idx => $row)
        {
            $tableFields[] = $row->Column ?? $row;
        }
        return $tableFields;
    }
}
