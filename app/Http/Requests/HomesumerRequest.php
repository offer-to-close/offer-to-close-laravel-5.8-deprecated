<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomesumerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ID'              => 'integer',
            'Transaction_ID'  => 'integer|required',
            'NameFull' => 'alpha_dash|required',
            'NameFirst'       => 'string|required',
            'NameLast'        => 'string|required',
            'Street1'         => 'string|required',
            'City'            => 'alpha-dash|required',
            'State'           => 'alpha|required',
//            'Zip'             => [new US_Zip],
 //           'PrimaryPhone'    => [new US_Phone],
//            'ContactSMS'      => [new US_Phone],
        ];

    }
}
