<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewTransactionQuestionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'purchasePrice' => 'required',
            'propertyType'  => 'required',
            'yearBuilt'     => 'required',
            'loanType'      => 'required',
            'rentBack'      => 'required',
            'extra'         => '',
        ];
    }
}


