<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 3/1/2019
 * Time: 11:33 AM
 */

namespace App\Http\Requests;


class RequestHelper
{
    protected $host;
    protected $intended;
    protected $protocol;
    protected $url;

    public function __construct($request)
    {
        $this->host         = $request->server->get('HTTP_HOST');
        $this->intended     = $request->server->get('REQUEST_URI');
        $this->protocol     = env('APP_ENV') == 'live' ? 'https://' : 'http://';
        $this->url          = $this->protocol.$this->host.$this->intended;
    }

    public function getCompleteURL()
    {
        return $this->url;
    }

    public function getProtocol()
    {
        return $this->protocol;
    }

    public function getIntendedPath()
    {
        return $this->intended;
    }

    public function getHost()
    {
        return $this->host;
    }
}