<?php

namespace App\Http\Middleware;

use App\Http\Controllers\AccessController;
use Closure;
use Illuminate\Support\Facades\Log;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isServerLocal()) Log::info(__METHOD__);
//        Log::debug('is admin = ' . session('isAdmin'));
//        Log::debug('userType = ' . session('userType'));
        // ... If user has admin access or better, then continue, otherwise go elsewhere
        if (AccessController::hasAccess('a'))
        {
            if (session('isAdmin')) return $next($request);  //todo why does this not continue to destination?
            else return redirect(route('admin.login'));
        }
// todo: return and pop up error - ask bryan how
        return redirect(route(config('otc.DefaultRoute.dashboard')));
//        return redirect()->back();
    }
}
