<?php

namespace App\Http\Middleware;

use App\Http\Requests\RequestHelper;
use Closure;
use App\Http\Middleware\Authenticate_OTC;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AuthFilter
{
    /**
     * Handle an incoming request.
     * This is an after Middleware call as it will take the response first.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    protected $whitelistedRoutes;
    protected $whitelistedStrings;
    protected $forbiddenStrings;


    public function __construct()
    {
        /*
         * This is an array of routes that does not require the user to log in.
         */
        $this->whitelistedRoutes = [
            route('otc.login'),
            route('login.chooseRole'),
            route('login.recordRole'),
            route('preLogin.welcome'),
            route('prelogin.privacyPolicy'),
            route('prelogin.termsConditions'),
            route('preLogin.fsbo'),
            route('preLogin.fsbo.get-started'),
            route('preLogin.mls'),
            route('preLogin.mls.get-started'),
            route('preLogin.agents'),
            route('preLogin.agents.get-started'),
            route('preLogin.tc.get-started'),
            route('preLogin.about'),
            route('preLogin.offerAssistant'),
            route('preLogin.offers'),
            route('preLogin.brands'),
            route('prelogin.requestMembership'),
            route('requestMembership'),
            route('prelogin.membership'),
            route('prelogin.contactUs'),
            route('submitContactRequest'),
            route('home').'/',
            route('home'),
            route('prelogin'),
            route('firstTimeCoupon'),
            route('requestBetaEmail'),
            route('verify_reCaptcha'),
            route('password.request'),
        ];

        /*
         * This is a list of strings for routes that may contain extra parameters.
         * If a string contained in this array is found, that route is whitelisted.
         */
        $this->whitelistedStrings = [
            'transaction-timeline',
            'blog',
            'press-release',
            'register',
            'logs',
            '/api/otcAdmin',
            '/api/agent',
            'password/reset',
            '/api/otcAdmin',
        ];

        /*
         * List of URL requests not allowed to exist inside of intended URL.
         */
        $this->forbiddenStrings = [
            '/css/',
            '/js/',
            '/fonts/',
            '/password/email'
        ];
    }

    private function isWhiteListed($url)
    {
        $length                 = count($this->whitelistedStrings);
        $whiteListed            = FALSE;
        for ($i = 0; $i < $length; $i++) if(strpos($url, $this->whitelistedStrings[$i]) !== FALSE) $whiteListed = TRUE;
        if(in_array($url,$this->whitelistedRoutes)) $whiteListed = TRUE;

        return $whiteListed;
    }

    /*
     * This is used to check if the intended URL is erroneous, not intended to be used with request URIs
     * For example: if there is a timeout or delay in the server response of some files, authentication might think that
     * the intended URL is something like public/css/fonts/some_font.tff. Since the server was making a request for that file
     * we cannot forbid this forbid this request, but we can prevent the user from being redirected to public/css/fonts/some_font.tff
     * after logging in.
     *
     *
     */
    private function isForbidden($url)
    {
        $length    = count($this->forbiddenStrings);
        $forbidden = FALSE;
        for ($i = 0; $i < $length; $i++) if(strpos($url, $this->forbiddenStrings[$i]) !== FALSE) $forbidden = TRUE;
        return $forbidden;
    }

    public function handle($request, Closure $next)
    {
        $response               = $next($request);
        $requestHelper          = new RequestHelper($request);
        $url                    = $requestHelper->getCompleteURL();
        //Log::info(['URL' => $url, 'AuthCheck()'=> Auth::check(), 'WhiteListed?'=> $this->isWhiteListed($url), __METHOD__ => __LINE__]);
        if(!Auth::check() && !$this->isWhiteListed($url))
        {
            return app(Authenticate_OTC::class)->handle($request,$next);
        }
        //Log::info(['Intended From AuthFilter' => session('url.intended'), __METHOD__ => __LINE__]);
        if(!Auth::check() && $this->isForbidden(session('url.intended'))) session()->forget('url.intended');
        //Log::info(['Post Intended From AuthFilter' => session('url.intended'), __METHOD__ => __LINE__]);
        return $response;
    }
}
