<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class apiAdminAccess
{
    /**
     * Handle an incoming request from otcAdmin.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $id = $request->route('securityID');
        if ($id != config('security.secureAdminHandshake'))
        {
            Log::alert('Handshake offered = "' . $id . '"; Handshake expected = "' . config('security.secureAdminHandshake') . '"');
            Log::alert(['Handshake Error!', 'Request' => request()]);
            return new Response('Invalid Access Attempted - Handshake not accepted');
        }
        return $next($request);
    }
}