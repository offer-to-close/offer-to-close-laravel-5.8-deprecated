<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 2/26/2019
 * Time: 5:06 PM
 */

/**
 * This is a modified copy of Authenticate.php found inside of the vendor folder. It uses sessions to save
 * the intended url and also redirects the users to the login page if they are not logged in.
 */

namespace App\Http\Middleware;

use App\Http\Requests\RequestHelper;
use Closure;
use Illuminate\Support\Facades\Log;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate_OTC
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string[]  ...$guards
     * @return mixed
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards)
    {
        /*
         * Check to see if the url.intended is already set.
         * If not set and user is not logged in, set it and reroute to login form.
         */
        $intended       = session('url.intended') ?? FALSE;
        if(!$intended)
        {
            $requestHelper  = new RequestHelper($request);
            $url            = $requestHelper->getCompleteURL();
            session()->put('url.intended',$url);
        }
        //Log::info(['Intended URL' => session('url.intended'), __METHOD__ => __LINE__]);
        if(!\Illuminate\Support\Facades\Auth::check()) return redirect()->to('/login');
        $this->authenticate($guards);

        return $next($request);
    }

    /**
     * Determine if the user is logged in to any of the given guards.
     *
     * @param  array  $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate($request, array $guards)
    {
        if (empty($guards)) {
            return $this->auth->authenticate();
        }

        foreach ($guards as $guard) {
            if ($this->auth->guard($guard)->check()) {
                return $this->auth->shouldUse($guard);
            }
        }

        throw new AuthenticationException('Unauthenticated.', $guards);
    }
}
