<?php

namespace App\Http\Controllers;

use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Models\Agent;
use App\Models\Broker;
use App\Models\Brokerage;
use App\Models\BrokerageOffice;
use App\Models\Buyer;
use App\Models\Seller;
use App\Models\Escrow;
use App\Models\Loan;
use App\Models\Title;
use App\Models\Transaction;
use App\Models\TransactionCoordinator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use App\Library\otc\AddressVerification;
use App\Combine\TransactionCombine;
use App\Combine\AccountCombine;
use PhpParser\Node\Expr\AssignOp\Concat;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class RoleController extends Controller
{
    public $modelPath = 'App\\Models\\';

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View|mixed|string
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function view(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $inputMethod = ['a' => 'Agent',
            'e' => 'Escrow',
            'l' => 'Loan',
            't' => 'Title',
            'br' => 'Broker',
            'bg' => 'Brokerage',
            'bo' => 'BrokerageOffice',
            'tc' => 'TC',
            'p' => 'Property',
        ];

        if (isset($request->role)) {
            $role = $request->role;
        } elseif (isset($request->_roleCode)) {
            $role = $request->_roleCode;
        } else {
            $role = session('role');
        }

        if (!isset($inputMethod[$role])) {
            ddd([__Method__, 'role' => $role, 'inputMethod' => $inputMethod,
                '_Request' => $_REQUEST, 'request' => $request]);
        }

        $method = 'input' . $inputMethod[$role];
        session()->put('role', $role);
        if (isset($request->selection) && $request->selection) {
            return $this->{$method}($request->selection);
        } elseif (isset($request->_pickedID) && $request->_pickedID) {
            return $this->{$method}($request->_pickedID);
        } else {
            return $this->{$method}(session('selection'));
        }

        return __METHOD__ . ' ' . __LINE__ . ' == ' . var_export($request->attributes, true);
        //        return "<h1>Crash and burn</h1>";
        //        return redirect()->back() ;
    }

    /**
     * @param int $agentID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputAgent($ID = 0, $fromFind = false)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $transactionID = 0;
        $recEmpty = (new Agent())->getEmptyRecord();
        $view = 'user.inputAgent';
        if (!$ID || !is_numeric($ID)) {
            $agentData = session()->get('agentData');
            $agentID = $agentData['ID'];
        } else {
            $agentID = $ID;
        }

        session()->put('transaction_id', $transactionID);

        if ($agentID && is_numeric($agentID)) {
            $data = Agent::find($agentID);
            $screenMode = ($data->count() < 1) ? 'create' : 'edit';
        } else {
            //           ddd([__METHOD__, __LINE__, $ID]);
            $data = collect([]);
            $screenMode = 'create';
        }

        $data = $data->toArray();

        if (empty($data)) {
            $data = $recEmpty;
        }

        $data = array_merge($data, ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role' => ['code' => Config::get('constants.USER_ROLE.AGENT'),
                'display' => 'Agent'],
            '_screenMode' => $screenMode,
            'data' => $data,
            'sameSide' => true,
            'noAgent' => ($ID === 0),
            'fromFind' => $fromFind,
        ]);
    }

    /**
     * @param int $tcID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputTC($ID = 0, $fromFind = false)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $transactionID = 0;
        $recEmpty = (new TransactionCoordinator())->getEmptyRecord();
        $view = 'user.inputTC';
        $tcID = $ID;

        session()->put('transaction_id', $transactionID);

        if ($tcID) {
            $data = TransactionCoordinator::find($tcID);
            $screenMode = ($data->count() < 1) ? 'create' : 'edit';
        } else {
            $data = collect([]);
            $screenMode = 'create';
        }

        $data = $data->toArray();

        if (empty($data)) {
            $data = $recEmpty;
        }

        $data = array_merge($data, ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role' => ['code' => Config::get('constants.USER_ROLE.TRANSACTION_COORDINATOR'),
                'display' => 'TC'],
            '_screenMode' => $screenMode,
            'data' => $data,
            'sameSide' => true,
            'fromFind' => $fromFind,
        ]);
    }

    /**
     * @param int $roleID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputEscrow($ID = 0, $fromFind = false)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $transactionID = 0;
        $recEmpty = (new Escrow())->getEmptyRecord();
        $view = 'user.inputAuxillaryRole';
        $roleID = $ID;

        session()->put('transaction_id', $transactionID);

        if ($roleID) {
            $data = Escrow::find($roleID);
            $screenMode = ($data->count() < 1) ? 'create' : 'edit';
        } else {
            $data = collect([]);
            $screenMode = 'create';
        }

        $data = $data->toArray();

        if (empty($data)) {
            $data = $recEmpty;
        }

        $data = array_merge($data, ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role' => ['code' => Config::get('constants.USER_ROLE.ESCROW'),
                'display' => 'Escrow'],
            '_screenMode' => $screenMode,
            'data' => $data,
            'sameSide' => true,
            'fromFind' => $fromFind,
        ]);
    }

    public function getAgentData(Request $request)
    {
        $ID = $request->input('ID');
        $agent = Agent::where('Agents.ID', $ID)
            ->selectRaw(
                'ID,
                Users_ID,
                License,
                NameFull,
                NameFirst,
                NameLast,
                CONCAT(
                COALESCE(Street1,"")," ",
                COALESCE(Street2,"")," ",
                COALESCE(Unit,""),",",
                COALESCE(City,""),",",
                COALESCE(State,"")," ",
                COALESCE(Zip,"")) as Address,
                PrimaryPhone,
                SecondaryPhone,
                Email'
            )
            ->get()
            ->toArray();
        if(!empty($agent))
        {
            return response()->json([
                'status'    => 'success',
                'data'      => $agent,
            ]);
        }

        return response()->json([
            'status'    => 'success',
            'message'   => 'Agent not found.',
            'error_code'=> 'get-agent-001',
        ]);
    }

    public function getAuxData(Request $request)
    {
        $ID = $request->input('ID');
        $model = $request->input('_model');
        $modelData = $model::where('ID', $ID)
            ->selectRaw(
                'ID,
                Users_ID,
                License,
                NameFull,
                NameFirst,
                NameLast,
                CONCAT(
                COALESCE(Street1,"")," ",
                COALESCE(Street2,"")," ",
                COALESCE(Unit,""),",",
                COALESCE(City,""),",",
                COALESCE(State,"")," ",
                COALESCE(Zip,"")) as Address,
                PrimaryPhone,
                SecondaryPhone,
                Email,
                Company'
            )
            ->get()
            ->toArray();
        if(!empty($modelData))
        {
            return response()->json([
                'status'    => 'success',
                'data'      => $modelData,
            ]);
        }

        return response()->json([
            'status'    => 'success',
            'message'   => 'Model not found.',
            'error_code'=> 'get-aux-001',
        ]);
    }

    /**
     * @param int $roleID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputBroker($ID = 0, $fromFind = false)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $transactionID = 0;
        $recEmpty = (new Broker())->getEmptyRecord();
        $view = 'user.inputBroker';
        $roleID = $ID;

        session()->put('transaction_id', $transactionID);

        if ($roleID) {
            $data = Broker::find($roleID);
            $screenMode = ($data->count() < 1) ? 'create' : 'edit';
        } else {
            $data = collect([]);
            $screenMode = 'create';
        }

        $data = $data->toArray();

        if (empty($data)) {
            $data = $recEmpty;
        }

        $data = array_merge($data, ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role' => ['code' => Config::get('constants.USER_ROLE.BROKER'),
                'display' => 'Broker'],
            '_screenMode' => $screenMode,
            'data' => $data,
            'sameSide' => true,
            'fromFind' => $fromFind,
        ]);
    }

    /**
     * @param int $roleID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputBrokerage($ID = 0, $fromFind = false)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $transactionID = 0;
        $recEmpty = (new Brokerage())->getEmptyRecord();
        $view = 'user.inputBrokerage';
        $roleID = $ID;

        session()->put('transaction_id', $transactionID);

        if ($roleID) {
            $data = Brokerage::find($roleID);
            $screenMode = ($data->count() < 1) ? 'create' : 'edit';
        } else {
            $data = collect([]);
            $screenMode = 'create';
        }

        $data = $data->toArray();

        if (empty($data)) {
            $data = $recEmpty;
        }

        $data = array_merge($data, ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role' => ['code' => Config::get('constants.USER_ROLE.BROKERAGE'),
                'display' => 'Brokerage'],
            '_screenMode' => $screenMode,
            'data' => $data,
            'sameSide' => true,
            'fromFind' => $fromFind,
        ]);
    }

    /**
     * @param int $roleID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputBrokerageOffice($ID = 0, $fromFind = false)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $transactionID = 0;
        $recEmpty = (new BrokerageOffice())->getEmptyRecord();
        $view = 'user.inputBrokerageOffice';
        $roleID = $ID;

        session()->put('transaction_id', $transactionID);

        if ($roleID) {
            $data = BrokerageOffice::find($roleID);
            $screenMode = ($data->count() < 1) ? 'create' : 'edit';
        } else {
            $data = collect([]);
            $screenMode = 'create';
        }

        $data = $data->toArray();

        if (empty($data)) {
            $data = $recEmpty;
        }

        $data = array_merge($data, ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role' => ['code' => Config::get('constants.USER_ROLE.BROKERAGE_OFFICE'),
                'display' => 'Brokerage Office'],
            '_screenMode' => $screenMode,
            'data' => $data,
            'sameSide' => true,
            'fromFind' => $fromFind,
        ]);
    }

    /**
     * @param int $roleID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputTitle($ID = 0, $fromFind = false)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $transactionID = 0;
        $recEmpty = (new Title())->getEmptyRecord();
        $view = 'user.inputAuxillaryRole';
        $roleID = $ID;

        session()->put('transaction_id', $transactionID);

        if ($roleID) {
            $data = Title::find($roleID);
            $screenMode = ($data->count() < 1) ? 'create' : 'edit';
        } else {
            $data = collect([]);
            $screenMode = 'create';
        }

        $data = $data->toArray();

        if (empty($data)) {
            $data = $recEmpty;
        }

        $data = array_merge($data, ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role' => ['code' => Config::get('constants.USER_ROLE.TITLE'),
                'display' => 'Title'],
            '_screenMode' => $screenMode,
            'data' => $data,
            'sameSide' => true,
            'fromFind' => $fromFind,
        ]);
    }

    /**
     * @param int $roleID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function inputLoan($ID = 0, $fromFind = false)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $transactionID = 0;
        $recEmpty = (new Loan())->getEmptyRecord();
        $view = 'user.inputAuxillaryRole';
        $roleID = $ID;

        session()->put('transaction_id', $transactionID);

        if ($roleID) {
            $data = Loan::find($roleID);
            $screenMode = ($data->count() < 1) ? 'create' : 'edit';
        } else {
            $data = collect([]);
            $screenMode = 'create';
        }

        $data = $data->toArray();

        if (empty($data)) {
            $data = $recEmpty;
        }

        $data = array_merge($data, ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role' => ['code' => Config::get('constants.USER_ROLE.LOAN'),
                'display' => 'Loan'],
            '_screenMode' => $screenMode,
            'data' => $data,
            'sameSide' => true,
            'fromFind' => $fromFind,
        ]);
    }

    /**
     * @param int $ID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function viewSomething(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $info = ['a' => ['view' => 'user.inputAgent'],
            'e' => ['view' => 'user.inputAuxillaryRole'],
            'l' => ['view' => 'user.inputAuxillaryRole'],
            't' => ['view' => 'user.inputAuxillaryRole'],
            'br' => ['view' => 'user.inputAuxillaryRole'],
            'bg' => ['view' => 'user.inputAuxillaryRole'],
            'bo' => ['view' => 'user.inputAuxillaryRole'],
            'tc' => ['view' => 'user.inputTC'],
            'p' => ['view' => 'user.inputProperty'],
        ];

        $recEmpty = (new Loan())->getEmptyRecord();
        $view = $info[$request->role]['view'];
        $model = $this->modelPath . str_singular($request->luTable) ?? false;

        if ($request->selection && $model) {
            $data = ($model)::find($request->selection);
            $screenMode = 'view';
        } else {
            ddd([__METHOD__, __LINE__, 'Storm it!']);
            return redirect()->back();
        }

        $data = $data->toArray();

        //Log::debug([__METHOD__, __LINE__, 'view' => $view, 'model' => $model, 'data' => $data]);

        if (empty($data)) {
            $data = $recEmpty;
        }

        return view($view, [
            '_role' => ['code' => $request->role,
                'display' => str_singular(str_singular($request->luTable))],
            '_screenMode' => $screenMode,
            'data' => $data,
            'sameSide' => true,
        ]);
    }

    /**
     * @param int $ID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function findSomething($role)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        $info = ['a' => ['table' => 'Agents'],
            'e' => ['table' => 'Escrows'],
            'l' => ['table' => 'Loans'],
            't' => ['table' => 'Titles'],
            'br' => ['table' => 'Brokers'],
            'bg' => ['table' => 'Brokerages'],
            'bo' => ['table' => 'BrokerageOffices'],
            'tc' => ['table' => 'TransactionCoordinators'],
            'p' => ['table' => 'Properties'],
        ];

        $table = $info[$role]['table'];
        $model = $this->modelPath . str_singular($table) ?? false;

        session()->forget('transaction_id');

        $view = 'user.findPage';

        $data = ($model)::get();

        return view($view, [
            'role' => $role,
            'data' => $data,
            'table' => $table,
            'route' => 'viewRole.' . str_singular($table),
        ]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveAgent(Request $request)
    {
// ... Make sure the user is logged in
        if (!\Auth::check()) return redirect()->guest('/login');

        $data = [];
        $modalModel = $request->modalModel ?? false;

        if ($modalModel) {
            $add = '||' . $modalModel;
            // ... Save the modal form's data
            $validation = [
                'NameFirst' . $add => 'required|alpha_spaces|max:100',
                'NameLast' . $add => 'required|alpha_spaces|max:100',
                'City' . $add => 'required|string|max:100',
                'State' . $add => 'required|string|max:100',
                'Zip' . $add => 'required|string|max:10',
            ];

            $validator = Validator::make(Input::all(),$validation);
            if($validator->fails())
            {
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray()

                ], 400);
            }

            $dat = $request->all();
            foreach ($dat as $fld => $val) {
                $data[str_replace($add, null, $fld)] = $val;
            }
            $modalModel = $this->modelPath . $modalModel;
            $modal = new $modalModel();
            $data['NameFull'] = $data['NameFull'] ?? implode(' ', [$data['NameFirst'], $data['NameLast']]);
            $modal_inserted = $modal->upsert($data);

            $id = $modal_inserted->ID ?? $modal_inserted->id;
            $rv = Agent::where('ID', $data['Agents_ID'])->update(['Brokers_ID' => $id]);

            if ($modal_inserted) {
                return redirect()->back();
            }

        } else {
            // ... Save the main form's data

            $validation = [
                'NameFull' => 'required|alpha_spaces|max:100',
                'NameFirst' => 'required|alpha_spaces|max:100',
                'NameLast' => 'required|alpha_spaces|max:100',
                'Street1' => 'required|string|max:100',
                'City' => 'required|alpha_spaces|max:100',
                'State' => 'required|string|max:100',
                'Zip' => 'required|digits:5',
                'PrimaryPhone' => 'required',
                'Email' => 'required|email',
                'EmailFormat' => 'required',
            ];

            $validator = Validator::make(Input::all(),$validation);
            if($validator->fails())
            {
                return response()->json([
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray()

                ], 400);
            }

            $data = $request->all();

            $agent = new Agent();
            $agent = $agent->upsert($data);
            $data['ID'] = $data['ID'] ?? $agent->id;
            session()->put('role', $data['_roleCode'] ?? ($data['role'] ?? ''));
            session()->put('selection', $data['selection'] ?? ($data['_PickedID'] ?? '*'));
            if (session()->get('role') == '*') {
                ddd($request);
            }

            if ($agent) {
                return redirect()->back()->with('agentData', $data);
            }
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveTC(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }

        $validation = [
            'Company' => 'required|string|max:100',
            'NameFirst' => 'required|max:100',
            'NameLast' => 'required|max:100',
            //'Address' => 'required|string|max:100',
            'PrimaryPhone' => 'required',
            'Email' => 'required|email',
//            'EmailFormat' => 'required',
        ];

        $validator = Validator::make(Input::all(),$validation);
        if($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ], 400);
        }

        $data = $request->all();

        $address       = $request->input('Address');

        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList = $addressVerify->isValid($address);
            if ($addressList == false) {
                return response()->json([
                    'status' => 'AddressError',
                    'Address' => 'Invalid Address',
                ]);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City'] = $addressList[0]['City'];
            $data['State'] = $addressList[0]['State'];
            $data['Zip'] = $addressList[0]['Zip'];
        }
        if(is_null($address))
        {
            $data['Street1']    =
            $data['Street2']    =
            $data['City']       =
            $data['State']      =
            $data['Zip']        = NULL;
        }

        $tc = new TransactionCoordinator();
        $tc = $tc->upsert($data);
        $data['ID'] = $data['ID'] ?? $tc->ID ?? $tc->id;

        $ajax = $data['ajax'];
        if($ajax && $tc) return response()->json([
            'status' => 'success',
            'tc_ID'  => $data['ID'],
        ]);

        if ($tc) {
            return redirect()->back()->with('data_saved', 'Data Saved Successfully');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveEscrow(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        // ... Save the main form's data
        $validation = [
 //           'NameFull' => 'required|string|max:100',
            'NameFirst' => 'required|alpha_spaces|max:100',
            'NameLast' => 'required|alpha_spaces|max:100',
            'Street1' => 'required|string|max:100',
            'City' => 'required|string|max:100',
            'State' => 'required|string|max:100',
            'Zip' => 'required|string|max:10',
            'PrimaryPhone' => 'required',
            'Email' => 'required|email',
//            'EmailFormat' => 'required',
        ];

        $validator = Validator::make(Input::all(),$validation);
        if($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ], 400);
        }

        $data = $request->all();
        $tc = new Escrow();
        $tc = $tc->upsert($data);
        $data['ID'] = $data['ID'] ?? $tc->id;

        if ($tc) {
            return redirect()->back()->with('data_saved', 'Data Saved Successfully');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveBroker(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        // ... Save the main form's data
        $request->validate([
            'NameFull' => 'required|string|max:100',
            'NameFirst' => 'required|alpha_spaces|max:100',
            'NameLast' => 'required|alpha_spaces|max:100',
            'Street1' => 'required|string|max:100',
            'City' => 'required|string|max:100',
            'State' => 'required|string|max:100',
            'Zip' => 'required|string|max:10',
            'PrimaryPhone' => 'required',
            'Email' => 'required|email',
 //           'EmailFormat' => 'required',
        ]);

        $data = $request->all();
        $tc = new Broker();
        $tc = $tc->upsert($data);
        $data['ID'] = $data['ID'] ?? $tc->id;

        if ($tc) {
            return redirect()->back()->with('data_saved', 'Data Saved Successfully');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveBrokerage(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        // ... Save the main form's data
        $request->validate([
            'Name' => 'required|string|max:100',
        ]);

        $data = $request->all();
        $tc = new Brokerage();
        $tc = $tc->upsert($data);
        $data['ID'] = $data['ID'] ?? $tc->id;

        if ($tc) {
            return redirect()->back()->with('data_saved', 'Data Saved Successfully');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveBrokerageoffice(Request $request)
    {
        $validationList = [
            'Name'            => 'required',
            'Brokers_ID'      => 'required|integer',
            'Address'         => 'required',
        ];

        $validator = Validator::make(Input::all(), $validationList);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $data    = $request->all();
        $address = $request->input('Address');
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);
            if ($addressList == false)
            {
                return response()->json([
                    'status'  => 'AddressError',
                    'Address' => 'Invalid Address',
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }
        $brokerageOffice = new BrokerageOffice();
        $data_inserted = $brokerageOffice->upsert($data);
        if($data_inserted)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'fail',
                'message'=> 'Unable to save the data.',
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveTitle(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        // ... Save the main form's data
        $validation = [
  //          'NameFull' => 'required|string|max:100',
            'NameFirst' => 'required|alpha_spaces|max:100',
            'NameLast' => 'required|alpha_spaces|max:100',
            'Street1' => 'required|string|max:100',
            'City' => 'required|string|max:100',
            'State' => 'required|string|max:100',
            'Zip' => 'required|string|max:10',
            'PrimaryPhone' => 'required',
            'Email' => 'required|email',
 //           'EmailFormat' => 'required',
        ];

        $validator = Validator::make(Input::all(),$validation);
        if($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ], 400);
        }

        $data = $request->all();
        $tc = new Title();
        $tc = $tc->upsert($data);
        $data['ID'] = $data['ID'] ?? $tc->id;

        if ($tc) {
            return redirect()->back()->with('data_saved', 'Data Saved Successfully');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveLoan(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in

        // ... Save the main form's data
        $validation = [
 //           'NameFull' => 'required|string|max:100',
            'NameFirst' => 'required|alpha_spaces|max:100',
            'NameLast' => 'required|alpha_spaces|max:100',
            'Street1' => 'required|string|max:100',
            'City' => 'required|string|max:100',
            'State' => 'required|string|max:100',
            'Zip' => 'required|string|max:10',
            'PrimaryPhone' => 'required',
            'Email' => 'required|email',
 //           'EmailFormat' => 'required',
        ];

        $validator = Validator::make(Input::all(),$validation);
        if($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ], 400);
        }

        $data = $request->all();
        $tc = new Loan();
        $tc = $tc->upsert($data);
        $data['ID'] = $data['ID'] ?? $tc->id;

        if ($tc) {
            return redirect()->back()->with('data_saved', 'Data Saved Successfully');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function assignAuxillary(Request $request)
    {
        if (!\Auth::check()) {
            return redirect()->guest('/login');
        }
        // ... Make sure the user is logged in
/*
        $request->validate([
            '_pickedID' => 'integer',
        ]);
*/
        $pickedID = $request->_pickedID;

        $role = $request->_roleCode;

        $transactionID = session('transaction_id');
        //request None means no agent is being assigned.
        if($request->None == 'None') $pickedID = 0;
        /**
         * @param     $type
         * @param int $bsID
         * @param int $transactionID
         *
         * @return bool
         */
        function assignBuyerSellerFromExisting($type, $bsID = 0,$transactionID = 0)
        {
            if($bsID == 0 || $transactionID == 0) return false;
            if($type == 's'){$type = 'Sellers'; $model = new Seller();}
            elseif($type == 'b') {$type = 'Buyers'; $model = new Buyer();}
            else return false;
            $qry = DB::table($type)
                ->select('Users_ID','Transactions_ID','NameFull','NameLast','NameFirst','Street1','Street2','Unit',
                    'City','State','Zip','PrimaryPhone','SecondaryPhone','Email','EmailFormat','ContactSMS','isTest')
                ->where('ID',$bsID)
                ->where('Transactions_ID', $transactionID)
                ->limit(1);
                $qry = Model_Parent::scopeNoTest($qry);
                $bs = $qry->get();
            if(!empty($bs)){
                return false;
            }
            else
            {
                $qry = DB::table($type)
                    ->select('Users_ID','Transactions_ID','NameFull','NameLast','NameFirst','Street1','Street2','Unit',
                        'City','State','Zip','PrimaryPhone','SecondaryPhone','Email','EmailFormat','ContactSMS','isTest')
                    ->where('ID',$bsID)
                    ->limit(1);
                $qry = Model_Parent::scopeNoTest($qry);
                $bs = $qry->get();
            }
            $bs = $bs->first();
            if($bs->Transactions_ID == $transactionID) return false;
            else
            {
                $bs->Transactions_ID = $transactionID;
                $bs = _Convert::toArray($bs);
                $model->upsert($bs);
            }

        }

        if (empty($transactionID)) {
            return redirect()->back()->with('data', ['role' => $role]);
        }

        $transaction = Transaction::find($transactionID);
        if(!is_null($transaction)) $transaction = $transaction->toArray();
        else return redirect()->back()->with('failMessage', 'The role could not be saved because the transaction does not exist.');

        switch ($role) {
            case Config::get('constants.USER_ROLE.ESCROW'):
                $transaction['Escrows_ID'] = $pickedID;
                break;

            case Config::get('constants.USER_ROLE.LOAN'):
                $transaction['Loans_ID'] = $pickedID;
                break;

            case Config::get('constants.USER_ROLE.TITLE'):
                $transaction['Titles_ID'] = $pickedID;
                break;

            case Config::get('constants.USER_ROLE.BUYERS_AGENT'):
                $transaction['BuyersAgent_ID'] = $pickedID;
                break;

            case Config::get('constants.USER_ROLE.SELLERS_AGENT'):
                $transaction['SellersAgent_ID'] = $pickedID;
                break;

            case Config::get('constants.USER_ROLE.BUYERS_TRANSACTION_COORDINATOR'):
                $transaction['BuyersTransactionCoordinators_ID'] = $pickedID;
                break;

            case Config::get('constants.USER_ROLE.SELLERS_TRANSACTION_COORDINATOR'):
                $transaction['SellersTransactionCoordinators_ID'] = $pickedID;
                break;

            case Config::get('constants.USER_ROLE.BUYER'):
                assignBuyerSellerFromExisting(Config::get('constants.USER_ROLE.BUYER'),$pickedID,$transactionID);
                break;

            case Config::get('constants.USER_ROLE.SELLER'):
                assignBuyerSellerFromExisting(Config::get('constants.USER_ROLE.SELLER'),$pickedID,$transactionID);
                break;
        }

        $ta = new Transaction();
        $rv = $ta->upsert($transaction);
        if ($request->None == 'None' && $rv)
        {
            return response()->json([
                "success" => "yes",
            ]);
        }
        if ($rv) {
            return redirect()->back()->with('data_saved', 'Data Saved Successfully');
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function savePersonModal(Request $request)
    {
        if (!\Auth::check()) {
            return response()->json([
                'status' => 'error',
            ]);
        } // ... Make sure the user is logged in

        // ... Save the main form's data
        $request->validate([
            'modalModel' => 'required',
            'NameFirst' => 'required|alpha_spaces|max:100',
            'NameLast' => 'required|alpha_spaces|max:100',
            'City' => 'required|string|max:100',
            'State' => 'required|string|max:100',
            'Zip' => 'required|numeric|max:10',
        ]);
        $data = $request->all();
        $modalModel = $this->modelPath . $data['modalModel'];
        $modal = new $modalModel();
        $data['NameFull'] = $data['NameFull'] ?? implode(' ', [$data['NameFirst'], $data['NameLast']]);
        $modal_inserted = $modal->upsert($data);
        $mainTable = $this->modelPath . $data['mainTable'];
        $updateField = $data['updateField'];

        $id = $modal_inserted->ID ?? $modal_inserted->id;
        $rv = ($mainTable)::where('ID', $data['mainID'])->update([$updateField => $id]);

        if ($modal_inserted) {
            return response()->json([
                'status' => 'success',
                'ID' => $modal_inserted->id,
                'return' => $data['NameFull'],
            ]);
        } else {
            return response()->json([
                'status' => 'Failure',
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveOfficeModal(Request $request)
    {
        if (!\Auth::check()) {
            return response()->json([
                'status' => 'error',
            ]);
        } // ... Make sure the user is logged in
        if(env('UI_VERSION') == '2a')
        {
            $request->validate([
                'modalModel' => 'required',
                'Name'       => 'required|alpha_spaces|max:100',
                'Street1'    => 'required|string|max:100',
                'City'       => 'required|string|max:100',
                'State'      => 'required|string|max:100',
                'Zip'        => 'required|string|max:10',
            ]);
            $data = $request->all();
            $modalModel = $this->modelPath . $data['modalModel'];
            $modal = new $modalModel();
            $modal_inserted = $modal->upsert($data);
            $mainTable = $this->modelPath . $data['mainTable'];
            $updateField = $data['updateField'];

            $id = $modal_inserted->ID ?? $modal_inserted->id;
            $rv = ($mainTable)::where('ID', $data['mainID'])->update([$updateField => $id]);

            if ($modal_inserted) {
                return response()->json([
                    'status' => 'success',
                    'ID' => $modal_inserted->id,
                    'return' => $data['Name'],
                ]);
            } else {
                return response()->json([
                    'status' => 'Failure',
                ]);
            }
        }
        // ... Save the main form's data
        $request->validate([
            'modalModel' => 'required',
            'Name' => 'required|alpha_spaces|max:100',
            'Street1' => 'required|string|max:100',
            'City' => 'required|string|max:100',
            'State' => 'required|string|max:100',
            'Zip' => 'required|string|max:10',
        ]);
        $data = $request->all();
        $modalModel = $this->modelPath . $data['modalModel'];
        $modal = new $modalModel();
        $modal_inserted = $modal->upsert($data);
        $mainTable = $this->modelPath . $data['mainTable'];
        $updateField = $data['updateField'];

        $id = $modal_inserted->ID ?? $modal_inserted->id;
        $rv = ($mainTable)::where('ID', $data['mainID'])->update([$updateField => $id]);

        if ($modal_inserted) {
            return response()->json([
                'status' => 'success',
                'ID' => $modal_inserted->id,
                'return' => $data['Name'],
            ]);
        } else {
            return response()->json([
                'status' => 'Failure',
            ]);
        }
    }

    /**
     * @param \Illuminate\Http\AJAXRequest $request
     *
     * @return \Illuminate\Http\JSONResponse
     */
    public function searchPerson(Request $request)
    {
        $request->validate([
            '_table'          => 'required',
            '_model'          => 'required|max:100',
            '_roleCode'       => 'required|max:100',
            'search_query'    => 'required|max:100',
        ]);
        if( request('_roleCode') == 'e'  ||
            request('_roleCode') == 't'  ||
            request('_roleCode') == 'l'  ||
            request('_roleCode') == 'btc'||
            request('_roleCode') == 'stc') $company = true;
        else $company = false;
        //check if looking for buyers or sellers
        if(request('_roleCode') === 'b' || request('_roleCode') === 's') $bs = true;
        else $bs = false;
        //Fetch the data from table
        $model   = request('_model');
        $table   = request('_table');
        $query   = '%' . str_replace(' ', '%', request('search_query')) . '%';
        if (substr_count(request('search_query'), ' ') > 1)
        {
            $qParts = explode(' ', request('search_query'));
            $qFirst = array_shift($qParts);
            $qLast = array_pop($qParts);
            $countParts = count($qParts);
        }
        else $countParts = 0;

        $limit   = request('limit');
        $offset  = request('offset');

        $license = str_replace('%','',$query);
        if(is_numeric($license) && !$bs)
        {
            $license = (int)$license;
            if ($company)
            {
                $result = $model::select('NameFirst', 'NameLast', 'ID', 'License', 'Company')
                    ->where('License', $license)
                    ->skip($offset)
                    ->take($limit)
                    ->orderBy(DB::raw("CONCAT(NameFirst,' ',NameLast)"), 'ASC')
                    ->get();
            }
            else
            {
                $result = $model::select('NameFirst', 'NameLast', 'ID', 'License')
                    ->where('License', $license)
                    ->skip($offset)
                    ->take($limit)
                    ->orderBy(DB::raw("CONCAT(NameFirst,' ',NameLast)"), 'ASC')
                    ->get();
            }

            return response()->json($result);
        }

        if (preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $query))
        {

            if($bs)
            {
                $result = $model::select('NameFirst','NameLast', 'ID')
                    ->where(DB::raw("CONCAT(Street1,' ',City)"),'like',$query)
                    ->orderBy('NameFull', 'ASC')
                    ->get();
            }
            else
            {
                if ($company)
                {
                    $result = $model::select('NameFirst', 'NameLast', 'ID', 'License', 'Company')
                        ->where(DB::raw("CONCAT(Street1,' ',City)"), 'like', $query)
                        ->orderBy('NameFull', 'ASC')
                        ->get();
                }
                else
                {
                    $result = $model::select('NameFirst', 'NameLast', 'ID', 'License')
                        ->where(DB::raw("CONCAT(Street1,' ',City)"), 'like', $query)
                        ->orderBy('NameFull', 'ASC')
                        ->get();
                }
            }

            return response()->json($result);
        }

        if ($request->isMethod('get'))
        {
            if($company)$query = $model::select('NameFirst','NameLast','ID', 'Company')
                ->where(DB::raw("CONCAT(NameFirst,' ',NameLast)"), 'like', $query)
                ->orWhere('NameFull', 'like',$query)
                ->orWhere('Company','like',$query);
            else $query = $model::select('NameFirst','NameLast','ID', 'Company')
                ->where(DB::raw("CONCAT(NameFirst,' ',NameLast)"), 'like', $query)
                ->orWhere('NameFull', 'like',$query);

            if ($countParts) $query = $query->whereRaw('NameFirst like "'.$qFirst.'%" AND NameLast like "' . $qLast.'%"');
            $result = $query->distinct()->count();
            return response()->json($result);
        }


        if($bs) $query = $model::select('NameFirst','NameLast', 'ID')->where(DB::raw("CONCAT(NameFirst,' ',NameLast)"), 'like', $query);
        else
        {
            if($company)$query = $model::select('NameFirst','NameLast', 'ID', 'License', 'Company')
                ->where(DB::raw("CONCAT(NameFirst,' ',NameLast)"), 'like', $query)
                ->orWhere('NameFull', 'like',$query)
                ->orWhere('Company','like',$query);
            else $query = $model::select('NameFirst','NameLast', 'ID', 'License')
                ->where(DB::raw("CONCAT(NameFirst,' ',NameLast)"), 'like', $query)
                ->orWhere('NameFull', 'like',$query);
        }

        if ($countParts) $query = $query->whereRaw('NameFirst like "' . $qFirst . '%" AND NameLast like "' . $qLast . '%"');
        $query = $query->skip($offset)
            ->take($limit)
            ->orderBy(DB::raw("CONCAT(NameFirst,' ',NameLast)"), 'ASC');

        $result = $query->distinct()->get();

        return response()->json($result);
    }

    /**
     * @param \Illuminate\Http\AJAXRequest $request
     *
     * @return \Illuminate\Http\JSONResponse
     */
    public function searchOffice(Request $request)
    {
        //Validate the data
        $request->validate([
            '_model' => 'required|max:100',
            'search_query' => 'required|max:100',
        ]);
        //Fetch the data from table
        $model  = request('_model');
        $query  = request('search_query') . '%';
        $result = $model::select('Name', 'ID')
            ->where('Name', 'like', $query)
            ->orderBy('Name', 'ASC')
            ->get();
        return response()->json($result);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOfficeDetails(Request $request)
    {
        if (!\Auth::check()) {return response()->json(['status' => 'error']);}
        //Validate the data
        $request->validate([
            '_model' => 'required|max:100',
            'id'     => 'numeric|max:99999',
        ]);
        $model = request('_model');
        $id = request('id');
        $user = $model::select('Name', 'Street1', 'Street2', 'Unit', 'City', 'State', 'Zip', 'PrimaryPhone')
                ->where('id', $id)->get();
        return response()->json($user);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addNew(Request $request)
    {
        $view = 'user.add';
        return view(_LaravelTools::addVersionToViewName($view));
    }
    /**
     * @param Request $request
     *
     * This is an ajax function
     */
    public function switchUserRole(Request $request)
    {
        $newRole = $request->input('role');
        $request->session()->put('userRole', $newRole);
        if (session('userRole') == $newRole) {
            return response()->json([
                'status'        => 'success',
                'newRole'       => $newRole,
                'currentRole'   => session('userRole') ?? NULL,
            ]);
        }
        else
        {
            return response()->json([
                'status'        => 'fail',
                'newRole'       => $newRole,
                'currentRole'   => session('userRole') ?? NULL,
            ]);
        }
    }

    public function saveNewUserToRole($role = NULL)
    {
        if(is_null($role))      $role = session('userRole');

        $models = Config::get('constants.RoleModel');

        if(isset($models[$role]))
        {
            $model = 'App\\Models\\'.$models[$role];
        }
        else return FALSE;
        $user = Auth::user();
        $data = [
            'ID'            => NULL,
            'Users_ID'      => $user->id,
            'NameFull'      => $user->name,
            'NameFirst'     => $user->NameFirst ?? NULL,
            'NameLast'      => $user->NameFirst ?? NULL,
            'Email'         => $user->email ?? NULL,
            'DateCreated'   => date('Y-m-d'),
        ];
        $result = $model::create($data);
        return $result;
    }

}
