<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine;
use App\Combine\RoleCombine;
use App\Http\Controllers\Auth\LoginController;
use App\Library\Utilities\_LaravelTools;
use App\Model\Activity;
use App\Models\ContactRequest;
use App\User;
use App\Model\Agent;
use App\Model\Listing;
use App\Model\Customer;
use App\Mail\RequestCall;
use App\Mail\RequestContact;
use Illuminate\Http\Request;
use App\Mail\CustomerSignup;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function userLogin(Request $request, $inviteCode=null)
    {
        if (!empty($request->all()))
        {
            $loginCtlr = new LoginController();
            try
            {
                $loginCtlr->login($request);
            }
            catch (\Exception $e)
            {
                dd(['&& Error',
                    'msg'        => $e->getMessage(),
                    'request'    => $request,
                    'file'       => $e->getFile(),
                    'line'       => $e->getLine(),
                    'trace'      => $e->getTraceAsString(),
                    'Auth check' => Auth::check(),
                    'auth user'  => Auth::user(),
                    'auth'       => auth(),
                    __METHOD__   => __LINE__]);
            }
        }
//        dump(['request'=>$request, 'invite code'=>$inviteCode, __METHOD__=>__LINE__]);
//        dump(['Auth check'=>Auth::check(), 'auth user'=>Auth::user(), __METHOD__=>__LINE__]);

        session(['isAdmin'=>false]);
        if (!empty(session('ic')))
        {
            $invCtrl = new InvitationController();
            $invite  = $invCtrl->verifyInviteCode(session('ic'));
            $userID  = RoleCombine::getUserID($invite['inviteeRole'], $invite['inviteeRoleID']);
            if (!$userID) ddd(['invite' => $invite, __METHOD__ => __LINE__]);
            $request->session()->forget('ic');
        }
        else $invite = [];
        if (Auth::check() || !is_null(Auth::user()))
        {
            $userID = CredentialController::current()->ID();
            $arguments = ['userID'=>$userID];

            $roles = AccountCombine::getRoles($userID);
            session(['userType' => AccountCombine::getType($userID)]);

            if ($roles->count() == 1)
            {
                $roles['role'] = $roles->first()->Role;

                session(['userRole'=> $roles['role']]);
                return redirect(route(config('otc.DefaultRoute.dashboard'), $arguments));
            }
            else return redirect(route('login.chooseRole'));
//        else return view('auth.chooseRole', ['roles'=>$roles, 'userID'=>$userID]);
        }
            return view('auth.login', ['invite'=>$invite]);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function recordRole(Request $request, $inviteCode=null)
    {
        $role = $userID = null;

        if (isset($request->role))
        {
            session(['userRole'=>$request->role]);
            $role = $request->role;
        }

        if (isset($request->userID))
        {
            session(['userID'=>$request->userID]);
            $userID = $request->userID;
        }

        if (!empty($userID))
        {
            $typeMax = AccountCombine::getTypeMax($userID);
            session(['userType'=> $typeMax]);
            session(['userType' => AccountCombine::getType($userID)]);
        }

        return redirect()->intended(route(config('otc.DefaultRoute.dashboard')));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null                     $roles
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function chooseRole(Request $request)
    {
        if (Auth::check() || !is_null(Auth::user()))
        {
            $userID = Auth::user()->id;
            $arguments = ['userID'=>$userID];
            $roles = AccountCombine::getRoles($userID);
            session(['userType' => AccountCombine::getType($userID)]);

            if ($roles->count() == 1)
            {
                $roles['role'] = $roles->first()->Role;
                session(['userRole'=> $roles['role']]);
                return redirect(route(config('otc.DefaultRoute.dashboard')) , $arguments);
            }
            else
            {
                return view('auth.chooseRole', ['roles'=>$roles, 'userID'=>$userID]);
            }
        }
        else
        {
            return view('auth.login');
        }
    }

    public function adminLogin()
    {
        Log::info(__METHOD__);
//        Log::debug('isAdmin = ' . session('isAdmin'));

        if (AccessController::hasAccess('a')) return view('auth.admin.login');
        return view('otc.login');

    }
    public function adminAuthorize()
    {
        Log::info(__METHOD__);
        if (Auth::check() || !is_null(Auth::user()))
        {
            if (AccessController::hasAccess('a'))
            {
                $isAdmin = true;
            }
            else $isAdmin = false;
            session(['isAdmin' => $isAdmin]);
            return redirect(route('dashboard'));

        }
    }
    public function home()  // todo: delete
    {

        $title       = 'Find Top Real Estate Agents Near Me at Home By Home';
        $description = 'Home By Home helps home buyers and home sellers find the right real estate agent at the right price.';

        return view('index', compact('title', 'description'));
    }

    public function about()
    {
        return view('about');
    }

    public function terms()
    {
        return view('terms');
    }

    public function privacy()
    {
        return view('privacy');
    }

    public function resources()
    {
        return view('resources');
    }

    public function contact()
    {
        return view('contact');
    }

    public function getForms(Request $request)
    {
        if ($request->type == 'seller')
        {
            return view('partials.form-seller');
        }
        else
        {
            return view('partials.form-buyer');
        }
    }

    public function validateForm(Request $request)
    {
        $user     = new User;
        $customer = new Customer;
        $list     = new Listing;

        // validating user
        if (array_key_exists('first_name', $request->all()) || $request->has('first_name') || array_key_exists('last_name', $request->all()) || $request->has('last_name') || array_key_exists('email', $request->all()) || $request->has('email') || array_key_exists('password', $request->all()) || $request->has('password'))
        {
            $user->fill($request->all());
            $user->phone_num = preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone_num);
            $user->password  = $request->password ? bcrypt($request->password) : null;
            $user->isValid();
        }

        // validating list
        $list->fill($request->all());
        $list->closed_deal = 0;

        $reqArray    = [];
        $validateArr = [];

        if (array_key_exists('zip_postal', $request->all()) || $request->has('zip_postal'))
        {
            $reqArray['zip_postal']               = $request->zip_postal;
            $errorMessages['zip_postal.required'] = 'Location is required.';
            $validateArr['zip_postal']            = 'required|numeric|min:5';
        }

        if (array_key_exists('budget', $request->all()) || $request->has('budget'))
        {
            $reqArray['budget']         = preg_replace(array('/\,/', '/\$/'), array(''), $request->budget);
            $validateArr['budget']      = 'required'; // |numeric
            $reqArray['text_budget']    = $request->budget; //preg_replace(array('/\,/', '/\$/'), array(''), )
            $validateArr['text_budget'] = 'required'; // |numeric
        }

        if (array_key_exists('until', $request->all()) || $request->has('until'))
        {
            $reqArray['text_until']    = $request->until;
            $validateArr['text_until'] = 'required'; //|date
        }

        $validateList = Validator::make($reqArray, $validateArr, $errorMessages);
        $validateList->fails();

        $errors = $user->getErrors()->merge($customer->getErrors());
        if ($validateList->errors()->count() > 0)
        {
            $errors = $validateList->errors()->merge($errors);
        }

        if (count($errors) > 0)
        {
            return response($errors);
        }

        return response('validated');
    }


    public function success(User $user = null)
    {
        if ($user)
        {
            Auth::login($user, true);

            if ($user->isAgent())
            {
                return view('register-success');
            }
            else
            {
                return view('register-user-success');
            }
        }

        return view('register-success');
    }

    public function fbRegister(User $user)
    {
        $customer                   = new Customer;
        $customer->user_id          = $user->id;
        $customer->status_to_lender = 'not-yet';

        if ($customer->save())
        {
            Auth::login($user, true);

            return redirect()->route('customer.account');
        }
    }

    // email
    public function requestCall(Request $request)
    {
        $errorMessages = [];

        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'required|numeric',
        ], $errorMessages);

        if (count($validate->errors()) == 0)
        {
            Mail::to([config('homebyhome.contact_email'), config('homebyhome.contact_email_2')])->cc(config('app.contact_sytian'))->send(new RequestCall($request->name, $request->email, $request->phone));
            return redirect()->back()
                             ->withSuccess('Your request has been received.');
        }
        else
        {
            $errors = $validate->errors();
            return redirect()->back()->withErrors($errors);
        }
    }

    public function contactUs_old(Request $request)
    {
        $errorMessages = [];

        $request->phone = (int) preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone);

        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'required',
        ], $errorMessages);

        if (count($validate->errors()) == 0)
        {
            Mail::to(config('app.contact_email'))->send(new RequestContact($request->first_name, $request->last_name, $request->email, $request->company, $request->phone, $request->question_comment));

            return redirect()->back()->withSuccess('Your request has been sent.');
        }
        else
        {
            $errors = $validate->errors();

            return redirect()->back()->withErrors($errors);
        }
    }
    public function submitContactUs(Request $request)
    {
        $view = 'prelogin.welcome';
        $data = $request->all();

        $contactRequest = new ContactRequest();

        $info = [
            'State'     => $data['State'],
            'NameFirst' => $data['NameFirst'],
            'NameLast'  => $data['NameLast'],
            'Company'   => $data['Company'] ?? '',
            'Email'     => $data['Email'],
            'Phone'     => $data['Phone'] ?? '',
            'Comments'  => $data['Comments'],
            'Role'      => $data['Role'],
        ];

        $conReq = $contactRequest->upsert($info);

        $mailCtrl = new MailController();

        $rv = $mailCtrl->contactUs($conReq);

        return view (_LaravelTools::addVersionToViewName($view), ['msgType'=>'confirmation', 'msg'=>'Thanks for your comments', 'autoOff'=>5]);
    }
}
