<?php

namespace App\Http\Controllers;

use App\Models\MemberRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OtcAdminController extends Controller
{

    public function ping()
    {
        Log::alert(['API Ping'=>__CLASS__, 'Request'=>request(), __METHOD__=>__LINE__]);
        return 'Hello World - ' . date('H:i:s Y-m-d');
    }
        public function approveMemberRequest($accessCode = '*', $memberRequestID = 0, $deciderID=0)
    {

        Log::debug(['memberReqID'=>$memberRequestID ,__METHOD__=>__LINE__]);

//        foreach(config('database.connections') as $key=>$config)
//        {
//            Log::info(__METHOD__);
//            Log::info([$key=>DB::connection($key)->getConfig(), 'config'=>$config, __METHOD__=>__LINE__]);
//        }


        $apiData = ['memReqID'=>$memberRequestID, 'DeciderUsersID'=>$deciderID];

        $invite = new InvitationController();
        $rv = $invite->approveRequest(request(), $memberRequestID, $apiData);

        Log::debug(['rv'=>$rv, 'memberReqID'=>$memberRequestID ,__METHOD__=>__LINE__]);

        return $rv;
    }

    public function rejectMemberRequest($accessCode = '*', $memberRequestID = 0, $decisionReason='?', $deciderID=0)
    {
        Log::debug([ 'memberReqID'=>$memberRequestID ,__METHOD__=>__LINE__]);

        $apiData = ['memberRequestID'=>$memberRequestID, 'DecisionReason'=>$decisionReason, 'DeciderUsersID'=>$deciderID];

        $invite = new InvitationController();
        $rv = $invite->rejectRequest(request(), $apiData);
        Log::debug(['rv'=>$rv, 'memberReqID'=>$memberRequestID ,__METHOD__=>__LINE__]);
        return $rv;
    }

}
