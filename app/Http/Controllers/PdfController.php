<?php

namespace App\Http\Controllers;

use App\Library\otc\Pdf_ConvertApi;
use App\Models\bag_TransactionDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PdfController extends Controller
{
    public function splitUI(Request $request)
    {
        $data = $request->all();
        $documentBagID = $data['documentBagID'] ?? false;

        $document = bag_TransactionDocument::find($documentBagID);

        if (count($document) == 0)
        {
            //            todo
        }
        try
        {
//  TODO:  This placeholder code... the $pages need to be displayed on screen so the user can organize them into documents and be saved

            $pdf  = new Pdf_ConvertApi($document->DoumentPath);
            $pages = $pdf->getAllPages();
            return $pages;
        }
        catch (\Exception $e)
        {
            ddd(['Exception'=>$e->getMessage(), $e->getFile()=>$e->getLine(),
                        'file'=>$document->DocumentPath,
                        __METHOD__=>__LINE__], '**');
        }
    }
}
