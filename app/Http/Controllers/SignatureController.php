<?php
/**
 * Created by PhpStorm.
 * User: Bryan
 * Date: 2/6/2019
 * Time: 10:10 AM
 */

namespace App\Http\Controllers;
use App\Library\Utilities\_Convert;
use Illuminate\Support\Facades\Session;
use App\Combine\DocumentCombine;
use App\Combine\SignatureCombine;
use App\Combine\TransactionCombine;
use App\Library\Utilities\_Crypt;
use App\Models\bag_TransactionDocument;
use App\Models\log_Signature;
use function GuzzleHttp\Promise\queue;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use App\Library\Utilities\_LaravelTools;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class SignatureController extends Controller
{
    /**
     * The purpose of this function is to call this function to initiate the signature integration from various locations.
     * For example calling it from disclosures is different than calling it from inside of document bag.
     *
     * This is a static function inside of controller in the case that you need to use ajax to access it from
     * @param $transactionID
     * A string of bagIDs from bag_TransactionDocuments concatenated with (.)
     * @param $bagIDString
     * @param string $signatureAPI
     * @return \Illuminate\Http\RedirectResponse
     */
    public static function signatureGateway($transactionID,$bagIDString, $signatureAPI = 'DocuSign')
    {
        $bagIDArray = array_unique(explode('.',$bagIDString));
        $documents = DB::table('bag_TransactionDocuments')
            ->leftJoin('lk_Transactions-Documents', 'lk_Transactions-Documents.ID','=','bag_TransactionDocuments.lk_Transactions-Documents_ID')
            ->leftJoin('Documents', 'Documents.Code', '=', 'lk_Transactions-Documents.Documents_Code')
            ->select([
                'bag_TransactionDocuments.ID',
                'DocumentPath',
                'OriginalDocumentName',
                'ShortName','SignedBy',
                'Documents_Code',
                'lk_Transactions-Documents.ID as lk_Trans_Doc_ID',
                'useTemplate'
            ])
            ->whereIn('bag_TransactionDocuments.ID', $bagIDArray)
            ->get()->toArray();

        session()->put('transactionID', $transactionID);
        if(empty($documents))
        {
            Session::flash('error', 'No documents found.');
            Session::push('flash.old','error');
            return \redirect()
                ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
        }

        $documentCode = NULL;
        if(!empty($documents)) $documentCode = $documents[0]->Documents_Code;
        $usingTemplateColumns       = array_column($documents, 'useTemplate');
        $usingTemplateColumnsSize   = count($usingTemplateColumns);
        $useTemplate                = FALSE;
        if($usingTemplateColumnsSize)
            $useTemplate = array_sum($usingTemplateColumns)/$usingTemplateColumnsSize == 1 ? TRUE:FALSE;

        $initialData = [
            'files'         => $documents,
            'originalFiles' => $documents,
            'documentCode'  => $documentCode,
            'transactionID' => $transactionID,
            'useTemplate'   => $useTemplate,
        ];

        switch ($signatureAPI)
        {
            case 'DocuSign':
                $initialData['useTemplate'] = FALSE; //using templates with DocuSign not supported until testing is done.
                SignatureCombine::setSignatureData($initialData);
                break;
            case 'EverSign':
                SignatureCombine::setSignatureData($initialData);
                break;
            default:
                SignatureCombine::setSignatureData($initialData);
        }
    }

    public function getDocuSignAuth()
    {
        $signatureData = \session('signatureData') ?? NULL;
        if(is_null($signatureData)) SignatureCombine::setSignatureData();
        $code = Input::get('code') ?? $signatureData['authCode'] ?? NULL;
        $signatureData['authCode'] = $code;
        \session()->put('signatureData', $signatureData);

        /**
         * Once this has the auth code from Docu sign it redirects.
         * The auth code only lasts 2 minutes before it expires.
         */

        if(is_null($code))
        {
            Session::flash('error', 'Authentication code has expired.');
            Session::push('flash.old','error');
            return \redirect()
                ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
        }
        return \redirect()->route('signature.docuSign');
    }

    public function docuSign()
    {
        $authData = SignatureCombine::getDocuSignAuthenticationData();
        $userData = SignatureCombine::getDocuSignUserInfo();
        $signatureData = \session()->get('signatureData') ?? FALSE;
        $transactionID = $signatureData['transactionID'] ?? FALSE;
        if(!$signatureData || !$transactionID)
        {
            Session::flash('error', 'Signature data has not been set, please try again.');
            Session::push('flash.old','error');
            return \redirect()
                ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
        }
        /*
         * Documents info should be set in here, they should be an array regardless of amount of documents in the envelope.
         */
        $documents = [];
        foreach ($signatureData['files'] as $key => $doc)
        {
            if (!is_array($signatureData['files'][$key])) //if it is an array it means that documents have already been set.
            {
                $fileExtension                  = explode('.',$doc->OriginalDocumentName)[1];
                $document['documentId']         = $doc->ID;
                $document['fileExtension']      = $fileExtension;
                $document['name']               = $doc->OriginalDocumentName;
                $document['documentBase64']     = chunk_split(base64_encode(file_get_contents($doc->DocumentPath)));
                $documents[]                    = $document;
            }
            else
            {
                $documents[] = $signatureData['files'][$key];
            }
        }

        if(empty($documents))
        {
            Session::flash('error', 'No documents were transmitted');
            Session::push('flash.old','error');
            return \redirect()
                ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
        }
        /*
         * At the moment our system does not support multiple
         * recipients so we will leave this code like this for now.
         * If the recipients are system users, then we will have to search for them and apply their user IDs here.
         */
        $recipientId = 1; //this should simply be the recipient number. 1,2,3, etc.
        $clientUserId = base64_encode(CredentialController::current()->ID().'-'.$transactionID); //encoded userID and transactionID

        $recipientArray = [];
        //for multiple signers you need to define and array for each as below
        $signer = [
            'email' => $signatureData['userData']['email'],
            'name' => $signatureData['userData']['name'],
            'recipientId' => $recipientId,
            'clientUserId' => $clientUserId,
        ];

        $recipientArray['signers'][] = $signer;
        //not all recipients are signers, specification is necessary.

        $signatureData['recipients'] = $recipientArray; //we will loop through this array later when logging.
        $signatureData['files']  = $documents;
        session()->put('signatureData', $signatureData);
        SignatureCombine::logDocuSignSignature();
        SignatureCombine::createDocuSignEnvelope();
        $signatureData = session('signatureData');

        //update the logs with the appropriate envelope ID.
        $updateLogs = DB::table('log_Signatures')
            ->whereIn('ID', $signatureData['log_ID_Array'])
            ->update(['APIIdentifier' => $signatureData['envelopeID']]);
        if($updateLogs)
        {
            $url = SignatureCombine::generateDocuSignRecipientURLs();
            if($url !== FALSE) return \redirect()->to($url);
            Session::flash('error', 'No Signing URL provided.');
            Session::push('flash.old','error');
            return \redirect()
                ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
        }
    }

    public function postDocuSign($logID)
    {
        $signatureData = session('signatureData');
        $decodedIDString = _Crypt::base64url_decode($logID);
        if(strpos($decodedIDString,'.') !== FALSE) $log_SignaturesID = explode('.',$decodedIDString);
        else $log_SignaturesID[] = $decodedIDString;


        $event = Input::get('event');
        $log = [];
        foreach ($log_SignaturesID as $id)
        {
            $id = (int)$id;
            $data = [
                'ID'    => $id,
                'Event' => $event,
            ];

            $log_SignaturesModel = new log_Signature();
            $log[] = $log_SignaturesModel->upsert($data);
        }
        switch ($event)
        {
            case 'cancel':
                Session::flash('info', 'You have cancelled the signature.');
                Session::push('flash.old','info');
                return \redirect()
                    ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
                break;
            case 'decline':
                Session::flash('info', 'You have declined to sign.');
                Session::push('flash.old','info');
                return \redirect()
                        ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
                break;
            case 'exception':
                Session::flash('error', 'An exception has occurred while signing, please try again.');
                Session::push('flash.old','error');
                return \redirect()
                    ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
                break;
            case 'fax_pending':
                if(!empty($log))
                {
                    return \redirect()->route('openDocumentBag', [
                        'transactionID' => \session('transactionID'),
                        $log[0]['Documents_Code'],
                    ]);
                }
                return redirect()->route(config('otc.DefaultRoute.dashboard'));
                break;
            case 'id_check_failed':
                Session::flash('error', 'ID check failed.');
                Session::push('flash.old','error');
                return \redirect()
                    ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
                break;
            case 'session_timeout':
                Session::flash('warning', 'Your session has timed out, please try again.');
                Session::push('flash.old','warning');
                return \redirect()
                    ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
                break;
            case 'signing_complete':
                //get the document(s)
                foreach ($log as $record)
                {
                    $document = SignatureCombine::getDocuSignDocumentsCombined();
                    $signedByArray = [];
                    foreach ($signatureData['originalFiles'] as $file)
                    {
                        if ($file->SignedBy != '' || !is_null($file->SignedBy))
                        {
                            if (strpos($file->SignedBy, '_')) $signedBy = explode('_', $file->SignedBy);
                            else $signedBy = [$file->SignedBy];

                            $signedByArray = array_merge($signedByArray,$signedBy);
                        }
                    }
                    $signedByArray = array_unique($signedByArray);
                    $signedByArray = array_values($signedByArray);
                    $newSignedBy = implode('_',$signedByArray);

                    $documentName = $signatureData['documentName'];
                    SignatureCombine::saveNewDocumentPostSignature($document,$documentName, $record['Documents_Code'], $newSignedBy);
                }
                if(!empty($log))
                {
                    return \redirect()->route('openDocumentBag', [
                        'transactionID' => \session('transactionID'),
                        $log[0]['Documents_Code'],
                    ]);
                }
                return redirect()->route(config('otc.DefaultRoute.dashboard'));
                break;
            case 'ttl_expired':
                Session::flash('error', 'Token expired or already accessed.');
                Session::push('flash.old','error');
                return \redirect()
                    ->route('transactionSummary.documents', ['transactionID' => session('transactionID')]);
                break;
            case 'viewing_complete':
                return \redirect()->route(config('otc.DefaultRoute.dashboard'));
                break;
            default:
                if(!empty($log))
                {
                    return \redirect()->route('openDocumentBag', [
                        'transactionID' => \session('transactionID'),
                        $log[0]['Documents_Code'],
                    ]);
                }
                return redirect()->route(config('otc.DefaultRoute.dashboard'));
                break;
        }
    }

    public function everSign()
    {
        /*
         * Leaving createEverSignDocument as test for now.
         */
        $document = SignatureCombine::createEverSignDocument(1);
        $signatureData = session('signatureData');
        if(isset($document->error) || $document === FALSE)
        {
            return response()->json([
                'status' => 'fail',
                'message' => 'An error occurred when creating the document. Error code: es-001',
            ]);
        }
        if($document !== FALSE && $signatureData['embeddedSigningURL'])
        {
            return response()->json([
                'status'        => 'success',
                'signing_url'   => $signatureData['embeddedSigningURL'],
                'document'      => $document,
                'sessionData'   => $signatureData
            ]);
        }
        else
        {
            return response()->json([
               'status'  => 'fail',
               'message' => 'Cannot sign this document. Error code: es-002',
            ]);
        }
    }

    public function saveEverSignSignedDocument()
    {
        $signatureData      = \session()->get('signatureData');
        if(!$signatureData['documentHash'])
        {
            return response()->json([
                'status' => 'fail',
                'message' => 'Document hash not found, please try again. If this error persists, please contact support. Error code: save-e-doc-001',
            ]);
        }
        try
        {
            $finalDocument      = file_get_contents(SignatureCombine::$everSignApiRequest.'download_final_document?access_key='.Config::get('constants.EVERSIGN_ACCESS_KEY').'&business_id='.Config::get('constants.EVERSIGN_BUSINESS_ID').'&document_hash='.$signatureData['documentHash']);
            if($finalDocument === FALSE)
            {
                Log::info(['FinalDocument' => FALSE, 'FinalDocumentOutput' => $finalDocument, __METHOD__ => __LINE__]);
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Document could not be fetched from server, please try again. If this error persists, please contact support. Error code: save-e-doc-002',
                ]);
            }

            $log = log_Signature::where('APIIdentifier', $signatureData['documentHash'])
                ->where('Users_ID', auth()->id())
                ->limit(1)
                ->get();

            if(!$log->isEmpty())
            {
                $useTemplate = $log->first()->useTemplate;
                $updateLog = new log_Signature();
                $updateLog->upsert([
                    'ID'    => $log->first()->ID,
                    'Event' => 'signing_complete',
                ]);
            }
            else
            {
                $useTemplate = FALSE;
            }
        }
        catch (\Exception $e)
        {
            Log::info(['exception' => $e->getMessage(), __METHOD__ => __LINE__]);
            ddd(['Exception message' => $e->getMessage()]);
            return response()->json([
                'status' => 'fail',
                'message' => 'Document could not be fetched from server, please try again. If this error persists, please contact support. Error code: save-e-doc-004',
            ]);
        }

        $signedByArray = [];
        foreach ($signatureData['originalFiles'] as $file)
        {
            if ($file->SignedBy != '' || !is_null($file->SignedBy))
            {
                if (strpos($file->SignedBy, '_')) $signedBy = explode('_', $file->SignedBy);
                else $signedBy = [$file->SignedBy];

                $signedByArray = array_merge($signedByArray,$signedBy);
            }
        }
        $signedByArray = array_unique($signedByArray);
        $signedByArray = array_values($signedByArray);
        $newSignedBy = implode('_',$signedByArray);

        $bag_DocumentSaved  = SignatureCombine::saveNewDocumentPostSignature($finalDocument,NULL,$signatureData['documentCode'],$newSignedBy,$useTemplate);
        if($bag_DocumentSaved)
        {
            return response()->json([
                'status'    => 'success',
                'bagID'     => $bag_DocumentSaved->ID,
                'message'   => 'Document Successfully Saved',
            ]);
        }
        else
        {
            return response()->json([
               'status'  => 'fail',
               'message' => 'New Document was not saved, please try again. If this error persists, please contact support. Error Code: save-e-doc-003'
            ]);
        }
    }

    public function cancelEverSignDocument(Request $request)
    {
        $transactionID  = $request->input('transactionID');
        $bagIDString    = $request->input('bagIDString');
        $documentCode   = $request->input('documentCode');
        $bagIDs         = explode('.',$bagIDString);
        $userID         = auth()->id();

        $log_signatures = log_Signature::where('Event', '=', 'created')
            ->where('Users_ID', $userID)
            ->where('Documents_Code', $documentCode)
            ->where('Transactions_ID', $transactionID)
            ->get();

        if(!$log_signatures->isEmpty())
        {
            foreach ($log_signatures as $log)
            {
                $loggedBagIDs   = explode('.', $log->BagIDString);
                $diff           = array_diff($loggedBagIDs,$bagIDs);
                if(empty($diff))
                {
                    $updateLog              = new log_Signature();
                    $updateData['ID']       = $log->ID;
                    $updateData['Event']    = 'canceled';
                    $updated = $updateLog->upsert($updateData);
                    if($updated)
                    {
                        return response()->json([
                            'status'            => 'success',
                        ]);
                    }
                    else
                    {
                        return response()->json([
                            'status'  => 'fail',
                            'message' => 'Logs found but unable to update. Error code: c-es-log-001',
                        ]);
                    }
                }
            }
        }
        return response()->json([
            'status'    => 'fail',
            'message'   => 'Logs not found. Error code: c-es-log-002'
        ]);
    }

    public function checkDocumentExistence(Request $request)
    {
        $transactionID  = $request->input('transactionID');
        $bagIDString    = $request->input('bagIDString');
        $documentCode   = $request->input('documentCode');
        $bagIDs         = explode('.',$bagIDString);
        $userID         = auth()->id();
        $log_signatures = log_Signature::where('Event', '=', 'created')
            ->where('Users_ID', $userID)
            ->where('Documents_Code', $documentCode)
            ->get();

        if(!$log_signatures->isEmpty())
        {
            foreach ($log_signatures as $log)
            {
                $loggedBagIDs   = explode('.', $log->BagIDString);
                $diff           = array_diff($loggedBagIDs,$bagIDs);
                if(empty($diff))
                {
                    $signatureData['embeddedSigningURL'] = $log->SigningURL;
                    $signatureData['documentHash']       = $log->APIIdentifier;
                    $signatureData['documentCode']       = $documentCode;
                    $signatureData['transactionID']      = $transactionID;
                    $signatureData['originalFiles']      = bag_TransactionDocument::whereIn('ID', $loggedBagIDs)
                        ->get();
                    \session()->put('signatureData', $signatureData);
                    return response()->json([
                        'status'            => 'found',
                        'signingURL'        => $log->SigningURL,
                    ]);
                }
            }
        }
        return response()->json([
            'status' => 'not found',
            'message' => 'Logs not found. Error code: check-es-doc-001'
        ]);
    }

}