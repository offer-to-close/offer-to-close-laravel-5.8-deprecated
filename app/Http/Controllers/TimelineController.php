<?php

namespace App\Http\Controllers;

use App\Combine\TaskCombine;
use App\Models\Task;
use Illuminate\Http\Request;
use App\Mail\TimelineDetails;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Models\TimelineDetail;
use App\Models\Transaction;
use App\Combine\MilestoneCombine;
use Illuminate\Support\Facades\Log;


class TimelineController extends Controller
{
    public function create(){
        if( env('UI_VERSION') == '2a' )
        {
            return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.transactionTimeline.create'));
        }
        return view( 'timeline.create' );
    }

    public function store(Request $request){
        //Validate the request
        $validator = Validator::make($request->all(), [
            'sender_email' => 'required|email|max:255',
            'reciever_email' => 'required|email|max:255',
            'property_address' => 'required|max:200',
            'mutual_acceptance_date' => 'required|date|max:191',
            'deposit_due_date' => 'nullable|date',
            'disclosures_due_date' => 'nullable|date',
            'inspection_contingency' => 'nullable|date',
            'appraisal_contingency' => 'nullable|date',
            'loan_contingency' => 'nullable|date',
            'close_of_escrow_date' => 'nullable|date',
            'possession_on_property' => 'nullable|date',

            'mutual_acceptance_days' => 'nullable|integer|min:1|max:365',
            'deposit_due_days' => 'nullable|integer|min:1|max:365',
            'disclosures_due_days' => 'nullable|integer|min:1|max:365',
            'inspection_contingency_days' => 'nullable|integer|min:1|max:365',
            'appraisal_contingency_days' => 'nullable|integer|min:1|max:365',
            'loan_contingency_days' => 'nullable|integer|min:1|max:365',
            'close_of_escrow_days' => 'nullable|integer|min:1|max:365',
            'possession_on_property_days' => 'nullable|integer|min:1|max:365',
        ]);

        if ( $validator->fails() ) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $sender = request('sender_email');
        $reciever = request('reciever_email');
        $property_address = request('property_address');
        $mutual_acceptance_date = request('mutual_acceptance_date');
        $deposit_due_date = request('deposit_due_date');
        $deposit_due_days = request('deposit_due_days');
        $disclosures_due_date = request('disclosures_due_date');
        $disclosures_due_days = request('disclosures_due_days');
        $inspection_contingency = request('inspection_contingency');
        $inspection_contingency_days = request('inspection_contingency_days');
        $appraisal_contingency = request('appraisal_contingency');
        $appraisal_contingency_days = request('appraisal_contingency_days');
        $loan_contingency = request('loan_contingency');
        $loan_contingency_days = request('loan_contingency_days');
        $close_of_escrow_date = request('close_of_escrow_date');
        $close_of_escrow_days = request('close_of_escrow_days');
        $possession_on_property = request('possession_on_property');
        $possession_on_property_days = request('possession_on_property_days');
        $link_id = str_random(50);

        $split_address = preg_split('/\r\n|\r|\n|,/', $property_address);
        $address = urlencode($split_address[0]);

        $link_address = url('/') . '/transaction-timeline/' . $address . '/' . $link_id;



        //Saving data into database
        $timeline_details = new TimelineDetail;
        $timeline_details->Sender = $sender;
        $timeline_details->Reciever = $reciever;
        $timeline_details->PropertyAddress = $property_address;
        $timeline_details->MutualAcceptanceDate = $mutual_acceptance_date;
        $timeline_details->DepositDueDate = $deposit_due_date;
        $timeline_details->DisclosureDueDate = $disclosures_due_date;
        $timeline_details->InspectionContingency = $inspection_contingency;
        $timeline_details->AppraisalContingency = $appraisal_contingency;
        $timeline_details->LoanContingency = $loan_contingency;
        $timeline_details->CloseOfEscrowDate = $close_of_escrow_date;
        $timeline_details->PossessionOnProperty = $possession_on_property;
        $timeline_details->LinkID = $link_id;
        $timeline_details->save();

        try{
            Mail::to($reciever)->bcc([$sender, env('MAIL_FROM_ADDRESS')])->send(new TimelineDetails($sender, $reciever, $property_address, $mutual_acceptance_date, $deposit_due_date, $deposit_due_days, $disclosures_due_date, $disclosures_due_days, $inspection_contingency, $inspection_contingency_days, $appraisal_contingency, $appraisal_contingency_days, $loan_contingency, $loan_contingency_days, $close_of_escrow_date, $close_of_escrow_days, $possession_on_property, $possession_on_property_days, $link_address));
            session()->flash('alert-success', "The Transaction Timeline was successfully sent to: ". $reciever );
            return redirect()->back()->withInput();
        }
        catch( \Exception $e ){
            if ( count(Mail::failures() ) > 0 ) {
                session()->flash('alert-danger', 'Something went wrong');
            }
            return redirect()->back()->withInput();
        }
    }

    public function show(Request $request){
        $link_id = request('link');
        $timeline_details = TimelineDetail::where('LinkID', $link_id)->first();
        if( env('UI_VERSION') == '2a' )
        {
            if($timeline_details){
                return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.transactionTimeline.show'), compact('timeline_details'));
            } else{
                return view('general.404');
            }
        }
        if($timeline_details){
            return view('timeline.show', compact('timeline_details'));
        } else{
            return view('general.404');
        }
    }

    public function edit(Request $request){
        $link_id = request('link');
        $timeline_details = TimelineDetail::where('LinkID', $link_id)->first();
        if( env('UI_VERSION') == '2a'  )
        {
            if ($timeline_details) {
                return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.transactionTimeline.edit'), compact('timeline_details'));
            }
            else
            {
                return view('general.404');
            }
        }
        if ($timeline_details) {
            return view('timeline.edit', compact('timeline_details'));
        } else {
            return view('general.404');
        }
    }

    public function update(Request $request, $link){

        $validator = Validator::make($request->all(), [
            'sender_email' => 'required|email|max:255',
            'reciever_email' => 'required|email|max:255',
            'property_address' => 'required|max:200',
            'mutual_acceptance_date' => 'required|date|max:191',
            'deposit_due_date' => 'nullable|date',
            'disclosures_due_date' => 'nullable|date',
            'inspection_contingency' => 'nullable|date',
            'appraisal_contingency' => 'nullable|date',
            'loan_contingency' => 'nullable|date',
            'close_of_escrow_date' => 'nullable|date',
            'possession_on_property' => 'nullable|date',

            'mutual_acceptance_days' => 'nullable|integer|min:1|max:365',
            'deposit_due_days' => 'nullable|integer|min:1|max:365',
            'disclosures_due_days' => 'nullable|integer|min:1|max:365',
            'inspection_contingency_days' => 'nullable|integer|min:1|max:365',
            'appraisal_contingency_days' => 'nullable|integer|min:1|max:365',
            'loan_contingency_days' => 'nullable|integer|min:1|max:365',
            'close_of_escrow_days' => 'nullable|integer|min:1|max:365',
            'possession_on_property_days' => 'nullable|integer|min:1|max:365',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $sender = request('sender_email');
        $reciever = request('reciever_email');
        $property_address = request('property_address');
        $mutual_acceptance_date = request('mutual_acceptance_date');
        $deposit_due_date = request('deposit_due_date');
        $deposit_due_days = request('deposit_due_days');
        $disclosures_due_date = request('disclosures_due_date');
        $disclosures_due_days = request('disclosures_due_days');
        $inspection_contingency = request('inspection_contingency');
        $inspection_contingency_days = request('inspection_contingency_days');
        $appraisal_contingency = request('appraisal_contingency');
        $appraisal_contingency_days = request('appraisal_contingency_days');
        $loan_contingency = request('loan_contingency');
        $loan_contingency_days = request('loan_contingency_days');
        $close_of_escrow_date = request('close_of_escrow_date');
        $close_of_escrow_days = request('close_of_escrow_days');
        $possession_on_property = request('possession_on_property');
        $possession_on_property_days = request('possession_on_property_days');
        $link_address = url('/') . '/transaction-timeline/?add=' . $property_address . '&link=' . $link;

        //update the values in timeline_details table
        //$timeline_details = TimelineDetail::where('LinkID', $link)->first();
        $timeline_details = TimelineDetail::where('LinkID', $link)->update([
            'Sender' => $sender,
            'Reciever' => $reciever,
            'PropertyAddress' => $property_address,
            'MutualAcceptanceDate' => $mutual_acceptance_date,
            'DepositDueDate' => $deposit_due_date,
            'DisclosureDueDate' => $disclosures_due_date,
            'InspectionContingency' => $inspection_contingency,
            'AppraisalContingency' => $appraisal_contingency,
            'LoanContingency' => $loan_contingency,
            'CloseOfEscrowDate' => $close_of_escrow_date,
            'PossessionOnProperty' => $possession_on_property,
        ]);

        if ( $timeline_details ) {

        }
        else {
            return view('general.404');
        }

        $split_address = preg_split('/\r\n|\r|\n|,/', $property_address);
        $address = urlencode( $split_address[0] );
        $link_address = url('/') . '/transaction-timeline/' . $address . '/' . $link;

        try {
            Mail::to($reciever)->bcc([$sender, env('MAIL_FROM_ADDRESS')])->send(new TimelineDetails($sender, $reciever, $property_address, $mutual_acceptance_date, $deposit_due_date, $deposit_due_days, $disclosures_due_date, $disclosures_due_days, $inspection_contingency, $inspection_contingency_days, $appraisal_contingency, $appraisal_contingency_days, $loan_contingency, $loan_contingency_days, $close_of_escrow_date, $close_of_escrow_days, $possession_on_property, $possession_on_property_days, $link_address));
            session()->flash('alert-success', "The Transaction Timeline was successfully sent to: " . $reciever);
            return redirect()->back()->withInput();
        } catch (\Exception $e) {
            if (count(Mail::failures()) > 0) {
                session()->flash('alert-danger', 'Something went wrong');
                return redirect()->back()->withInput();

            }
        }
    }

    public function recalculateTimeline(Request $request)
    {
        $transactionID  = $request->input('transactionID');
        $dateAcceptance = $request->input('dateAcceptance');
        $transaction    = Transaction::find($transactionID);
        if(!is_null($transaction))
        {
            $escrowLength   = $transaction->EscrowLength;
            $result = MilestoneCombine::saveTransactionTimeline($transactionID,$dateAcceptance,$escrowLength);
            TaskCombine::resetTasks($transactionID);
            Transaction::where('ID',$transactionID)->update(['DateAcceptance' => $dateAcceptance]);
            if ($result)
            {
                return response()->json([
                    'status' => 'success',
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Error code: recalculateTimeline001'
                ]);
            }
        }
        return response()->json([
            'status' => 'fail',
            'message' => 'Transaction could not be found. Error code: recalculateTimeline002',
        ]);
    }
}
