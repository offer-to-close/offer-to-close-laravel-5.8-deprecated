<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine;
use App\Combine\PropertyCombine;
use App\Library\otc\_Locations;
use App\Library\Utilities\_LaravelTools;
use App\Library\otc\AddressVerification;
use App\Combine\TaskCombine;
use App\Library\otc\Laravel;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_Time;
use App\Models\AddressOverride;
use App\Models\Agent;
use App\Models\Buyer;
use App\Models\Broker;

use App\Models\Escrow;
use App\Models\Title;
use App\Models\Loan;
use App\Models\lk_Transactions_Timeline;
use App\Models\Property;
use App\Models\Seller;
use App\Models\Timeline;
use App\Models\Transaction;

use App\Models\TransactionCoordinator;
use App\Combine\BaseCombine;
use App\Combine\DocumentCombine;
use App\Combine\MilestoneCombine;
use App\Combine\TransactionCombine;
use App\Library\Utilities\DisplayTable;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class TransactionController extends Controller
{
    /**
     * Instatiate a new controller instance
     *
     */

    public $modelPath = 'App\\Models\\';

    public function __construct()
    {
        // methods protected by auth middleware
        //$this->middleware('auth')->only('inputBuyer', 'inputSeller');
    }

    public $transactionID = null;
    public $transactionParts = ['transaction',
                                'property',
                                'buyer',
                                'buyersAgent',
                                'seller',
                                'sellersAgent',
                                'timeline',
    ];
    public $nonInputFields = ['ID',
                              'isTest',
                              'Status',
                              'DateCreated',
                              'DateUpdated',
                              'deleted_at',
    ];

    public $transaction = null;

    protected $isEmpty = true;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transaction.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home($transactionID, $firstTime = false)
    {
        $view = 'transaction.home';

        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess('u') || !AccessController::canAccessTransaction($transactionID)) // requires User access
        {
            if (request()->has('previous'))
            {
                return redirect()->back()
                                 ->with(['failTransaction' => 'You have no access to this transaction. Please try again.']);
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'),
                ['role' => session('userRole'), 'id' => CredentialController::current()->ID()]);
        }

        if (empty($transactionID)) $transactionID = session('transaction_id');

        $transactionData = TransactionCombine::fullTransaction($transactionID);
        $address         = AddressVerification::formatAddress($transactionData['property']->first(), 2);

        $side = $transactionData['transaction']->first()->Side;
        session()->put('transactionSide', $side);

        $existingPropertyImage = glob(public_path('images/propertyImages/'.$transactionID.'/propertyImage.*'));
        if (!empty($existingPropertyImage))
        {
            $propertyImageFile = explode('/', $existingPropertyImage[0]);
            $propertyImageFile = $propertyImageFile[count($propertyImageFile) - 1];
            $propertyImageURL = _Locations::url('images', '/propertyImages/' . $transactionID . '/' . $propertyImageFile);
        }
        else
        {
            $propertyImageURL = _Locations::url('images', '/propertyImagePlaceholder.png');
        }

        $transactionData['propertyImageURL'] = $propertyImageURL;
        $isCreated = TransactionCombine::isCreated($transactionData);
        return view(_LaravelTools::addVersionToViewName($view), [
            '_isCreated'    => $isCreated,
            'data'          => $transactionData,
            'transactionID' => $transactionID,
            'address'       => $address,
            'userRoles'     => TransactionCombine::getUserTransactionRoles($transactionID),
            'side'          => $side,
            'firstTime'     => $firstTime,
        ]);
    }

    public function getNavBuyer(Request $request)
    {
        $ID            = $request->ID;
        $transactionID = $request->transactionID;
        $buttons       = TransactionCombine::getNavBuyer($transactionID, $ID);
        return $buttons;
    }

    public function getNavSeller(Request $request)
    {
        $ID            = $request->ID;
        $transactionID = $request->transactionID;
        $buttons       = TransactionCombine::getNavSeller($transactionID, $ID);
        return $buttons;
    }

    private function getFieldName($roleCode)
    {
        $fields = ['btc' => 'BuyersTransactionCoordinators_ID',
                   'ba'  => 'BuyersAgent_ID',
                   'b'   => null,
                   'stc' => 'SellersTransactionCoordinators_ID',
                   'sa'  => 'SellersAgent_ID',
                   's'   => null,
                   'e'   => 'Escrows',
                   'l'   => 'Loans',
                   't'   => 'Titles'];
        if (!isset($fields[$roleCode])) return false;
        return $fields[$roleCode];
    }

    public function create(Request $request, $side = 'b')
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        if (!AccessController::hasAccess('u'))  // requires User access
        {
            if (request()->has('previous'))
            {
                return redirect()->back()
                                 ->with(['failTransaction' => 'You have no access to this transaction. Please try again.']);
            }
            else return redirect()->route(_LaravelTools::addVersionToViewName('dash.ta.list'));
        }

        $userID   = CredentialController::current()->ID();
        $userRole = session('userRole');
        $route    = 'transaction.home';

        session()->put('transactionSide', $side);

        // ... Create a new Transaction record and store TC data in the record
        $transaction                           = new Transaction;
        $transaction->DateOfferPrepared        = date('Y-m-d');
        $transaction->DateAcceptance           = date('Y-m-d', strtotime($transaction->DateOfferPrepared . ' + 1 day'));
        $transaction->Side                     = $side;
        $transaction->EscrowLength             = Config::get('constants.DEFAULT.ESCROW_LENGTH');
        $transaction->DateEnd                  = _Time::formatDate(_Time::addDays($transaction->DateAcceptance, $transaction->EscrowLength), 'Y-m-d');
        $transaction->CreatedByUsers_ID        = $userID;
        $transaction->OwnedByUsers_ID          = $userID;
        $transaction->hasInspectionContingency = Timeline::getByCode('ic')->DaysOffset ?? 0;
        $transaction->hasAppraisalContingency  = Timeline::getByCode('app')->DaysOffset ?? 0;
        $transaction->hasLoanContingency       = Timeline::getByCode('lc')->DaysOffset ?? 0;
        $transaction->Status                   = 'draft';

        $setHomesumer = false;
        if($side == 'bs')
        {
            $sides = ['b','s'];
            foreach ($sides as $side)
            {
                if($field = $this->getFieldName($side.$userRole))
                {
                    $roleID = AccountCombine::getRoleID($side.$userRole, $userID);
                    if($roleID->first()) $roleID = $roleID->first()->ID;
                    else Log::debug(['getRoleID error: ' => $roleID ]);
                    $transaction->$field = $roleID;
                }
            }
        }
        elseif ($field = $this->getFieldName($side.$userRole))
        {
            $roleID = AccountCombine::getRoleID($side . $userRole, $userID);
            if ($roleID->first())
            {
                $roleID = $roleID->first()->ID;
            }
            else Log::debug(['getRoleID error: ' => $roleID]);
            $transaction->$field = $roleID;
        }
        else
        {
            if (is_null($field))
            {
                $setHomesumer = true;
            }
            else Log::debug(['Role ID not found for role: ' . $side . $userRole, 'field' => $field, __METHOD__ => __LINE__]);
        }
//        Log::info(['role' => $side . $userRole, 'userRole' => $userRole, 'roleID' => $roleID, 'transaction' => $transaction]);
        if ($side == 'b')
        {
            $transaction->BuyersOwner_ID  = $userID;
            $transaction->BuyersOwnerRole = session('userRole');
        }
        else if ($side == 's')
        {
            $transaction->SellersOwner_ID  = $userID;
            $transaction->SellersOwnerRole = session('userRole');
        }
        else if($side == 'bs')
        {
            $transaction->BuyersOwner_ID = $userID;
            $transaction->BuyersOwnerRole = session('userRole');

            $transaction->SellersOwner_ID = $userID;
            $transaction->SellersOwnerRole = session('userRole');
        }

        $transaction->save();

        $taID = $transaction->id;

        if ($setHomesumer && $taID)
        {
            if ($side == 'b') Buyer::createFromUser($taID, $userID);
            if ($side == 's') Seller::createFromUser($taID, $userID);
        }

        session(['transaction_id' => $taID]);
        session(['transactionSide' => $side]);
        TransactionCombine::updateSessionTransactionList([$transaction->id => $transaction->Status]);
        return redirect()->route($route, ['taid' => $taID, 'firstTime' => true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array \Illuminate\Http\Response
     */
    public function getEmptyTransaction(array $values)
    {
        $models = ['transaction'  => new Transaction,
                   'property'     => new Property,
                   'buyer'        => new Buyer,
                   'buyersAgent'  => new Agent,
                   'seller'       => new Seller,
                   'sellersAgent' => new Agent,
                   'timeline'     => new Timeline,
        ];

        $rv = [];

        foreach ($models as $name => $model)
        {
            foreach ($model::getEmptyRecord() as $field => $value)
            {
                if (array_search($field, $model->nonInputFields) === false) continue;
                if (!isset($values[$name][$field]) || is_null($values[$name][$field])) continue;
                $model->$field = $value;
            }
            $rv[$name] = $model->save();
        }
        $this->isEmpty = true;
        return $rv;
    }

    /**
     * Show the form for creating a new resource.
     *
     * $param int $transactionsID
     *
     * @return array \Illuminate\Http\Response
     */
    public function getTransaction(int $transactionsID)
    {
        $transaction       = Transaction::find($transactionsID);
        $this->transaction =
            ['transaction'  => $transaction,
             'property'     => Property::where('ID', $transaction->Properties_ID)->get(),
             'buyer'        => Buyer::where('Transactions_ID', $transactionsID)->get(),
             'buyersAgent'  => Agent::where('ID', $transaction->BuyersAgent_ID)->get(),
             'buyersTC'     => TransactionCoordinator::where('ID', $transaction->BuyersTransactionCoordinators_ID)->get(),
             'seller'       => Seller::where('Transactions_ID', $transactionsID)->get(),
             'sellersAgent' => Agent::where('ID', $transaction->SellersAgent_ID)->get(),
             'sellersTC'    => TransactionCoordinator::where('ID', $transaction->SellersTransactionCoordinators_ID)
                                                     ->get(),
             'escrow'       => Escrow::where('ID', $transaction->Escrows_ID)->get(),
             'title'        => Title::where('ID', $transaction->Titles_ID)->get(),
             'loan'         => Loan::where('ID', $transaction->Loans_ID)->get(),
             'timeline'     => TransactionCombine::getTimeline($transactionsID),
            ];
        $this->isEmpty     = $this->isEmpty($this->transaction);
        return $this->transaction;
    }

    public function transaction($transactionsID, $justTransactionRecord = true)
    {
        $transaction = Transaction::find($transactionsID);
        if ($justTransactionRecord) return $transaction;

        return collect(
            ['transaction'  => $transaction,
             'property'     => Property::where('Transactions_ID', $transactionsID)->get(),
             'buyer'        => Buyer::where('Transactions_ID', $transactionsID)->get(),
             'buyersAgent'  => Agent::where('ID', $transaction->BuyersAgent_ID)->get(),
             'seller'       => Seller::where('Transactions_ID', $transactionsID)->get(),
             'sellersAgent' => Agent::where('ID', $transaction->SellersAgent_ID)->get(),
             'timeline'     => TransactionCombine::getTimeline($transactionsID),
            ]);
    }

    public function buyers($transactionsID): Collection
    {
        return TransactionCombine::buyers($transactionsID);
    }

    public function buyersAgent($transactionsID): Collection
    {
        return TransactionCombine::agents($transactionsID, Agent::TYPE_BUYER);
    }

    public function sellers($transactionsID): Collection
    {
        return TransactionCombine::sellers($transactionsID);
    }

    public function sellersAgent($transactionsID): Collection
    {
        return TransactionCombine::agents($transactionsID, Agent::TYPE_SELLER);
    }

    public function property($transactionsID): Collection
    {
        return TransactionCombine::property($transactionsID);
    }

    public function ajaxGetProperty($transactionsID)
    {
        return response()->json([
            'property' => TransactionCombine::property($transactionsID),
        ]);
    }

    /**
     * @param int $transactionsID
     *
     * @return \Illuminate\Support\Collection
     */
    public function toDo(int $transactionsID)
    {
        return TransactionCombine::toDo($transactionsID);
    }

    /**
     * @param int $transactionsID
     *
     * @return \Illuminate\Support\Collection
     */
    public function files(int $transactionsID)
    {
        return TransactionCombine::files($transactionsID);
    }

    /**
     * @param int $transactionsID
     *
     * @return \Illuminate\Support\Collection
     */
    public function activity(int $transactionsID)
    {
        $result = TransactionCombine::activity($transactionsID);
    }

    /**
     * @param int $transactionID
     * @param int $propertyID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function inputDetails($transactionID = 0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $recEmpty = (new Transaction())->getEmptyRecord();
        $view     = 'user.inputDetails';

        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);

        if (empty($transactionID)) $transactionID = session('transaction_id');

        $data = TransactionCombine::getDetails($transactionID);

        $model      = new Transaction();
        $screenMode = empty($data->PurchasePrice) ? 'create' : 'edit';

        $data = TransactionCombine::collectionToArray($data);
        if (empty($data[0])) $data[0] = $recEmpty;

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);
        // ddd($data);
        return view($view, [
            '_role'       => ['code'    => 'd',
                              'display' => 'Details',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => true,
        ]);
    }

    /**
     * @param int $transactionID
     * @param int $propertyID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inputProperty($transactionID = 0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $recEmpty = (new Property())->getEmptyRecord();
        $view     = 'user.inputProperty';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data    = TransactionCombine::getProperty($transactionID);
        $address = AddressVerification::formatAddress($data, 2);
        ddd(['ERROR', $address, __METHOD__ => __LINE__]);
        $data = TransactionCombine::collectionToArray($data) ?? ['Transactions_ID' => $transactionID];
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Property();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role'       => ['code'    => 'p',
                              'display' => 'Property',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'address'     => $address,
            'sameSide'    => true,
        ]);
    }

    /**
     * @param int $transactionID
     * @param int $buyerID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inputBuyer(Request $request, $transactionID = 0, $buyerID = 0)
    {
        if (env('UI_VERSION') == '2a')
        {
            $recEmpty = (new Buyer())->getEmptyRecord();

            $view = 'user.inputHomesumer';

            if ($transactionID)
            {
                session()->put('transaction_id', $transactionID);
            }
            else
            {
                return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
            }

            $clientRole = TransactionCombine::getClientRole($transactionID);
            $sameSide   = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);

            $buyers = TransactionCombine::getPerson(Config::get('constants.USER_ROLE.BUYER'), $transactionID);

            $buyerCount = $buyers['data']->count();
            if ($buyerID == 0)
            {
                $data = $recEmpty;
            }
            else
            {
                $data = TransactionCombine::getHomesumer(Config::get('constants.USER_ROLE.BUYER'), $buyerID);
                $data = $data->toArray();
                $data = (empty($data[0])) ? $recEmpty : $data[0];
            }
            $data = array_merge($data, ['Transactions_ID' => $transactionID]);
            $send = [
                '_role'       => ['code'    => Config::get('constants.USER_ROLE.BUYER'),
                                  'display' => 'Buyer',],
                '_screenMode' => ($buyerCount > 0) ? 'edit' : 'create',
                'data'        => $data,
                'buyers'      => $buyers['buyer'],
                'sameSide'    => $sameSide,
            ];
            //print_r($data);
            return response()->json($send);
        }

        #####################################################
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $recEmpty = (new Buyer())->getEmptyRecord();

        $view = 'user.inputHomesumer';

        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $clientRole = TransactionCombine::getClientRole($transactionID);
        $sameSide   = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);

        $buyers = TransactionCombine::getPerson(Config::get('constants.USER_ROLE.BUYER'), $transactionID);

        $buyerCount = $buyers['data']->count();
        if ($buyerID == 0)
        {
            $data = $recEmpty;
        }
        else
        {
            $data = TransactionCombine::getHomesumer(Config::get('constants.USER_ROLE.BUYER'), $buyerID);
            $data = $data->toArray();
            $data = (empty($data[0])) ? $recEmpty : $data[0];
        }
        $data = array_merge($data, ['Transactions_ID' => $transactionID]);
        // ddd([
        //     '_role'       => ['code'    => Config::get('constants.USER_ROLE.BUYER'),
        //                       'display' => 'Buyer',],
        //     '_screenMode' => ($buyerCount > 0) ? 'edit' : 'create',
        //     'data'        => $data,
        //     'buyers'      => $buyers['buyer'],
        //     'sameSide'    => $sameSide,
        // ]);
        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.BUYER'),
                              'display' => 'Buyer',],
            '_screenMode' => ($buyerCount > 0) ? 'edit' : 'create',
            'data'        => $data,
            'buyers'      => $buyers['buyer'],
            'sameSide'    => $sameSide,
        ]);
    }

    /**
     * @param int $transactionID
     * @param int $buyerID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inputBuyerAgent($transactionID = 0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $recEmpty = (new Agent())->getEmptyRecord();
        $view     = 'user.inputAgent';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $clientRole = TransactionCombine::getClientRole($transactionID);

        /**
         * Same side kluudge
         */
        $sameSide    = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);
        $transaction = Transaction::find($transactionID);
        $noAgent     = ($transaction->BuyersAgent_ID === 0) ? true : false;

        $data = TransactionCombine::getAgents($transactionID, Agent::TYPE_BUYER);
        $data = TransactionCombine::collectionToArray($data);
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Agent();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);
        //...Response for v2 Modals
        if (env('UI_VERSION') == '2a')
        {
            return response()->json([
                'data'  => $data,
                'role'  => $clientRole,
                '_role' => ['code'    => Config::get('constants.USER_ROLE.BUYERS_AGENT'),
                            'display' => 'Buyer\'s Agent',],
            ]);
        }

        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.BUYERS_AGENT'),
                              'display' => 'Buyer\'s Agent',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'noAgent'     => $noAgent,
            'sameSide'    => $sameSide,
        ]);
    }

    /**
     * /**
     * @param int $transactionID
     * @param int $buyerID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inputBroker($transactionID = 0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $recEmpty = (new Broker())->getEmptyRecord();
        $view     = 'user.inputBroker';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data = TransactionCombine::getPerson(Config::get('constants.USER_ROLE.BROKER'), $transactionID);

        $data = TransactionCombine::collectionToArray($data['data']);
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Broker();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.BROKER'),
                              'display' => 'Broker',],
            '_screenMode' => $screenMode,
            'data'        => $data,
        ]);
    }

    /**
     * @param int $transactionID
     * @param int $buyerID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inputBuyerTC($transactionID = 0, $ajax = false, $tc_type = 'btc')
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $recEmpty = (new TransactionCoordinator())->getEmptyRecord();

        $view = 'user.inputTC';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        if ($ajax)
        {
            if ($tc_type == 'btc' || $tc_type == 'ba') $type = TransactionCoordinator::TYPE_BUYERTC;
            if ($tc_type == 'stc' || $tc_type == 'sa') $type = TransactionCoordinator::TYPE_SELLERTC;
            //if($tc_type == 'bs') $type = TransactionCoordinator::TYPE_BOTH;
        }
        else $type = TransactionCoordinator::TYPE_BUYER;

        $clientRole = TransactionCombine::getClientRole($transactionID);
        $sameSide   = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);
        $data       = TransactionCombine::getTransactionCoordinators($transactionID, $type);
        $data       = TransactionCombine::collectionToArray($data);
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new TransactionCoordinator();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);

        if ($ajax)
        {
            return response()->json([
                'data' => $data,
                'role' => $clientRole,
            ]);
        }

        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.BUYERS_TRANSACTION_COORDINATOR'),
                              'display' => 'Buyer\'s TC',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => $sameSide,
        ]);
    }

    /**
     * @param int $transactionID
     * @param int $buyerID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inputSellerTC($transactionID = 0, $ajax = false)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $recEmpty = (new TransactionCoordinator())->getEmptyRecord();
        $view     = 'user.inputTC';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data = TransactionCombine::getTransactionCoordinators($transactionID, Agent::TYPE_SELLER);

        $clientRole = TransactionCombine::getClientRole($transactionID);
        $sameSide   = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);

        $data = TransactionCombine::collectionToArray($data);
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new TransactionCoordinator();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);

        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.SELLERS_TRANSACTION_COORDINATOR'),
                              'display' => 'Seller\'s TC',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => $sameSide,
        ]);
    }

    /**
     * Provide input form to create/edit Escrows record
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function inputEscrow($transactionID = 0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $recEmpty = (new Escrow())->getEmptyRecord();
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data = TransactionCombine::getEscrow($transactionID);

        $data = TransactionCombine::collectionToArray($data) ?? ['Transactions_ID' => $transactionID];
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Escrow();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);
        if (env('UI_VERSION') == '2a')
        {
            return response()->json([
                '_role'       => [
                    'code'    => Config::get('constants.USER_ROLE.ESCROW'),
                    'display' => 'Escrow',
                    'table'   => 'Escrows',
                    'model'   => 'App\\Models\\Escrow',
                ],
                '_screenMode' => $screenMode,
                'data'        => $data,
                'sameSide'    => true,
            ]);
        }

        return view('user.inputAuxillaryRole', [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.ESCROW'),
                              'display' => 'Escrow',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => true,
        ]);
    }

    /**
     * Provide input form to create/edit Loans record
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function inputLoan($transactionID = 0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $recEmpty = (new Loan())->getEmptyRecord();
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data = TransactionCombine::getLoan($transactionID);

        $data = TransactionCombine::collectionToArray($data) ?? ['Transactions_ID' => $transactionID];
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Loan();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);
        if (env('UI_VERSION') == '2a')
        {
            return response()->json([
                '_role'       => [
                    'code'    => Config::get('constants.USER_ROLE.LOAN'),
                    'display' => 'Loan',
                    'table'   => 'Loans',
                    'model'   => 'App\\Models\\Loan',
                ],
                '_screenMode' => $screenMode,
                'data'        => $data,
                'sameSide'    => true,
            ]);
        }
        return view('user.inputAuxillaryRole', [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.LOAN'),
                              'display' => 'Loan',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => true,
        ]);
    }

    /**
     * Provide input form to create/edit Titles record
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function inputTitle($transactionID = 0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $recEmpty = (new Title())->getEmptyRecord();
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $data = TransactionCombine::getTitle($transactionID);

        $data = TransactionCombine::collectionToArray($data) ?? ['Transactions_ID' => $transactionID];
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Title();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);

        if (env('UI_VERSION') == '2a')
        {
            return response()->json([
                '_role'       => [
                    'code'    => Config::get('constants.USER_ROLE.TITLE'),
                    'display' => 'Title',
                    'table'   => 'Titles',
                    'model'   => 'App\\Models\\Title',
                ],
                '_screenMode' => $screenMode,
                'data'        => $data,
                'sameSide'    => true,
            ]);
        }

        return view('user.inputAuxillaryRole', [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.TITLE'),
                              'display' => 'Title',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'sameSide'    => true,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function inputSeller($transactionID = 0, $sellerID = 0)
    {
        if (env('UI_VERSION') == '2a')
        {
            if ($transactionID)
            {
                session()->put('transaction_id', $transactionID);
            }
            else
            {
                return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
            }

            $clientRole = TransactionCombine::getClientRole($transactionID);
            $sameSide   = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);

            $recEmpty = (new Seller())->getEmptyRecord();

            $sellers     = TransactionCombine::getPerson(Config::get('constants.USER_ROLE.SELLER'), $transactionID);
            $sellerCount = $sellers['data']->count();

            if ($sellerID == 0)
            {
                $data = $recEmpty;

            }
            else
            {
                $data = TransactionCombine::getHomesumer(Config::get('constants.USER_ROLE.SELLER'), $sellerID);
                $data = $data->toArray();
                $data = (empty($data[0])) ? $recEmpty : $data[0];
            }
            $data = array_merge($data, ['Transactions_ID' => $transactionID]);
            $send = [
                '_role'       => ['code'    => Config::get('constants.USER_ROLE.SELLER'),
                                  'display' => 'Seller',],
                '_screenMode' => ($sellerCount > 0) ? 'edit' : 'create',
                'data'        => $data,
                'sellers'     => $sellers['seller'],
                'sameSide'    => $sameSide,
            ];
            return response()->json($send);
        }
        #######################################################
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $view = 'user.inputHomesumer';

        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $clientRole = TransactionCombine::getClientRole($transactionID);
        $sameSide   = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);

        $recEmpty = (new Seller())->getEmptyRecord();

        $sellers     = TransactionCombine::getPerson(Config::get('constants.USER_ROLE.SELLER'), $transactionID);
        $sellerCount = $sellers['data']->count();

        if ($sellerID == 0)
        {
            $data = $recEmpty;

        }
        else
        {
            $data = TransactionCombine::getHomesumer(Config::get('constants.USER_ROLE.SELLER'), $sellerID);
            $data = $data->toArray();
            $data = (empty($data[0])) ? $recEmpty : $data[0];
        }
        $data = array_merge($data, ['Transactions_ID' => $transactionID]);
        // ddd($data)
        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.SELLER'),
                              'display' => 'Seller',],
            '_screenMode' => ($sellerCount > 0) ? 'edit' : 'create',
            'data'        => $data,
            'sellers'     => $sellers['seller'],
            'sameSide'    => $sameSide,
        ]);
    }

    /**
     * Save Buyer Data
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveBuyer(Request $request)
    {
        if (isset($request->Transactions_ID)) $clientRole = TransactionCombine::getClientRole($request->Transactions_ID);
        $validationList = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
        ];

        $validator = Validator::make(Input::all(), $validationList);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $data    = $request->all();
        $address = $request->input('Address');
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);
            if ($addressList == false)
            {
                return response()->json([
                    'status'  => 'AddressError',
                    'Address' => 'Invalid Address',
                    'errors'  => [
                        'Address' => [
                            'Invalid Address',
                        ],
                    ],
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

        // ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = trim($data['NameFirst'] . ' ' . $data['NameLast']);
        }

        $buyer         = new Buyer();
        $data_inserted = $buyer->upsert($data);
        if ($data_inserted)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Unable to save the data.',
            ]);
        }
    }

    /**
     * Generic function that can be used to save any aux role if a _roleCode field is passed in.
     * Transactions_ID should be 0 if only the person is being saved.
     *
     * This function can also be used to attach someone to
     * a transaction.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveAux(Request $request)
    {
        $data          = $request->all();
        $transactionID = $data['Transactions_ID'];
        $roleCode      = $data['_roleCode'] ?? null;
        $noAttach      = false;
        if ($transactionID == 0) $noAttach = true;

        if ($roleCode != null)
        {
            switch ($roleCode)
            {
                case 'e':
                    $aux    = new Escrow();
                    $idSpot = 'Escrows_ID';
                    break;
                case 't':
                    $aux    = new Title();
                    $idSpot = 'Titles_ID';
                    break;
                case 'l':
                    $aux    = new Loan();
                    $idSpot = 'Loans_ID';
                    break;
                case 'btc':
                    $aux    = new TransactionCoordinator();
                    $idSpot = 'BuyersTransactionCoordinators_ID';
                    break;
                case 'stc':
                    $aux    = new TransactionCoordinator();
                    $idSpot = 'SellersTransactionCoordinators_ID';
                    break;
                case 'tc':
                    $noAttach = TRUE;
                    $aux      = new TransactionCoordinator();
                    break;
                case 'b':
                    $aux = new Buyer();
                    break;
                case 's':
                    $aux = new Seller();
                    break;
                case 'ba':
                    $aux = new Agent();
                    break;
                case 'sa':
                    $aux = new Agent();
                    break;
                case 'a':
                    $noAttach = TRUE;
                    $aux = new Agent();
                    break;
                case 'broker':
                    $noAttach = TRUE;
                    $aux = new Broker();
                    break;
                default:
                    return response()->json([
                        'data'   => $data,
                        'status' => 'fail',
                    ], 200);
            }
        }
        else
        {
            return response()->json([
                'data'   => $data,
                'status' => 'fail',
            ], 200);
        }

        $validationList = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'Company'         => 'max:100',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
            'Email'           => 'required|email',
            '_roleCode'       => 'required',
        ];

        //if there are any additional validations required (maybe based on role),
        //they can be merged with validationList.

        $validator = Validator::make(Input::all(), $validationList);
        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors'  => $validator->getMessageBag()->toArray(),

            ], 400);
        }

        $address       = $request->input('Address');
        $addressVerify = new AddressVerification();
        $addressList   = $addressVerify->isValid($address);

        $transactionID = $data['Transactions_ID'];

        if ($transactionID == 0)
        {
            unset($data[0]); //todo what is causing there to be a random 0 in this POST data.
            //todo also check loan,escrow, tc, etc.
        }

        if ($addressList == false)
        {
            return response()->json([
                'status'          => 'AddressError',
                'PropertyAddress' => 'Invalid Address',
                'errors'          => array(
                    'Address' => [
                        'Invalid Address',
                    ]
                )
            ], 404);
        }
        $data['Street1'] = $addressList[0]['Street1'];
        $data['Street2'] = $addressList[0]['Street2'];
        $data['City']    = $addressList[0]['City'];
        $data['State']   = $addressList[0]['State'];
        $data['Zip']     = $addressList[0]['Zip'];

// ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = $data['NameFirst'] . ' ' . $data['NameLast'];
        }

        $aux        = $aux->upsert($data);
        $data['ID'] = $data['ID'] ?? $aux->ID ?? $aux->id;

        if ($transactionID != 0 && !$noAttach)
        {
            $rv = Transaction::where('ID', $transactionID)->update([$idSpot => $data['ID']]);
        }
        else $rv = true;

        if ($aux && $rv)
        {
            return response()->json([
                'data'   => $data,
                'status' => 'success',
            ], 200);
        }
    }

    /**
     * Validate and update/insert Escrows record
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveEscrow(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $validation = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'Company'         => 'required|max:100',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
            'Email'           => 'required|email',
        ];

        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $data    = $request->all();
        $address = $request->input('Address');
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);
            if ($addressList == false)
            {
                return response()->json([
                    'status'          => 'AddressError',
                    'PropertyAddress' => 'Invalid Address',
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

// ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = $data['NameFirst'] . ' ' . $data['NameLast'];
        }

        $aux        = new Escrow();
        $aux        = $aux->upsert($data);
        $data['ID'] = $data['ID']  ?? $aux->ID ?? $aux->id;

        $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['Escrows_ID' => $data['ID']]);

        if ($aux && $rv)
        {
            return response()->json([
                'data'   => $data,
                'status' => 'success',
            ], 200);
        }
    }

    /**
     * Validate and update/insert Titles record
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveTitle(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $validation = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'Company'         => 'required|max:100',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
            'Email'           => 'required|email',
        ];

        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $data    = $request->all();
        $address = $request->input('Address');
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);

            if ($addressList == false)
            {
                return response()->json([
                    'status'          => 'AddressError',
                    'PropertyAddress' => 'Invalid Address',
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

// ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = $data['NameFirst'] . ' ' . $data['NameLast'];
        }

        $aux        = new Title();
        $aux        = $aux->upsert($data);
        $data['ID'] = $data['ID'] ?? $aux->ID ?? $aux->id;
        $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['Titles_ID' => $data['ID']]);

        if ($aux && $rv)
        {
            return response()->json([
                'data'   => $data,
                'status' => 'success',
            ], 200);
        }
    }

    /**
     * Validate and update/insert Loans record
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveLoan(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $validation = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'Company'         => 'required|max:100',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
            'Email'           => 'required|email',
        ];

        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $data    = $request->all();
        $address = $request->input('Address');
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);

            if ($addressList == false)
            {
                return response()->json([
                    'status'          => 'AddressError',
                    'PropertyAddress' => 'Invalid Address',
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

        // ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = $data['NameFirst'] . ' ' . $data['NameLast'];
        }

        $aux        = new Loan();
        $aux        = $aux->upsert($data);
        $data['ID'] = $data['ID'] ?? $aux->ID ?? $aux->id;

        $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['Loans_ID' => $data['ID']]);

        if ($aux && $rv)
        {
            return response()->json([
                'data'   => $data,
                'status' => 'success',
            ], 200);
        }
    }

    /**
     * Validate and update/insert Broker record
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveBroker(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $request->validate([
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'NameFull'        => 'string|max:100',
            'NameFirst'       => 'required|alpha_spaces|max:100',
            'NameLast'        => 'required|alpha_spaces|max:100',
            'Street1'         => 'required|string|max:100',
            'City'            => 'required|string|max:100',
            'State'           => 'required|string|max:100',
            'Zip'             => 'required|string|max:10',
            'PrimaryPhone'    => 'required',
            'Email'           => 'required|email',
        ]);

        $data       = $request->all();
        $aux        = new Broker();
        $aux        = $aux->upsert($data);
        $data['ID'] = $data['ID'] ?? $aux->id;

        //       $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['Loans_ID' => $data['ID']]);

        if ($aux)
        {
            return redirect()->back()->with('data_saved', 'Data Saved Successfully');
        }
    }

    /**
     * Save Buyer's Agent Info and update current Transactions Record
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveAgent(Request $request)
    {
        $data = $request->all();

        $validationList = [
            'Transactions_ID' => 'required|integer',
            'ID'              => 'integer',
            'NameLast'        => 'required|max:100',
            'NameFirst'       => 'required|max:100',
            'Email'           => 'required|email',
        ];

        $sameSideValidation = [
            'PrimaryPhone' => 'required',
        ];

        $address    = $request->input('Address');
        $isSameSide = TransactionController::isSameSide($data['Transactions_ID'], $data['_roleCode']);
        if ($isSameSide)
        {
            $validationList = array_merge($validationList, $sameSideValidation);

            if (!empty(trim($address)))
            {
                $addressVerify = new AddressVerification();
                $addressList   = $addressVerify->isValid($address);
                if ($addressList == false)
                {
                    return response()->json([
                        'status'          => 'AddressError',
                        'PropertyAddress' => 'Invalid Address',
                    ], 404);
                }
                $data['Street1'] = $addressList[0]['Street1'];
                $data['Street2'] = $addressList[0]['Street2'];
                $data['City']    = $addressList[0]['City'];
                $data['State']   = $addressList[0]['State'];
                $data['Zip']     = $addressList[0]['Zip'];
            }
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }
// ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = trim($data['NameFirst'] . ' ' . $data['NameLast']);
        }

        $validator = Validator::make(Input::all(), $validationList);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $agent      = new Agent();
        $agent      = $agent->upsert($data);
        $data['ID'] = $data['ID'] ?? $agent->ID ?? $agent->id;

        if ($data['_roleCode'] == 'ba')
        {
            $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['BuyersAgent_ID' => $data['ID']]);
        }
        else
        {
            $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['SellersAgent_ID' => $data['ID']]);
        }

        if ($agent && $rv)
        {
            return response([
                'status' => 'success',
            ], 200);
        }
        else
        {
            return response([
                'status' => 'Failure',
                'message' => 'Agent could not be added',
            ], 400);
        }
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function saveSeller(Request $request)
    {
        if (isset($request->Transactions_ID)) $clientRole = TransactionCombine::getClientRole($request->Transactions_ID);
        $validationList = [
            'ID'              => 'integer',
            'Transactions_ID' => 'required|integer',
            'NameFirst'       => 'required|max:100',
            'NameLast'        => 'required|max:100',
        ];

        $validator = Validator::make(Input::all(), $validationList);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        $data    = $request->all();
        $address = $request->input('Address');
        if (!empty(trim($address)))
        {
            $addressVerify = new AddressVerification();
            $addressList   = $addressVerify->isValid($address);
            if ($addressList == false)
            {
                return response()->json([
                    'status'  => 'AddressError',
                    'Address' => 'Invalid Address',
                    'errors'  => [
                        'Address' => [
                            'Invalid Address',
                        ],
                    ],
                ], 404);
            }
            $data['Street1'] = $addressList[0]['Street1'];
            $data['Street2'] = $addressList[0]['Street2'];
            $data['City']    = $addressList[0]['City'];
            $data['State']   = $addressList[0]['State'];
            $data['Zip']     = $addressList[0]['Zip'];
        }
        if (is_null($address))
        {
            $data['Street1'] =
            $data['Street2'] =
            $data['City'] =
            $data['State'] =
            $data['Zip'] = null;
        }

// ... Make sure a value is saved for NameFull
        if (!isset($data['NameFull']) || empty(trim($data['NameFull'])))
        {
            $data['NameFull'] = trim($data['NameFirst'] . ' ' . $data['NameLast']);
        }

        $seller        = new Seller();
        $data_inserted = $seller->upsert($data);
        if ($data_inserted)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status'  => 'fail',
                'message' => 'Unable to save the data.',
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveProperty(Request $request)
    {
        $maxYear     = 1 + date('Y');
        $singleInput = $request->input('singleInput') ?? false;
        $validation  = [
            'ID'           => 'integer',
            'YearBuilt'    => 'required|integer|min:1700|max:' . $maxYear,
            'hasHOA'       => 'required',
            'hasSeptic'    => 'required',
            'PropertyType' => 'required|alpha_spaces|max:100',
        ];

        $singleInputValidation = [
            'ID'              => 'integer',
            'search_query'    => 'required|string',
            'Transactions_ID' => 'required|integer',
        ];

        $validator = $singleInput ? Validator::make(Input::all(), $singleInputValidation) : Validator::make(Input::all(), $validation);
        if ($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors'  => $validator->getMessageBag()->toArray(),

            ], 400);
        }

        $data          = $request->all();
        $address       = $request->input('PropertyAddress') ?? $request->input('search_query');
        $addressVerify = new AddressVerification();

        $addressList = $addressVerify->isValid($address);

        $transactionID = $data['Transactions_ID'];
        $transaction   = Transaction::find($transactionID);
        if (!is_null($transaction))
        {
            $data['ID'] = $transaction->Properties_ID;
        }

        $hash = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $address));

        if ($addressList == false)
        {
            if (AddressOverride::where('Hash', $hash)->count() == 0)
            {
                if ($request->ajax())
                {
                    return response()->json([
                        'status'          => 'AddressError',
                        'PropertyAddress' => 'Invalid Address',
                    ], 404);
                }
                Session::flash('error', 'Invalid Property Address');
                Session::push('flash.old','error');
                return redirect()->back();
            }
        }

        if ($addressList == 1)
        {
            $data['Street1'] = $address ?? '';
            $data['Street2'] = '';
            $data['City']    = '';
            $data['Unit']    = '';
            $data['State']   = '';
            $data['Zip']     = '';
            $data['County']  = '';
            $property        = new Property();
            $property        = $property->upsert($data);
            $rv              = Transaction::where('ID', $request->input('Transactions_ID'))->update(['Properties_ID' => $property->ID ?? $property->id]);

            if ($property && $rv)
            {
                if ($request->ajax())
                {
                    if ($singleInput)
                    {
                        return response()->json([
                            'status' => 'success',
                        ]);
                    }
                    return response()->json([
                        'status'       => 'success',
                        'YearBuilt'    => $data['YearBuilt'],
                        'address'      => $address,
                        'PropertyType' => $data['PropertyType'],
                        'hasHOA'       => $data['hasHOA'] ? 'Yes' : 'No',
                        'hasSeptic'    => $data['hasSeptic'] ? 'Yes' : 'No',
                    ]);
                }
                return redirect()->back()->with('success', 'Property Address saved successfully');
            }
            else
            {
                if ($request->ajax())
                {
                    return response()->json([
                        'status' => 'error',
                    ]);
                }
                return redirect()->back()->with('fail', 'An error has occurred.');
            }
        }
        else
        {
            $data['Street1'] = $addressList[0]['Street1'] ?? '';
            $data['Street2'] = $addressList[0]['Street2'] ?? '';
            $data['City']    = $addressList[0]['City'] ?? '';
            $data['State']   = $addressList[0]['State'] ?? '';
            $data['Zip']     = $addressList[0]['Zip'] ?? '';
            $data['County']  = $addressList[0]['County'] ?? '';
            $property        = new Property();
            $property        = $property->upsert($data);

            $rv = Transaction::where('ID', $data['Transactions_ID'])->update(['Properties_ID' => $property->ID ?? $property->id]);

            if ($property && $rv)
            {
                if ($request->ajax())
                {
                    if ($singleInput)
                    {
                        return response()->json([
                            'status' => 'success',
                        ]);
                    }
                    return response()->json([
                        'status'       => 'success',
                        'YearBuilt'    => $data['YearBuilt'],
                        'address'      => $address,
                        'PropertyType' => $data['PropertyType'],
                        'hasHOA'       => $data['hasHOA'] ? 'Yes' : 'No',
                        'hasSeptic'    => $data['hasSeptic'] ? 'Yes' : 'No',
                    ]);
                }
                return redirect()->back()->with('success', 'Property Address saved successfully');
            }
            else
            {
                if ($request->ajax())
                {
                    return response()->json([
                        'status' => 'error',
                    ]);
                }
                return redirect()->back()->with('fail', 'An error has occurred.');
            }
        }
    }

    public function activePropertyCheck(Request $request)
    {
        $address       = $request->input('PropertyAddress');
        $addressVerify = new AddressVerification();
        $addressList   = $addressVerify->isValid($address);

        $hash = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $address));

        if ($addressList == false)
        {
            if (AddressOverride::where('Hash', $hash)->count() == 0)
            {
                return response()->json([
                    'status'          => 'AddressError',
                    'PropertyAddress' => 'Invalid Address',
                ], 404);
            }
        }

        if ($addressList == 1)
        {

            $activeProperties = PropertyCombine::getActiveProperties(
                $address,
                '',
                '',
                '');

        }
        else
        {
            $activeProperties = PropertyCombine::getActiveProperties(
                $addressList[0]['Street1'],
                $addressList[0]['City'],
                $addressList[0]['Zip'],
                $addressList[0]['State']);
        }
        $userID     = CredentialController::current()->ID();
        $roles      = AccountCombine::getRolesByUserId($userID);
        $validRoles = [
            'a'  => 'Agent',
            'tc' => 'TransactionCoordinator',
            'e'  => 'Escrow',
            't'  => 'Title',
            'l'  => 'Loan',
        ];

        $actualRoles = [];
        foreach ($roles as $r) if (isset($validRoles[$r])) $actualRoles[] = $r;
        $allTransactions = [];
        foreach ($actualRoles as $role)
        {
            $roleID = AccountCombine::getRoleID($role, $userID);
            foreach ($roleID as $id)
            {
                $transactionList = TransactionCombine::transactionList($role, $id->ID, $userID, true);
                $allTransactions = array_merge($allTransactions, $transactionList);
            }
        }

        $allTransactionIDs   = array_column($allTransactions, 'Status', 'ID');
        $activePropertyCount = 0;
        foreach ($activeProperties as $property)
        {
            if (isset($allTransactionIDs[$property->ID])) $activePropertyCount++;
        }

        if ($activePropertyCount == 1)
        {
            return response()->json([
                'proceed' => false,
                'message' => 'A transaction with that address is already open, would you like to proceed?',
            ]);
        }
        else
        {
            if ($activePropertyCount > 1)
            {
                return response()->json([
                    'proceed' => false,
                    'message' => 'Various transactions with that address are already open, would you like to proceed?',
                ]);
            }
            else
            {
                return response()->json([
                    'proceed' => true,
                ]);
            }
        }
    }

    public function cancelTransaction($transactionID)
    {
        $query = DB::table('Transactions')
                   ->where('ID', $transactionID)
                   ->update([
                       'Status' => 'canceled',
                   ]);

        if ($query)
        {
            return response()->json([
                'status' => 'success',
            ]);
        }
        else
        {
            return response()->json([
                'status' => 'fail',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveDetails(Request $request)
    {
        $data = $request->all();
        /*
         * In order to circumvent the native date pickers implemented by chrome/firefox, etc.
         * We are going to use juqery ui's datepicker, this will make the date standard across browsers but
         * we have to pass in dates as text, then convert them.
         */
        $data['DateAcceptance']    = date('Y-m-d', strtotime($data['DateAcceptance']));
        $data['DateOfferPrepared'] = date('Y-m-d', strtotime($data['DateOfferPrepared']));
        $data['PurchasePrice'] = str_replace(',','',$data['PurchasePrice']);
        $data['PurchasePrice'] = (int)$data['PurchasePrice'];

        $validationList = [
            'ID'                       => 'integer',
            'DateAcceptance'           => 'required|date',
            'DateOfferPrepared'        => 'required|date',
            'EscrowLength'             => 'integer|min:7|max:180',
            'PurchasePrice'            => 'integer',
            'LoanType'                 => 'required',
            'SaleType'                 => 'required',
            'willRentBack'             => 'required',
            'hasBuyerSellContingency'  => 'required',
            'hasSellerBuyContingency'  => 'required',
            'hasInspectionContingency' => 'required',
            'hasAppraisalContingency'  => 'required',
            'hasLoanContingency'       => 'required',
            'needsTermiteInspection'   => 'required',
            'willTenantsRemain'        => 'required',
        ];

        $validator = Validator::make($data,$validationList);
        if ($validator->fails())
        {
            return response()->json([
                'status' => 'fail',
                'errors' => $validator->getMessageBag()->toArray(),
            ], 400);
        }

        // ... Take the data from the form and make it fit the database
        if ($request->RentBack == 0)
        {
            $data['willRentBack']   = false;
            $data['RentBackLength'] = 0;
        }
        else
        {
            $data['willRentBack']   = true;
            $data['RentBackLength'] = $request->RentBack;
        }

        //... if the client role is an agent, set isClientAgent = true
        if (substr($request->ClientRole, -1) == 'a')
        {
            $data['isClientAgent'] = true;
        }
        else $data['isClientAgent'] = false;

        $transaction = new Transaction();
        $transaction->upsert($data);

        if (!isset($data['ID']))
        {
            $data['ID'] = $transaction->id;
        }
//Log::debug(['&&', 'recalc dates'=>$data['recalculateDates'] ??  'not set', 'hasInspCon'=>$data['hasInspectionContingency'], __METHOD__=>__LINE__]);
        if (isset($data['recalculateDates']))
        {
            $recalculate = $data['recalculateDates'] === 'true' ? true : false;
            if ($recalculate)
            {
                $rv = MilestoneCombine::saveTransactionTimeline($data['ID'], $data['DateAcceptance'], $data['EscrowLength']);
                TaskCombine::resetTasks($data['ID']);
            }
        }

        return;
    }

    /**
     * Save uploaded file
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveFile(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $request->validate([
            'documentID' => 'required',
            'signedBy'   => 'required',
            'document'   => 'required',
        ]);

        if (!$request->hasFile('document'))
        {
            return response()->json(['upload_file_not_found'], 400);
        }

        $file          = $request->file('document');
        $transactionID = $request->transactionID;
        $documentID    = $request->documentID;
        $signedBy      = implode('_', $request->signedBy);

        if (!$file->isValid())
        {
            return response()->json(['invalid_file_upload'], 400);
        }

        if ($transactionID)
        {
            $newFileName = $transactionID . '-' .
                           $documentID . '-' .
                           $signedBy . '.' .
                           pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);

            if (!is_dir(public_path('_uploaded_documents'))) mkdir(public_path('_uploaded_documents'));
            if (!is_dir(public_path('_uploaded_documents/transactions'))) mkdir(public_path('_uploaded_documents/transactions'));

            $dirStore = public_path('_uploaded_documents/transactions/' . $transactionID);
            if (!is_dir($dirStore)) mkdir($dirStore);
        }
        else
        {
            return response()->json(['invalid_target_transaction'], 400);
        }

        $file->move($dirStore, $newFileName);

        session()->flash('alert-success', 'File ' . $file->getClientOriginalName() . ' Uploaded');
        return redirect()->back()->with([
            'transactionID' => $transactionID,
            'documentID'    => $documentID,
            'document'      => $request->document,
            'signedBy'      => $signedBy,
        ]);
    }


    /**
     * @param int $transactionID
     * @param int $buyerID
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inputSellerAgent($transactionID = 0)
    {
        if (!\Auth::check())
        {
            return redirect()->guest('/login');
        } // ... Make sure the user is logged in
        $recEmpty = (new Agent())->getEmptyRecord();
        $view     = 'user.inputAgent';
        if ($transactionID)
        {
            session()->put('transaction_id', $transactionID);
        }
        else
        {
            return redirect()->route('dash.ta.list', ['role' => 'tc', 'id' => CredentialController::current()->ID()]);
        }

        $clientRole  = TransactionCombine::getClientRole($transactionID);
        $sameSide    = strtolower(substr(__FUNCTION__, 5, 1)) == substr($clientRole, 0, 1);
        $transaction = Transaction::find($transactionID);
        $noAgent     = ($transaction->SellersAgent_ID === 0) ? true : false;

        $data = TransactionCombine::getAgents($transactionID, Agent::TYPE_SELLER);

        $data = TransactionCombine::collectionToArray($data);
        if (empty($data[0])) $data[0] = $recEmpty;

        $model      = new Agent();
        $screenMode = $model->isRecordEmpty($data) ? 'create' : 'edit';

        $data = array_merge($data[0], ['Transactions_ID' => $transactionID]);
        //...Response for v2 Modals
        if (env('UI_VERSION') == '2a')
        {
            return response()->json([
                'data'  => $data,
                '_role' => ['code'    => Config::get('constants.USER_ROLE.SELLERS_AGENT'),
                            'display' => 'Seller\'s Agent',],
            ]);
        }

        return view($view, [
            '_role'       => ['code'    => Config::get('constants.USER_ROLE.SELLERS_AGENT'),
                              'display' => 'Seller\'s Agent',],
            '_screenMode' => $screenMode,
            'data'        => $data,
            'noAgent'     => $noAgent,
            'sameSide'    => $sameSide,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request)
    {
        if (!\Auth::check())
        {
            return response('Access Denied', 404);
        }
        $request->validate([
            'transactionID' => 'required|integer',
        ]);
        $taID        = request('transactionID');
        $status      = (request('status') == "true") ? 'open' : 'closed';
        $transaction = Transaction::where('ID', $taID)->update(['Status' => $status]);
        if ($transaction)
        {
            return response()->json([
                'currentStatus' => $status,
                'status'        => 'success',
            ]);
        }
        else return response('Failure', 404);

    }

    public function override(Request $request)
    {
        $address = $request->input('address');
        $hash    = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $address));

        $record                   = AddressOverride::where('Hash', $hash)->get()->first();
        $addressOverride          = new AddressOverride();
        $data['ID']               = $record->ID ?? null;
        $data['Hash']             = $hash;
        $data['Address']          = $address;
        $data['OverrideUsers_ID'] = CredentialController::current()->ID() ?? null;
        $data['OverrideUserRole'] = session('userRole') ?? null;
        $data['DateOverridden']   = date('Y-m-d');
        $addressOverride          = $addressOverride->upsert($data);
    }

    public function getNonUserTC($transactionID = 0)
    {
        if (!$transactionID) $transactionID = session()->get('transaction_id');
        if (!$transactionID) collect();

        $userTC = AccountCombine::getTransactionCoordinators(CredentialController::current()->ID());
        $tcID   = $userTC->first()->ID;
        $tcs    = TransactionCombine::getTransactionCoordinators($tcID);
        foreach ($tcs as $tc)
        {
            if ($tc->ID != $tcID) return $tc;
        }
        return collect();
    }

    public function getUserTC($transactionID = 0)
    {
        if (!$transactionID) $transactionID = session()->get('transaction_id');
        if (!$transactionID) collect();

        $userTC = AccountCombine::getTransactionCoordinators(CredentialController::current()->ID());
        $tcID   = $userTC->first()->ID;
        $tcs    = TransactionCombine::getTransactionCoordinators($tcID);
        foreach ($tcs as $tc)
        {
            if ($tc->ID == $tcID) return $tc;
        }
        return collect();
    }

    public function searchTransactionProperty(Request $request)
    {
        $searchQuery = request('query');
        $hereAppId   = env('HERE_APP_ID');
        $hereAppCode = env('HERE_APP_CODE');
        $url         = 'http://autocomplete.geocoder.api.here.com/6.2/suggest.json?app_id=' . $hereAppId . '&app_code=' . $hereAppCode . '&query=' . $searchQuery . '&country=USA';
        $curl        = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $sugession = curl_exec($curl);
        return response($sugession["suggestions"]);
    }

    /**
     * @param $transactionID
     * @param $source : is the source of the request, for example 'ba' would mean from buyer's agent.
     *                Source should be a string like 'ba' or 'btc'
     *
     * So for example if this request was coming from buyer's agent modal or page, this function is to
     * determine whether the user is on the same side, ergo the buyer's side.
     */
    public static function isSameSide($transactionID, $source, $ajax = false)
    {
        /**
         * Get all of the roles of this user for this particular transaction.
         */
        $userRoles = TransactionCombine::getUserTransactionRoles($transactionID);

        $source   = $source[0]; //extract first letter
        $sameSide = false;
        foreach ($userRoles as $u)
        {
            if ($source == $u[0]) $sameSide = true;
        }

        if ($ajax) return response()->json(['answer' => $sameSide]);
        return $sameSide;
    }

    public static function isSameSideSystemRoles($transactionID, $source, $ajax = false)
    {
        /**
         * Get all of the system roles of this user for this particular transaction.
         */
        $systemRoles = TransactionCombine::getUserTransactionSystemRoles($transactionID, CredentialController::current()->ID());

        $source   = $source[0]; //extract first letter
        $sameSide = false;

        foreach ($systemRoles as $key => $u)
        {
            if ($source == $key[0]) $sameSide = true;
        }

        if ($ajax) return response()->json(['answer' => $sameSide]);
        return $sameSide;
    }

    public function getOwnedTransactions($ajax = false, $userID = null)
    {
        if (AccessController::hasAccess('a') && $userID == null) //user is at least an admin for all transactions
        {
            if ($ajax) return response()->json(['ownedTransactions' => Transaction::select('ID')->get()->toArray()]);
            return Transaction::all();
        }
        $ownedTransactions = TransactionCombine::getOwnedTransactions($userID);
        if ($ajax) return response()->json(['ownedTransactions' => $ownedTransactions]);
        return $ownedTransactions;
    }

    public function updateMilestoneLength(Request $request)
    {
        $newDate                = $request->input('newDate');
        $transactionID          = $request->input('transactionID');
        $milestone              = $request->input('milestone');
        $transaction            = Transaction::find($transactionID);
        if(!is_null($transaction))
        {
            $dateAcceptance                     = $transaction->DateAcceptance;
Log::debug(['newDate'=>$newDate, 'dateAcceptance'=>$dateAcceptance]);
            $dateDifference                     = _Time::dateDiff($newDate,$dateAcceptance);
Log::debug(['dateDifference'=>$dateDifference]);
            $transactionArray                   = $transaction->toArray();
            switch ($milestone)
            {
                case 'Close Escrow':
                    $addInclusion   = 1;
                    $transactionArray['EscrowLength'] = (int)$dateDifference['days_total'] + $addInclusion;
                    break;
                case 'Appraisal Completed':       //  Deprecated
                case 'Appraisal Contingency Completed':
                    $transactionArray['hasAppraisalContingency'] = (int)$dateDifference['days_total'];
                    break;
                case 'Loan Contingency':
                    $transactionArray['hasLoanContingency'] = (int)$dateDifference['days_total'];
                    break;
                case 'Inspection Contingency':
                    $transactionArray['hasInspectionContingency'] = (int)$dateDifference['days_total'];
                    break;
            }
            $rv = $transaction->upsert($transactionArray);
            if($rv)
            {
                return response()->json([
                    'status' => 'success',
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 'fail',
                ]);
            }
        }
    }

    public function uploadPropertyImage(Request $request)
    {
        $rules = [
            'image'         => 'required | mimes:jpeg,jpg,png',
            'transactionID' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails())
        {
            return response()->json([
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ], 400);
        }

        if (!$request->hasFile('image'))
        {
            return response()->json([
                'upload_file_not_found'
            ], 400);
        }

        $file = $request->file('image');

        if (!$file->isValid())
        {
            return response()->json([
                'invalid_file_upload'
            ], 400);
        }
        $transactionID = (int)$request->input('transactionID');
        $newDir = 'public/images/'.config('constants.DIRECTORIES.propertyImages');
        $moveDir = str_replace(['public/public', 'public\\public'], 'public', public_path($newDir));
        $moveDir .= $transactionID.'/';
        $existingFiles = glob($moveDir.'propertyImage.*');
        foreach ($existingFiles as $eaFile)
        {
            if(is_file($eaFile))
            {
                unlink($eaFile);
            }
        }
        $ext = $request->file('image')->guessExtension();
        $newFile = 'propertyImage.' . $ext;
        $file->move($moveDir, $newFile);
        $url = _Locations::url('images', '/propertyImages/'.$transactionID.'/'.$newFile);
        return response()->json([
            'status' => 'success',
            'url'    => $url,
        ], 200);
    }
}
