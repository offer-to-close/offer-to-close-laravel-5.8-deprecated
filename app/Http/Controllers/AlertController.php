<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Alert;
use Illuminate\Support\Facades\Log;
class AlertController extends Controller
{
    public static $modelPath = 'App\\Models\\';
    public static function createAlert($model, $modelID, $priority = 5)
    {
        try {
            Alert::insert([
                'Type'      => 'WebPush',
                'Model'     => $model,
                'Model_ID'  => $modelID,
                'Priority'  => $priority
            ]);
        } catch (\Exception $e) {
            Log::info(['Exception Message' => $e->getMessage(), __METHOD__ => __LINE__]);
        }
    }

    /*
     * If transaction ID is not set to 0, this function will soft delete all alerts associated with
     * that model/transaction. If it is set to 0, this function will only delete the model IDs specified.
     *
     */
    public static function deleteAlerts($model, $modelIDs = [], $transactionID = 0, $deleteAdHoc = FALSE)
    {
        $fullModelName = self::$modelPath.$model;
        if($transactionID)
        {
            $tableName = with(new $fullModelName)->getTable();
            if($deleteAdHoc)
            {
                $models = $fullModelName::where('Transactions_ID', $transactionID)
                    ->select($tableName.'.ID')
                    ->leftJoin('Tasks', 'Tasks.Code', '=', 'Tasks_Code')
                    ->where($tableName.'.deleted_at', NULL)
                    ->get();
            }
            else
            {
                $models = $fullModelName::where('Transactions_ID', $transactionID)
                    ->select($tableName.'.ID')
                    ->leftJoin('Tasks', 'Tasks.Code', '=', 'Tasks_Code')
                    ->where('Tasks.Category', '!=' , 'ah')
                    ->where($tableName.'.deleted_at', NULL)
                    ->get();
            }
            $modelIDs = array_column($models->toArray(), 'ID');
        }
        $IDsLength = count($modelIDs);
        for ($i = 0; $i < $IDsLength; $i++)
        {
            $find = $fullModelName::find($modelIDs[$i]);
            if(!is_null($find))
            {
                $alert = $fullModelName::find($modelIDs[$i]);
                $deleted = $alert->alerts()->update([
                    'deleted_at' => date('Y-m-d'),
                ]);
            }
        }
    }

    public function markAlertsAsRead(Request $request)
    {
        $alerts = $request->input('alerts');
        foreach ($alerts as $alertID)
        {
            Alert::where('ID', $alertID)->update([
                'DateRead' => date('Y-m-d H:i:s'),
            ]);
        }
        return response()->json([
            'status' => 'success',
        ]);
    }

    public function clearAllAlerts(Request $request)
    {
        $alerts = $request->input('alerts');
        foreach ($alerts as $alertID)
        {
            Alert::where('ID', $alertID)->update([
                'isActive' => 0,
            ]);
        }
        return response()->json([
            'status' => 'success',
        ]);
    }
}
