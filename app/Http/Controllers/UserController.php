<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine;
use App\Combine\DocumentCombine;
use App\Combine\MailCombine;
use App\Combine\OfferCombine;
use App\Combine\RoleCombine;
use App\Combine\TransactionCombine;
use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_LaravelTools;
use App\Models\Document;
use App\Models\lk_Transactions_Documents;
use App\Models\lk_Transactions_Tasks;
use App\Models\lk_Transactions_Timeline;
use App\Models\lk_Users_Roles;
use App\Models\Model_Parent;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    public static $modelPath = 'App\\Models\\';

    public function userNew()
    {
        return view('user.index');
    }

    public function inputProfile($userID=null)
    {
        //    if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $user   = new User();
        $view   = 'userProfile';

        try
        {
            $data = $user->findOrFail($userID);
        }
        catch (\Exception $e)
        {
            $data = $user->getEmptyCollection();
        }

        return view(_LaravelTools::addVersionToViewName($view), [
            'data' => $data,
        ]);
    }


    public function uploadImage(Request $request)
    {
        return $this->saveImage($request); //todo clean this up.
    }

    public function saveImage (Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $rules = [
            'image' => 'required | mimes:jpeg,jpg,png',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) return redirect('/userProfile')->withErrors($validator)->withInput();

        if (!$request->hasFile('image')) return response()->json(['upload_file_not_found'], 400);

        $file = $request->file('image');

        if (!$file->isValid()) return response()->json(['invalid_file_upload'], 400);

        $newDir = config('constants.DIRECTORIES.avatars');
        $moveDir = str_replace(['public/public', 'public\\public'], 'public', public_path($newDir));

        $user = User::find(Auth::user()->id);
        $ext = $request->file('image')->guessExtension();
        $newFile = Auth::user()->id. '.' . $ext;
        $user->image = $newDir . $newFile;
        session::put('avatarPath', $newDir . $newFile);

        $user->save();
        Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], true);
        $file->move($moveDir, $newFile);

        session()->flash('alert-success', 'Image Uploaded');
        return redirect()->back();
    }

    public function saveProfile(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        if ($request->isMethod('get')) {
            return view('2a.userProfile');
        }
        if ($request->isMethod('post')){
            $rules = [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,'.Auth::user()->id,
            ];

            if(strlen($request->input('password'))>0){
                $rules [ 'password']= 'required|min:6';
                $rules ['password_confirmation']= 'same:password';
            }

            $validator = Validator::make($request->all(), $rules);

            if (!$validator->fails()) {

                if (!Hash::check($request['old_password'], Auth::user()->password) && strlen($request->input('password'))>0) {
                    return back()->withErrors(['old_password' => 'Old Password Does Not Match']);
                }

                $user = User::find(Auth::user()->id);
                $user->name = $request->input('name');
                $user->email = $request->input('email');
                if (Hash::check($request['old_password'], Auth::user()->password) && strlen($request->input('password'))>0){
                    $user->password = bcrypt($request->input('password'));
                }
                $user->save();
                Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')], true);

                return redirect('/userProfile');
            }
            if ($validator->fails()) return redirect('/userProfile')->withErrors($validator)->withInput();
        }
    }

    public function getAlertsByUserId($userID = false, $role = false, $daysLookAhead = 2,$seekPast = true, $cron = false)
    {
        if(!$cron) //if the code is not being run through cron, then authenticate
        {
            if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        }

        $lookAheadTime = strtotime('+'.$daysLookAhead.' days');
        $numericalToday = strtotime('today');

        /**
         * Seek Past if you want to get previous potentially very old tasks.
         */
        if($seekPast) $numericalToday = -1;

        $alerts = null;
        if (!$userID) $userID = CredentialController::current()->ID();
        if (!$role) $role = session('userRole');
        if (empty($role)) $role = 'tc';
        if ($role == 'tc') $role = ['btc', 'stc'];
        if ($role == 'a') $role = ['ba', 'sa'];
        $transactions = AccountCombine::getTransactions( $userID, 'open');
        $transactions = array_unique(array_column($transactions->toArray(), 'ID'));
        $properties = $ignore = [];
        if($transactions == NULL)
        {
            $alerts = [];
            return $alerts;
        }
        $roles = [];
        foreach ($transactions as $idx=>$transactionID)
        {
            if (isset($properties[$transactionID])) continue;

            $properties[$transactionID] = TransactionCombine::getPropertySummary($transactionID);

            $roles = array_column(TransactionCombine::getRoleByUserID($transactionID, $userID), 'role');
            $inArray = FALSE;
            foreach ($role as $eaRole)
            {
                if(in_array($eaRole,$roles)) $inArray = TRUE;
            }
            if (!$inArray) unset($transactions[$idx]);
        }
        $lkMilestones   = new lk_Transactions_Timeline();
        $lkTasks        = new lk_Transactions_Tasks();
        $lkDocuments    = new lk_Transactions_Documents();

        $milestones = $tasks = $documents = $docNames = [];

        foreach  ($transactions as $idx=>$transactionID)
        {
            if (in_array($idx, $ignore)) continue;
            $roles = array_column(TransactionCombine::getRoleByUserID($transactionID, $userID), 'role');
            /** -----------------------------------------------
             * Get specific milestones.
             */
            $tmp = [];
            foreach ($lkMilestones::timelineByTransactionId($transactionID) as $row)
            {
                if (strtotime($row->MilestoneDate) < $lookAheadTime &&
                    $row->isComplete != 1 && strtotime($row->MilestoneDate) >= $numericalToday) $tmp[] = $row->ID;
            }
            if (count($tmp) > 0) $milestones[$transactionID] = $tmp;
            /**
             * End get specific milestones.
             *
             * ------------------------------------------------/

            /** -----------------------------------------------
             * Get specific tasks
             */
            $tmp = [];
            foreach ($lkTasks::tasksByTransactionId($transactionID, $roles) as $row)
            {
                if (strtotime($row->DateDue) < $lookAheadTime &&
                    $row->DateCompleted == NULL && strtotime($row->DateDue) >= $numericalToday ) $tmp[] = $row->ID;
            }
            if (count($tmp) > 0) $tasks[$transactionID] = $tmp;
            /**
             * End get specific tasks.
             * -------------------------------------------------/


            /**
             * The way alerts should work for documents has not been defined yet.
            $tmp = [];
            foreach ($lkDocuments::documentsByTransactionId($taid) as $row)
            {
                if (strtotime($row->DateDue) < $lookAheadTime && $row->isComplete != 1 && strtotime($row->DateDue) >= $numericalToday)
                {
                    if (!isset($docNames[$row->Documents_Code]))
                    {
                        $dn = Document::where('Code', $row->Documents_Code)->value('Description');
                    }
                    else $dn = $docNames[$row->Documents_Code];
                    $tmp[] = array_merge($row->toArray(), ['DocumentName'=>$dn]);
                }
            }
            if (count($tmp) > 0) $documents[$taid] = $tmp;
            **/
        }
        $alerts = [];
        foreach ($milestones as $transactionID=>$milestoneArray)
        {
            foreach($milestoneArray as $milestone)
            {
                $thisMilestone = $lkMilestones::find($milestone);
                if (!is_null($thisMilestone))
                {
                    if(!$thisMilestone->isComplete && is_null($thisMilestone->deleted_at))
                    {
                        $thisMilestoneAlerts = $lkMilestones::find($milestone)->alerts
                            ->where('Model', class_basename($lkMilestones))
                            ->where('isActive', 1)
                            ->where('deleted_at', NULL);
                        foreach ($thisMilestoneAlerts as $alert)
                        {
                            $alerts[] = [
                                'alertID'       => $alert->ID,
                                'transactionID' => $transactionID,
                                'type'          => 'timeline',
                                'address'       => $properties[$transactionID]['Address'],
                                'description'   => $thisMilestone->MilestoneName, //MilestoneName
                                'dueDate'       => $thisMilestone->MilestoneDate,
                                'dateRead'      => $alert->DateRead,
                            ];
                        }
                    }
                }
            }
        }

        foreach ($tasks as $transactionID=>$taskArray)
        {
            foreach ($taskArray as $task)
            {
                $thisTask = $lkTasks::find($task);
                if (!is_null($thisTask))
                {
                    if(is_null($thisTask->DateCompleted) && is_null($thisTask->deleted_at))
                    {
                        $thisTaskAlerts = $lkTasks::find($task)->alerts
                            ->where('Model', class_basename($lkTasks))
                            ->where('isActive', 1)
                            ->where('deleted_at', NULL);
                        foreach ($thisTaskAlerts as $alert)
                        {
                            $alerts[] = [
                                'alertID'           => $alert->ID,
                                'transactionID'     => $transactionID,
                                'type'              => 'task',
                                'address'           => $properties[$transactionID]['Address'],
                                'description'       => $thisTask->Description,
                                'dueDate'           => $thisTask->DateDue,
                                'dateRead'          => $alert->DateRead,
                            ];
                        }
                    }
                }
            }
        }

        /* The way alerts works for documents has not been defined yet.
        foreach ($documents as $taid=>$dat)
        {
            foreach ($dat as $data)
            {
                $alerts[] = [
                    'transactionID' => $taid,
                    'type'        => 'document',
                    'address'     => $properties[$taid]['Address'],
                    'description' => $data['DocumentName'],
                    'dueDate'     => $data['DateDue']
                ];
            }
        }
        */

        _Arrays::sortByTwoColumns($alerts, 'dueDate', SORT_DESC, 'type', SORT_ASC);
        return $alerts;
    }

    public function loginRoles($userID)
    {
        $cRoles = config('constants.USER_ROLE');
        $roles = lk_Users_Roles::rolesByUserId($userID);
        $rv = [];
        foreach ($roles as $role)
        {
            $rv[] = ['Role'=>title_case(str_replace('_', ' ', array_search($role->Role, $cRoles))),
                     'Active?'=>($role->isActive) ? 'Yes' : 'No'];
        }
        _Arrays::sortByColumn($rv, 'Role');
        return $rv;
    }
    public function transactionRoles($userID)
    {
        $cRoles = config('constants.USER_ROLE');
        $roles = RoleCombine::transactionsRoles($userID);
        $rv = [];

        foreach ($roles as $role=>$count)
        {
            $t = ['Role'=>title_case(str_replace('_', ' ', array_search($role, $cRoles))),
                'Count'=>$count];
            if (empty($t)) continue;
            $rv[] = $t;
        }
      //  ddd([$roles, $rv]);
        _Arrays::sortByColumn($rv, 'Role');
        return $rv;
    }
    public function getIndexValueArray($indexColumn='id',
                                       $valueColumn='name',
                                       $sortBy = Model_Parent::SORT_BY_VALUE,
                                       $sortOrder = SORT_ASC,
                                       $removeDuplicates = true,
                                       $excludeBlanks = false)
    {
        $sortByList = [Model_Parent::SORT_BY_INDEX, Model_Parent::SORT_BY_VALUE, ];
        if (!in_array($sortBy, $sortByList)) return false;

        $query = User::select($indexColumn . ' as index', $valueColumn . ' as value');
        if ($removeDuplicates) $query = $query->distinct();
        if ($sortOrder == SORT_ASC)  $direction = 'asc';
        elseif ($sortOrder == SORT_DESC) $direction = 'desc';

        if ($sortBy == Model_Parent::SORT_BY_VALUE) $query = $query->orderBy($valueColumn, $direction);
        elseif ($sortBy == Model_Parent::SORT_BY_INDEX) $query = $query->orderBy($indexColumn, $direction);

        if ($excludeBlanks) $query = $query->whereRaw('length(`'. trim($valueColumn) . '`)  > 0');
        $result = $query->get();

        $rv = [];
        foreach ($result as $idx=>$row)
        {
            $rv[$row->index] = $row->value;
        }
        if ($removeDuplicates) $rv = array_unique($rv);

        return $rv;
    }

    public function transactions($userID)
    {
        return TransactionCombine::transactionsByUser($userID);
    }

    public function runMailTest()
    {
        MailCombine::sendReminderEmails();
    }
    public function ajaxAlertsRead(Request $request)
    {
        $isAlertsRead = $request->input('alertsRead');
        $request->session()->put('alertsRead', $isAlertsRead);
    }

}

