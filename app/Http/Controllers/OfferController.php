<?php

namespace App\Http\Controllers;

use App\Combine\OfferCombine;
use App\Library\otc\_Helpers;
use App\Library\otc\AddressVerification;
use App\Library\Utilities\_LaravelTools;
use App\Models\Offer;
//use App\Models\Offers;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Combine\AccountCombine;
use App\Combine\MilestoneCombine;
use Illuminate\Support\Facades\Validator;



class OfferController extends Controller
{
    /**
     * Instatiate a new controller instance
     *
     */

    public $modelPath = 'App\\Models\\';

    public function __construct()
    {
        // methods protected by auth middleware
        //$this->middleware('auth')->only('inputBuyer', 'inputSeller');
    }

    public function index(Request $request){

        return view('2a.offers.standard');
    }

    public function decideIfCreateTransaction()
    {
        $input = Input::get('convertToTBtn');
        if(isset($input)) return true;
        return false;
    }

    public function inputOffer($offerID = 0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        _Helpers::clearTransactionSession();
        $offer   = new Offer();
        $view     = '2a.offers.standard';
        $offers = [];
        $counterOffers = [];
        try
        {
            $data = Offer::findOrFail($offerID);
            $data['PriceListing'] = number_format($data['PriceListing']);
            $data['PriceAsking'] = number_format($data['PriceAsking']);
            $data['Fee'] = number_format($data['Fee']);

            $screenMode = 'edit';
            if(file_exists(public_path("_uploaded_documents/offers/$offerID/offer"))){
                $offers = collect(scandir(public_path("_uploaded_documents/offers/$offerID/offer")))->splice(2)->all();
            }
            if(file_exists(public_path("_uploaded_documents/offers/$offerID/counter_offer"))){
                $counterOffers = collect(scandir(public_path("_uploaded_documents/offers/$offerID/counter_offer")))->splice(2)->all();
            }
        }
        catch (\Exception $e)
        {
            $data = $offer->getEmptyCollection();

            $screenMode =  'create';
        }
        return view(_LaravelTools::addVersionToViewName($view), [
            '_screenMode' => $screenMode,
            'data'        => $data,
            'uploads'     => ['offers'=>$offers,
                              'counterOffers'=>$counterOffers]
        ]);
    }

    public function getOfferByTCID($tcID = '*')
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        _Helpers::clearTransactionSession();
        if (!is_numeric($tcID) ) $tcID = AccountCombine::getTransactionCoordinators(CredentialController::current()->ID())->first()->ID;

        $offer = new Offer();
        $view = '2a.offers.all';

        if( $tcID == 0 )
        {
            $data = Offer::where(function ($query) {
                $query->where('Status', '!=', 'closed')
                      ->where('Status', '!=', 'converted');})
                      ->orWhereNull('Status')
                      ->orderBy('DateOffer', 'desc')->get();
        }
        else if( is_numeric($tcID) )
        {
            $q = Offer::where('TransactionCoordinators_ID', $tcID)
                            ->where(function ($qry) {
                                $qry->where(function ($query) {
                                   $query->where('Status', '!=', 'closed')
                                       ->where('Status', '!=', 'converted');})
                                       ->orWhereNull('Status');})
                           ->orderBy('DateOffer', 'desc');
            $data = $q->get();
        }
        else{
            return $offer->getEmptyCollection();
        }

        return view(_LaravelTools::addVersionToViewName($view), [
                    'data' => $data,
        ]);
    }


    public function saveOffer(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $validation = [
            'DateOffer'    => 'required|date',
            'AcceptedDate' => 'required|date',
            'EscrowLength' => 'integer|min:7|max:180',
            //'PriceListing' => 'numeric',
            //'PriceAsking'  => 'numeric',
            'NameFirst'    => 'required',
            'NameLast'     => 'required',
            'Street1'      => 'required',
            'City'         => 'required',
            'State'        => 'required',
            'Zip'          => 'numeric|required',
            'Status'       => 'max:10'
        ];

        $validator = Validator::make(Input::all(), $validation);
        if($validator->fails())
        {
            return redirect()->back()->with([
                'errors' => $validator->getMessageBag(),
            ])->withInput();
        }

        $data = $request->all();
        $data['PriceListing'] = str_replace(',', '', $data['PriceListing']);
        $data['PriceAsking'] = str_replace(',', '', $data['PriceAsking']);
        $data['Fee'] = str_replace(',', '', $data['Fee'] ?? 0);

        if (!isset($data['TransactionCoordinators_ID']) || empty(trim($data['TransactionCoordinators_ID'])))
        {
            $data['TransactionCoordinators_ID'] = AccountCombine::getTransactionCoordinators(CredentialController::current()->ID())->first()->ID;
        }


        // ... If ID is null remove to force insert in upsert()
        $offerID = $data['ID'];
        if (is_null($data['ID']) || $data['ID'] == 0) unset($data['ID']);

        $offer = new Offer();
        $offer->upsert($data);

        if($offer && $offer->id) $offerID = $offer->id;

        $transaction = $request->input('makeTransaction');
        $transaction = $transaction == 'true' ? 1 : 0;
        if($transaction)
        {
            $agentID = OfferCombine::addAgentOrGetId($data['NameFirst'],$data['NameLast']);
            $property_ID = OfferCombine::addPropertyGetID($data['Street1'],$data['City'],$data['State'],$data['Zip']);
            if($property_ID == false)
            {
                ddd(['property id'=>$property_ID]);
                return redirect()->back()->withInput($data)->with('failure','The address you entered is not valid.');
            }
            $transaction_ID = OfferCombine::createTransaction(
                $data['PriceListing'],
                $agentID,
                $property_ID,
                $data['TransactionCoordinators_ID'],
                $offerID,
                $data['DateOffer'],
                $data['AcceptedDate'], //accepted date is transaction's DateStart
                $data['EscrowLength']
            );
           // echo "<pre>";print_r($transaction_ID);die("Hello Here");

            //insert the foreign key to the transaction into offers Transactions_ID column
            $data['Transactions_ID'] = $transaction_ID; //for record keeping
            $data['Status'] = 'converted'; //convert the offer (removes from all offers)
            $offer->upsert($data); //update the offer with the data

            //create and save a milestone based on this transaction: escrow is 3rd parameter defaulted to 45.
            //milestone variable below is not needed in this scope.
            $milestone = MilestoneCombine::saveTransactionTimeline($transaction_ID, $data['AcceptedDate'],$data['EscrowLength']);

            //redirect to the transaction page now that you have an ID
            return redirect()->route('transaction.home', ['taid' => $transaction_ID]);

        }
        return redirect()->route('input.Offer', ['offerID' => $offerID])->with('data_saved', 'Offer Has Been Saved');
    }

    public static function getOfferDocuments($offerID)
    {
        $dirOffers = public_path('_uploaded_documents/offers/' . $offerID . '/');
        $urlOffers = asset('_uploaded_documents/offers/' . $offerID . '/');
        $rv = [];

        if (!is_dir($dirOffers)) return $rv;

        foreach (['/offer' => 'offers', '/counter_offer' => 'counterOffers'] as $subDir => $var) {
            $files = [];
            $dir = $dirOffers . $subDir . '/';
            $url = $urlOffers . $subDir . '/';

            if (is_dir($dir)) {
                $files = glob($dir . '*');
                foreach ($files as $file) {
                    $base = pathinfo($file, PATHINFO_BASENAME);
                    $rv[$var][$base] = $url . $base;
                }
            }
        }
        return $rv;
    }


    public function uploadOffer(Request $request){
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $data = $request->all();
        $offerID = $data['offerID'];
        $time = $data['time'];
        if ($offerID == NULL)
        {
            return response()->json([
                'saved' => 'failed',
                'offerExists' => false,
            ]);
        }

        if($request->file("file")){
            if (!file_exists(public_path("_uploaded_documents/offers/".$offerID."/offer"))) {
                mkdir(public_path("_uploaded_documents/offers/".$offerID."/offer"), 0777, true);
            }
            $extension = $request->file("file")->getClientOriginalExtension();
            $filename = pathinfo($request->file("file")->getClientOriginalName(), PATHINFO_FILENAME);
            $new_file = public_path("_uploaded_documents/offers/".$offerID."/offer/".$filename."+".$time.".".$extension);
            File::move($request->file("file"), $new_file);
            chmod($new_file, 0644);
            echo json_encode($offerID);
        }
    }

    public function uploadCounterOffer(Request $request){
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $data = $request->all();
        $offerID = $data['offerID'];
        $time = $data['time'];
        if ($offerID == NULL)
        {
            return response()->json([
                'saved' => 'failed',
                'offerExists' => false,
            ]);
        }

        if($request->file("file")){
            if (!file_exists(public_path("_uploaded_documents/offers/".$offerID."/counter_offer"))) {
                mkdir(public_path("_uploaded_documents/offers/".$offerID."/counter_offer"), 0777, true);
            }
            $extension = $request->file("file")->getClientOriginalExtension();
            $filename = pathinfo($request->file("file")->getClientOriginalName(), PATHINFO_FILENAME);
            $new_file = public_path("_uploaded_documents/offers/".$offerID."/counter_offer/".$filename."+".$time.".".$extension);
            File::move($request->file("file"), $new_file);
            chmod($new_file, 0644);
            echo json_encode($offerID);
        }

    }

    public function uploadDocument(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
         $request->validate([
            'ID'           => 'required|integer',
            'offerFile'    => 'required'
        ]);
        $data = $request->all();

// ... If ID is null remove to force insert in upsert()
        $offerID = request('ID');
        $offer   = Offer::where('id', $offerID)->first();

        if( $offer ){
            if( request('UploadType') == 1 ){
                if (!file_exists("_uploaded_documents/offers/".$offerID."/offer")) {
                    mkdir("_uploaded_documents/offers/".$offerID."/offer", 0777, true);
                }
                $extension = $request->file("offerFile")->getClientOriginalExtension();
                $filename  = pathinfo($request->file("offerFile")->getClientOriginalName(), PATHINFO_FILENAME);
                $new_file  = "_uploaded_documents/offers/".$offerID."/offer/".$filename."+".date('Ymd-his').".".$extension;
                File::move($request->file("offerFile"), $new_file);
                return response('Success', 200);
            }

            else if( request('UploadType') == 2 ){
                if (!file_exists("_uploaded_documents/offers/".$offerID."/counter_offer")) {
                    mkdir("_uploaded_documents/offers/".$offerID."/counter_offer", 0777, true);
                }
                $extension = $request->file("offerFile")->getClientOriginalExtension();
                $filename  = pathinfo($request->file("offerFile")->getClientOriginalName(), PATHINFO_FILENAME);
                $new_file  = "_uploaded_documents/offers/".$offerID."/counter_offer/".$filename."+".date('Ymd-his').".".$extension;
                File::move($request->file("offerFile"), $new_file);
                return response('Success', 200);
            }


        }
        return response('Failure', 400);
    }

    public function checkProperty(Request $request)
    {
        $data = $request->all();
        $addressArray = [
            'Street1'   => $data['street'],
            'Street2'   => NULL,
            'City'      => $data['city'],
            'State'     => 'CA',
            'Zip'       => $data['zip']
        ];
        $formattedAddress   = AddressVerification::formatAddress($addressArray);
        $addressVerify      = new AddressVerification();
        $addressList        = $addressVerify->isValid($formattedAddress);
        if ($addressList == false)
        {
 //           return response()->json(['error' => 'AddressError']);
        }
        else{
            return response()->json([
                'status' => 'success'
            ]);
        }
    }

}
