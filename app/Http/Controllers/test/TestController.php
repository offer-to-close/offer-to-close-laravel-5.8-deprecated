<?php

namespace App\Http\Controllers\test;

use App\Combine\AccountCombine;
use App\Combine\DocumentCombine;
use App\Combine\InvoiceCombine;
use App\Combine\MilestoneCombine;
use App\Combine\TaskCombine;
use App\Combine\TransactionCombine;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\MilestoneController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use App\Library\otc\AddressVerification;
use App\Library\otc\IncludeWhen;
use App\Library\otc\_Locations;
use App\Library\otc\Pdf_ConvertApi;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Time;
use App\Library\Utilities\SeedBuilder;
use App\Mail\HelloWorld;
use App\Mail\Template_Parent;
use App\Models\AccountAccess;
use App\Models\Agent;
use App\Models\log_Mail;
use App\Models\MailTemplates;
use App\models\ZipCityCountyState;
use ConvertApi\ConvertApi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Spipu\Html2Pdf;
use App\Library\otc\_PdfMaker;
use Imagick;

class TestController extends Controller
{

    public function f001()
    {
        $taCon       = new TransactionController();
        $taRecord    = $taCon->getTransaction(1);
        $expressions = ['[property.YearBuilt] .LT. 1974',
                        '[Transaction.isBuyerATrust] .eq. !true',
                        '[Transaction.SaleType] .eq. shortSale'];
        $iw          = new IncludeWhen($taRecord, true);

        foreach ($expressions as $idx => $exp)
        {
            $rv[] = ['expression' => $exp, 'value' => $iw->evaluate($exp)];
        }

        return '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function f002()
    {
        $dList = DocumentCombine::createDocumentList(1);
        $rv    = DocumentCombine::fillTransactionDocumentTable(2);
        return '<pre>' . var_export(['document list = ' => $dList], true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function f003($transactionID)
    {
        $ts      = 1526342400;
        $fd[$ts] = _Time::formatDate($ts);
        return '<pre>' . var_export(['dates' => $fd], true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function view($view)
    {
        $view = 'PreLogIN.marc.zev';
        return _LaravelTools::addVersionToViewName($view);
    }

    public function q_01()
    {
        $a  = 'b';
        $$a = 'c';
        $b  = 'a';
        return __FUNCTION__ . ': ' . $b;
    }

    public function q_02()
    {
        $a = '$a = 37';
        $b = "$a = 37";
        $c = $a == $b;
        return __FUNCTION__ . ': ' . $c;
    }

    public function q_03()
    {
        $a = true;
        $b = true;
        $c = $a ? 'A is TRUE' : $b ? 'B is TRUE' : 'There is no truth';
        return __FUNCTION__ . ': ' . $c;
    }

    public function q_03a()
    {
        $a = true;
        $b = true;
        $c = $a ? 'A is TRUE' : ($b ? 'B is TRUE' : 'There is no truth');
        return __FUNCTION__ . ': ' . $c;
    }

    public function q_04()
    {

        $a = '
for ($i=0; $i<$g; ++$i)
{
    if (isset($ay[$i]))
    {
        if (in_array($i, $ay))
        {
            unset($ay[$i]);
        }
    }
}
        ';
        $b = '
for ($i=0; $i<$g; ++$i) {
    if (isset($ay[$i])) {
        if (in_array($i, $ay)) {
            unset($ay[$i]);
        }
    }
}
        ';
        $c = 'In your opinion, which format is the best to use: A or B?';
        return $c . PHP_EOL . 'A' . PHP_EOL . $a . PHP_EOL .
               PHP_EOL . ' or ' . PHP_EOL . PHP_EOL .
               'B' . PHP_EOL . $b . PHP_EOL;
    }

    public function q_05($a)
    {
        for ($i = 0; $i < count($a) - 1; $i++)
        {
            for ($j = $i + 1; $j < count($a); $j++)
            {
                if ($a[$i] < $a[$j])
                {
                    $t     = $a[$i];
                    $a[$i] = $a[$j];
                    $a[$j] = $t;
                }
            }
        }
        return $a;
    }

    public function getShuffledDeck()
    {
        $suits = ['H', 'C', 'S', 'D'];
        $ranks = array_merge(range(2, 10, 1), ['J', 'Q', 'K', 'A']);
        $deck  = [];

        foreach ($suits as $suit)
        {
            foreach ($ranks as $rank)
            {
                $deck[] = $rank . '-' . $suit;
            }
        }
        shuffle($deck);
        return $deck;
    }

    public function devTest()
    {
        $results[] = $this->q_01();
        $results[] = $this->q_02();
        $results[] = $this->q_03();
        $results[] = $this->q_03a();
        $results[] = $this->q_04();
        $results[] = '[9]  ' . $this->q_05(7);
        $results[] = '[10] ' . is_array($rv = $this->q_05([1, 6, 33, 30, 0])) ? implode(', ', $rv) : $rv;
        $results[] = '[11] ' . is_array($rv = $this->q_05(['b', 1, 6, 33, '30', '0b', 'a', '0', 0])) ? implode(', ', $rv) : $rv;

        $results[] = implode(', ', $this->getShuffledDeck());
        $results[] = '[Chaudhry] ' . implode(', ', $this->getShuffledDeck_Chaudhry());
        ddd($results);
    }

    public function getShuffledDeck_Chaudhry()
    {
        $suits = ['Clubs', 'Diamonds', 'Hearts', 'Spades'];
        $cards = ['Ace', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'Jack', 'Queen', 'King'];
        $deck  = range(0, 51);
        $i     = count($deck) - 1;
        $cnt   = 0;
        while ($i)
        {
            $j = mt_rand(0, $i);
            if ($i != $j)
            {
                $tmp      = $deck[$j];
                $deck[$j] = $deck[$i];
                $deck[$i] = $tmp;
            }
            if (++$cnt > $i) break; //ddd(['cnt'=>$cnt, 'i'=>$i, 'j'=>$j, 'deck'=>$deck,]);
        }
        $shuffled = [];
        for ($index = 0; $index < count($deck); $index++)
        {
            $shuffled[] = $cards[$deck[$index] / 4] . ' of ' . $suits[$deck[$index] % 4];
        }
        return $shuffled;
    }

    public function seeder($table = 'TransactionControllers', $className = null, $classDir = null)
    {
        //        $className = $className . '_' . $table . '_' . date('Ymd');
        $seeder = new SeedBuilder();
        $rv     = $seeder->buildClass($table, $className, $classDir);

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');

        //        return __METHOD__ . '<pre>' .  "\n</pre>" . 'the end - ' . date('H:i:s');

    }

    public function timeline()
    {
        $dateStart    = '2018-04-01';
        $escrowLength = 45;
        $rv           = MilestoneCombine::calculateMilestones_new($dateStart, $escrowLength);

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');

        //        return __METHOD__ . '<pre>' .  "\n</pre>" . 'the end - ' . date('H:i:s');

    }

    public function routes()
    {
        $rv = _LaravelTools::routesAsArray();

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function tasks($tid = 7, $clientRole = 'b')
    {
        $rv = TaskCombine::createNewTaskList($tid, $clientRole);

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }
    public function getRoles($userID=1)
    {
        $rv = AccountCombine::getRoles($userID);

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function county($zip)
    {
        $rv = ZipCityCountyState::getCountyByZip($zip);

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function transaction($taID)
    {
//        $rv  = TransactionCombine::fullTransaction($taID);
        $rv  = TransactionCombine::getUserIDs($taID);
//        $rv2 = TransactionCombine::isCreated($rv);
//        foreach ($rv as $idx => $data)
//        {
//            $rv[$idx] = $data->toArray();
//        }
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function smartyStreets()
    {
        $address       = '19525 ventura blvdaa tarzana ca 91356';
        $smartyStreets = new AddressVerification('smartyStreets');
        $usps          = new AddressVerification('usps');

        $rv = $smartyStreets->isValid($address);
        //        $rv = $usps->isValid($address);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function holidays()
    {
        $dates = ['2018-09-3', '2018-08-31', '2019-12-25', '2020-6-28'];
        foreach ($dates as $date)
        {
            $rv[$date] = _Time::isHoliday($date);
        }

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function invoice()
    {
        $rv = InvoiceCombine::getInvoice(1001);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function tcType($taID = 1)
    {
        $controller = new TransactionController();
        $rv         = $controller->getNonUserTC($taID);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function propertySummary($taID = 1)
    {
        $controller = new TransactionCombine();
        $mail = new MailTemplates;

        $rv         = $controller->getDataForMail ($taID, $mail, ['ba', 'tc']);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
               date('H:i:s');
    }

    public function email($receiver = 'mzev@offertoclose.com')
    {
        $rv       = 'undefined';
        $view = _LaravelTools::addVersionToViewName('emails.helloWorld');
        $sysTemplate = new Template_Parent([$receiver], 'emails.helloWorld');

        try
        {
            //$rv = Mail::to($receiver)->bcc([env('MAIL_FROM_ADDRESS')])->send(new HelloWorld($receiver));
            $rv = $sysTemplate->markdown($view);
            return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
                   date('H:i:s');
        }
        catch (\Exception $e)
        {
            ddd(['EXCEPTION' => $e, 'view'=>$view, 'rv' => $rv, __METHOD__ => __LINE__]);
        }
    }

    public function editEmail($logMailID=10)
    {

        $email = log_Mail::find($logMailID)->value('Message');

        try
        {
 //           $rv = $sysTemplate->markdown($view);
  //          return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' .
  //                 date('H:i:s');
        }
        catch (\Exception $e)
        {
     //       ddd(['EXCEPTION' => $e, 'view'=>$view, 'rv' => $rv, __METHOD__ => __LINE__]);
        }
    }

    public function factory()
    {
        $rv         = _LaravelTools::modelFactory('AccountAccess');
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }
    public function alert()
    {
        $userCtlr = new UserController();
        $rv       = $userCtlr->getAlertsByUserId(1, 'aaaaa');
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }
    public function invite()
    {
        $ctlr = new InvitationController();
        $rv[0] = $ctlr->makeInviteCode(666, 'ba', 777, 0);
        $rv[1] = $ctlr::translateInviteCode($rv[0]);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }
    public function dir()
    {
        $env = env('APP_ENV');
        $dir[$env]['dir']['app_path'] = app_path('**');
        $dir[$env]['dir']['base_path'] = base_path('**');
        $dir[$env]['dir']['config_path'] = config_path('**');
        $dir[$env]['dir']['database_path'] = database_path('**');
//        $dir['mix'] = mix('**');
        $dir[$env]['dir']['public_path'] = public_path('**');
        $dir[$env]['dir']['resource_path'] = resource_path('**');
        $dir[$env]['dir']['storage_path'] = storage_path('**');

        $dir[$env]['url']['action'] = action('AdminController@getInviteCode');
        $dir[$env]['url']['asset'] = asset('**');
        $dir[$env]['url']['secure_asset'] = secure_asset('**');
        $dir[$env]['url']['route'] = route('editEmail');
        $dir[$env]['url']['secure_url'] = secure_url('**');
        $dir[$env]['url']['url'] = url('**');

        $dir[$env]['custom']['public'] = _Locations::url('public');
        $dir[$env]['custom']['images'] = _Locations::url('images', '**');
        $dir[$env]['custom']['imports'] = _Locations::url('imports', '**');
        $dir[$env]['custom']['press'] = _Locations::url('press', '**');

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($dir, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }

    public function pdf_new()
    {
 //       dump('php.ini = ' . php_ini_loaded_file());
        $documentBagID = 13;
        $pdf = new PdfController();
        $req = request()->merge(['documentBagID'=>$documentBagID]);
        $rv = $pdf->splitUI($req);
    }
        public function pdf()
    {
        $input   = storage_path('/app/pdf/' . 'test-0.pdf');
        $output  = storage_path('/app/pdf/' . 'test-0-split-'.date('His') .'.pdf');

        try
        {
            $pdf = new Pdf_ConvertApi($input);
            $pages = $pdf->getAllPages();

            dump( $pdf->getAllPages());
            return true;
        }
        catch (\Exception $e)
        {
            ddd(['&&', 'Exception'=>$e->getMessage(), $e->getFile().'::'.$e->getLine(), __METHOD__=>__LINE__], '**');
        }
        return;
    }

        public function pdf_()
    {
        $source =  'restpack'; // 'rocket';

        //restpack >>>>>>>>>>>>>>d
        if ($source == 'restpack')
        {
            $token = 'j1xMZSk2HlUCGx7354H6jFYup3zrLzN8w1aZorKknebpIAdx';
//            $htmlpdf = new HTMLToPDF("j1xMZSk2HlUCGx7354H6jFYup3zrLzN8w1aZorKknebpIAdx");
            $input   = storage_path('/app/pdf/' . 'avid-.2019-04-09-10-35-41.html');
            $output  = storage_path('/app/pdf/restpack_' . pathinfo($input, PATHINFO_FILENAME) . '.pdf');
            $doc     = file_get_contents($input);

// These are the request paramteres. You can check the full list of possible parameters at
// https://restpack.io/html2pdf/docs#route
            $postdata = http_build_query(
                [
                    'access_token' => $token,
                    'delay'        => "1", //wait 1 second before convert
                    'html' => $doc,
                    'pdf_page' => 'Letter',
                ]
            );
Log::debug($doc);
// Build a POST request
            $opts = ['http' =>
                         [
                             'method'  => 'POST',
                             'header'  => 'Content-type: application/x-www-form-urlencoded',
                             'content' => $postdata,
                         ],
            ];

            $context = stream_context_create($opts);

            try
            {
                $result = file_get_contents("https://restpack.io/api/html2pdf/v5/convert", false, $context);
            }
            catch (\Exception $e)
            {
                ddd(['&&', 'Exception'=>$e->getMessage()], '**');
            }

            file_put_contents($output, $result);

// Save to file
            file_put_contents($output, $result);

            return __METHOD__ . PHP_EOL . $source . '<pre>' . var_export($output, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
        }
        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

//        ... HtmltoPdfRocket >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        if ($source == 'rocket')
        {
            // --- Using Library Class
            //        $input = storage_path('/app/html/CA+Earthquake-Hazards-Report.html');
            $input  = storage_path('/app/pdf/' . 'avid-.2019-04-09-10-35-41.html');
            $output = storage_path('/app/pdf/rocket_' . pathinfo($input, PATHINFO_FILENAME) . '.pdf');
            $doc    = file_get_contents($input);

            // Set parameters
            $apikey = '62a4a162-5d92-42f3-a261-e00eeb0aef01';
            $value  = $doc;

            $postdata = http_build_query(
                [
                    'apikey'       => $apikey,
                    'value'        => $value,
                    'MarginBottom' => '0',
                    'MarginTop'    => '',
                    'PageSize'     => 'Letter',
                ]
            );

            $opts = ['http' =>
                         [
                             'method'  => 'POST',
                             'header'  => 'Content-type: application/x-www-form-urlencoded',
                             'content' => $postdata
                         ]
            ];

            $context = stream_context_create($opts);

            // Convert the HTML string to a PDF using those parameters
            $result = file_get_contents('http://api.html2pdfrocket.com/pdf', false, $context);

            // Save to root folder in website
            file_put_contents($output, $result);

            return __METHOD__ . PHP_EOL . $source . '<pre>' . var_export($output, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
        }
        //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<



        $pdf = new  _PdfMaker($doc);
        $pdf->setDisplayMode(['layout'=>'P',]);
        $file = $pdf->write(storage_path('/app/pdf/'. pathinfo($input, PATHINFO_FILENAME).'.pdf'));

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($file, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');


        // --- Using raw calls::
        // after that, you can use the class like so
        $html2pdf = new Html2Pdf\Html2Pdf('P','Letter','en',false,'UTF-8', [12,12,12,12]);
        $doc = "<page><h1>Hello World</h1></page>";
        $doc = '<page>'.view('2a.emails.fromBTC.welcome_sa').'</page>';

        $html2pdf->pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

        $html2pdf->pdf->SetAuthor('Offer To Close');
        $html2pdf->pdf->SetTitle('OTC - Test');
        $html2pdf->pdf->SetSubject('Fun with pdf');
        $html2pdf->pdf->SetKeywords('Tag, OTC, Documents');

        $html2pdf->setDefaultFont('Arial');
        $html2pdf->writeHTML($doc,false);
        try
        {
            $rv = $html2pdf->output(base_path() . '/storage/app/helloworld.pdf', 'F');
        }
        catch (\Exception $e)
        {ddd('broken');}

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv ?? 'Done', true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');

    }
    public function queryTest ($lk_taTask_ID=938)
    {
//        $lk_taTask_ID = 937;
        $rv = TaskCombine::previewTaskDueDatesChanges($lk_taTask_ID);

//        TaskCombine::adjustTaskDueDatesToMilestone($lk_taTask_ID);

//        DocumentCombine::adjustDocumentDueDatesToMilestone($lk_taTask_ID);
        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }

    public function delete ()
    {
        $rv = '**';
        $rv = AccountAccess::where('ID', 2)->delete();


        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }

    public function snap()
    {
        $repCntlr = new ReportController;
        $rv = $repCntlr->snapNHD(1, 'btc', 'standard');

        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }
    public function testApi()
    {

//        $rv = $this->testApiCurl();
        $rv = $this->testApi2Curl();
        $rv = is_bool($rv)  ? 'TRUE' : $rv ;
        return $rv;
        //        return __METHOD__ . PHP_EOL . '<pre>' . var_export($rv, true) . "\n\n</pre>" . 'the end - ' . date('H:i:s');
    }
    public function testApiCurl($type=null)
    {
        if (true) $type = 'byName';

        switch ($type)
        {
            case 'byName':
                $accessID = 'xchop-temporary-access';
                $data = ['NameLast' => 'Zev',];

                $searchQuery  = $accessID . '/' . json_encode($data);

                $url = 'http://test.OfferToClose.com/api/agent-name/';
                $suffix = '/CA/15/onlyTest';

                $url .=  $accessID . '/' . $searchQuery . $suffix;
                $rv = file($url);
                break;
            case null:
                $data = ['NameFirst' => 'George', 'NameLast' => 'Washington', 'License' => 'OTC-000' . date('is')];

                $data = [
                    'License'      => 'TEST-0',
                    'NameFull'     => urlencode('First0 3 Last0'),
                    'NameFirst'    => 'First0',
                    'NameLast'     => 'Last0',
                    'State'        => 'CA',
                    'PrimaryPhone' => '111-222-3330',
                    'Email'        => 'TEST-0@mailinator.com',
                    'isTest'       => 1
                ];

                $accessID = 'xchop-temporary-access';
                $payload  = $accessID . '/' . json_encode($data);

                $url = 'http://test.OfferToClose.com/api/agent-insert';

                $headers = ["Content-Type: application/json"];

                $fh = fopen(storage_path('___curl___.log'), 'w');

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_URL, $url . '/' . $payload);
                curl_setopt($curl, CURLOPT_VERBOSE, 1);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_STDERR, $fh);
                curl_setopt($curl, CURLOPT_USERPWD, 'API:&&otcAccess%');
                $rv = curl_exec($curl);
                return $rv;
                break;

        }
    }
    public function testApi2Curl($type=null)   //$accessCode='*', $memberRequestID=0, $answer='*')
    {
        dump (__METHOD__);

        if (true) $type = 'approveRequest';

        switch ($type)
        {
            case 'approveRequest':
                $accessCode = config('security.secureAdminHandshake');
                $memberRequestID = 68;

                $searchQuery  = $memberRequestID . '/' ;

                $url = 'http://test.OfferToClose.com/api/otcAdmin/';
                $suffix = '/approve';
                $url .=  $accessCode . '/member-request/' . $searchQuery . $suffix;

                $headers = ["Content-Type: application/json"];

                $fh = fopen(storage_path('___curl___.log'), 'w');
dump($url);
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_URL, $url );
                curl_setopt($curl, CURLOPT_VERBOSE, 1);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_STDERR, $fh);
                curl_setopt($curl, CURLOPT_USERPWD, 'API:&&otcAccess%');
                $rv = curl_exec($curl);
                return $rv;
dump($rv);
//                $rv = file($url);
                break;
            case 'rejectRequest':
                $accessCode = config('security.secureAdminHandshake');
                $memberRequestID = 68;

                $searchQuery  = $memberRequestID . '/' ;

                $url = 'http://test.OfferToClose.com/api/otcAdmin/';
                $suffix = '/reject';
                $url .=  $accessCode . '/member-request/' . $searchQuery . $suffix;
                $rv = file($url);
                break;
        }
    }

}