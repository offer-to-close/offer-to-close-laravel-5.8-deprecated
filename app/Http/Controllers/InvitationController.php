<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine;
use App\Combine\RoleCombine;
use App\Combine\TransactionCombine;
use App\Library\otc\AddressVerification;
use App\Library\Utilities\_LaravelTools;
use App\Models\LoginInvitation;
use App\Models\lu_UserRoles;
use App\Models\MemberRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use function Psy\debug;

class InvitationController extends Controller
{

    /**
     * @deprecated
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function chooseInvitees(Request $request)   // ToDo: Deprecated - remove
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $data = $request->all();
        if (empty($data['fromRole'])) $data['fromRole'] = session('userRole');
        if (empty($data['fromRole'])) $data['fromRole'] = 'btc';
        $users = TransactionCombine::getUsers($data['transactionID']);
        $transactionID = $data['transactionID'];

        $clientRole  = session('clientRole');

        if (empty($clientRole)) $clientRole = TransactionCombine::getClientRole($transactionID);
        $client = TransactionCombine::getClient($transactionID);

        $address = AddressVerification::formatAddress(TransactionCombine::getProperty($transactionID)->first(), 1);

        $view = 'invitations.chooseInvitee';
        if (!isset($view)) ddd(['view not set', 'data'=>$data, __METHOD__=>__LINE__]);

        return view(_LaravelTools::addVersionToViewName($view), [
            'senderRole'    => $data['fromRole'],
            'toRole'        => $data['toRole'],
            'transactionID' => $transactionID,
            'users'         => $users,
            'property'      => $address,
        ]);
    }
    public function confirmInvitation(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $view = 'invitations.confirmInvitation';
        $data = $request->all();

        $subCode = json_decode($data['submitCode'], true);
        $a = rawurldecode($subCode['inviteCode']);
        $atrim = substr($a, 0, strpos($a, '}')+1);
        $inviteCode = json_decode($atrim, true);
        $a = TransactionCombine::getPerson($inviteCode['role'], $inviteCode['transactionID']);

        $person = $a['data']->first();
        $roles = config('constants.USER_ROLE');
        $dspRole = title_case(str_replace('_', ' ', array_search($inviteCode['role'], $roles)));
        return view(_LaravelTools::addVersionToViewName($view),
            [
                'address' => $subCode['address'],
                'invitee' => implode(' ', [$person->NameFirst, $person->NameLast]),
                'role'    => $dspRole,
                'inviteCode' =>$subCode['inviteCode'],
            ]);
    }

    public function recordInvitation($inviteCode, $transactionID = 0)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in

        $inviteCodeArray = $this->translateInviteCode($inviteCode);
        if ($transactionID > 0)
        {
            $invitee = TransactionCombine::getPerson($inviteCodeArray['inviteeRole'], $transactionID);
            $invitee = $invitee['data']->first();
            $inviteeUserID = empty($invitee->Users_ID) ? 0 : $invitee->Users_ID;
        }

//dd(['inviteCode'=>$inviteCode, 'taid'=>$transactionID,'inviteCodeArray'=>$inviteCodeArray,'invitee'=>$invitee, 'inviteeUserID'=>$inviteeUserID]);

        $currentRole = TransactionCombine::getTransactionRole($transactionID);
        $inviter = TransactionCombine::getPerson($currentRole, $transactionID);
        $inviter = $inviter['data']->first();
        if (isset($inviteCodeArray['requestID'])) $memReq = MemberRequest::find($inviteCodeArray['requestID']);
        else $memReq = NULL;
        if(!is_null($memReq) && $transactionID === 0)
        {
            $nameFirstInvitee = $memReq->NameFirst  ?? '';
            $nameLastInvitee  = $memReq->NameLast   ?? '';
            $emailInvitee     = $memReq->Email      ?? '';
        }

        $model                          = [];
        $model['InviteCode']            = $inviteCode;
        $model['InvitedBy_ID']          = CredentialController::current()->ID();
        $model['InvitedByRole']         = session('userRole');
        $model['InviteeRole']           = $inviteCodeArray['inviteeRole'];
        $model['InviteeRole_ID']        = $inviteCodeArray['inviteeRoleID'];
        $model['NameFirstInvitee']      = $invitee->NameFirst ?? $nameFirstInvitee ?? '';
        $model['NameLastInvitee']       = $invitee->NameLast ?? $nameLastInvitee ?? '';
        $model['EmailInvitee']          = $invitee->Email ?? $emailInvitee ?? '';
        $model['EmailInviter']          = $inviter->Email ?? '';
        $model['Transactions_ID']       = $inviteCodeArray['transactionID'] ?? '';
        $model['DateInvite']            = date('Y-m-d H:i:s');
        $model['DateInvitationExpires'] = date('Y-m-d H:i:s', strtotime('+1 week'));
        $model['DateCreated']           = date('Y-m-d H:i:s');

        $loginInvitation = new LoginInvitation();
        try
        {
            $lI = $loginInvitation->upsert($model);
            $a = LoginInvitation::where('InviteCode', $inviteCode)->get();
 //Log::debug(['inviteCode'=>$inviteCode, 'upsert return '=>$lI->toArray(), 'LoginInvitations'=>$a->toArray()]);
        }
        catch (\Exception $e)
        {
            //currently empty to avoid failure but needs thoughtful error handling
            ddd(['Exception'=>$e->getMessage()]);
        }
    }
    public function confirmRequestedInvite(Request $request)
    {
        if (!\Auth::check()) return redirect()->guest('/login'); // ... Make sure the user is logged in
        $view = 'invitations.requestedInvitation';
        $data = $request->all();

        $memReqID = $data['requestID'];

        $memReq = MemberRequest::find('ID', $memReqID)->get()->first();

        $inviteCtrl = new InvitationController();
        $inviteCode = $inviteCtrl->makeInviteCodeFromRequest($memReqID);

        $roles = config('constants.USER_ROLE');
        $dspRole = title_case(str_replace('_', ' ', array_search($memReq->RequetedRole??'', $roles)));
        return view(_LaravelTools::addVersionToViewName($view),
            [
                'address' => $memReq->PropertyAddress,
                'invitee' => implode(' ', [$memReq->NameFirst, $memReq->NameLast]),
                'role'    => $dspRole,
                'inviteCode' =>$inviteCode,
                'action'  => null,
            ]);

    }
    public function makeInviteCode($transactionID, $role, $roleID, $userID=0, $shareRoomID=0, $userType='u')
    {
        $array = ['transactionID'=>$transactionID,
                  'inviteeRole'=>$role,
                  'inviteeRoleID'=>$roleID,
                  'inviteeUserID'=>$userID,
                  'shareRoomID'=>$shareRoomID,
                  'userType'=>$userType,
                  'checksum'=>date('is'),
        ];
        $json = str_rot13(json_encode($array));
        $invitation_string = rawurlencode($json);
        $this->recordInvitation($invitation_string,$transactionID);

        return $invitation_string;
    }
    public function makeInviteCodeFromRequest($memberRequestID)
    {
        if (!is_numeric($memberRequestID))
        {
            //ToDo
        }
        $roleID = $userID = 0;
        $userType = 'u'; //the default user type (user)
        $memReq = MemberRequest::where('ID', '=', $memberRequestID)->get()->first();
        if (empty($memReq)) ddd(['&& Error &&','memberRequestID'=>$memberRequestID, 'memReq'=>$memReq]);
        $transactionID = ($memReq->TransactionID > 0) ? $memReq->TransactionID : 0;
        $role = (strlen($memReq->RequestedRole) > 0) ? $memReq->RequestedRole : '';

        $array = [
            'transactionID'     => $transactionID,
            'requestID'         => $memberRequestID,
            'inviteeRole'       => $role,
            'inviteeRoleID'     => $roleID,
            'inviteeUserID'     => $userID,
            'shareRoomID'       => $transactionID,
            'userType'          => $userType,
            'checksum'          => date('is'),
        ];

        $json = str_rot13(json_encode($array));
        $invitation_string = rawurlencode($json);
        $this->recordInvitation($invitation_string,$transactionID);
//dd($invitation_string);
        return $invitation_string;
    }
    public static function translateInviteCode($inviteCode)
    {
        $a = rawurldecode($inviteCode);
        $atrim = substr($a, 0, strpos($a, '}')+1);
        $inviteCode = json_decode(str_rot13($atrim), true);
        unset($inviteCode['checksum']);
        return $inviteCode;
    }
    public function verifyInviteCode($inviteCode)
    {
        $codeArray = $this->translateInviteCode($inviteCode);
        $record = LoginInvitation::where('InviteCode', $inviteCode)->get();

        if ($record->count() < 1) return false;
        $record = $record->first();

        $codeArray['ID'] = $record->ID;

        $errors = [];
        if (!empty($record->DateRegistered)) $errors[] = 'Invitation Already Used. ['. date('Y-m-d', strtotime($record->DateRegistered)) .']';
        if ($codeArray['transactionid']??$codeArray['transactionID'] != $record->Transactions_ID) $errors[] = 'Transaction IDs don\'t match [' .
                                                                                      $codeArray['transactionid']??$codeArray['transactionID'] .
                                                                                   ' != ' . $record->Transations_ID .']';

        if ($codeArray['inviteeRole'] != $record->InviteeRole) $errors[] = 'Roles don\'t match [' . $codeArray['inviteeRole'] . ' != ' .
                                                                           $record->InviteeRole .']';
        if ($codeArray['inviteeRoleid']??$codeArray['inviteeRoleID'] != $record->InviteeRole_ID) $errors[] = 'Role IDs don\'t match [' .
                                                                              $codeArray['inviteeRoleid']??$codeArray['inviteeRoleID'] . ' != ' .
                                                                            $record->InviteeRole_ID .']';
        if (strtotime($record->DateInvitationExpires) < strtotime('now'))
        {
            $errors[] = 'Invitation has expired.'; // [' . strtotime($record->DateInvitationExpires) . ' !< ' . strtotime('now') .']';
        }

        if (count($errors) > 0)
        {
            $error_message = NULL;
            foreach ($errors as $e) $error_message .= $e.'. ';
            $codeArray['errors'] = $error_message;
        }
        return $codeArray;
    }

    public function saveMemberRequest(Request $request)
    {
        $request->validate([
            'TransactionsID'  => 'integer',
            'RequestedRole'   => 'required|string|max:5',
            'PropertyAddress' => 'string|max:200',
            'State'           => 'string|max:2',
            'NameFirst'       => 'required|alpha_spaces|max:100',
            'NameLast'        => 'required|alpha_spaces|max:100',
            'Email'           => 'required|email',
            'Company'         => 'string|max:100',
            'TransactionSide'    =>'string',
        ]);

        $data          = $request->all();

        $memberRequest = new MemberRequest();
        $request = $memberRequest->upsert($data);

        return redirect(route('home'));
    }
    public function sendInvitation(Request $request)
    {
        $request->validate([
            'inviteCode'  => 'required|string',
            'address'   => 'required|string',
        ]);

        $data          = $request->all();

        $memberRequest = new MemberRequest();
        $request = $memberRequest->upsert($data);

        return redirect(route('home'));
    }
    public function approveRequest(Request $request, $memReqID=0, $apiData=[])
    {
        $view = 'invitations.requestedInvitation';

        $memReqID = $apiData['memReqID'] ?? $memReqID;

        if (!is_null($memReqID) && is_numeric($memReqID) && $memReqID > 0)
        {

            $memReq = MemberRequest::find($memReqID);

            if (!empty($memReq) && count($memReq->toArray()) > 0)
            {
                if (!empty($memReq->TransactionID) && $memReq->TransactionID > 0)
                {
                    $transaction = TransactionCombine::fullTransaction($memReq->TransactionID);
                }
                else $transaction = false;

                $rv = [
                    'memberRequest'     => $memReq,
                    'memberRequestID'   => $memReq->ID,
                    'displayRole'       => lu_UserRoles::getDisplay($memReq->RequestedRole, true),
                    'transaction'       => $transaction,
                ];

                if (empty($apiData)) return view(_LaravelTools::addVersionToViewName($view),$rv);
                else
                {
                    $this->sendInvitationFromRequestAPI($memReq->ID);
                    return json_encode($rv);
                }
            }
        }
    }
    public function rejectRequestReason(Request $request, $memReqID=0, $apiCall=false)
    {
        $view = 'invitations.rejectMembershipRequest';

        if (!is_null($memReqID) && is_numeric($memReqID) && $memReqID > 0)
        {

            $memReq = MemberRequest::find($memReqID);

            if (!empty($memReq) && count($memReq->toArray()) > 0)
            {
                if (!empty($memReq->TransactionID) && $memReq->TransactionID > 0)
                {
                    $transaction = TransactionCombine::fullTransaction($memReq->TransactionID);
                }
                else $transaction = false;

                return view(_LaravelTools::addVersionToViewName($view),
                    [
                        'memberRequest'     => $memReq,
                        'memberRequestID'   => $memReq->ID,
                        'displayRole'       => lu_UserRoles::getDisplay($memReq->RequestedRole, true),
                        'transaction'       => $transaction,
                    ]);
            }
        }
    }

    public function rejectRequest(Request $request, $apiData=[])
    {
        Log::debug(['API data'=>$apiData,__METHOD__=>__LINE__]);

        $route = 'staff.menuProcess';
        $data = $request->all();

        $memberRequestID = $apiData['memberRequestID'] ?? $data['memberRequestID'];
        $decisionReason = $apiData['DecisionReason'] ?? $data['DecisionReason'];

        if (is_numeric($memberRequestID) && $memberRequestID > 0)
        {
            $memReq                  = MemberRequest::find($memberRequestID);
            $memReq->isApproved      = false;
            $memReq->isRejected      = true;
            $memReq->DecisionReason  = $decisionReason ?? '';
            $memReq->DeciderUsers_ID = (CredentialController::current()->ID()) ?? 0;
            $memReq->DateDecision    = date('Y-m-d H:i:s');
            $memReq->upsert($memReq->toArray());
            $mr = MemberRequest::find($memberRequestID);
        }

        $rv = ['record'=>$mr->toArray()];

        if (empty($apiData)) return redirect(route($route,$rv));
        else return json_encode($rv);
    }

    public function sendInvitationFromRequest(Request $request)
    {
        $request->validate([
            'memberRequestID'  => 'required|integer|min:1',
        ]);
        $data          = $request->all();

        if ($data['inviteButton'] != 'cancelInvitation')
        {
            if (!is_null($data['memberRequestID']) && is_numeric($data['memberRequestID']) && $data['memberRequestID'] > 0)
            {
                $memReq                  = MemberRequest::find($data['memberRequestID']);
                $memReq->isApproved      = true;
                $memReq->isRejected      = false;
                $memReq->DeciderUsers_ID = CredentialController::current()->ID();
                $memReq->DateDecision = date('Y-m-d H:i:s');
                $memReq->upsert($memReq->toArray());
                $mr = MemberRequest::find($data['memberRequestID']);

                $inviteCode = InvitationController::makeInviteCodeFromRequest($data['memberRequestID']);
                MailController::sendRequestedInvitation($data['memberRequestID'], $inviteCode, true, false);
            }
        }
        return redirect(route('staff.menuProcess', ['path'=>'reviewMembershipRequest']));
    }

    public function sendInvitationFromRequestAPI($memberRequestID)
    {
            if (!is_null($memberRequestID) && is_numeric($memberRequestID) && $memberRequestID > 0)
            {
                $memReq                  = MemberRequest::find($memberRequestID);
                $memReq->isApproved      = true;
                $memReq->isRejected      = false;
                $memReq->DeciderUsers_ID = CredentialController::current()->ID();
                $memReq->upsert($memReq->toArray());
                $mr = MemberRequest::find($memberRequestID);

                $inviteCode = InvitationController::makeInviteCodeFromRequest($memberRequestID);
                MailController::sendRequestedInvitation($memberRequestID, $inviteCode, true, isServerLocal());
            }
        return true;
    }

    public function markRequestSpam($ID)
    {
        if(!$ID) return response()->json([
            'status'    => 'fail',
            'message'   => 'Record does not exist, no ID. Error code:  mrk-s-001',
        ]);
        $deleted = MemberRequest::where('ID', $ID)->update([
            'deleted_at'        => date('Y-m-d h:i:s'),
            'isRejected'        => 1,
            'DecisionReason'    => 'spam',
            'DeciderUsers_ID'   => auth()->id(),
            'DateDecision'      => date('Y-m-d h:i:s'),
        ]);
        if($deleted)
        {
            return response()->json([
                'status'    => 'success',
                'message'   => 'Request marked as spam.',
            ]);
        }
        else
        {
            return response()->json([
                'status'    => 'fail',
                'message'   => 'Unable to mark as spam. Error code: mrk-s-002',
            ]);
        }
    }
}
