<?php

namespace App\Http\Controllers;

use App\Combine\AccountCombine;
use App\Combine\TransactionCombine;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_Log;
use App\Models\lk_UsersTypes;
use App\Models\lu_UserTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class AccessController extends Controller
{
    private static $accessUserTypes = [];

    /**
     * @param string $minimumType
     *
     * @return bool
     */
    public static function hasAccess($minimumType='u', $userID=0)
    {

        if (!$userID) $userID = CredentialController::current()->ID();

        if (empty($userType = session('userType')) && $userID > 0)
        {
            $userType = AccountCombine::getType($userID);
            if (empty($userType)) return false;
            session(['userType'=>$userType]);
        }
        else
        {
            if ($userID == 0) return false;
        }

        if (empty(self::$accessUserTypes)) self::loadAccessTypes();
        try
        {
            if (!isset(self::$accessUserTypes[$minimumType]) || !isset(self::$accessUserTypes[$userType])) return false;
            if (self::$accessUserTypes[$minimumType]['valueInt'] <= self::$accessUserTypes[$userType]['valueInt']) return true;
            return false;
        }
        catch (\Exception $e)
        {
            Log::error(['EXCEPTION'=>$e,
                        'minimumType'=>$minimumType,
                        'userType'=>$userType,
                        'accessUserTypes'=>self::$accessUserTypes,
                        __METHOD__=>__LINE__]);
            return false;
        }
    }
    public static function loadAccessTypes()
    {
        lu_UserTypes::loadValueTable();
        self::$accessUserTypes = lu_UserTypes::$valueTable;
    }

    public static function canAccessTransaction($transactionID)
    {
        if(AccessController::hasAccess('a')) return TRUE;
        $transactionList = session('transactionList') ?? FALSE;
        if(!$transactionList) return FALSE;
        if(isset($transactionList[$transactionID])) return TRUE;
        else
        {
            $role   = session('userRole');
            $userID = CredentialController::current()->ID();
            $roleID = AccountCombine::getRoleID($role,$userID);
            if (!$roleID->isEmpty())
            {
                $roleID = $roleID->first();
                $roleID = is_array($roleID) ? $roleID['ID'] : $roleID->ID;
            }
            $transactions = TransactionCombine::transactionList($role, $roleID, $userID);
            if($transactions['type'] == 'success')
            {
                $transactions = $transactions['success']['info']['collection'];
                $transactions = _Convert::toArray($transactions);
                TransactionCombine::instantiateSessionTransactionList($transactions);
                if (in_array($transactionID, array_column($transactions,'ID'))) return TRUE;
            }
        }
        return FALSE;
    }

    public function verify_reCaptcha(Request $request)
    {
        $token = $request->input('token');
        $secret = Config::get('constants.RECAPTCHA_SECRET_KEY');
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $headers = [
            'Content-type: application/x-www-form-urlencoded',
        ];

        $data = [
            'secret'    => $secret,
            'response'  => $token,
        ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $result = json_decode(curl_exec($curl));
        if($result->success)
        {
            return response()->json([
                'status' => 'success',
                'score' => $result->score,
            ]);
        }
        return response()->json([
            'status' => 'fail',
        ]);
    }
}
