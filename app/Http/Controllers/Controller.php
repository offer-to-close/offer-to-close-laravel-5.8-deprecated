<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Walk record array and determine if any value is not empty
     *
     * @return array \Illuminate\Http\Response
     */
    protected function isEmpty($record)
    {
        if (!is_array($record) || empty($record)) return true;
        foreach ($record as $part)
        {
            if (is_array($part))
            {
                foreach ($part as $field)
                {
                    if (!empty($field)) return false;
                }
            }
        }
        return true;
    }

}
