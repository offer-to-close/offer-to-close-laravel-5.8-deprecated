<?php

namespace App\Mail;

use App\Library\Utilities\_LaravelTools;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SystemTemplate extends Template_Parent
{
    use Queueable, SerializesModels;
    public function __construct($to, $body, $parameters = [])
    {
        $defaultValues = ['subject'=>'OTC {{$data[\'address\']}}'];

        $parameters = $this->processParameters($to, $parameters, $defaultValues);

        parent::__construct($to, $body, $parameters);
    }
    /*
    public function build()
    {
        return parent::build();
    }
    */
}