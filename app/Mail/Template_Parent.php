<?php

namespace App\Mail;

use App\Library\Utilities\_Arrays;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Time;
use App\Library\Utilities\Toumai;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class Template_Parent extends Mailable
{
    use Queueable, SerializesModels;
    public $data = [];
    public $body = null;
    public $emailAttributes = ['to', 'from', 'subject', 'cc', 'bcc', 'replyTo', 'attach'];


    /**
     * Template_Parent constructor.
     *
     * @param       $to
     * @param       $body
     * @param array $parameters
     */
    public function __construct($to, $body, $parameters = [])
    {
        $defaultValues = [
                          'from'    => env('MAIL_FROM_ADDRESS'),
                          'bcc'     => null,
                          'subject' => 'News from Offer To Close',
                          'cc'      => null,
                          'replyTo' => null,
                          'attach'  => null,
        ];
        $this->body = $body;;
        $this->data = $this->processParameters($to, $parameters, $defaultValues);
        $this->setAttributes($this->data);
        return;
    }

    /**
     * @param mixed $to
     * @param array $parameters
     * @param array $defaults
     *
     * @return array
     */
    public function processParameters($to, array $parameters, array $defaults) :array
    {
        $parameters['to'] = explode(',',$to);
        return _Arrays::assignDefaults($parameters, $defaults);
    }

    public function setAttributes($parameters)
    {
        $details = null;
        $vars = [];
        foreach($parameters as $par=>$val)
        {
            if(!is_array($val))
            {
                $y = _Time::validateDate($val);
                if(!$y) $y = _Time::validateDate($val,'Y-m-d');
                if($y)
                {
                    $val = date('F j Y', strtotime($val));
                    $this->data[$par] = $val;
                }

            }
            if (in_array($par, $this->emailAttributes))
            {
                if ($par == 'subject')
                {
// ... Replace the variables in the Subject vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv

                    $origSubject = $subject = str_replace(['{{', '}}'], ['{', '}'], $val);

                    while ($v = Toumai::_findSet($subject, '{', true, $details))
                    {
                        $vars[] = $v;
                        $subject = substr($subject, $details['setEnd']+1);
                    }

                    $subject = $origSubject;
                    foreach($vars as $v)
                    {
                        $var = str_replace(['$', 'data', "['", "']", '["', '"]'],NULL, trim(substr($v, 1, -1)));
                        $subject = str_replace($v, $this->data[$var] ?? '{{' . $var . '}}', $subject);
                    }
                    $val = $subject;
                    $this->data['subject'] = $val;
                    $this->{$par} = $this->data['subject'];
// ... ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                }
                else if (!empty($val))
                {
                    try
                    {
                        $this->{$par}($val);
                    }catch(\Exception $e)
                    {
                        //in the try we're assigning the value of the parameter,
                        //but if the object does not exist this keeps it from failing.
                    }
                }
                //else Log::info(['this->par(val)' => $this->{$par}]);

            }
            else $this->{$par} = $val;
        }
//        Log::info(['TEMPLATE'=>$this, __METHOD__=>__LINE__]);
        return;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = _LaravelTools::addVersionToViewName($this->body);

        try
        {
//            Log::info('inside build pre markdown '. __METHOD__.'=>'.__LINE__);
            $rv =  $this->markdown($view);
 //           Log::info('inside build after markdown '. __METHOD__.'=>'.__LINE__);
            return $rv;
        }
        catch (\Exception $e)
        {
            ddd(['EXCEPTION' => $e, 'view' => $view, 'data' => $this->data,'this' => $this, __METHOD__ => __LINE__]);
        }
    }
}
