<?php

namespace App\Library\Utilities;

use App\Library\otc\Credentials;
use App\Models\Agent;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

/*******************************************************************************
 * Class _LaravelTools
 *
 * Author: Marc Zev
 * Development Date: Aug 20, 2018
 *
 * Provides a set of static methods that are very useful
 *
 *******************************************************************************/
class _LaravelTools
{
    private static $uiVersion = null;
    private static $uiVersionFallback = null;
    private static $preloginVersion = null;
    private static $preloginVersionFallback = null;
    private static $hasInitialized = false;
    private static $obfuscateDelimiter = '>';
    private static $obfuscateLength = 11;

    public static function modelFactory($model)
    {
        if($model === 'User')
        {
            $namespace = strpos($model, '\\') !== FALSE ? null : 'App\\' ;
        }
        else
        {
            $namespace = strpos($model, '\\') !== FALSE ? null : 'App\\Models\\' ;
        }
        $model = $namespace . $model;
        if (class_exists($model, true)) return new $model();
        return false;
    }
    public static function routesAsArray()
    {
        $collection = Route::getRoutes();

        $routes = [];

        foreach ($collection->getRoutesByName() as $name => $route)
        {
            $routes[$name] = $route->uri;
        }

        asort($routes);
        return $routes;
    }

    public static function addVersionToViewName($string, $delimiter = '.')
    {
        $prelogin = 'prelogin';
        $delimiter = is_string($delimiter) ? $delimiter : '.';
        if (!self::isReady()) self::initialize();
        $new = empty(self::$uiVersion) ? $string : self::$uiVersion . $delimiter . $string;

// ... if prelogin view
        if (substr(strtolower($string), 0, strlen($prelogin)) == $prelogin)
        {
            $parts = explode('.', $string);
            $pre = array_shift($parts);
            if (!empty(self::$preloginVersion)) array_unshift($parts, self::$preloginVersion);
            array_unshift($parts, $pre);
            $new = implode('.', $parts);
            if (view()->exists($new)) return $new;

            $parts = explode('.', $string);
            $pre = array_shift($parts);
            if (!empty(self::$preloginVersionFallback)) array_unshift($parts, self::$preloginVersionFallback);
            array_unshift($parts, $pre);
            $new = implode('.', $parts);
            if (view()->exists($new)) return $new;
            return $string;
        }

// ... NOT prelogin view
        if (view()->exists($new)) return $new;

        $new = empty(self::$uiVersionFallback) ? $string : self::$uiVersionFallback . $delimiter . $string;
        if (view()->exists($new)) return $new;

        return $string;
    }
    public static function isView($viewName)
    {
        return view()->exists($viewName);
    }

    private static function initialize()
    {
        $ui = Input::get('ui', null);
        if (!empty($ui) && $ui != '*')
        {
            session()->put('_ui', $ui);
        }
        else
        {
            if (!empty($ui) && $ui == '*')
            {
                session()->forget('_ui');
            }
        }
        $uiSession = session('_ui');

        $prelogin = Input::get('prelogin', null);
        if (!empty($prelogin) && $prelogin != '*')
        {
            session()->put('_prelogin', $ui);
        }
        else
        {
            if (!empty($prelogin) && $prelogin == '*')
            {
                session()->forget('_prelogin');
            }
        }
        $preloginSession = session('_prelogin');

        self::$uiVersion         = !empty($uiSession) ? ($uiSession == '-' ? null : $uiSession) : config('otc.UI.version');
        self::$uiVersionFallback = !empty($uiSession) ? config('otc.UI.version') : config('otc.UI.fallback');

        self::$preloginVersion         = !empty($preloginSession) ? ($preloginSession == '-' ? null : $preloginSession) : config('otc.Prelogin.version');
        self::$preloginVersionFallback = !empty($preloginSession) ? config('otc.Prelogin.version') : config('otc.Prelogin.fallback');

        self::$hasInitialized    = true;
        return;
    }

    private static function isReady()
    {
        return self::$hasInitialized;
    }

    public static function uiVersionComment()
    {
        self::$uiVersion = config('otc.UI.version');
        if (!self::$hasInitialized) self::initialize();
        return 'OTC-a' . date('y') . '-' .
               self::$uiVersion . '+' .
               (empty(self::$uiVersionFallback) ? 0 : self::$uiVersionFallback) . '-' .
               date('m.d');
    }

    public static function pageContext($page = 'undefined', $note = null)
    {
        self::$uiVersion = config('otc.UI.version');
        $uiVersion = self::uiVersionComment();
        $context   = [
            'page'      => $page,
            'session'   => session()->all(),
            'uiVersion' => $uiVersion,
            'timestamp' => date('Y-m-d h:i:s'),
            'request'   => Request::all(),
            'note'      => $note,
        ];
        return $uiVersion . '--' . self::obfuscateString(json_encode($context));

    }

    public static function obfuscateString($string)
    {
        $chunks = str_split($string, self::$obfuscateLength);
        foreach ($chunks as $idx => $chunk)
        {
            $chunk        = implode(null, array_reverse(str_split($chunk, 1)));
            $chunks[$idx] = $chunk;
        }
        return implode(self::$obfuscateDelimiter, $chunks);
    }

    public static function deobfuscateString($string)
    {
        $chunks = explode(self::$obfuscateDelimiter, $string);
        foreach ($chunks as $idx => $chunk)
        {
            $chunk        = implode(null, array_reverse(str_split($chunk, 1)));
            $chunks[$idx] = $chunk;
        }
        return implode(null, $chunks);
    }
}