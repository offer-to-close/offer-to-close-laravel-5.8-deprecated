<?php

namespace App\Library\otc;

use App\Models\lu_UserRoles;

class RequiredSignatures
{
    static private $rolesByInt = [];
    static private $intRoles = [];

    static public function decodeNumber($num)
    {
        if (empty(self::$rolesByInt)) self::getRoles();

        $rv = [];

        foreach (self::$rolesByInt as $val=>$role)
        {
            if ($num & $val) $rv [] = $role;
        }

        sort($rv);
        return implode(', ', $rv);
    }
    static public function getNumber($roleCodes)
    {
        if (empty(self::$intRoles)) self::getRoles();

        $rv = 0;

        foreach ($roleCodes as $role)
        {
            $rv += self::$intRoles[$role] ?? 0;
        }

        return $rv;
    }

    static private function getRoles()
    {
        $roles = lu_UserRoles::all();

        foreach ($roles as $role)
        {
            self::$rolesByInt[$role->ValueInt] = $role->Display;
            self::$intRoles[$role->Value] = $role->ValueInt;
        }
    }
}