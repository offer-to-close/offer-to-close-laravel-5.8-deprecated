<?php

namespace App\Library\otc;


class _Locations
{
    public static $url = ['asset'   => 'public/',
                          'images'  => 'public/images/',
                          'imports' => 'public/_imports/',
                          'press'   => 'public/press/',
                          'public'   => 'public/',
                          'uploads' => '/public/_uploaded_documents/'
        ];

    public static function url($location, $addon=null)
    {
        if (!isset(self::$url[$location])) return $location;
        $path = self::cleanPath(asset(null));
        $path = self::addEndSlash($path)
                . self::addEndSlash(self::removeLeadingSlash(self::$url[$location]))
                . self::removeLeadingSlash($addon);
        return $path;
    }
    private static function cleanPath($path, $trimString='public/')
    {
        $trimLen = strlen($trimString);
        $path = self::addEndSlash($path);
        if (substr($path, -$trimLen) == $trimString) $path = substr($path, 0, -$trimLen);
        $path = self::addEndSlash($path);
        return $path;
    }
    private static function addEndSlash($path)
    {
        if (substr($path, -1) != '/') $path .= '/';
        return $path;
    }
    private static function removeLeadingSlash($path)
    {
        if (substr($path, 0, 1) == '/') $path = substr($path, 1);
        return $path;
    }
    }