<?php

namespace App\Models;

use App\Combine\TransactionCombine;
use App\Http\Controllers\DocumentAccessController;
use App\Library\otc\Credentials;
use App\Library\Utilities\_Log;
use App\Library\Utilities\_Variables;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ShareRoom extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'ShareRooms';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    protected static function createRecord(int $transactionID, $userID=0, $docID=0)
    {
        try
        {
            $rec = self::upsert(['Transactions_ID'             => $transactionID,
                                 'Users_ID'                    => $userID,
                                 'bag_TransactionDocuments_ID' => $docID,
                                 'DateCreated'                 => date('Y-m-d H:i:s'),
            ]);
            return $rec->ID;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }

    public static function close(int $transactionID)
    {
        try
        {
            static::where('Transactions_ID', $transactionID)->update(['isRoomOpen' => false]);
            return true;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }

    /**
     * Return array contain the IDs for existing Share Rooms. If $onlyOpen = TRUE then only open rooms are included in list
     *
     * @param bool $onlyOpen If TRUE (default) then only open rooms are included in the list.
     *                       If FALSE then all the existing rooms are included in the list.
     *
     * @return array
     */
    public static function rooms($onlyOpen = true) :array
    {
        $query = static::select('ID')
                       ->distinct();
        if ($onlyOpen) $query = $query->where('isRoomOpen', '=', true);
        return array_column($query->get()->toArray(), 'ID');
    }
    public static function open(int $transactionID)
    {
        try
        {
            static::where('Transactions_ID', $transactionID)->update(['isRoomOpen' => true]);
            return true;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }
    public static function addUser(int $transactionID, $userID)
    {
        $room = static::where('Transactions_ID', $transactionID)
                      ->where('Users_ID', $userID)->get();
        if (count($room) == 0) return $room->first()->ID;

        return self::createRecord($transactionID, $userID, 0);
    }
    public static function removeUser(int $transactionID, $userID)
    {
        try
        {
            static::where('Transactions_ID', $transactionID)
                  ->where('Users_ID', $userID)
                  ->where('Status', '!=', 'placeholder')
                  ->update(['isAvailable' => false]);
            return true;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }
    public static function addDocument(int $transactionID, $documentID)
    {
        $room = static::where('Transactions_ID', $transactionID)
                      ->where('bag_TransactionDocuments_ID', $documentID)->get();
        if (count($room) == 0) return $room->first()->ID;

        return self::createRecord($transactionID, 0, $documentID);
    }
    public static function removeDocument(int $transactionID, $documentID)
    {
        try
        {
            static::where('Transactions_ID', $transactionID)
                  ->where('bag_TransctionDocuments_ID', $documentID)
                  ->where('Status', '!=', 'placeholder')
                  ->update(['isAvailable' => false]);
            return true;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }
    public static function removeAll(int $transactionID)
    {
        try
        {
            static::where('Transactions_ID', $transactionID)
                  ->where('Status', '!=', 'placeholder')
                  ->update(['isAvailable' => false]);
            return true;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }
    public static function openAll(int $transactionID)
    {
        try
        {
            static::where('Transactions_ID', $transactionID)
                  ->where('Status', '!=', 'placeholder')
                  ->update(['isAvailable' => true]);
            return true;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }

    /**
     * @param int  $transactionID
     * @param bool $onlyAvailable
     * @param bool $onlyOpen
     *
     * @return mixed
     */
    public static function contents(int $transactionID, $onlyAvailable=true, $onlyOpen=true)
    {
        $query = static::where('Transactions_ID', $transactionID);
        if ($onlyOpen) $query = $query->where('isRoomOpen', true);
        if ($onlyAvailable) $query = $query->where('isAvailable', true);
        return $query->get();
    }

    /**
     * @param int  $transactionID
     * @param bool $onlyAvailable
     * @param bool $onlyOpen
     *
     * @return array of arrays 'documents' is a list of documents the current user can see in the ShareRoom.
     *                         'transferList' is a list of roles with whom the current user can share the doucment.
     */
    public static function view(int $transactionID, $userID=null, $onlyAvailable=true, $onlyOpen=true)
    {
        $contents = self::contents($transactionID, $onlyAvailable, $onlyOpen);
        if(empty($userID)) $userId = auth()->id();
        $bagIDs = [];

        foreach ($contents as $doc)
        {
            $bagIDs[] = $doc->bag_TransactionDocuments_ID;
        }

        $docs  = bag_TransactionDocument::getDocumentsWithMetaByID($bagIDs);

        $canView = [];
        $transfer = [];
        $transactionRoles = TransactionCombine::getTransactionRole($transactionID);

        foreach ($docs as $doc)
        {
            if (!in_array($doc->PovRole, $transactionRoles)) continue;

            $access = DocumentAccessController::decodeAccessSum($doc->AccessSum);

            if (count($access) > 0)
            {
                $canView[$doc->bagID] = $doc;
                $tfr = DocumentAccessController::decodeTransferSum(($doc->TransferSum));
                if (count($tfr) > 0) $transfer[$doc->bagID] = $tfr;
            }
        }
        ddd(['bagIDs'=>$bagIDs, 'docs'=>$docs, 'transaction roles'=>$transactionRoles]);

        return ['documents'=>$canView, 'transferList'=>$transfer];
    }
    public static function documentCount(int $transactionID, $onlyAvailable=true, $onlyOpen=true)
    {
        $query = static::where('Transactions_ID', $transactionID)
                       ->where('bag_TransactionDocuments_ID', '>', 0)
                       ->select('ID');
        if ($onlyOpen) $query = $query->where('isRoomOpen', true);
        if ($onlyAvailable) $query = $query->where('isAvailable', true);
        return $query->get()->count();
    }
    public static function createPlaceholder(int $transactionID)
    {
        try
        {
            $rec = self::upsert(['Transactions_ID' => $transactionID,
                                 'isAvailable'           => false,
                                 'status'                => 'placeholder',
                                 'DateCreated'           => date('Y-m-d H:i:s'),
            ]);
            return $rec->ID;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }
    public static function isPlaceholder($room)
    {
        if (count($room ) != 1) return false;

        $room = $room->first();

        if ($room->Status == 'placeholder') return true;
    }
    static public function getDirectory($transaction)
    {
        if (is_numeric($transaction)) $transactionID = $transaction;
        elseif (_Variables::getObjectName($transaction) == 'Transaction') $transactionID = $transaction->ID;
        else return [];

        $query = DB::table('bag_TransactionDocuments')
                   ->select('bag_TransactionDocuments.ID as bagID',
                       'ShortName', 'Description',
                       'DocumentPath', 'OriginalDocumentName',
                       'SignedBy', 'bag_TransactionDocuments.ApprovedByUsers_ID',
                       'UploadedBy_ID', 'UploadedByRole', 'lk_Transactions-Documents.Transactions_ID',
                       'lk_Transactions-Documents.Documents_Code', 'isIncluded as isRequired',
                       'lk_TransactionsDocumentAccess.PovRole' , 'AccessSum', 'TransferSum'
                   )
                   ->join('ShareRooms', 'bag_TransactionDocuments.ID', '=', 'ShareRooms.bag_TransactionDocuments_ID')
                   ->leftjoin('lk_Transactions-Documents', 'bag_TransactionDocuments.lk_Transactions-Documents_ID', '=',
                       'lk_Transactions-Documents.ID')
                   ->leftjoin('Documents', 'lk_Transactions-Documents.Documents_Code', '=',
                       'Documents.Code')
                   ->leftjoin('lk_TransactionsDocumentAccess', 'Documents.Code', '=',
                       'lk_TransactionsDocumentAccess.Documents_Code')
                   ->leftjoin('lk_TransactionsDocumentTransfers', 'Documents.Code', '=',
                       'lk_TransactionsDocumentTransfers.Documents_Code')
                   ->where('bag_TransactionDocuments.isTest', false)
                   ->where('isActive', true)
                   ->where('lk_Transactions-Documents.Transactions_ID', $transactionID)
                   ->whereRaw('`lk_TransactionsDocumentAccess`.`Transactions_ID` = `lk_Transactions-Documents`.`Transactions_ID`')
                   ->whereRaw('`lk_TransactionsDocumentTransfers`.`Transactions_ID` = `lk_Transactions-Documents`.`Transactions_ID`')
                   ->whereRaw('`lk_TransactionsDocumentAccess`.`PovRole` = `lk_TransactionsDocumentTransfers`.`PovRole`')
                   ->distinct();
        $rv = $query->get();
        if (isServerLocal())
        {
            if (count($rv) >= 0) Log::info(['transactionID'=>$transactionID, 'sql'=>$query->toSql()]);
        }
        return $rv;
    }

}
