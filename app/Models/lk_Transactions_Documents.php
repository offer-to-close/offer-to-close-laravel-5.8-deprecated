<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class lk_Transactions_Documents extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_Transactions-Documents';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public static function eraseByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->delete();
    }
    public static function documentsByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->get();
    }
}
