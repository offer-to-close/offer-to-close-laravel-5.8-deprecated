<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class lk_Users_Roles extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_Users-Roles';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    protected $fillable = ['Users_ID', 'Role'];
    static public function eraseByUserId($userID)
    {
        return static::where('Users_ID', '=', $userID)->delete();
    }
    static public function rolesByUserId($userID, $onlyActive=true)
    {
        if (empty($userID)) return false;
        $query = static::where('Users_ID', '=', $userID)
            ->where('isTest', '!=', true);
        if ($onlyActive) $query = $query->where('isActive', '=', true);
        return $query->get();
    }
    static public function insertUserRolePair($userID, $roleCode, $isTest=0)
    {
        if (empty($roleCode)) return false;
        $result = static::where('Users_ID', '=', $userID)->where('Role', '=', $roleCode)->get();
        if ($result->count() > 0) return true;

        $model = new lk_Users_Roles(['Users_ID'=>$userID, 'Role'=>$roleCode,
                                     'isTest'=>$isTest, 'isActive'=>1, 'DateCreated'=>date('Y-m-d H:i:s')]);
        return $model->save();
    }
}
