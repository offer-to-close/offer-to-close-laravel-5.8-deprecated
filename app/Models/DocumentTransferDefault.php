<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentTransferDefault extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'DocumentTransferDefaults';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public static function getByDocument($documentID, $state='CA')
    {
        $query = static::where('State', $state);
        if (empty($documentID) || $documentID == 0 || $documentID = '*') return $query->get();

        if (!is_array($documentID)) $documentID = [$documentID];
        return $query->whereIn('Documents_ID', $documentID)->get();
    }
}
