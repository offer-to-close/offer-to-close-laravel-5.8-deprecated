<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Timeline extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Timeline';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    static public function getByCode($code, $state='CA')
    {
        return static::where('State', '=', $state)
                     ->where('Code', '=', $code)
                     ->get()
                     ->first();
    }
}
