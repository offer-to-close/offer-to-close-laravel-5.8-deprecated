<?php

namespace App\Models;

use App\Library\Utilities\_Convert;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guest extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Guests';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public static function makeFromUser($data=[])
    {
        if (!is_array($data)) $data = _Convert::toArray($data);
        reset($data);
        if (key($data) == 0) $data = $data[0];

        if (!isset($data['ID']) && (!isset($data['Users_ID']) || !empty($data['Users_ID']))) $data['ID'] = $data['Users_ID'];
        return self::upsert($data);
    }
}
