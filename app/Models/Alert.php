<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alert extends Model
{
    use SoftDeletes;
    public $table = 'Alerts';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}
