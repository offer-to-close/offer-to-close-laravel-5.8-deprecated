<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MemberRequest extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'MemberRequests';
    protected $connection = 'mysql';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

}
