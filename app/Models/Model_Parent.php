<?php

namespace App\Models;

use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Variables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes\NoTestScope;
use App\Scopes\OnlyTestScope;
use Illuminate\Support\Facades\Schema;

class Model_Parent extends Model
{
    protected $connection = 'mysql';

    use SoftDeletes;
    public $tableName = null;
    public $schema = null;
    public $schemaName = null;

    public static $fieldName = [];
    public static $fieldType = [];
    public static $fieldDefault = [];
    public static $fieldNullable = [];

    const SORT_BY_NONE = null;
    const SORT_BY_VALUE = 'value';
    const SORT_BY_DISPLAY = 'display';
    const SORT_BY_ORDER = 'order';
    const SORT_BY_INDEX = 'index';


    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['deleted_at'];


    /**
     * Model_Parent constructor.
     *
     * @param array $attributes
     *
     * @throws \Doctrine\DBAL\Schema\SchemaException
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->instantiate();
    }

    protected function instantiate()
    {
        $this->tableName = $this->getTable();
        if (strtolower($this->tableName) != 'otcusers')
        {
            $this->schemaName = 'mysql-otc';
            self::setConnection($this->schemaName);
        }
        else
        {
            $this->schema     = $this->getConnection();
            $this->schemaName = $this->schema->getConfig()['name'];
            self::setConnection($this->schemaName);
        }
        if (!empty(self::$fieldName[$this->tableName])) return;

        $sql = 'show columns from `' . $this->tableName . '`';

        try
        {
            $result = DB::connection($this->schemaName)->select($sql);
        }
        catch (\Exception $e)
        {
                Log::error('*** ERROR $');
                Log::error(['&&Error', 'table' => $this->tableName, 'sql' => $sql, 'connection' => DB::connection($this->schemaName)->getConfig(),
                            'msg'=>$e->getMessage(), __METHOD__ => __LINE__]);

                Log::debug(['list of connections'=>config('database.connections'),__METHOD__=>__LINE__]);
        }
        foreach ($result as $row)
        {
            $fn                       = $row->Field;
            self::$fieldName[$this->tableName][]        = $fn;
            self::$fieldType[$this->tableName][$fn]     = $row->Type;
            self::$fieldDefault[$this->tableName][$fn]  = $row->Default;
            self::$fieldNullable[$this->tableName][$fn] = ($row->Null == 'YES') ? true : false;
        }
//        Log::debug(['connection'=>$this->schemaName, 'table' => $this->tableName, 'fields' => self::$fieldName, __METHOD__=>__LINE__]);

    }

    /**
     * @return array
     */
    public function getEmptyRecord()
    {
        if (empty(self::$fieldName[$this->tableName])) self::instantiate();
        foreach (array_combine(self::$fieldName[$this->tableName], array_fill(0, count(self::$fieldName[$this->tableName]), null)) as $col => $val)
        {
            $rv[$col] = self::$fieldDefault[$col] ?? '';
        }
        return $rv;
    }

    /**
     * @return Collection
     */
    public function getEmptyCollection()
    {
        return collect($this->getEmptyRecord());
    }

    /**
     * @return Mixed UserID if part of record, else FALSE. If $tableID is NULL then return true if present.
     */
    public function getUserID($tableID = null)
    {
        if (empty($tableID)) return isset($this->Users_ID);
        $rv = static::find($tableID);
        return $rv->Users_ID;
    }

    /**
     * @return mixed
     */
    public function isRecordEmpty($input, $exceptionFields = ['ID'])
    {
        if (!is_array($input) && !(_Variables::getObjectName($input) == 'Collection'))
        {
            if (!is_numeric($input) || $input < 1) return false;
            $input = $this->get($input);
        }
        if ((is_object($input) && _Variables::getObjectName($input) == 'Collection')) $input = $input->first();
        if ((is_object($input) && _Variables::getObjectName($input) == 'Collection'))
        {
            $input = $input->toArray();
        }
        else $input = _Convert::toArray($input);

        if (count($input) < 2) $input = reset($input);
        if (empty($exceptionFields) || !in_array('ID', $exceptionFields)) $exceptionFields[] = 'ID';

        foreach ($input as $key => $val)
        {
            if (in_array($key, $exceptionFields)) continue;
            if (!is_null($val) && !empty($val))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * Accepts passed array, removes values where key is not a valid column name and performs a save
     * when no primary key value is supplied and performs an "update or insert" operation if the primary
     * key is supplied.
     *
     * @param $data
     *
     * @return $this|mixed
     */
    public function upsert(array $data)
    {

        dump(['data'=>$data , __METHOD__=>__LINE__]);
        foreach ($data as $col => $val)
        {
            if (array_search($col, self::$fieldName[$this->tableName]) === false)
            {
                unset ($data[$col]);
            }
        }

        if (isset($data['ID']))
        {
            $arg1 = ['ID' => $data['ID']];
            $arg2 = $data;
            unset($arg2['ID']);

            $rv = $this->updateOrInsert($arg1, $arg2);
            return $rv;
        }
        else
        {
            foreach ($data as $col => $val)
            {
                $this->{$col} = $data[$col];
            }
            $rv = $this->save();

            $model = '\\App\\Models\\' . _Variables::getObjectName($this);
            $rv =  $model::withoutGlobalScope(NoTestScope::class)->find($this->ID ?? $this->id);
            return $rv;
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////
    /// ... This code is here because Laravel's updateOrCreate method is not working. It Creates
    ///      but does not update.
    /**
     * @param array $attributes
     * @param array $values
     *
     * @return mixed
     */
    protected function updateOrInsert(array $attributes, array $values = [])
    {
        $instance = $this->where($attributes);
        if ($instance->count() != 0)
        {
            $instance->update($values);
        }
        else
        {
            $instance = $this->updateOrCreate($attributes, $values);
        }
        return $this->where($attributes)->get()->first();
    }

    public static function dataModel($list = '*')
    {
        if ($list == '*')
        {
            $fields = self::getTableFields();
        }
        else
        {
            if (!is_array($list)) $list = [$list];
            $fields = self::getTableFields($list);
        }

        $dataModel = [];
        $endings   = ['ID', 'Value', 'Code'];
        $tableMap  = config('otc.ColumnNameToTableMap');
        foreach ($fields as $row)
        {
            $t = $row['Table'];
            $c = $row['Column'];

            $dataModel[$t][$c] = null;
            foreach ($endings as $end)
            {
                $len = strlen($end) + 1;
                if (substr($c, -$len) != '_' . $end) continue;
                $tbl = substr($c, 0, -$len);
                if (isset($tableMap[$tbl]) && !is_null($tableMap[$tbl])) $tbl = $tableMap[$tbl];
                $dataModel[$t][$c] = $tbl . '.' . $end;
                break;
            }
        }
        return $dataModel;
    }

    public static function getAllTables()
    {
        foreach (DB::select('SHOW TABLES') as $table)
        {
            $tmp  = _Convert::toArray($table);
            $rv[] = reset($tmp);
        }
        return $rv;
    }

    public static function getTableFields($tableList = '*')
    {
        $tableFields = [];
        $query       = DB::table('information_schema.columns')
                         ->select('Table_name as Table', 'column_name as Column')
                         ->where('table_schema', '=', DB::getDatabaseName());

        if ($tableList != '*')
        {
            if (!is_array($tableList)) $tableList = [$tableList];
            $query->whereIn('Table_Name', $tableList);
        }
        if ($tableList[0] == 'lk_Transactions_Timeline')
        {
            dump(['table' => $tableList, 'schema' => DB::getDatabaseName(), $query->toSql(), __METHOD__ => __LINE__]);
        }
        foreach ($query->get()->toArray() as $idx => $row)
        {
            $tableFields[] = _Convert::toArray($row);
        }
        return $tableFields;
    }

    protected static function boot()
    {
        parent::boot();

        $table   = self::currentTable();
        $fields1 = self::getTableFields($table);
        $fields  = array_column($fields1, 'Column');
        if (!in_array('isTest', $fields))
        {
//            Log::debug(['isTest Column not found', 'table' => $table, 'fields' => $fields, 'fields1' => $fields1, __METHOD__=>__LINE__]);
            Log::info(('isTest Column not found in table ' . $table ?? 'unknown') . ';  ' . __METHOD__ . '=>' . __LINE__);
            return;
        }

        $includeTest     = env('includeTestRecords', false);
        $includeOnlyTest = env('includeOnlyTestRecords', false);

        // ... The two criteria are mutually exclusive, so if they are both true, then Only Test Records are selected.
        if ($includeTest && $includeOnlyTest) $includeTest = false;
        // ... If test records are included, this is the same as running the user-defined query without alteration, so if
        // ... $includeTest is TRUE then NoTestScope is NOT run, but if it is FALSE then NoTestScope is run.
        if (!$includeTest)
        {
            static::addGlobalScope((new NoTestScope));
        }

        // ... If ONLY test records are included ($includeOnlyTest = TRUE), then the OnlyTestScope needs to be run
        // ... $includeOnlyTest is TRUE then OnlyTestScope is run, but if it is FALSE then it is Not run.
        //        if ($includeOnlyTest)
        //        {
        //            static::addGlobalScope((new OnlyTestScope()));
        //        }
    }

    private static function currentTable()
    {
        try
        {
            $class = get_called_class();
            $a     = new $class;
            $t     = $a->table;
            return $t;
        }
        catch (\Exception $e)
        {
            if (strpos($class, '\\') === false) return $class;

            $parts = explode('\\', $class);

            $table = array_pop($parts);
            if (Schema::hasTable($table))
            {
                return $table;
            }
            else if (Schema::hasTable(str_plural($table))) return str_plural($table);
            return $table;
        }
    }

    public static function scopeNoTest($query, $tables=[false])
    {
        // ... If test records are included, this is the same as running the user-defined query without alteration, so if
        // ... $includeTest is TRUE then NoTestScope is NOT run, but if it is FALSE then NoTestScope is run.
        $includeTest     = env('includeTestRecords', false);
        if ($includeTest) return $query;

        if (!is_array($tables)) $tables = [$tables];
        try
        {
            foreach ($tables as $table)
            {
                $field = ($table) ? $table . '.isTest' : 'isTest';
                $query = $query->where(function ($q) use ($query, $field)
                {
                    $q->where($field, false)
                      ->orWhereNull($field);
                }
                );
            }
            return $query;
        }
        catch (\Exception $e)
        {
            Log::error(['&& Exception',
                        'Msg'=>$e->getMessage(),
                        'Code'=>$e->getCode(),
                        'Line'=>$e->getLine(),
                        'Query'=>$query,
                        'Table'=>$table,
                        __METHOD__=>__LINE__]
            );
        }
    }
    public static function groupWhere($query, $tables=[false])
    {
        // ... If test records are included, this is the same as running the user-defined query without alteration, so if
        // ... $includeTest is TRUE then NoTestScope is NOT run, but if it is FALSE then NoTestScope is run.
        $includeTest     = env('includeTestRecords', false);
        if ($includeTest) return $query;

        if (!is_array($tables)) $tables = [$tables];
        try
        {
            foreach ($tables as $table)
            {
                $field = ($table) ? $table . '.isTest' : 'isTest';
                $query = $query->where(function ($q) use ($query, $field)
                {
                    $q->where($field, false)
                      ->orWhereNull($field);
                }
                );
            }
            return $query;
        }
        catch (\Exception $e)
        {
            Log::error(['&& Exception',
                        'Msg'=>$e->getMessage(),
                        'Code'=>$e->getCode(),
                        'Line'=>$e->getLine(),
                        'Query'=>$query,
                        'Table'=>$table,
                        __METHOD__=>__LINE__]
            );
        }
    }
}