<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Loan extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Loans';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}