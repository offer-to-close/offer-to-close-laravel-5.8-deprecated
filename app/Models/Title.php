<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Title extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Titles';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}