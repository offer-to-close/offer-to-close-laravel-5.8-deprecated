<?php

namespace App\Models;

use App\Library\Utilities\_Time;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class log_Mail extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'log_Mail';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    static public function sentByDate($dateStart, $dateEnd=false)
    {
        $dateStart = _Time::formatDate($dateStart, 'Y-m-d');
        if (!$dateEnd) $dateStart = $dateStart;
        else $dateEnd = _Time::formatDate($dateEnd, 'Y-m-d');
        $between = [$dateStart, $dateEnd];
        if ($dateEnd < $dateStart) $between = array_reverse($between);

        return static::where('wasSent', 1)->whereBetween('DateTransmitted', $between)->get();
    }
    static public function receivedByDate($dateStart, $dateEnd=false)
    {
        $dateStart = _Time::formatDate($dateStart, 'Y-m-d');
        if (!$dateEnd) $dateStart = $dateStart;
        else $dateEnd = _Time::formatDate($dateEnd, 'Y-m-d');
        $between = [$dateStart, $dateEnd];
        if ($dateEnd < $dateStart) $between = array_reverse($between);

        return static::where('wasReceived', 1)->whereBetween('DateTransmitted', $between)->get();
    }
}
