<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Broker extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Brokers';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const EMAIL_FORMAT_HTML = 'html';
    const EMAIL_FORMAT_TEXT = 'text';

}
