<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MilestoneReference extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'MilestoneReferences';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
}
