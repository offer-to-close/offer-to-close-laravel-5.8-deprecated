<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class lk_Transactions_Timeline extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'lk_Transactions-Timeline';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
    public function alerts()
    {
        return $this->hasMany(Alert::class,'Model_ID', 'ID');
    }
    static public function eraseByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->delete();
    }
    static public function timelineByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->get();
    }
}
