<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model_Parent
{
    use SoftDeletes;
    public $table = 'Agents';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const TYPE_BUYER = 'b';
    const TYPE_SELLER = 's';
    const TYPE_BOTH = 'bs';

    const EMAIL_FORMAT_HTML = 'html';
    const EMAIL_FORMAT_TEXT = 'text';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['DateCreated', 'DateUpdated', 'deleted_at',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $autoUpsert = [
        'DateCreated', 'DateUpdated',
    ];
}
