<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class lu_UserRoles extends LookupTable
{
    use SoftDeletes;
    protected $table = 'lu_UserRoles';
}
