<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Document extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'Documents';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';
/*  ToDo: Deprecated
    const DOCUMENT_TYPE_AGENCY_DISCLOSURS = 'A';
    const DOCUMENT_TYPE_CONTRACTS_DEPOSIT_CHECK_TRUST_LOG = 'B';
    const DOCUMENT_TYPE_ESCROW_DOCUMENTS = 'C';
    const DOCUMENT_TYPE_DISCLOSURES = 'D';
    const DOCUMENT_TYPE_INSPECTIONS_REPORTS = 'E';
    const DOCUMENT_TYPE_VERIFICATION_CONTINGENCY_REMOVALS_RECEIPTS = 'F';
    const DOCUMENT_TYPE_CORRESPONDENCE_NOTES = 'G';
*/

    public function questionnaires()
    {
        return $this->hasMany(Questionnaire::class,'Documents_Code', 'Code');
    }
}
