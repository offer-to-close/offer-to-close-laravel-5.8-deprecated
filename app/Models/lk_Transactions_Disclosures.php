<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class lk_Transactions_Disclosures extends Model_Parent
{
    protected $table = 'lk_Transactions-Disclosures';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    public static function eraseByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->delete();
    }
    public static function disclosuresByTransactionId($transactionID)
    {
        return static::where('Transactions_ID', '=', $transactionID)->get();
    }
}
