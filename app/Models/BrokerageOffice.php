<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class BrokerageOffice extends Model_Parent
{
    use SoftDeletes;
    protected $table = 'BrokerageOffices';

    const CREATED_AT = 'DateCreated';
    const UPDATED_AT = 'DateUpdated';

    const EMAIL_FORMAT_HTML = 'html';
    const EMAIL_FORMAT_TEXT = 'text';

    public static function getName($id)
    {
        $result = static::find($id);
        if (count($result) == 0) return null;
        return $result->Name;
    }
}
