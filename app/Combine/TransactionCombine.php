<?php

namespace App\Combine;

use App\Http\Controllers\AccessController;
use App\Library\otc\_Locations;
use App\Http\Controllers\CredentialController;
use App\Library\otc\AddressVerification;
use App\Library\Utilities\_Convert;
use App\Library\Utilities\_LaravelTools;
use App\Library\Utilities\_Time;
use App\Library\Utilities\_Variables;
use App\Models\Document;
use App\Models\Escrow;
use App\Models\Loan;
use App\Models\Model_Parent;
use App\Models\Questionnaire;
use App\Models\ShareRoom;
use App\Models\Title;
use App\Models\TransactionCoordinator;
use Illuminate\Support\Collection;
use App\Models\Agent;
use App\Models\Buyer;
use App\Models\lk_Transactions_Timeline;
use App\Models\Property;
use App\Models\Seller;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Library\Utilities\DisplayTable;
use Illuminate\Support\Facades\File;


class TransactionCombine extends BaseCombine
{

    static $indicesToRoleCode = ['transaction'  => null,
                                 'property'     => null,
                                 'buyer'        => 'b',
                                 'buyersAgent'  => 'ba',
                                 'buyersTC'     => 'btc',
                                 'seller'       => 's',
                                 'sellersAgent' => 'sa',
                                 'sellersTC'    => 'stc',
                                 'timeline'     => null,
                                 'escrow'       => 'e',
                                 'title'        => 't',
                                 'loan'         => 'l',
    ];
    /**
     * @param int $transactionsID
     * @param     $type
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function agents(int $transactionsID, $type = Agent::TYPE_BOTH): Collection
    {
        return getAgents($transactionsID, $type);
    }

    public static function getAgents(int $transactionsID, $type = Agent::TYPE_BOTH): Collection
    {
        $query1 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.BuyersAgent_ID', '=', 'Agents.ID')
                    ->selectRaw('Agents.*, \'Buyer\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1, ['Transactions', 'Agents']);

        $query2 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                    ->selectRaw('Agents.*, \'Seller\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query2 = Model_Parent::scopeNoTest($query2, ['Transactions', 'Agents']);

        if ($type == Agent::TYPE_BUYER) return $query1->get();
        if ($type == Agent::TYPE_SELLER) return $query2->get();
        if ($type == Agent::TYPE_BOTH) return $query1->union($query2)->get();
    }

    public static function getOwners(int $transactionsID)
    {
        $query1 = DB::table('Transactions')
            ->leftJoin('users as cu', 'Transactions.CreatedByUsers_ID', '=', 'cu.id')
            ->leftJoin('users as bou', 'Transactions.BuyersOwner_ID', '=', 'bou.id')
            ->leftJoin('users as sou', 'Transactions.SellersOwner_ID', '=', 'sou.id')
                    ->selectRaw('cu.id as CreatorID, cu.name as CreatorName, 
                    bou.id as BuyerOwnerID, bou.name as BuyerOwnerName, 
                    sou.id as SellerOwnerID, sou.name as SellerOwnerName')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1, ['Transactions', 'cu', 'bou', 'sou']);

        return $query1->get()->first();
    }

    public static function getOwnedTransactions($userID = NULL)
    {
        $userID = $userID ?? CredentialController::current()->ID();
        $qry = DB::table('Transactions')->select('ID')->distinct()
            ->where(function ($q) use ($userID)
            {
                $q->where('BuyersOwner_ID' , $userID)
                    ->orWhere('SellersOwner_ID', $userID)
                    ->orWhere('OwnedByUsers_ID', $userID);
            })
                    ->orderBy('ID');
        $qry = Model_Parent::scopeNoTest($qry);

        $ownedTransactions = $qry->get()->toArray();

        return $ownedTransactions;
    }

    public static function getTransactionCoordinators(int $transactionsID, $type = TransactionCoordinator::TYPE_BOTH): Collection
    {
        $query1 = DB::table('Transactions')
                    ->leftJoin('TransactionCoordinators', 'Transactions.BuyersTransactionCoordinators_ID', '=', 'TransactionCoordinators.ID')
                    ->selectRaw('TransactionCoordinators.*, \'Buyer\' as TCType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1, ['Transactions', 'TransactionCoordinators']);

        $query2 = DB::table('Transactions')
                    ->leftJoin('TransactionCoordinators', 'Transactions.SellersTransactionCoordinators_ID', '=', 'TransactionCoordinators.ID')
                    ->selectRaw('TransactionCoordinators.*, \'Seller\' as TCType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query2 = Model_Parent::scopeNoTest($query2, ['Transactions', 'TransactionCoordinators']);

        if ($type == TransactionCoordinator::TYPE_BUYERTC) return $query1->get();
        if ($type == TransactionCoordinator::TYPE_SELLERTC) return $query2->get();
        if ($type == Agent::TYPE_BOTH) return $query1->union($query2)->get();
    }

    /**
     * Returns the Transaction Coordinator's model for the client's TC
     *
     * @param $transactionID
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getClientTC($transactionID)
    {
        $tcRole = substr(self::getClientRole($transactionID), 0, 1);
        return  self::getTransactionCoordinators($transactionID, $tcRole);
    }

    /**
     * Returns the Transaction Coordinator's model for the TC that is not the client's
     *
     * @param $transactionID
     *
     * @return \Illuminate\Support\Collection
     */
    public static function getOtherTC($transactionID)
    {
        $other = ['btc'=>'stc', 'stc'=>'btc'];
        $tcRole = substr(self::getClientRole($transactionID), 0, 1) . 'tc';
        return self::getTransactionCoordinators($transactionID, $other[$tcRole]);
    }


    /**
     * Returns an array containing the roles that the user has for the given transaction. Based on parameters the values returned can either be ALL
     * the user's roles, or just the roles related to the role the user has made active.
     *
     * @param      $transactionID
     * @param bool $returnOnlyActiveRoles If FALSE (default), return all role.
     *                                    If TRUE, return only roles consistent with the role the user
     *                                    has made active. If the user made the TC role active and $returnOnlyActiveRoles then regardless of how
     *                                    many roles the user might have in transaction, the only possible return values are btc and/or stc.
     *
     * @return array List of just the user's role(s) in the provided transaction
     */
    public static function getUserTransactionRoles($transactionID, $returnOnlyActiveRoles=false)
    {
        $taRoles = self::getRoleByUserID($transactionID, CredentialController::current()->ID(), true);
        $userRole = session('userRole');
        $roleLen = strlen($userRole);

        if (!$returnOnlyActiveRoles) return $taRoles;

        $myRoles = [];
        foreach($taRoles as $idx=>$role)
        {
            if (substr($role, -$roleLen) == $userRole) $myRoles[] = $role;
        }
        return $myRoles;
    }

    /**
     * Returns an array containing the sides (buyer, or seller) that the user is on for the given transaction
     *
     * @param $transactionID
     *
     * @return array
     */
    public static function getUserTransactionSides($transactionID)
    {
        $mySides = [];
        foreach(self::getUserTransactionRoles($transactionID) as $role)
        {
            $mySides[substr($role, 0, 1)] = true;
        }
        return array_keys($mySides);
    }
    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function brokerages($transactionsID, $type = Agent::TYPE_BOTH)
    {
        $query1 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.BuyersAgent_ID', '=', 'Agents.ID')
                    ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                    ->leftJoin('BrokerageOffices', 'Brokers.BrokerageOffices_ID', '=', 'BrokerageOffices.ID')
                    ->leftJoin('Brokerages', 'BrokerageOffices.Brokerages_ID', '=', 'Brokerages.ID')
                    ->selectRaw('Brokerages.*, \'Buyer\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1,
            ['Transactions', 'Agents', 'Brokers', 'BrokerageOffices', 'Brokerages']);

        $query2 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                    ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                    ->leftJoin('BrokerageOffices', 'Brokers.BrokerageOffices_ID', '=', 'BrokerageOffices.ID')
                    ->leftJoin('Brokerages', 'BrokerageOffices.Brokerages_ID', '=', 'Brokerages.ID')
                    ->selectRaw('Brokerages.*, \'Seller\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query2 = Model_Parent::scopeNoTest($query2,
            ['Transactions', 'Agents', 'Brokers', 'BrokerageOffices', 'Brokerages']);

        if ($type == Agent::TYPE_BUYER) return $query1->get();
        if ($type == Agent::TYPE_SELLER) return $query2->get();
        if ($type == Agent::TYPE_BOTH) return $query1->union($query2)->get();
    }


    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function brokers($transactionsID, $type = Agent::TYPE_BOTH)
    {
        $query1 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.BuyersAgent_ID', '=', 'Agents.ID')
                    ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                    ->selectRaw('Brokers.*, \'Buyer\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1,['Transactions', 'Agents', 'Brokers', ]);

        $query2 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                    ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                    ->selectRaw('Brokers.*, \'Seller\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query2 = Model_Parent::scopeNoTest($query2,['Transactions', 'Agents', 'Brokers', ]);

        if ($type == Agent::TYPE_BUYER) return $query1->get();
        if ($type == Agent::TYPE_SELLER) return $query2->get();
        if ($type == Agent::TYPE_BOTH) return $query1->union($query2)->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function brokerageOffices($transactionsID, $type = Agent::TYPE_BOTH): Collection
    {
        $query1 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.BuyersAgent_ID', '=', 'Agents.ID')
                    //->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                    ->leftJoin('BrokerageOffices', 'Agents.BrokerageOffices_ID', '=', 'BrokerageOffices.ID')
                    ->selectRaw('BrokerageOffices.*, \'Buyer\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1,['Transactions', 'Agents', 'BrokerageOffices', ]);

        $query2 = DB::table('Transactions')
                    ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                    //->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                    ->leftJoin('BrokerageOffices', 'Agents.BrokerageOffices_ID', '=', 'BrokerageOffices.ID')
                    ->selectRaw('BrokerageOffices.*, \'Seller\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query2 = Model_Parent::scopeNoTest($query2,['Transactions', 'Agents', 'BrokerageOffices', ]);

        if ($type == Agent::TYPE_BUYER) return $query1->get();
        if ($type == Agent::TYPE_SELLER) return $query2->get();
        if ($type == Agent::TYPE_BOTH) return $query1->union($query2)->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function property($transactionsID)
    {
        $query = DB::table('Transactions')
                   ->leftJoin('Properties', 'Transactions.Properties_ID', '=', 'Properties.ID')
                   ->select('Properties.*')
                   ->where('Transactions.ID', '=', $transactionsID);
        $query = Model_Parent::scopeNoTest($query,['Transactions', 'Properties', ]);

        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed array = [purchase price, property type, loan type, 'Close Escrow', CloseEscrow(date),
     *                        offered prepared (date), address ]
     */
    public static function getPropertySummary($transactionsID)
    {
        $query   = DB::table('Properties')
                     ->leftJoin('Transactions', 'Transactions.Properties_ID', '=', 'Properties.ID')
                     ->leftJoin('lu_PropertyTypes', 'Properties.PropertyType', '=', 'lu_PropertyTypes.Value')
                     ->leftJoin('lu_LoanTypes', 'Transactions.LoanType', '=', 'lu_LoanTypes.Value')
                     ->leftJoin('lk_Transactions-Timeline', 'Transactions.ID', '=', 'lk_Transactions-Timeline.Transactions_ID')
                     ->select('Transactions.PurchasePrice as PurchasePrice',
                         'lu_PropertyTypes.Display as PropertyType',
                         'lu_LoanTypes.Display as LoanType',
                         'lk_Transactions-Timeline.MilestoneName',
                         'lk_Transactions-Timeline.MilestoneDate as CloseEscrow',
                         'Transactions.DateOfferPrepared as OfferPrepared',
                         'Properties.Street1',
                         'Properties.Street2',
                         'Properties.City',
                         'Properties.County',
                         'Properties.State',
                         'Properties.Zip',
                         'Properties.ParcelNumber',
                         'Properties.YearBuilt'
                         )
                     ->selectRaw('concat(coalesce(Properties.Street1, ""),", ", 
                            coalesce(Properties.City,""),", ", coalesce(Properties.State, "")," ", coalesce(Properties.Zip,"")) as  Address')
                     ->where('Transactions.ID', '=', $transactionsID)
                     ->where('lk_Transactions-Timeline.MilestoneName', '=', 'Close Escrow')
                     ->orderBy('MilestoneDate', 'Asc');
        $query = Model_Parent::scopeNoTest($query,['Transactions', 'Properties', 'lu_PropertyTypes',
                                                   'lu_LoanTypes', 'lk_Transactions-Timeline']);

        $results = $query->get();
        return _Convert::toArray($results->first());
    }


    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getDocuments(int $transactionsID, $sort=0)
    {
        $sorts = [['Descriptions', 'asc'],];
        $query = DB::table('lk_Transactions-Documents')
                   ->leftJoin('Documents', 'lk_Transactions-Documents.Transactions_id', '=', 'Documents.ID')
                   ->select('*')
                   ->where('Transactions_ID', '=', $transactionsID);
        $query = Model_Parent::scopeNoTest($query,['Documents', 'lk_Transactions-Documents']);
        if ($sort !== false)
        {
            foreach ($sorts[$sort] as $srt)
            {
                $query = $query->orderBy($srt[0], $srt[1]);
            }
        }
        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getTimeline(int $transactionsID)
    {
        $query = DB::table('lk_Transactions-Timeline')
                   ->leftJoin('Timeline', 'Name', '=', 'lk_Transactions-Timeline.MilestoneName')
                   ->select(
                       'lk_Transactions-Timeline.*',
                       'Timeline.Code',
                       'Timeline.mustBeBusinessDay'
                   )
                   ->where('Transactions_ID', '=', $transactionsID)
                   ->where('isActive', true)
                   ->orderBy('MilestoneDate');
        $query = Model_Parent::scopeNoTest($query,['Timeline', 'lk_Transactions-Timeline']);
//Log::debug(['taid'=>$transactionsID, $query->toSql(), 'milestones'=>$query->get()->toArray(), __METHOD__=>__LINE__]);
        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getProperty($transactionsID)
    {
        return self::property($transactionsID);
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function transactionCoordinators($transactionsID, $type = Agent::TYPE_BOTH): Collection
    {
        $query1 = DB::table('Transactions')
                    ->leftJoin('TransactionCoordinators', 'Transactions.buyersTransactionCoordinators_ID', '=', 'TransactionCoordinators.ID')
                    ->selectRaw('TransactionCoordinators.*, \'Buyer\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1,['Transactions', 'TransactionCoordinators']);

        $query2 = DB::table('Transactions')
                    ->leftJoin('TransactionCoordinators', 'Transactions.sellersTransactionCoordinators_ID', '=', 'TransactionCoordinators.ID')
                    ->selectRaw('TransactionCoordinators.*, \'Seller\' as AgentType')
                    ->where('Transactions.ID', '=', $transactionsID);
        $query2 = Model_Parent::scopeNoTest($query2,['Transactions', 'TransactionCoordinators']);

        if ($type == Agent::TYPE_BUYER) return $query1->get();
        if ($type == Agent::TYPE_SELLER) return $query2->get();
        if ($type == Agent::TYPE_BOTH) return $query1->union($query2)->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getNavBuyer($transactionID = 0, $buyerID = null)
    {
        $buttons = [
            'previous'       => null,
            'save'           => null,
            'same'           => null,
            'other'          => null,
            'title'          => null,
            'data-form-type' => 'buyers',
        ];

        $transactionData = TransactionCombine::fullTransaction($transactionID);
        $nextbuyerID     = ($buyerID == null) ? null : Buyer::where('Transactions_ID', $transactionID)->where('ID', '>', $buyerID)->min('ID');

        $buyers = $transactionData['buyer'];
        if ($buyers->count() == 0)
        {
            $buttons['same']         = ['label' => 'Save & Add Buyer']; // todo
            $buttons['same']['next'] = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $nextbuyerID]);
            $buttons['title']        = 'New Buyer'; // todo
        }
        else if ($buyers->count() == 1)
        {
            $buttons['same']          = ['label' => 'Save & Add Buyer']; // todo
            $buttons['title']         = ($buyerID == null) ? 'New Buyer' : 'Edit Buyer'; // todo
            $buttons['same']['label'] = ($buyerID == null) ? 'Save & Edit Buyer' : 'Save & Add Buyer';

            if ($buyerID == null)
            {//you in the current creating seller now
                $nextbuyerID = Buyer::where('Transactions_ID', $transactionID)->orderBy('ID', 'ASC')->first()->ID;
            }
            else $nextbuyerID = null;
            $buttons['same']['next'] = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $nextbuyerID]);
        }
        else if ($buyers->count() > 1)
        {
            $buyerID     = ($buyerID == null) ? Buyer::where('Transactions_ID', $transactionID)->orderBy('ID', 'DESC')->first()->ID : $buyerID;
            $nextbuyerID = Buyer::where('Transactions_ID', $transactionID)->where('ID', '>', $buyerID)->min('ID');

            if ($nextbuyerID == null)
            {//this is the last buyer
                $nextbuyerID = Buyer::where('Transactions_ID', $transactionID)->orderBy('ID', 'ASC')->first()->ID;
                //return $nextbuyerID;
            }
            else $nextbuyerID = Buyer::where('Transactions_ID', $transactionID)->where('ID', '>', $buyerID)->min('ID');

            $buttons['same']         = ['label' => 'Save & Edit Other Buyer']; // todo
            $buttons['same']['next'] = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $nextbuyerID]);
            $buttons['title']        = 'Edit Buyer'; // todo
        }

        $buttons['same']['nextbuyerID'] = $nextbuyerID;

        $sellers = $transactionData['seller'];
        if ($sellers->count() == 0)
        {
            $buttons['other']        = ['label' => 'Save & Add Seller']; // todo
            $buttons['other']['url'] = route('inputSeller', ['transactionID' => $transactionID]);
        }
        else if ($sellers->count() >= 1)
        {
            $sellerID                = Seller::where('Transactions_ID', $transactionID)->orderBy('ID', 'ASC')->first()->ID;
            $buttons['other']        = ['label' => 'Save & Edit Seller']; // todo
            $buttons['other']['url'] = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $sellerID]);
        }
        $buyser_sellers_list_url = [
            'sellers' => [],
            'buyers'  => [],
        ];
        foreach ($sellers as $key)
        {
            $id                                   = $key['ID'];
            $url                                  = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $id]);
            $buyser_sellers_list_url['sellers'][] = $url;
        }
        foreach ($buyers as $key)
        {
            $id                                  = $key['ID'];
            $url                                 = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $id]);
            $buyser_sellers_list_url['buyers'][] = $url;
        }
        $buttons['list_url'] = $buyser_sellers_list_url;
        //$buttons['previous'] = 'send them to the Property input';
        $buttons['sellersList'] = $sellers;
        $buttons['buyersList']  = $buyers;
        return $buttons;
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function getNavSeller($transactionID = 0, $sellerID)
    {
        $buttons = [
            'previous'       => null,
            'save'           => null,
            'same'           => null,
            'other'          => null,
            'title'          => null,
            'data-form-type' => 'sellers',
        ];

        $transactionData = TransactionCombine::fullTransaction($transactionID);
        $nextsellerID    = ($sellerID == null) ? null : Seller::where('Transactions_ID', $transactionID)->where('ID', '>', $sellerID)->min('ID');

        $sellers = $transactionData['seller'];
        if ($sellers->count() == 0)
        {
            $buttons['same']         = ['label' => 'Save & Add Seller']; // todo
            $buttons['same']['next'] = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $nextsellerID]);
            $buttons['title']        = 'New Seller'; // todo
        }
        else if ($sellers->count() == 1)
        {
            $buttons['same']          = ['label' => 'Save & Add Seller']; // todo
            $buttons['title']         = ($sellerID == null) ? 'New Seller' : 'Edit Seller'; // todo
            $buttons['same']['label'] = ($sellerID == null) ? 'Save & Edit Other Seller' : 'Save & Add Seller';

            if ($sellerID == null)
            {//you in the current creating seller now
                $nextsellerID = Seller::where('Transactions_ID', $transactionID)->orderBy('ID', 'ASC')->first()->ID;
            }
            else $nextsellerID = null;
            $buttons['same']['next'] = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $nextsellerID]);
        }
        else if ($sellers->count() > 1)
        {
            $sellerID = ($sellerID == null) ? Seller::where('Transactions_ID', $transactionID)->orderBy('ID', 'DESC')->first()->ID : $sellerID;

            $nextsellerID = Seller::where('Transactions_ID', $transactionID)->where('ID', '>', $sellerID)->min('ID');

            if ($nextsellerID == null)
            {//this is the last seller
                $nextsellerID = Seller::where('Transactions_ID', $transactionID)->orderBy('ID', 'ASC')->first()->ID;
            }
            else $nextsellerID = Seller::where('Transactions_ID', $transactionID)->where('ID', '>', $sellerID)->min('ID');

            $buttons['same']         = ['label' => 'Save & Edit Other Seller']; // todo
            $buttons['same']['next'] = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $nextsellerID]);
            $buttons['title']        = 'Edit Seller'; // todo
        }

        $buttons['same']['nextsellerID'] = $nextsellerID;
        #############################################################################################

        $buyers = $transactionData['buyer'];
        if ($buyers->count() == 0)
        {
            $buttons['other']           = ['label' => 'Save & Add Buyer']; // todo
            $buttons['previous']['url'] = route('inputBuyer', ['transactionID' => $transactionID]);
            $buttons['other']['url']    = route('inputBuyer', ['transactionID' => $transactionID]);
        }

        else if ($buyers->count() >= 1)
        {
            $buttons['other']           = ['label' => 'Save & Edit Buyer']; // todo
            $buyerID                    = Buyer::where('Transactions_ID', $transactionID)->first()->ID;
            $buttons['previous']['url'] = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $buyerID]);
            $buttons['other']['url']    = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $buyerID]);
        }
        // $buttons['previous'] = 'send them to the Property input';
        $buyser_sellers_list_url = [
            'sellers' => [],
            'buyers'  => [],
        ];
        foreach ($sellers as $key)
        {
            $id                                   = $key['ID'];
            $url                                  = route('inputSeller', ['transactionID' => $transactionID, 'sellerID' => $id]);
            $buyser_sellers_list_url['sellers'][] = $url;
        }
        foreach ($buyers as $key)
        {
            $id                                  = $key['ID'];
            $url                                 = route('inputBuyer', ['transactionID' => $transactionID, 'buyerID' => $id]);
            $buyser_sellers_list_url['buyers'][] = $url;
        }
        $buttons['list_url']    = $buyser_sellers_list_url;
        $buttons['sellersList'] = $sellers;
        $buttons['buyersList']  = $buyers;
        return $buttons;

    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function buyers($transactionsID)
    {
        $query = DB::table('Transactions')
                   ->leftJoin('Buyers', 'Transactions.ID', '=', 'Buyers.Transactions_ID')
                   ->select('Buyers.*')
                   ->where('Transactions.ID', '=', $transactionsID);
        $query = Model_Parent::scopeNoTest($query,['Transactions', 'Buyers']);

        return $query->get();
    }

    /**
     * @param $transactionsID
     *
     * @return mixed
     */
    public static function sellers($transactionsID)
    {
        $query = DB::table('Transactions')
                   ->leftJoin('Sellers', 'Transactions.ID', '=', 'Sellers.Transactions_ID')
                   ->select('Sellers.*')
                   ->where('Transactions.ID', '=', $transactionsID);
        $query = Model_Parent::scopeNoTest($query,['Transactions', 'Sellers']);

        return $query->get();
    }


    /**
     * @param $transactionsID
     *
     * @return Collection of Collections with indices: people, buyers, sellers
     */
    public static function keyPeople($transactionsID): Collection
    {
        $select = "
             concat(ba.NameFirst, ' ', ba.NameLast) as `Buyer's Agent` 
           , concat(sa.NameFirst, ' ', sa.NameLast) as `Seller's Agent`
           , concat(btc.NameFirst, ' ', btc.NameLast) as `Buyer's Transaction Coordinator`
           , concat(stc.NameFirst, ' ', stc.NameLast) as `Seller's Transaction Coordinator`
           , concat(Escrows.NameFirst, ' ', Escrows.NameLast) as `Escrow`
           , concat(Loans.NameFirst, ' ', Loans.NameLast) as `Loan`
           , concat(Titles.NameFirst, ' ', Titles.NameLast) as `Title`
        
           , ba.PrimaryPhone as `BA Phone`
           , sa.PrimaryPhone as `SA Phone`
           , btc.PrimaryPhone as `BTC Phone`
           , stc.PrimaryPhone as `STC Phone`
           , Escrows.PrimaryPhone as `E Phone`
           , Loans.PrimaryPhone as `L Phone`
           , Titles.PrimaryPhone as `T Phone`
        
           , ba.Email as `BA Email`
           , sa.Email as `SA Email`
           , btc.Email as `BTC Email`
           , stc.Email as `STC Email`
           , Escrows.Email as `E Email`
           , Loans.Email as `L Email`
           , Titles.Email as `T Email`
           
           , concat(
                coalesce(ba.Street1, ''),' ',
                coalesce(ba.Street2,''),' ',
                coalesce(ba.Unit,''),' ',
                coalesce(ba.City,''),' ',
                coalesce(ba.State,''),' ',
                coalesce(ba.Zip,'')) as `BA Address`           
           , concat(
                coalesce(sa.Street1, ''),' ',
                coalesce(sa.Street2,''),' ',
                coalesce(sa.Unit,''),' ',
                coalesce(sa.City,''),' ',
                coalesce(sa.State,''),' ',
                coalesce(sa.Zip,'')) as `SA Address`
           , concat(
                coalesce(btc.Street1, ''),' ',
                coalesce(btc.Street2,''),' ',
                coalesce(btc.Unit,''),' ',
                coalesce(btc.City,''),' ',
                coalesce(btc.State,''),' ',
                coalesce(btc.Zip,'')) as `BTC Address`
           , concat(
                coalesce(stc.Street1, ''),' ',
                coalesce(stc.Street2,''),' ',
                coalesce(stc.Unit,''),' ',
                coalesce(stc.City,''),' ',
                coalesce(stc.State,''),' ',
                coalesce(stc.Zip,'')) as `STC Address`
            , concat(
                coalesce(Escrows.Street1, ''),' ',
                coalesce(Escrows.Street2,''),' ',
                coalesce(Escrows.Unit,''),' ',
                coalesce(Escrows.City,''),' ',
                coalesce(Escrows.State,''),' ',
                coalesce(Escrows.Zip,'')) as `E Address`
            , concat(
                coalesce(Loans.Street1, ''),' ',
                coalesce(Loans.Street2,''),' ',
                coalesce(Loans.Unit,''),' ',
                coalesce(Loans.City,''),' ',
                coalesce(Loans.State,''),' ',
                coalesce(Loans.Zip,'')) as `L Address`
            , concat(
                coalesce(Titles.Street1, ''),' ',
                coalesce(Titles.Street2,''),' ',
                coalesce(Titles.Unit,''),' ',
                coalesce(Titles.City,''),' ',
                coalesce(Titles.State,''),' ',
                coalesce(Titles.Zip,'')) as `T Address`
           ";
        $select = str_replace("\r\n", "\n", $select);

        $query1 = DB::table('Transactions')
                    ->leftJoin('Agents as ba', 'Transactions.BuyersAgent_ID', '=', 'ba.ID')
                    ->leftJoin('Agents as sa', 'Transactions.SellersAgent_ID', '=', 'sa.ID')
                    ->leftJoin('TransactionCoordinators as btc', 'BuyersTransactionCoordinators_ID', '=', 'btc.ID')
                    ->leftJoin('TransactionCoordinators as stc', 'SellersTransactionCoordinators_ID', '=', 'stc.ID')
                    ->leftJoin('Escrows', 'Transactions.Escrows_ID', '=', 'Escrows.ID')
                    ->leftJoin('Loans', 'Transactions.Loans_ID', '=', 'Loans.ID')
                    ->leftJoin('Titles', 'Transactions.Titles_ID', '=', 'Titles.ID')
                    ->selectRaw($select)
                    ->where('Transactions.ID', '=', $transactionsID);
        $query1 = Model_Parent::scopeNoTest($query1,['Transactions', 'ba', 'sa', 'btc', 'stc', 'Escrows', 'Loans', 'Titles']);

        $transaction = $query1->get()->first(); // run query
        $transaction = _Convert::toArray($transaction);

        $buyers = Buyer::selectRaw('concat(NameFirst, \' \', NameLast) as `Buyer`, PrimaryPhone as `B Phone`, Email as `B Email`
                   , concat(
                            coalesce(Street1, \'\'),\' \',
                            coalesce(Street2,\'\'),\' \',
                            coalesce(Unit,\'\'),\' \',
                            coalesce(City,\'\'),\' \',
                            coalesce(State,\'\'),\' \',
                            coalesce(Zip,\'\')) as `B Address`
                    ')
                     ->where('Transactions_ID', $transactionsID)
                     ->orderBy('ID')->get();

        if (count($buyers) == 0)
        {}
        else
        {
            foreach ($buyers as $idx=>$buyer)
            {
                foreach ($buyer->toArray() as $fld=>$val)
                {
                    $transaction[$fld . '_' . $idx] = $val;
                }
            }
        }

        $sellers = Seller::selectRaw('concat(NameFirst, \' \', NameLast) as `Seller`, PrimaryPhone as `S Phone`, Sellers.Email as `S Email`
                   , concat(
                            coalesce(Street1, \'\'),\' \',
                            coalesce(Street2,\'\'),\' \',
                            coalesce(Unit,\'\'),\' \',
                            coalesce(City,\'\'),\' \',
                            coalesce(State,\'\'),\' \',
                            coalesce(Zip,\'\')) as `S Address`
                    ')
                     ->where('Transactions_ID', $transactionsID)
                     ->orderBy('ID')->get();

        if (count($sellers) == 0)
        {}
        else
        {
            foreach ($sellers as $idx=>$seller)
            {
                foreach ($seller->toArray() as $fld=>$val)
                {
                    $transaction[$fld . '_' . $idx] = $val;
                }
            }
        }
 //       ddd(['transaction'=>$transaction]);

        return  collect($transaction);
    }

    /**
     * @param $transactionsID
     *
     * @return Collection of Collections with indices: people, buyers, sellers
     */
    public static function files($transactionsID): Collection
    {
        return collect(['status' => 'Not yet working.']);
    }

    /**
     * @param $transactionsID
     *
     * @return Collection of Collections with indices: people, buyers, sellers
     */
    public static function toDo($transactionsID): Collection
    {
        return collect(['status' => 'Not yet working.']);
    }

    /**
     * @param $transactionsID
     *
     * @return Collection of Collections with indices: people, buyers, sellers
     */
    public static function activity($transactionsID): Collection
    {
        return collect(['status' => 'Not yet working.']);
    }

    public static function transactionList($role=null, $roleID=false, $userID=false, $getTransactions = false)
    {
        $roles          = Config::get('constants.USER_ROLE');

        $bindings[] = $roleID;

        $where = [$roles['BUYER']                           => 'b.ID = ?',
                  $roles['SELLER']                          => 's.ID = ?',
                  $roles['BUYERS_AGENT']                    => 'trans.BuyersAgent_ID = ?',
                  $roles['SELLERS_AGENT']                   => 'trans.SellersAgent_ID = ?',
                  $roles['AGENT']                           => '(trans.BuyersAgent_ID = ? OR trans.SellersAgent_ID = ?)',
                  $roles['BUYERS_TRANSACTION_COORDINATOR']  => 'trans.BuyersTransactionCoordinators_ID = ?',
                  $roles['SELLERS_TRANSACTION_COORDINATOR'] => 'trans.SellersTransactionCoordinators_ID = ?',
                  $roles['TRANSACTION_COORDINATOR']         => 'trans.BuyersTransactionCoordinators_ID = ? OR ' .
                                                               'trans.SellersTransactionCoordinators_ID = ?',
                  $roles['ESCROW']                          => 'trans.Escrows_ID = ?',
                  $roles['TITLE']                           => 'trans.Titles_ID = ?',
                  $roles['LOAN']                            => 'trans.Loans_ID = ?',
                  $roles['GUEST']                           => '',
                  '*'                                       => '',
                  null                                      => '',
        ];

        if (!isset($where[$role]))
        {
            return ['type'  => 'genericError',
                    'error' => ['info' => [
                        'title'    => 'The user role is invalid',
                        'class'    => __CLASS__,
                        'function' => __FUNCTION__,
                        'line'     => __LINE__,
                        'message'  => 'role = ' . (is_null($role) ? 'NULL' : $role),
                    ],
                    ],
            ];
        }

        if ($role == $roles['TRANSACTION_COORDINATOR']) $bindings[] = $roleID;
        if ($role == $roles['AGENT']) $bindings[] = $roleID;

        if ($userID)
        {
            $whereUser = (!empty($where[$role]) ? ' OR ' : null) . ' trans.CreatedByUsers_ID = ? OR trans.OwnedByUsers_ID = ? ';
            $bindings[] = $userID;
            $bindings[] = $userID;
        }
        else $whereUser = null;

        $select = 'trans.ID
                    , lu_ur.Display as `Client Role`
                    , IF(trans.ClientRole = "b", concat(b.NameFirst, " ", b.NameLast) 
                    , IF(trans.ClientRole = "s", concat(s.NameFirst, " ", s.NameLast) 
                    , IF(trans.ClientRole = "ba", concat(ba.NameFirst, " ", ba.NameLast)
                    , IF(trans.ClientRole = "sa", concat(sa.NameFirst, " ", sa.NameLast), "Unknown")))) Client
                    
                    , IF(trans.ClientRole = "b", concat(ba.NameFirst, " ", ba.NameLast)
                    , IF(trans.ClientRole = "s", concat(sa.NameFirst, " ", sa.NameLast), "Unknown")) Agent
                                        
                    , IF(trans.ClientRole = "b", concat(btc.NameFirst, " ", btc.NameLast)
                    , IF(trans.ClientRole = "s", concat(stc.NameFirst, " ", stc.NameLast), "Unknown")) TC
                    
                    , concat(p.Street1, ", ", p.City, ", ", p.State, " ", p.Zip) Address 
                    , trans.Status
                    , p.Street1
                    , p.Street2
                    , p.City
                    , p.State
                    , p.Zip
                ';
        $select = str_replace(['\r','\n'], ' ', $select);

        //
        // ... Run query for transactions
        //
        $query = DB::table('Transactions AS trans')
                   ->leftJoin('Buyers AS b', 'trans.ID', '=', 'b.Transactions_ID')
                   ->leftJoin('Sellers AS s', 'trans.ID', '=', 's.Transactions_ID')
                   ->leftJoin('Agents AS ba', 'trans.BuyersAgent_ID', '=', 'ba.ID')
                   ->leftJoin('Agents AS sa', 'trans.SellersAgent_ID', '=', 'sa.ID')
                   ->leftJoin('TransactionCoordinators AS btc', 'trans.BuyersTransactionCoordinators_ID', '=', 'btc.ID')
                   ->leftJoin('TransactionCoordinators AS stc', 'trans.SellersTransactionCoordinators_ID', '=', 'stc.ID')
                   ->leftJoin('lu_UserRoles AS lu_ur', 'trans.ClientRole', '=', 'lu_ur.Value')
                   ->leftJoin('Properties AS p', 'trans.Properties_ID', '=', 'p.ID')
                   ->selectRaw($select);
// ... Adds code to Only Select records with isTest NOT True
        $query = Model_Parent::scopeNoTest($query, ['trans', 'ba', 'sa', 'btc', 'stc', 'b', 's', 'p']);

//        $query->whereRaw('(' . $where[$role].$whereUser . ')', $bindings)->distinct();

        $w = $where[$role].$whereUser;
        $query->where(function ($q) use ($query, $w, $bindings)
           {$q->whereRaw($w, $bindings)->distinct();});
//dd(['binding'=>$bindings, $query->toSql()]);

        $transactions = $query->get();


        //
        // ... add dates
        $ayTransactions = [];
        $mimeTypes = [
            'jpeg',
            'jpg',
            'png'
        ];
        foreach ($transactions as $idx => $transaction)
        {
            if (!isset($transaction->ID)) continue;

            $timeline = lk_Transactions_Timeline::timelineByTransactionId($transaction->ID);
            if ($timeline->isEmpty())
            {
                $ta = DB::table('Transactions')->find($transaction->ID);
                $ms = MilestoneCombine::saveTransactionTimeline($transaction->ID,
                    $ta->DateAcceptance, $ta->EscrowLength);
                $timeline = lk_Transactions_Timeline::timelineByTransactionId($transaction->ID);
            }

            $milestones = [];
            foreach ($timeline as $milestone)
            {
                $milestones[strtoupper(str_replace(' ', '_', $milestone->MilestoneName))]
                    = $milestone->MilestoneDate;
            }

            $constMilestones    = Config::get('constants.MILESTONES');
            $constMilestonesMap = Config::get('constants.MILESTONES_DISPLAY_MAP');
            $propertyImage      = Config::get('constants.DIRECTORIES.propertyImages');
            $transactions[$idx]->PropertyImagePath = NULL;
            $localPath = public_path('images/'.$propertyImage.$transaction->ID);
            if(File::isDirectory($localPath))
            {
                $url = _Locations::url('images',$propertyImage.$transaction->ID.'/');
                $firstFile = scandir($localPath)[2] ?? '';
                $transactions[$idx]->PropertyImagePath = $url.$firstFile;
            }

            $list = [];
            foreach (Config::get('constants.MILESTONES') as $msName => $ms)
            {
                if (!isset($milestones[$msName])) continue;
                $msIndex = $constMilestones[strtoupper($msName)];

                $list[$constMilestonesMap[$msIndex]] = $milestones[$msName];
            }

            $ayTransactions[$idx] = array_merge(_Convert::toArray($transaction), $list);
        }

        $groupedTransactions = [];
        $otc                 = [];
        foreach ($ayTransactions as $index => $trans)
        {
            $otcID = array_search($trans['ID'], $otc);
            if ($otcID !== false)
            {
                if (trim($groupedTransactions[$otcID]['Client']) != trim($trans['Client']))
                {
                    $groupedTransactions[$otcID]['Client'] .= " \n " . $trans['Client'];
                }
            }
            else
            {
                $otc[$index]                 = $trans['ID'];
                $groupedTransactions[$index] = $trans;
            }
        }
        if($getTransactions) return $groupedTransactions;

        return ['type'    => 'success',
                'success' => ['info' => [
                    'collection' => collect($groupedTransactions ?? []),
                ],
                ],
        ];
    }

    public static function transactionsByUser($userID = false)
    {
        $where = ['b.Users_ID = ?',
                  's.Users_ID = ?',
                  'ba.Users_ID = ?',
                  'sa.Users_ID = ?',
                  'btc.Users_ID = ?',
                  'stc.Users_ID = ?',
                  'e.Users_ID = ?',
                  't.Users_ID = ?',
                  'l.Users_ID = ?',
                  'trans.CreatedByUsers_ID = ?',
                  'trans.OwnedByUsers_ID = ? ',
        ];

        $bindings = array_fill(0, count($where), $userID);

        $select = 'trans.ID
                    , concat(p.Street1, ", ", p.City, ", ", p.State, " ", p.Zip) Address 
                    , concat(b.NameFirst, " ", b.NameLast) as Buyer 
                    , concat(s.NameFirst, " ", s.NameLast) as Seller 
                    , concat(ba.NameFirst, " ", ba.NameLast) as BuyerAgent
                    , concat(sa.NameFirst, " ", sa.NameLast) as SellerAgent
                                        
                    , concat(btc.NameFirst, " ", btc.NameLast) as BuyersTC
                    , concat(stc.NameFirst, " ", stc.NameLast) as SellersTC
                    
                    , concat(e.NameFirst, " ", e.NameLast) as Escrow 
                    , concat(l.NameFirst, " ", l.NameLast) as Loan
                    , concat(t.NameFirst, " ", t.NameLast) as Title
                    
                    , concat(c.NameFirst, " ", c.NameLast) as TransactionCreator 
                    , concat(o.NameFirst, " ", o.NameLast) as TransactionOwner
                    
                    , trans.Status
                    , trans.DateOfferPrepared
                    , trans.DateAcceptance
                    , trans.DateEnd
                    
                    , b.Users_ID as Buyer_ID
                    , s.Users_ID as Seller_ID
                    , ba.Users_ID as BuyerAgent_ID
                    , sa.Users_ID as SellerAgent_ID
                    , btc.Users_ID as BuyersTC_ID
                    , stc.Users_ID as SellersTC_ID
                    , e.Users_ID as Escrow_ID
                    , l.Users_ID as Loan_ID
                    , t.Users_ID as Title_ID

                    , trans.CreatedByUsers_ID as TransactionCreator_ID
                    , trans.OwnedByUsers_ID as TransactionOwner_ID
                ';
        $select = str_replace(["\r", "\n"], '', $select);

        //
        // ... Run query for transactions
        //
        $query = DB::table('Transactions AS trans')
                   ->leftJoin('Buyers AS b', 'trans.ID', '=', 'b.Transactions_ID')
                   ->leftJoin('Sellers AS s', 'trans.ID', '=', 's.Transactions_ID')
                   ->leftJoin('Agents AS ba', 'trans.BuyersAgent_ID', '=', 'ba.ID')
                   ->leftJoin('Agents AS sa', 'trans.SellersAgent_ID', '=', 'sa.ID')
                   ->leftJoin('TransactionCoordinators AS btc', 'trans.BuyersTransactionCoordinators_ID', '=', 'btc.ID')
                   ->leftJoin('TransactionCoordinators AS stc', 'trans.SellersTransactionCoordinators_ID', '=', 'stc.ID')
                   ->leftJoin('Escrows AS e', 'trans.Escrows_ID', '=', 'e.ID')
                   ->leftJoin('Loans AS l', 'trans.Loans_ID', '=', 'l.ID')
                   ->leftJoin('Titles AS t', 'trans.Titles_ID', '=', 't.ID')
                   ->leftJoin('users AS c', 'trans.CreatedByUsers_ID', '=', 'c.ID')
                   ->leftJoin('users AS o', 'trans.OwnedByUsers_ID', '=', 'o.ID')
                   ->leftJoin('Properties AS p', 'trans.Properties_ID', '=', 'p.ID')
                   ->selectRaw($select);

        $query->whereRaw('(' . implode(' OR ', $where) . ')', $bindings)
              ->orderBy('trans.ID')->orderBy('b.ID')->orderBy('s.ID')
              ->distinct();
        $query = Model_Parent::scopeNoTest($query,
            ['trans', 'b', 's', 'ba', 'sa', 'btc', 'stc', 'e', 'l', 't', 'c', 'o', 'p']);
        $transactions = $query->get();

        foreach ($transactions as $i => $transaction)
        {
            $tmp = _Convert::toArray($transaction);
            foreach ($tmp as $fld => $val)
            {
                if (substr($fld, -3) != '_ID') continue;
                unset($tmp[$fld]);
                if ($val != $userID) continue;
                $tmp[substr($fld, 0, -3)] .= ' **';
            }
            $transactions[$i] = $tmp;
        }
        return $transactions;
    }
    public static function transactionSearch($role = 'tc', $roleID, $search)
    {
        $roles          = Config::get('constants.USER_ROLE');
        $ayTransactions = [];

        $bindings[] = $roleID;

        $where = [$roles['BUYER']                           => 'b.ID = ?',
                  $roles['SELLER']                          => 's.ID = ?',
                  $roles['BUYERS_AGENT']                    => 'trans.BuyersAgent_ID = ?',
                  $roles['SELLERS_AGENT']                   => 'trans.SellersAgent_ID = ?',
                  $roles['BUYERS_TRANSACTION_COORDINATOR']  => 'trans.BuyersTransactionCoordinators_ID = ?',
                  $roles['SELLERS_TRANSACTION_COORDINATOR'] => 'trans.SellersTransactionCoordinators_ID = ?',
                  $roles['TRANSACTION_COORDINATOR']         => '(trans.BuyersTransactionCoordinators_ID = ? OR ' .
                                                               'trans.SellersTransactionCoordinators_ID = ?)',
                  $roles['ESCROW']                          => 'trans.Escrows_ID = ?',
                  $roles['TITLE']                           => 'trans.Titles_ID = ?',
                  $roles['LOAN']                            => 'trans.Loans_ID = ?',
        ];

        if (!isset($where[$role]))
        {
            return ['type'  => 'genericError',
                    'error' => ['info' => [
                        'title'    => 'The user role is invalid',
                        'class'    => __CLASS__,
                        'function' => __FUNCTION__,
                        'line'     => __LINE__,
                        'message'  => 'role = ' . (is_null($role) ? 'NULL' : $role),
                    ],
                    ],
            ];
        }

        if ($role == $roles['TRANSACTION_COORDINATOR']) $bindings[] = $roleID;

        // ... Query to get transaction IDs

        $select = 'trans.ID';
        //
        // ... Run query for transactions
        //
        $query = DB::table('Transactions AS trans')
                   ->leftJoin('Buyers AS b', 'trans.ID', '=', 'b.Transactions_ID')
                   ->leftJoin('Sellers AS s', 'trans.ID', '=', 's.Transactions_ID')
                   ->leftJoin('Agents AS ba', 'trans.BuyersAgent_ID', '=', 'ba.ID')
                   ->leftJoin('Agents AS sa', 'trans.SellersAgent_ID', '=', 'sa.ID')
                   ->leftJoin('TransactionCoordinators AS btc', 'trans.BuyersTransactionCoordinators_ID', '=', 'btc.ID')
                   ->leftJoin('TransactionCoordinators AS stc', 'trans.SellersTransactionCoordinators_ID', '=', 'stc.ID')
                   ->leftJoin('lu_UserRoles AS lu_ur', 'trans.ClientRole', '=', 'lu_ur.Value')
                   ->leftJoin('Properties AS p', 'trans.Properties_ID', '=', 'p.ID')
                   ->selectRaw($select);

        $query->whereRaw('('.$where[$role].')', $bindings);

        $query->where(function ($q) use ($search)
        {
            $q->orwhere('p.Street1', 'LIKE', $search)
              ->orwhere('p.City', 'LIKE', $search)
              ->orwhere('b.NameFull', 'LIKE', $search)
              ->orwhere('b.NameFirst', 'LIKE', $search)
              ->orwhere('b.NameLast', 'LIKE', $search)
              ->orwhere('s.NameFull', 'LIKE', $search)
              ->orwhere('s.NameFirst', 'LIKE', $search)
              ->orwhere('s.NameLast', 'LIKE', $search)
              ->orwhere('ba.NameFull', 'LIKE', $search)
              ->orwhere('ba.NameFirst', 'LIKE', $search)
              ->orwhere('ba.NameLast', 'LIKE', $search)
              ->orwhere('sa.NameFull', 'LIKE', $search)
              ->orwhere('sa.NameFirst', 'LIKE', $search)
              ->orwhere('sa.NameLast', 'LIKE', $search)
              ->orwhere('btc.NameFull', 'LIKE', $search)
              ->orwhere('btc.NameFirst', 'LIKE', $search)
              ->orwhere('btc.NameLast', 'LIKE', $search)
              ->orwhere('stc.NameFull', 'LIKE', $search)
              ->orwhere('stc.NameFirst', 'LIKE', $search)
              ->orwhere('stc.NameLast', 'LIKE', $search);
        });
        $query = Model_Parent::scopeNoTest($query,
            ['trans', 'b', 's', 'ba', 'sa', 'btc', 'stc', 'lu_ur', 'p']);
        Log::debug(['sql'=>$query->toSql(), __METHOD__=>__LINE__]);
        $transactionIDs = $query->distinct()->get();
        if (!sizeof($transactionIDs))
        {
            return 0;
        }
        $ids = [];
        foreach ($transactionIDs as $rec)
        {
            $ids[] = $rec->ID;
        }

        // ... Query to get transaction list records

        $select = 'trans.ID
                    , lu_ur.Display as `Client Role`
                    , IF(trans.ClientRole = "b", concat(b.NameFirst, " ", b.NameLast) 
                    , IF(trans.ClientRole = "s", concat(s.NameFirst, " ", s.NameLast) 
                    , IF(trans.ClientRole = "ba", concat(ba.NameFirst, " ", ba.NameLast)
                    , IF(trans.ClientRole = "sa", concat(sa.NameFirst, " ", sa.NameLast), "Unknown")))) Client
                    
                    , IF(trans.ClientRole = "b", concat(ba.NameFirst, " ", ba.NameLast)
                    , IF(trans.ClientRole = "s", concat(sa.NameFirst, " ", sa.NameLast), "Unknown")) Agent
                                        
                    , IF(trans.ClientRole = "b", concat(btc.NameFirst, " ", btc.NameLast)
                    , IF(trans.ClientRole = "s", concat(stc.NameFirst, " ", stc.NameLast), "Unknown")) TC
                    
                    , concat(p.Street1, ", ", p.City, ", ", p.State, " ", p.Zip) Address 
                ';
        $select = str_replace("\r\n", "\n", $select);

        //
        // ... Run query for transactions
        //
        $query = DB::table('Transactions AS trans')
                   ->leftJoin('Buyers AS b', 'trans.ID', '=', 'b.Transactions_ID')
                   ->leftJoin('Sellers AS s', 'trans.ID', '=', 's.Transactions_ID')
                   ->leftJoin('Agents AS ba', 'trans.BuyersAgent_ID', '=', 'ba.ID')
                   ->leftJoin('Agents AS sa', 'trans.SellersAgent_ID', '=', 'sa.ID')
                   ->leftJoin('TransactionCoordinators AS btc', 'trans.BuyersTransactionCoordinators_ID', '=', 'btc.ID')
                   ->leftJoin('TransactionCoordinators AS stc', 'trans.SellersTransactionCoordinators_ID', '=', 'stc.ID')
                   ->leftJoin('lu_UserRoles AS lu_ur', 'trans.ClientRole', '=', 'lu_ur.Value')
                   ->leftJoin('Properties AS p', 'trans.Properties_ID', '=', 'p.ID')
                   ->selectRaw($select);

        $query->whereRaw('(trans.ID in (' . implode(',', $ids) . '))');
        $query = Model_Parent::scopeNoTest($query,
            ['trans', 'b', 's', 'ba', 'sa', 'btc', 'stc', 'lu_ur', 'p']);
        $transactions = $query->distinct()->get();
        //
        // ... add dates
        $ayTransaction = [];
        foreach ($transactions as $idx => $transaction)
        {
            if (!isset($transaction->ID)) continue;

            $qry = DB::table('lk_Transactions-Timeline')
                     ->leftJoin('Transactions', 'Transactions.ID', '=', '`lk_Transactions-Timeline`.Transactions_ID')
                     ->select('Transactions.ID', '`lk_Transactions-Timeline`.*')
                     ->whereRaw('Transactions.ID = ?', [$transaction->ID])
                     ->where('isActive', true)
                     ->orderBy('`lk_Transactions-Timeline`.MilestoneDate');
            $qry = Model_Parent::scopeNoTest($qry,['lk_Transactions-Timeline', 'Transactions']);

            $qry = str_replace('```', '`', $qry->toSql());

            $timeline = DB::select($qry, [$transaction->ID]);

            foreach ($timeline as $milestone)
            {
                $milestones[strtoupper(str_replace(' ', '_', $milestone->MilestoneName))]
                    = $milestone->MilestoneDate;
            }

            $constMilestones    = Config::get('constants.MILESTONES');
            $constMilestonesMap = Config::get('constants.MILESTONES_DISPLAY_MAP');

            $list = [];
            foreach (Config::get('constants.MILESTONES') as $msName => $ms)
            {
                if (!isset($milestones[$msName])) continue;
                $msIndex = $constMilestones[strtoupper($msName)];

                $list[$constMilestonesMap[$msIndex]] = $milestones[$msName];
            }

            $ayTransactions[$idx] = array_merge(_Convert::toArray($transaction), $list);
        }

        $groupedTransactions = [];
        $otc                 = [];
        foreach ($ayTransactions as $index => $trans)
        {
            $otcID = array_search($trans['ID'], $otc);
            if ($otcID !== false)
            {
                $groupedTransactions[$otcID]['Client'] .= " \n " . $trans['Client'];
            }
            else
            {
                $otc[$index]                 = $trans['ID'];
                $groupedTransactions[$index] = $trans;
            }
        }

        return ['type'    => 'success',
                'success' => ['info' => [
                    'collection' => collect($groupedTransactions ?? []),
                ],
                ],
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * $param int $transactionsID
     *
     * @return array \Illuminate\Http\Response
     */
    public static function fullTransaction(int $transactionsID)
    {
        if (empty($transactionsID)) return false;

        $indices = ['buyer'=>'b', 'buyersAgent'=>'ba', 'buyersTC'=>'btc', 'seller'=>'s', 'sellersAgent'=>'sa', 'sellersTC'=>'stc', 'escrow'=>'e',
                    'title'=>'t', 'loan'=>'l'];

        $transaction = Transaction::find($transactionsID);

        if (is_null($transaction)) return []; // ['taID'=>$transactionsID];
        $t =   ['transaction'  => $transaction,
                'property'     => Property::where('ID', $transaction->Properties_ID)->get(),
                'buyer'        => Buyer::where('Transactions_ID', $transactionsID)->get(),
                'buyersAgent'  => Agent::where('ID', $transaction->BuyersAgent_ID)->get(),
                'buyersTC'     => TransactionCoordinator::where('ID', $transaction->BuyersTransactionCoordinators_ID)->get(),
                'seller'       => Seller::where('Transactions_ID', $transactionsID)->get(),
                'sellersAgent' => Agent::where('ID', $transaction->SellersAgent_ID)->get(),
                'sellersTC'    => TransactionCoordinator::where('ID', $transaction->SellersTransactionCoordinators_ID)->get(),
                'timeline'     => TransactionCombine::getTimeline($transactionsID),
                'escrow'       => Escrow::where('ID', $transaction->Escrows_ID)->get(),
                'title'        => Title::where('ID', $transaction->Titles_ID)->get(),
                'loan'         => Loan::where('ID', $transaction->Loans_ID)->get(),
        ];

        $users = [];
        foreach ($indices as $index=>$code)
        {
            if (count($t[$index]) == 0) continue;
            foreach($t[$index] as $rec)
            {
                $users[] = ['role'=>$code, 'roleID'=>$rec->ID ?? 0, 'userID'=>$rec->Users_ID ?? null];
            }
        }
        $t['users'] = $users;

        return $t;
    }

    public static function getUsers($transactionID)
    {
        $users       = [];
        $transaction = self::fullTransaction($transactionID);

        foreach ($transaction as $src => $collection)
        {
            foreach ($collection as $col)
            {
                $uid = $col->Users_ID ?? 0;
                $id = $col->ID ?? 0;
                if (isset(self::$indicesToRoleCode[$src])) if (is_null(self::$indicesToRoleCode[$src])) continue;
                else continue;
                if ($id != 0)
                {
                    if (($col->table ?? null) == 'Agents')
                    {
                        $qry = DB::table('BrokerageOffices')
                                 ->select('Name')
                                 ->where('ID', $col->BrokerageOffices_ID)
                                 ->limit(1);
                        $qry = Model_Parent::scopeNoTest($qry);

                        $brokerageOfficeName = $qry->get();

                        if (empty($brokerageOfficeName->first()))
                        {
                            $col->Company = null;
                        }
                        else $col->Company = $brokerageOfficeName->first()->Name;
                    }

                    if ($src != 'timeline')
                    {
                        $users[self::$indicesToRoleCode[$src]][] = ['ID'             => $col->ID,
                                                                    'Users_ID'       => $uid,
                                                                    'NameFirst'      => $col->NameFirst,
                                                                    'NameLast'       => $col->NameLast,
                                                                    'PrimaryPhone'   => $col->PrimaryPhone,
                                                                    'SecondaryPhone' => $col->SecondaryPhone,
                                                                    'Company'        => $col->Company,
                                                                    'Email'          => $col->Email,];
                    }
                }
            }
        }
        return $users;
    }
    /**
     * Show the form for creating a new resource.
     *
     * $param int $transactionData expects to be the output from self::fullTransaction
     *
     * @return array \Illuminate\Http\Response
     */
    public static function isCreated($transactionData)
    {
        $legend = ['p'  => 'property', 'b' => 'buyer', 's' => 'seller',
                   'ba' => 'buyersAgent', 'sa' => 'sellersAgent', 'btc' => 'buyersTC', 'stc' => 'sellersTC',
                   'e'  => 'escrow', 'l' => 'loan', 't' => 'title',];

        if (empty($transactionData)) return false;

        $isCreated['d'] = ($transactionData['transaction']->PurchasePrice > 0);
        foreach ($legend as $key => $index)
        {
            $isCreated[$key] = !($transactionData[$index]->isEmpty());
        }
        return $isCreated;
    }

    /**
     * Show the form for creating a new resource.
     *
     * $param int $transactionsID
     *
     * @return array \Illuminate\Http\Response
     */

    public static function getHomesumer($role, int $personID)
    {
        if ($role == Config::get('constants.USER_ROLE.BUYER')) return Buyer::where('ID', $personID)->get();
        if ($role == Config::get('constants.USER_ROLE.SELLER')) return Seller::where('ID', $personID)->get();
    }

    /**
     * @param int  $transactionID
     * @param null $userID
     *
     * @return mixed
     */
    public static function getBuyers(int $transactionID, $userID=null)
    {
        $query = Buyer::where('Transactions_ID', $transactionID);
        if (!empty($userID)) $query = $query->where('Users_ID', $userID);
        return $query->get();
    }

    /**
     * @param int  $transactionID
     * @param null $userID
     *
     * @return mixed
     */
    public static function getSellers(int $transactionID, $userID=null)
    {
        $query = Seller::where('Transactions_ID', $transactionID);
        if (!empty($userID)) $query = $query->where('Users_ID', $userID);
        return $query->get();
    }

    /**
     * Return Escrows record
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getEscrow(int $transactionID)
    {
        $query = DB::table('Transactions')
                   ->leftJoin('Escrows', 'Transactions.Escrows_ID', '=', 'Escrows.ID')
                   ->select('Escrows.*')
                   ->where('Transactions.ID', '=', $transactionID);
        $query = Model_Parent::scopeNoTest($query, ['Transactions', 'Escrows']);

        return $query->get();
    }

    /**
     * Return Client Role
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getClientRole(int $transactionID)
    {
        $query = DB::table('Transactions')
                   ->select('ClientRole')
                   ->where('Transactions.ID', '=', $transactionID);
        $query = Model_Parent::scopeNoTest($query );
        return $query->get()->first()->ClientRole;
    }

    /**
     * Return Client Data
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getClient(int $transactionID)
    {
        $person = self::getPerson(self::getClientRole($transactionID), $transactionID);
        return ['role' => $person['role'], 'record' => $person['data']];
    }

    /**
     * Return ShareRoom record
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getShareRoom(int $transactionID, $onlyAvailable=false)
    {
        $query = DB::table('ShareRooms')
                   ->where('Transactions.ID', '=', $transactionID);
        $query = Model_Parent::scopeNoTest($query);

        $room = $query->get();

        if (count($room) == 0) return false;

        if (ShareRoom::isPlaceholder($room)) return 0;

        if ($onlyAvailable)
        {
            foreach ($room as $idx=>$item)
            {
                if (!$item->isAvailable) $room->forget($idx);
            }
        }
        return $room;
    }

    /**
     * Return Titles record
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getTitle(int $transactionID)
    {
        $query = DB::table('Transactions')
                   ->leftJoin('Titles', 'Transactions.Titles_ID', '=', 'Titles.ID')
                   ->select('Titles.*')
                   ->where('Transactions.ID', '=', $transactionID);
        $query = Model_Parent::scopeNoTest($query, ['Transactions', 'Titles']);

        return $query->get();
    }

    /**
     * Return Loans record
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getLoan(int $transactionID)
    {
        $query = DB::table('Transactions')
                   ->leftJoin('Loans', 'Transactions.Loans_ID', '=', 'Loans.ID')
                   ->select('Loans.*')
                   ->where('Transactions.ID', '=', $transactionID);
        $query = Model_Parent::scopeNoTest($query, ['Transactions', 'Loans']);

        return $query->get();
    }

    public static function getUserIDs(int $transactionID)
    {
        $query = DB::table('Transactions')
            ->leftJoin('Agents as ba', 'Transactions.BuyersAgent_ID', '=', 'ba.ID')
            ->leftJoin('Agents as sa', 'Transactions.SellersAgent_ID', '=', 'sa.ID')
            ->leftJoin('Agents as btc', 'Transactions.BuyersTransactionCoordinators_ID', '=', 'btc.ID')
            ->leftJoin('Agents as stc', 'Transactions.BuyersTransactionCoordinators_ID', '=', 'stc.ID')
            ->leftJoin('Agents as e', 'Transactions.Escrows_ID', '=', 'e.ID')
            ->leftJoin('Agents as l', 'Transactions.Loans_ID', '=', 'l.ID')
            ->leftJoin('Agents as t', 'Transactions.Titles_ID', '=', 't.ID')

           ->select('Transactions.ID as transactionID',
               'ba.Users_ID as ba',    'sa.Users_ID as sa',
               'btc.Users_ID as btc',  'stc.Users_ID as stc',
               'e.Users_ID as e',      'l.Users_ID as l',    't.Users_ID as t' )

           ->where('Transactions.ID', '=', $transactionID);
        $query = Model_Parent::scopeNoTest($query, ['Transactions', 'ba', 'sa', 'btc', 'stc', 'e', 'l', 't']);

        $record = _Convert::toArray($query->get()->first());

        $query = DB::table('Buyers')
                   ->select('Users_ID')
                   ->where('Transactions_ID', $transactionID);
        $query = Model_Parent::scopeNoTest($query);

        $buyers = $query->get();


        $query = DB::table('Sellers')
                   ->select('Users_ID')
                   ->where('Transactions_ID', $transactionID);
        $query = Model_Parent::scopeNoTest($query);

        $sellers = $query->get();

        $i = 0;
        foreach($buyers as $buyer)
        {
            $record['b-'.++$i] = $buyer->Users_ID;
        }

        $i = 0;
        foreach($sellers as $seller)
        {
            $record['s-'.++$i] = $seller->Users_ID;
        }

        return $record;
    }

    /**
     * Return Details subset of Transactions record
     *
     * @param int $transactionID
     *
     * @return mixed
     */
    public static function getDetails(int $transactionID)
    {
        $query = DB::table('Transactions')
                   ->select('ID', 'PurchasePrice',
                       'DateOfferPrepared', 'DateAcceptance', 'EscrowLength',
                       'ClientRole', 'SaleType', 'LoanType',
                       'willRentBack', 'hasBuyerSellContingency',
                       'hasSellerBuyContingency', 'hasInspectionContingency',  'hasAppraisalContingency',
                       'hasLoanContingency', 'willTenantsRemain',
                       'needsTermiteInspection', 'Notes')
                   ->where('Transactions.ID', '=', $transactionID);
        $query = Model_Parent::scopeNoTest($query);

        return $query->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * $param int $transactionsID
     *
     * @return array \Illuminate\Http\Response
     */
    public static function getPerson($role, int $transactionsID)
    {
        $transaction = Transaction::find($transactionsID);
        switch ($role)
        {
            case (Config::get('constants.USER_ROLE.BUYER')):
                $index  = 'buyer';
                $person = Buyer::where('Transactions_ID', $transactionsID)->get();
                break;
            case (Config::get('constants.USER_ROLE.SELLER')):
                $index  = 'seller';
                $person = Seller::where('Transactions_ID', $transactionsID)->get();
                break;

            case (Config::get('constants.USER_ROLE.BUYERS_AGENT')):
                $index  = 'buyersAgent';
                $qry = DB::table('Agents')
                            ->leftJoin('Transactions', 'Transactions.BuyersAgent_ID', '=', 'Agents.ID')
                            ->select('Agents.*')
                            ->where('Transactions.ID', '=', $transactionsID);
                $qry = Model_Parent::scopeNoTest($qry, ['Agents', 'Transactions']);
                $person = $qry->get();
                break;
            case (Config::get('constants.USER_ROLE.SELLERS_AGENT')):
                $index  = 'sellersAgent';
                $qry = DB::table('Agents')
                            ->leftJoin('Transactions', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                            ->select('Agents.*')
                            ->where('Transactions.ID', '=', $transactionsID);
                $qry = Model_Parent::scopeNoTest($qry, ['Agents', 'Transactions']);
                $person = $qry->get();
                break;
            case (Config::get('constants.USER_ROLE.BUYERS_TRANSACTION_COORDINATOR')):
                $index  = 'buyersTC';
                $qry = DB::table('TransactionCoordinators')
                            ->leftJoin('Transactions', 'Transactions.BuyersTransactionCoordinators_ID', '=', 'TransactionCoordinators.ID')
                            ->select('TransactionCoordinators.*')
                            ->where('Transactions.ID', '=', $transactionsID);
                $qry = Model_Parent::scopeNoTest($qry, ['TransactionCoordinators', 'Transactions']);
                $person = $qry->get();
                break;
            case (Config::get('constants.USER_ROLE.SELLERS_TRANSACTION_COORDINATOR')):
                $index  = 'sellersTC';
                $qry = DB::table('TransactionCoordinators')
                            ->leftJoin('Transactions', 'Transactions.SellersTransactionCoordinators_ID', '=', 'TransactionCoordinators.ID')
                            ->select('TransactionCoordinators.*')
                            ->where('Transactions.ID', '=', $transactionsID);
                $qry = Model_Parent::scopeNoTest($qry, ['TransactionCoordinators', 'Transactions']);
                $person = $qry->get();
                break;
            case (Config::get('constants.USER_ROLE.ESCROW')):
                $index  = 'escrow';
                $qry = DB::table('Escrows')
                            ->leftJoin('Transactions', 'Transactions.Escrows_ID', '=', 'Escrows.ID')
                            ->select('Escrows.*')
                            ->where('Transactions.ID', '=', $transactionsID);
                $qry = Model_Parent::scopeNoTest($qry, ['Escrows', 'Transactions']);
                $person = $qry->get();
                break;
            case (Config::get('constants.USER_ROLE.TITLE')):
                $index  = 'title';
                $qry = DB::table('Titles')
                            ->leftJoin('Transactions', 'Transactions.Titles_ID', '=', 'Titles.ID')
                            ->select('Titles.*')
                            ->where('Transactions.ID', '=', $transactionsID);
                $qry = Model_Parent::scopeNoTest($qry, ['Transactions', 'Titles']);
                $person = $qry->get();

                break;
            case (Config::get('constants.USER_ROLE.LOAN')):
                $index  = 'loan';
                $qry = DB::table('Loans')
                            ->leftJoin('Transactions', 'Transactions.Loans_ID', '=', 'Loans.ID')
                            ->select('Loans.*')
                            ->where('Transactions.ID', '=', $transactionsID);
                $qry = Model_Parent::scopeNoTest($qry, ['Transactions', 'Loans']);
                $person = $qry->get();
                break;

            case (Config::get('constants.USER_ROLE.BUYERS_BROKER')):
                $index  = 'buyersBroker';
                $qry = DB::table('Transactions')
                         ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                         ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                         ->select('Brokers.*')
                         ->where('Transactions.ID', '=', $transactionsID);
                $qry = Model_Parent::scopeNoTest($qry, ['Transactions', 'Agents', 'Brokers']);
                $person = $qry->get();
                break;
            case (Config::get('constants.USER_ROLE.SELLERS_BROKER')):
                $index  = 'sellersBroker';
                $qry = DB::table('Transactions')
                            ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                            ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                            ->select('Brokers.*')
                            ->where('Transactions.ID', '=', $transactionsID);
                $qry = Model_Parent::scopeNoTest($qry, ['Transactions', 'Agents', 'Brokers']);
                $person = $qry->get();
                break;
            case (Config::get('constants.USER_ROLE.BROKER')):
                $index = 'broker';
                $buyer = DB::table('Transactions')
                           ->leftJoin('Agents', 'Transactions.BuyersAgent_ID', '=', 'Agents.ID')
                           ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                           ->select('Brokers.*')
                           ->where('Transactions.ID', '=', $transactionsID);
                $buyer = Model_Parent::scopeNoTest($buyer, ['Transactions', 'Agents', 'Brokers']);

                $seller = DB::table('Transactions')
                            ->leftJoin('Agents', 'Transactions.SellersAgent_ID', '=', 'Agents.ID')
                            ->leftJoin('Brokers', 'Agents.Brokers_ID', '=', 'Brokers.ID')
                            ->select('Brokers.*')
                            ->where('Transactions.ID', '=', $transactionsID);
                $seller = Model_Parent::scopeNoTest($seller, ['Transactions', 'Agents', 'Brokers']);

                $person = $buyer->union($seller)->get();
                break;

            default:
                $index  = '*';
                $person = collect(null);
                break;
        }
        return ['transaction' => $transaction,
                $index        => $person,
                'role'        => $role,
                'data'        => $person,];
    }

    public static function daysUntilClose($transactionID)
    {
        $timeline = self::getTimeline($transactionID);
        foreach ($timeline as $milestone)
        {
            if ($milestone->MilestoneName != 'Close Escrow') continue;
            return _Time::daysBetween($milestone->MilestoneDate, strtotime('today'));
        }
    }

    public static function getDataForMail(int $transactionID, $mailTemplate, $people = ['tc'])
    {
        $data                   = [];
        $details                = self::getDetails($transactionID)->first();
        $data['escrowLength']   = $details->EscrowLength;
        $data['clientRole']     = $details->ClientRole;

        $user                   = \auth()->user();
        $data['u']              = $user->name ?? $user->NameFirst.' '.$user->NameLast;
        $data['uFirstName']     = $user->NameFirst;
        $data['uLastName']      = $user->NameLast;
        $data['uEmail']         = $user->email;

        $property               = self::getPropertySummary($transactionID);
        $data['p']              = $property['Address'];
        $data['pStreet']        = $property['Street1'].' '.$property['Street2'];
        $data['pCloseEscrow']   = $property['CloseEscrow'];
        $data['pofferPrepared'] = $property['OfferPrepared'];
        $data['Price']          = $details->PurchasePrice;

        $timeline = self::getTimeline($transactionID);
        $data['ma'] = MilestoneCombine::pullMilestoneByCode($timeline, 'aoo');
        $data['crLoanDueDate'] = MilestoneCombine::pullMilestone($timeline, 'Loan Contingency');
        $data['crLoanLength'] = $details->hasLoanContingency;
        $data['crInspectionDueDate'] = MilestoneCombine::pullMilestone($timeline, 'Inspection Contingency');
        $data['crInspectionLength'] = $details->hasInspectionContingency;
        $data['AppraisalDueDate'] = MilestoneCombine::pullMilestone($timeline, 'Appraisal Completed');       //  Deprecated
        $data['AppraisalDueDate'] = MilestoneCombine::pullMilestone($timeline, 'Appraisal Contingency Completed');
        $data['AppraisalLength'] = $details->hasAppraisalContingency;
        $data['emdDueDate'] = MilestoneCombine::pullMilestone($timeline, 'Delivery of the initial deposit');
        $data['PossessionDueDate'] = MilestoneCombine::pullMilestoneByCode($timeline,'pp');
        $data['disToBDueDate'] = MilestoneCombine::pullMilestoneByCode($timeline,'dis-to-b');
        $data['needsTermiteInspection'] = $details->needsTermiteInspection;
        $data['Timeline'] = route('transactionSummary.timeline', ['transactionID' => $transactionID]);

        $documents = Document::all()->toArray();
        $transactionDocuments = DocumentCombine::byTransaction($transactionID)->toArray();
        $questionnaires = Questionnaire::all();
        foreach ($questionnaires as $q)
        {
            $codes = explode('|',$q->Documents_Code);
            foreach ($codes as $code)
            {
                $key = array_search($code, array_column($documents, 'Code'));
                if ($key !== FALSE)
                {
                    $data[$q->ShortName . '_QuestionnaireLink'] = route('answer.questionnaire', [
                        'questionnaireID' => $q->ID,
                        'transactionID' => $transactionID
                    ]);
                }
                else
                {
                    $data[$q->ShortName . '_QuestionnaireLink'] = NULL;
                }
            }
        }

        foreach ($documents as $doc)
        {
            $key = array_search($doc['Code'], array_column($transactionDocuments, 'Code'));
            $documentName = str_replace(' ','',$doc['ShortName']);
            if($key !== FALSE)
            {
                $data[$documentName.'_FolderLink'] = route('openDocumentBag', [
                    'transactionID' => $transactionID,
                    'documentCode' => $doc['Code']]);
            }
            else
            {
                $data[$documentName.'_FolderLink'] = NULL;
            }
        }

        if (end($people) == 'tc') array_push($people, substr($data['clientRole'], 0, 1) . 'tc');
        /* Users DATA
         * 'ID' => $col->ID,
                                                            'Users_ID'  => $uid,
                                                            'NameFirst' => $col->NameFirst,
                                                            'NameLast'  => $col->NameLast,
                                                            'Email'     => $col->Email,];
         */
        //this contains some limited information for now
        //To expand the information edit the getUsers function.
        $users = self::getUsers($transactionID);
        foreach ($users as $role => $u)
        {
            if( $role == 'e'  ||
                $role == 't'  ||
                $role == 'sa' ||
                $role == 'ba' ||
                $role == 'l'  ||
                $role == 'b'  ||
                $role == 's' )
            {
                $data[$role. 'Email']               = $u[0]['Email'];
                $nameFull                           = $u[0]['NameFirst'].' '.$u[0]['NameLast'];
                $data[$role. 'Full']                = $nameFull;
                $data[$role]                        = $nameFull;
                $data[$role. 'Company']             = $u[0]['Company'];
                $phone = $u[0]['PrimaryPhone'] ?? $u[0]['SecondaryPhone'];
                $data[$role. 'Phone']               = $phone;
            }
        }
        foreach ($people as $role)
        {
            try
            {
                $peep                       = self::getPerson($role, $transactionID)['data']->first();
                if (empty($peep))
                {
                    $data[$role]                =
                    $data[$role . 'FirstName']  =
                    $data[$role . 'LastName']   =
                    $data[$role . 'Address']    =
                    $data[$role . 'Phone']      =
                    $data[$role . 'Email']      =
                    $data[$role . 'Company']    = NULL;
                }
                else
                {
                    $data[$role]                = $peep->NameFull ?? implode(' ', [$peep->NameFirst, $peep->NameLast]) ?? NULL;
                    $data[$role . 'FirstName']  = $peep->NameFirst ?? NULL;
                    $data[$role . 'LastName']   = $peep->NameLast ?? NULL;
                    $data[$role . 'Address']    = AddressVerification::formatAddress($peep) ?? NULL;
                    $data[$role . 'Phone']      = $peep->PrimaryPhone ?? NULL;
                    $data[$role . 'Email']      = $peep->Email ?? NULL;
                    $data[$role . 'Company']    = $peep->Company ?? null;
                }
            }
            catch (\Exception $e)
            {
                ddd(['EXCEPTION' => $e, 'peep' => $peep, __METHOD__ => __LINE__]);
            }
        }
        $data['from']     = $data[$mailTemplate->FromRole.'Email'] ?? env('MAIL_FROM_ADDRESS');
        $data['subject']  = $mailTemplate->Subject;

        foreach (['bcc', 'cc'] as $att)
        {
            $tmpAtt = title_case($att) . 'Role';
            if (empty(trim($mailTemplate->{$tmpAtt})))
            {
                $data[$att] = null;
            }
            else
            {
                $lst = [];
                foreach (array_map('trim', explode(',', $mailTemplate->{$tmpAttt})) as $role)
                {
                    $lst[] = $data[$mailTemplate->{$tmpAttt} . 'Email'] ?? null;
                }
                $data[$att] = $lst;
            }
        }
        $data['category'] = $mailTemplate->Category;
        $data['code']     = $mailTemplate->Code;
        $data['attach']   = $mailTemplate->Attachment;

        return $data;
    }

    /**
     * Returns an array containing ALL the roles that the current user has for the given transaction
     *
     * @param $transactionID
     *
     * @return array List of just the user's role(s) in the provided transaction
     */
    public static function getTransactionRole($transactionID)
    {
        return self::getRoleByUserID($transactionID, CredentialController::current()->ID(), true);
    }

    /**
     * Returns an array containing ALL the roles that the current user has within the given transaction
     *
     * @param      $transactionID
     * @param null $userID
     * @param bool $returnJustRoles If FALSE (default) return an array of arrays that contain the role and the index into the roles data table in
     *                              the form ['id'=>$index, 'role'=>$role].
     *                              If TRUE return an array of just the role codes
     *
     * @return array
     */
    public static function getRoleByUserID($transactionID, $userID=null, $returnJustRoles=false)
    {
//        Log::info([__FUNCTION__, func_get_args()]);
        if (empty($userID)) $userID = CredentialController::current()->ID();

        $roleByTable = [    'Buyers'=> 'b',
                            'Sellers'=>'s',
                            'Agents'=>'*a',
                            'TransactionCoordinators'=>'*tc',
                            'Escrow'=>'e',
                            'Loans'=>'l',
                            'Titles'=>'t'];

        $roleByField = [    'Buyers'=> 'b',
                            'Sellers'=>'s',
                            'BuyersAgent'=>'ba',
                            'SellersAgent'=>'sa',
                            'SellersTransactionCoordinators'=>'stc',
                            'BuyersTransactionCoordinators'=>'btc',
                            'Escrow'=>'e',
                            'Loans'=>'l',
                            'Titles'=>'t',
                            'Escrows' => 'e'];

        $tablesByField = ['BuyersTransactionCoordinators'=>'TransactionCoordinators',
                          'SellersTransactionCoordinators' => 'TransactionCoordinators',
                          'BuyersAgent'                   => 'Agents',
                          'SellersAgent'                  => 'Agents',
                          'Escrows'                       => 'Escrows',
                          'Titles'                        => 'Titles',
                          'Loans'                         => 'Loans',];

        $qry = DB::table('Transactions')
            ->where('ID', $transactionID);
        $qry = Model_Parent::scopeNoTest($qry);
        $transaction = $qry->get()->toArray();

        if (is_null($transaction) || empty($transaction)) return [];
        $ids = $roles = [];
        foreach ($transaction[0] as $field=>$val)
        {
            if (strtoupper(substr($field, -3)) == '_ID')
            {
                $ids[substr($field, 0, -3)] = $val;
            }
        }
        $buyers = TransactionCombine::getBuyers($transactionID, CredentialController::current()->ID())->toArray();
        foreach($buyers as $buyer)
        {
            try
            {
                $roles[] = ['id'   => (is_array($buyer)) ? $buyer['ID'] : $buyer->ID,
                            'role' => 'b',
                ];
            }
            catch (\Exception $e)
            {
                ddd(['buyers'=>$buyers, 'buyer'=>$buyer]);
            }
        }

        $sellers = TransactionCombine::getSellers($transactionID, CredentialController::current()->ID())->toArray();
        foreach($sellers as $seller)
        {
            $roles[] = ['id'   => (is_array($seller)) ? $seller['ID'] : $seller->ID,
                        'role' => 's',
            ];
        }

        foreach ($ids as $field=>$id)
        {
            $table = $tablesByField[$field] ?? false;
            if (empty($id) || $id == 0 || !$table) continue;
            $tbl = DB::table($table)->find($id);

            if(empty($tbl) || !$tbl || $tbl == NULL) continue;
            if ($userID == $tbl->Users_ID)
            {
                $roles[] = ['id'   => $id,
                            'role' => $roleByField[$field] ?? '?'
                ];
            }
        }
        if ($returnJustRoles)
        {
            foreach($roles as $idx=>$role)
            {
                $roles[$idx] = $role['role'];
            }
            $roles = array_unique($roles);
        }
        return $roles;
    }
    public static function getUserTransactionSystemRoles($transactionID, $userID)
    {
        $qry = DB::table('Transactions')
            ->where('ID', $transactionID)
            ->limit(1);
        $qry = Model_Parent::scopeNoTest($qry);
        $transaction = $qry->get()->toArray();

        if (is_null($transaction) || empty($transaction)) return [];
        $transaction = $transaction[0];
        $ids = [
            'c'     => 'CreatedByUsers_ID',
            'o'     => 'OwnedByUsers_ID',
            'bou'   => 'BuyersOwner_ID',
            'sou'   => 'SellersOwner_ID',
        ];
        $systemRoles = [];
        foreach ($ids as $key => $metaRole)
        {
            if($transaction->{$metaRole} == $userID)
            {
                $systemRoles[$key] = $metaRole;
            }
        }
        return $systemRoles;
    }

    /**
     * Takes transactions as arrays.
     * @param $transactions
     */
    public static function instantiateSessionTransactionList($transactions = [])
    {
        $sessionTransactions = session('transactionList') ?? FALSE;
        if($sessionTransactions) session()->forget('transactionList');
        if(!is_array($transactions)) $transactions = _Convert::toArray($transactions);
        $transactionList = [];
        foreach ($transactions as $t) $transactionList[$t['ID']] = $t['Status'];
        session()->put('transactionList', $transactionList);
    }

    /**
     * Data should be in the format of data[TransactionID] = Transaction Status.
     * Returns the updated Transaction List that can also be accessed via the session.
     * @param $data
     */
    public static function updateSessionTransactionList($data)
    {
        $transactionList = session('transactionList') ?? FALSE;
        if(!$transactionList) self::instantiateSessionTransactionList([]);
        if (isset($data[0]))  return FALSE;
        foreach ($data as $id => $status) $transactionList[$id] = $status;
        session()->put('transactionList', $transactionList);

        return $transactionList;
    }

}