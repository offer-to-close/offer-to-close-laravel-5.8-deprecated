<?php

namespace App\Combine;

use App\Http\Controllers\DocumentAccessController;
use App\Library\otc\_Locations;
use App\Library\otc\Credentials;
use App\Library\Utilities\_Convert;
use App\Models\bag_TransactionDocument;
use App\Models\lu_UserRoles;
use App\Models\ShareRoom;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ShareRoomCombine extends BaseCombine
{
    public static function roomCountByUser($userID, $onlyOpen = true)
    {
        return (count(self::getRoomsByUser($userID, $onlyOpen)));
    }

    public static function getVisibleDocuments($transactionID, $userID = null)
    {
        if (empty($userID)) $userID = auth()->id();
        $room = ShareRoom::getDirectory($transactionID);

        $transactionRoles = array_column(TransactionCombine::getRoleByUserID($transactionID, $userID), 'role');

        $docs = [];
        foreach ($room as $doc)
        {
            if (!in_array($doc->PovRole, $transactionRoles)) continue;
            if ($doc->AccessSum == 0) continue;
            $docs[] = $doc;
        }

       // if (empty($docs)) ddd(['room'=>$room, 'roles'=>$transactionRoles, 'docs'=>$docs]);
        return $docs;
    }

    public static function getRoomsByUser($userID, $onlyOpen = true)
    {
        $where = ['b.Users_ID = ?',
                  's.Users_ID = ?',
                  'ba.Users_ID = ?',
                  'sa.Users_ID = ?',
                  'btc.Users_ID = ?',
                  'stc.Users_ID = ?',
                  'e.Users_ID = ?',
                  't.Users_ID = ?',
                  'l.Users_ID = ?',
                  'trans.CreatedByUsers_ID = ?',
                  'trans.OwnedByUsers_ID = ? ',
        ];

        $bindings = array_fill(0, count($where), $userID);

        $select = 'trans.ID, trans.Status';

        //
        // ... Run query for transactions
        //
        $query = DB::table('Transactions AS trans')
                   ->leftJoin('Buyers AS b', 'trans.ID', '=', 'b.Transactions_ID')
                   ->leftJoin('Sellers AS s', 'trans.ID', '=', 's.Transactions_ID')
                   ->leftJoin('Agents AS ba', 'trans.BuyersAgent_ID', '=', 'ba.ID')
                   ->leftJoin('Agents AS sa', 'trans.SellersAgent_ID', '=', 'sa.ID')
                   ->leftJoin('TransactionCoordinators AS btc', 'trans.BuyersTransactionCoordinators_ID', '=', 'btc.ID')
                   ->leftJoin('TransactionCoordinators AS stc', 'trans.SellersTransactionCoordinators_ID', '=', 'stc.ID')
                   ->leftJoin('Escrows AS e', 'trans.Escrows_ID', '=', 'e.ID')
                   ->leftJoin('Loans AS l', 'trans.Loans_ID', '=', 'l.ID')
                   ->leftJoin('Titles AS t', 'trans.Titles_ID', '=', 't.ID')
                   ->leftJoin('users AS c', 'trans.CreatedByUsers_ID', '=', 'c.ID')
                   ->leftJoin('users AS o', 'trans.OwnedByUsers_ID', '=', 'o.ID')
                   ->selectRaw($select);

        $query->whereRaw(implode(' OR ', $where), $bindings)
              ->orderBy('trans.ID')
              ->distinct();

        $transactions = $query->get();

        $rooms = ShareRoom::rooms($onlyOpen);
        $rv = [];
        foreach ($transactions as $i => $transaction)
        {
            if ($onlyOpen && $transaction->Status == 'closed') continue;
            if (!in_array($transaction->ID, $rooms)) continue;
            $rv[] = $transaction->ID;
        }

        return $rv;
    }
    public static function getDocumentsByUser($userID, $onlyOpen=true)
    {
        $documents = [];
        $rooms = self::getRoomsByUser($userID, $onlyOpen);
        foreach($rooms as $roomNum=>$room)
        {
            $documents[$room] = _Convert::toArray(self::getVisibleDocuments($room, $userID));
            foreach($documents[$room] as $idx=>$rm)
            {
                $aSum = $rm['AccessSum'];
                $documents[$room][$idx]['AccessSum'] =
                    implode(', ', array_column(DocumentAccessController::decodeAccessSum($aSum), 'display'));

                $tSum = $rm['TransferSum'];
                $documents[$room][$idx]['TransferSum'] =
                    implode(', ', array_column(DocumentAccessController::decodeTransferSum($tSum), 'value'));

                $userID = RoleCombine::getUserID('tc', $rm['ApprovedByUsers_ID']);
                //                ddd($userID);
                $documents[$room][$idx]['ApprovedByUsers_ID'] =
                    implode(' ', AccountCombine::getName($userID));


                $userID = RoleCombine::getUserID($rm['UploadedByRole'], $rm['UploadedBy_ID']);
                //                ddd($userID);
                $documents[$room][$idx]['UploadedBy_ID'] =
                    implode(' ', AccountCombine::getName($userID));

                $signers = [];
                $sBy = DocumentCombine::decodeSigners($rm['SignedBy']);

                foreach($sBy as $role=>$user)
                {
                    foreach ($user as $uid=>$rec)
                    {

                        $signers[] = $role . ': ' . $rec['NameFull'];
                    }
                }
                $documents[$room][$idx]['SignedBy'] =
                    implode(', ', $signers);
            }
            if (empty($documents[$room])) unset($documents[$room]);
        }
        return $documents;
    }
}