<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* View Composer*/
View::composer(['*'], function($view){
    $user = Auth::user();
    $view->with('user', $user);
});
Route::get('sys/logout', function () { return view('logout'); });

//....Routes for new prelogin layouts
Route::get('/fp', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.welcome')); })->name('preLogin.welcome');
Route::get('/privacy-policy', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.privacyPolicy')); })->name('prelogin.privacyPolicy');
Route::get('/terms-conditions', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.termsConditions')); })->name('prelogin.termsConditions');
Route::get('/fsbo', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.fbso.saleByOwner')); })->name('preLogin.fsbo');
Route::get('/mls', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.mls.mlsListingService')); })->name('preLogin.mls');
Route::get('/transaction-coordinator', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.agents.agents')); })->name('preLogin.agents');
Route::get('/about', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.about')); })->name('preLogin.about');
Route::get('/offers', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.offers')); })->name('preLogin.offers');
Route::get('/brands', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.brands')); })->name('preLogin.brands');
Route::get('/offer-assistant', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.offerAssistant.officeAssistant')); })->name('preLogin.offerAssistant');
Route::match(['get', 'post'], '/membership', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.register1')); })->name('prelogin.membership');
Route::post('/request-membership', 'InvitationController@saveMemberRequest')->name('prelogin.requestMembership');
Route::get('/fsbo/get-started', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.fbso.fsboGetStarted')); })->name('preLogin.fsbo.get-started');
Route::get('/mls/get-started', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.mls.mlsGetStarted')); })->name('preLogin.mls.get-started');
Route::get('/agent/get-started', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.agents.agentGetStarted')); })->name('preLogin.agents.get-started');
Route::get('/transaction-coordinator/get-started', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.agents.agentGetStarted')); })->name('preLogin.tc.get-started');

// ... Press Releases
Route::get('/press-release/1',
    function () { return \Illuminate\Support\Facades\Redirect::to( asset('press') . '/20180413-transaction_coordinator_service.pdf');})->name('press.1');
Route::get('/press-release/2',
    function () { return \Illuminate\Support\Facades\Redirect::to( asset('press') . '/20180515-transaction_timeline.pdf');})->name('press.2');
Route::get('/press-release/3',
    function () { return \Illuminate\Support\Facades\Redirect::to( asset('press') . '/20180713-hire_of_leading_transaction_coordinator.pdf');})->name('press.3');


// ... Contact Us
//
Route::get('/otc/contact/{nameFirst?}/{nameLast?}/{email?}/{phone?}/{role?}','MailController@contactUs')->name('email.contactUs');

// ... Notifications
//
Route::get('/alert','MailController@sendReminderEmails')->name('email.alert');
Route::post('/shared/document/email', 'MailController@documentSharedEmail')->name('documentSharedEmail');

Route::get('/get_offer_image/{offerID}/{offerType}/{image}',
    function ($offerID, $offerType, $image)
    {
        $path = public_path("_uploaded_documents/offers/$offerID/$offerType/$image");
        return file_exists($path) ? response(file_get_contents($path))->header('Content-Type', 'image') : abort(404);
    });

Route::resource('agents', 'AgentController');

Route::get('/home', 'DashboardController@prosumerDashboard')->name('user.dashboard');
Route::get('/', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.welcome')); })->name('home');
Route::get('/welcome', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.welcome')); })->name('prelogin');
Route::get('/contactUs', function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('prelogin.contactUs')); })->name('prelogin.contactUs');
Route::post('/contactUs/submit', 'HomeController@submitContactUs')->name('submitContactRequest');

Route::post('/ta/status/', 'TransactionController@updateStatus')->name('transaction.updateStatus');
Route::post('/ta/overrideaddress/', 'TransactionController@override')->name('override.address');

//...........Invoice development ............
Route::get('/invoices/{userID?}', 'InvoiceController@getInvoicesByUserID')->name('invoices.byUserID');
Route::get('/invoice/{invoiceID?}', 'InvoiceController@inputInvoice')->name('invoice.input');
Route::get('/invoice/update/status/{invoiceID?}', 'InvoiceController@updateInvoiceStatus')->name('update.InvoiceStatus');
Route::post('/saveInvoice','InvoiceController@saveInvoice')->name('saveInvoice');
Route::post('/invoices/search','InvoiceController@searchInvoice')->name('search.Invoice');
Route::match(['get', 'post'],'/PrintInvoice/{invoiceID}','InvoiceController@PrintInvoice')->name('PrintInvoice');
//................... Offers ..........................................................................
Route::post('/offer/check', 'OfferController@checkProperty')->name('offer.checkProperty');
Route::get('/offer/{offerID?}', 'OfferController@inputOffer')->name('input.Offer');
Route::get('/transaction/offers/{tcID?}', 'OfferController@getOfferByTCID')->name('offers.byTC');
Route::post('/offer/save', 'OfferController@saveOffer')->name('save.Offer');
Route::post('/offer/upload', 'OfferController@uploadDocument')->name('offer.uploadDocument');
Route::post('/offer/upload-offer', 'OfferController@uploadOffer')->name('offer.uploadOffer');
Route::post('/offer/upload-counter-offer', 'OfferController@uploadCounterOffer')->name('offer.uploadCounterOffer');
Route::get ('/offer', 'OfferController@index');


Route::get('/userProfile', 'UserController@saveProfile')->name('userProfile');
Route::post('/userProfile', 'UserController@saveProfile');
Route::post('/uploadImage', 'UserController@uploadImage')->name('userProfile.uploadImage');

/*** Alerts *****/
Route::post('/alerts/mark/read', 'AlertController@markAlertsAsRead')->name('markAlertsAsRead');
Route::post('/alerts/clear', 'AlertController@clearAllAlerts')->name('clearAllAlerts');

Route::get('/transaction', 'TransactionController@index')->name('transaction.index');
Route::get('/transaction/create/{side}', 'TransactionController@create')->name('transaction.create');
Route::get('/transaction/owned/{ajax?}/{userID?}', 'TransactionController@getOwnedTransactions')->name('getOwnedTransactions');

// ... routes for Dashboard
Route::get('/dash/ta/list/{role?}/{userID?}', 'DashboardController@transactionListByUser')->name('dash.ta.list');
//Route::get('/dashboard/questions/{taID}', 'DashboardController@questionList')->name('dashboard.questions');
//Route::get('/dashboard/uid/{userID}', 'DashboardController@transactionList')->name('dashboard.uid');
Route::get('/source/side/{transactionID}/{source}/{ajax?}', 'TransactionController@isSameSide')->name('isSameSide');
Route::get('/dashboard', 'DashboardController@prosumerDashboard')->name('dashboard');

// ... Input Role Data

Route::get('/ta/b/{transactionID?}/{buyerID?}', 'TransactionController@inputBuyer')->name('inputBuyer');
Route::get('/ta/s/{transactionID?}/{sellerID?}', 'TransactionController@inputSeller')->name('inputSeller');
Route::get('/ta/ba/{transactionID?}/{buyersAgentID?}', 'TransactionController@inputBuyerAgent')->name('inputBuyerAgent');
Route::get('/ta/sa/{transactionID?}/{sellerAgentID?}', 'TransactionController@inputSellerAgent')->name('inputSellerAgent');
Route::get('/ta/btc/{transactionID?}/{ajax?}/{tcType?}', 'TransactionController@inputBuyerTC')->name('inputBuyerTC');
Route::get('/ta/stc/{transactionID?}/{ajax?}/{tcType?}', 'TransactionController@inputSellerTC')->name('inputSellerTC');

Route::get('/ta/br/{transactionID?}', 'TransactionController@inputBroker')->name('inputBroker');
Route::get('/ta/e/{transactionID?}', 'TransactionController@inputEscrow')->name('inputEscrow');
Route::get('/ta/t/{transactionID?}', 'TransactionController@inputTitle')->name('inputTitle');
Route::get('/ta/l/{transactionID?}', 'TransactionController@inputLoan')->name('inputLoan');
Route::get('/ta/p/{transactionID?}', 'TransactionController@inputProperty')->name('inputProperty');
Route::get('/ta/d/{transactionID?}', 'TransactionController@inputDetails')->name('inputDetails');
Route::get('/ta/share/{transactionID?}', function () { return view('2a.transaction.rooms.share'); })->name('share');

// ... Save Role Data
Route::match(['get', 'post'],'/ta/save/getNavBuyer',  'TransactionController@getNavBuyer')->name('getNavBuyer');
Route::match(['get', 'post'],'/ta/save/getNavSeller',  'TransactionController@getNavSeller')->name('getNavSeller');

Route::match(['get', 'post'],'/ta/save/b',  'TransactionController@saveBuyer')->name('saveBuyer');
Route::match(['get', 'post'],'/ta/save/s',  'TransactionController@saveSeller')->name('saveSeller');
Route::match(['get', 'post'],'/ta/save/a',  'TransactionController@saveAgent')->name('saveAgent');

Route::match(['get', 'post'],'/ta/save/br',  'TransactionController@saveBroker')->name('saveBroker');

Route::match(['get', 'post'],'/ta/save/e',  'TransactionController@saveEscrow')->name('saveEscrow');
Route::match(['get', 'post'],'/ta/save/t',  'TransactionController@saveTitle')->name('saveTitle');
Route::match(['get', 'post'],'/ta/save/l',  'TransactionController@saveLoan')->name('saveLoan');

Route::match(['get', 'post'],'/ta/save/p',  'TransactionController@saveProperty')->name('saveProperty');
Route::match(['get', 'post'],'/ta/save/d',  'TransactionController@saveDetails')->name('saveDetails');

Route::match(['get', 'post'],'/update/p/', 'TransactionController@updatePropertyAddress')->name('property.updateAddress');
Route::post('/save/aux', 'TransactionController@saveAux')->name('saveAux');
Route::post('/active/property/check','TransactionController@activePropertyCheck')->name('checkProperty');
Route::get('/cancel/transaction/{transactionID}', 'TransactionController@cancelTransaction')->name('cancelTransaction');

// ... View Roles - Independent of a Transaction ID
Route::match(['get', 'post'],'/view/', 'RoleController@viewSomething')->name('view.Something');
Route::match(['get', 'post'],'/find/{role}', 'RoleController@findSomething')->name('find.Something');
Route::match(['get', 'post'],'/view/a/{ID?}', 'RoleController@view')->name('viewRole.Agent');
Route::match(['get', 'post'],'/view/tc/{ID?}', 'RoleController@view')->name('viewRole.TransactionCoordinator');
Route::match(['get', 'post'],'/view/e/{ID?}', 'RoleController@view')->name('viewRole.Escrow');
Route::match(['get', 'post'],'/view/t/{ID?}', 'RoleController@view')->name('viewRole.Title');
Route::match(['get', 'post'],'/view/l/{ID?}', 'RoleController@view')->name('viewRole.Loan');
Route::match(['get', 'post'],'/view/br/{ID?}', 'RoleController@view')->name('viewRole.Broker');

// ... Direct access to Roles - Independent of a Transaction ID
Route::get('/a/{ID?}', 'RoleController@inputAgent')->name('inputRole.Agent');
Route::get('/tc/{ID?}', 'RoleController@inputTC')->name('inputRole.TC');
Route::get('/e/{ID?}', 'RoleController@inputEscrow')->name('inputRole.Escrow');
Route::get('/t/{ID?}', 'RoleController@inputTitle')->name('inputRole.Title');
Route::get('/l/{ID?}', 'RoleController@inputLoan')->name('inputRole.Loan');
Route::get('/br/{ID?}', 'RoleController@inputBroker')->name('inputRole.Broker');
Route::get('/bg/{ID?}', 'RoleController@inputBrokerage')->name('inputRole.Brokerage');
Route::get('/bo/{ID?}', 'RoleController@inputBrokerageOffice')->name('inputRole.BrokerageOffice');

Route::match(['get', 'post'],'/save/a',  'RoleController@saveAgent')->name('saveRole.Agent');
Route::match(['get', 'post'],'/save/tc',  'RoleController@saveTC')->name('saveRole.TC');
Route::match(['get', 'post'],'/save/tc1',  'RoleController@saveTC')->name('saveRole.TransactionCoordinator');
//Route::match(['get', 'post'],'/save/e',  'RoleController@saveEscrow')->name('saveRole.Escrow');
Route::match(['get', 'post'],'/save/t',  'RoleController@saveTitle')->name('saveRole.Title');
Route::match(['get', 'post'],'/save/l',  'RoleController@saveLoan')->name('saveRole.Loan');
Route::match(['get', 'post'],'/save/br',  'RoleController@saveBroker')->name('saveRole.Broker');
Route::match(['get', 'post'],'/save/bg',  'RoleController@saveBrokerage')->name('saveRole.Brokerage');
Route::match(['get', 'post'],'/save/bo',  'RoleController@saveBrokerageOffice')->name('saveRole.BrokerageOffice');

Route::post('/save/modal/person',  'RoleController@savePersonModal')->name('save.Modal.Person');
Route::post('/save/modal/office',  'RoleController@saveOfficeModal')->name('save.Modal.Office');

Route::match(['get','post'], '/add/everything', 'RoleController@addNew')->name('add.New');

// ... Assign a role from a list
Route::match(['get', 'post'],'/assign/e',  'RoleController@assignAuxillary')->name('assignRole.Escrow');
Route::match(['get', 'post'],'/assign/l',  'RoleController@assignAuxillary')->name('assignRole.Loan');
Route::match(['get', 'post'],'/assign/t',  'RoleController@assignAuxillary')->name('assignRole.Title');
Route::match(['get', 'post'],'/assign/a',  'RoleController@assignAuxillary')->name('assignRole.Agent');
Route::match(['get', 'post'],'/assign/tc', 'RoleController@assignAuxillary')->name('assignRole.TC');
Route::match(['get', 'post'],'/assign/b', 'RoleController@assignAuxillary')->name('assignRole.B');
Route::match(['get', 'post'],'/assign/s', 'RoleController@assignAuxillary')->name('assignRole.S');

Route::match(['get', 'post'],'/assign/br', 'RoleController@assignAuxillary')->name('assignRole.Broker'); // todo
Route::match(['get', 'post'],'/assign/bg', 'RoleController@assignAuxillary')->name('assignRole.Brokerage'); // todo
Route::match(['get', 'post'],'/assign/bo', 'RoleController@assignAuxillary')->name('assignRole.BrokerageOffice');

// ... Other Role Related
Route::post('/switch/role', 'RoleController@switchUserRole')->name('switch.Role');


//search using AJAX
Route::match(['get', 'post'],'/search/person',  'RoleController@searchPerson')->name('search.Person');
Route::match(['get', 'post'],'/search/office',  'RoleController@searchOffice')->name('search.Office');
Route::get('/search/property',  'TransactionController@searchTransactionProperty')->name('search.TransactionProperty');
Route::get('/office/details',  'RoleController@getOfficeDetails')->name('details.Office');

Route::post('/get/agent/info', 'RoleController@getAgentData')->name('getAgentData');
Route::post('/get/tc/info', 'RoleController@getAuxData')->name('getAuxData');

// Vue
Route::get('/property/get/{taID}', 'TransactionController@ajaxGetProperty')->name('get.Property');

// ... Upload files
Route::get('/ta/doc/{taID?}', 'TransactionController@inputFile')->name('inputFile');
Route::match(['get', 'post'], '/ta/save/doc', 'TransactionController@saveFile')->name('saveFile');
Route::match(['get', 'post'], '/ta/save/doc', 'TransactionSummaryController@saveTransactionDocument')->name('save.TransactionDocument');
Route::post('/upload/propertyImage', 'TransactionController@uploadPropertyImage')->name('property.uploadImage');

Route::get('/ta/home/{taid?}/{firstTime?}', 'TransactionController@home')->name('transaction.home');


// ... routes for Documents
Route::get('/docs/{state}', 'DocumentController@allByState')->name('document.byState');
Route::get('/vw/doc/{taid}/{file)', 'DocumentController@displayDocument');
Route::post('/docs/fetch', 'DocumentController@ajaxFetchDocument')->name('ajaxFetchDocument');
Route::post('/docs/get/bag', 'DocumentController@ajaxGetDocumentsInBag')->name('ajaxGetDocumentsInBag');
Route::get('/document/bag/{transactionID}/{documentCode}', 'DocumentController@openDocumentBag')->name('openDocumentBag');
Route::get('/document/encryptions', 'DocumentController@getFilestackSignature')->name('getFilestackSignature');
Route::post('/document/getSigners', 'DocumentController@getSigners')->name('getSigners');
Route::post('document/getRequiredSigners', 'DocumentController@getRequiredSigners')->name('getRequiredSigners');
Route::post('/mark/document/complete/incomplete', 'DocumentController@markDocumentAsCompleteIncomplete')->name('markDocumentAsCompleteIncomplete');
Route::post('/mark/bag/document/complete/incomplete', 'DocumentController@markBagDocumentAsCompleteIncomplete')->name('markBagDocumentAsCompleteIncomplete');
Route::post('/document/signer/add/remove', 'DocumentController@signerAddRemove')->name('signerAddRemove');
Route::post('/save/adhoc/document', 'DocumentController@saveAdHocDocument')->name('saveAdHocDocument');

// ... routes used solely for testing
Route::get('/mztest', function () { return view('transaction.upload'); });
Route::get('/mztest/1', 'test\TestController@f001');
Route::get('/mztest/2', 'test\TestController@f002');
Route::get('/mztest/4/{view?}', 'test\TestController@view');
Route::get('/mztest/3/{model}', 'DashboardController@create');
Route::get('/mztest/seed/{table?}/{className?}/{classDir?}', 'test\TestController@seeder');
Route::get('/mztest/timeline', 'test\TestController@timeline');
Route::get('/mztest/routes', 'test\TestController@routes');
Route::get('/mztest/tasks/{tid?}/{clientRole?}', 'test\TestController@tasks');
Route::get('/mztest/county/{zip}', 'test\TestController@county');

Route::get('/mztest/transaction/{taID}', 'test\TestController@transaction');
Route::get('/mztest/smartyStreets', 'test\TestController@smartyStreets');
Route::get('/mztest/holidays', 'test\TestController@holidays');
Route::get('/mztest/obfuscate', 'test\TestController@obfuscate');
Route::get('/mztest/invoice', 'test\TestController@invoice');
Route::get('/mztest/tcType/{taid?}', 'test\TestController@tcType');
Route::get('/mztest/propertySummary/{taid?}/{includeDates?}', 'test\TestController@propertySummary');

Route::get('/mztest/email/{fromRole?}/{mailTemplate?}/{transactionID?}', 'MailController@sendEmail');
Route::get('/mztest/questionnaires', function () { return view('2a.questionnaires.questionMaster'); });

//Route::get('/mztest/email/{mailTemplate?}/{transactionID?}/{editFirst?}', 'MailController@sendEmail');//check
Route::get('/mztest/editEmail/{mailTemplate?}', function () { return view('2a.tools.editEmailTemplate'); });
Route::get('/mztest/email/{fromRole?}/{mailTemplate?}/{transactionID?}', 'MailController@sendEmail');//check
Route::get('/mztest/factory', 'test\TestController@factory');
Route::get('/mztest/alerts', 'test\TestController@alert');
Route::get('/mztest/dir', 'test\TestController@dir');
Route::get('/mztest/pdf', 'test\TestController@pdf');
Route::get('/mztest/queryTest', 'test\TestController@queryTest');
Route::get('/mztest/delete', 'test\TestController@delete');
Route::get('/mztest/snap', 'test\TestController@snap');
Route::get('/mztest/getRoles/{userID?}', 'test\TestController@getRoles');
Route::get('/mztest/api', 'test\TestController@testApi2Curl');
Route::get('/mztest/apiAdmin', function(){ return view('test.apiForm'); });
Route::get('/mztest/ins', function(){ return view('insertTest'); });

// ... Transaction Summary ...
Route::post('/documents/get', 'TransactionSummaryController@ajaxDocuments')->name('ajaxDocuments');
Route::get('/ts/documents/{transactionID?}/{sortBy?}', 'TransactionSummaryController@documents')->name('transactionSummary.documents');
Route::post('/ts/timeline/', 'TransactionSummaryController@post_timeline')->name('transaction.posttimeline');
Route::post('/ts/todos/save', 'TransactionSummaryController@saveTask')->name('transactionSummary.saveTask');
Route::get('/ts/documents/{transactionID?}/{sortBy?}', 'TransactionSummaryController@documents')->name('transactionSummary.documents');
Route::get('/ts/todos', function () { return view('2a.transactionSummary.todos'); })->name('transactionSummary.todos');
Route::get('/ts/disclosures/{transactionID}', 'TransactionSummaryController@disclosures')->name('transactionSummary.disclosures');
Route::get('/ts/timeline/{transactionID}', 'TransactionSummaryController@timeline')->name('transactionSummary.timeline');
Route::get('/ts/tasks/{transactionID}/{sort?}', 'TransactionSummaryController@tasks')->name('transactionSummary.tasks');
Route::post('/ts/timeline/update-transaction-status', 'TransactionSummaryController@update_IsCompleted')->name('updateStatus');

Route::post('/get/timeline', 'TransactionSummaryController@getMergedTasksTimeline')->name('getMergedTasksTimeline');

Route::get('/ts/documents/{transactionID}', 'TransactionSummaryController@documents')->name('transactionSummary.documents');
Route::get('/ts/email/{transactionID?}/{mailTemplate?}', 'MailController@chooseTemplate')->name('chooseTemplate');
Route::post('/ts/task-date-change','TransactionSummaryController@adjustTaskDueDatesToMilestone')->name('approveTaskChange');
Route::post('/ts/task-list-to-change', 'TransactionSummaryController@listAffectedTasks')->name('listAffectedTasks');
Route::get('/ts/share/{transactionID?}', 'TransactionSummaryController@shareRoom')->name('transactionSummary.shareRoom');
Route::post('/ts/update/milestone/length', 'TransactionController@updateMilestoneLength')->name('updateMilestoneLength');



// ... Documents ...
Route::view('/documents/bag', \App\Library\Utilities\_LaravelTools::addVersionToViewName('documents.bagView'))->name('bagView');
Route::get('/documentPermission/view/{transactionID}/{documentCode}', 'DocumentAccessController@documentPermissionTable')->name('documentPermissionTable');
Route::get('/document/share/view/{transactionID}/{documentCode}/{bagID?}', 'DocumentAccessController@documentBagShareView')->name('documentBagShareView');
Route::get('/document/share/data/{transactionID}/{documentCode}','DocumentAccessController@documentBagShareData')->name('documentBagShareData');
Route::post('/document/share/sum/change', 'DocumentAccessController@changeBagDocumentSharingSum')->name('changeBagDocumentSharingSum');

// ... Questionnaires ...
Route::post('/ts/questionnaires/updateAnswer', 'QuestionnairesController@ajaxUpdateAnswer')->name('update.questionnaireAnswer');
Route::get('/ts/questionnaire/review/{transactionID}/{lk_ID}/{returnJSON?}/{returnHTML?}/{sign?}', 'QuestionnairesController@reviewSignQuestionnaire')->name('review.questionnaire');
Route::get('/ts/questionnaire/answer/{questionnaireID}/{transactionID}/{type?}/{key?}', 'QuestionnairesController@questionnaire')->name('answer.questionnaire');
Route::get('/ts/questionnaire/editanswer/{questionnaireID}/{transactionID}/{type?}/{key?}', 'QuestionnairesController@questionnaire')->name('editanswer.questionnaire');
Route::post('/ts/disclosure/get', 'QuestionnairesController@ajaxGetDisclosures')->name('get.disclosures');
Route::post('/ts/questionnaires/getQuestionaire', 'QuestionnairesController@ajaxAnswersDisplaysAndValues')->name('get.questionnaireQuestions');
Route::post('/ts/questionnaires/checkAnswer', 'QuestionnairesController@ajaxCheckAnswer')->name('check.questionnaireAnswer');
Route::match(['get', 'post' ], '/ts/documents/complete/', 'TransactionSummaryController@markDateComplete')->name('transactionSummary.documents.markDateComplete');


Route::post('ts/ajax/tasks/' , 'TransactionSummaryController@ajaxGetTasks')->name('get.tasks');
Route::post('/ts/ajax/updateTask/' , 'TransactionSummaryController@updateTask')->name('update.tasks');


// ... Signatures ...
Route::get('/docusign/auth/', 'SignatureController@getDocuSignAuth')->name('signature.getDocuSignAuth');
Route::get('/eversign/auth/', 'SignatureController@getEversignAuth')->name('signature.getEverSignAuth');
Route::get('/documents/docuSign/','SignatureController@docuSign')->name('signature.docuSign');
Route::get('/documents/everSign/','SignatureController@everSign')->name('signature.everSign');
Route::get('/eversign/saveSignedDocuments/','SignatureController@saveEverSignSignedDocument')->name('saveEverSignSignedDocument');
Route::get('/ts/post/docuSign/{logID}', 'SignatureController@postDocuSign')->name('postDocuSign');
Route::get('/signature/gateway/{transactionID}/{bagIDString}/{signatureAPI?}', 'SignatureController@signatureGateway')->name('signature.gateway');
Route::post('/everSign/document/existence', 'SignatureController@checkDocumentExistence')->name('checkDocumentExistence');
Route::post('/everSign/document/cancel', 'SignatureController@cancelEverSignDocument')->name('cancelEverSignDocument');

// ... reCaptcha ...
Route::post('/recaptcha/verify', 'AccessController@verify_reCaptcha')->name('verify_reCaptcha');

Route::get('/adobe/auth/', 'SignatureController@getAdobeSignAuth')->name('signature.getAdobeSignAuth');

// ... Emails  ...
Route::match(['get', 'post'],'/email/tmp/edit/{mailTemplateID?}/{transactionID?}/{editFirst?}', 'MailController@sendEmail')->name('editEmail');
Route::match(['get', 'post'], '/email/str/send', 'MailController@sendEmailString')->name('sendEmail.string');
Route::post('/firsttime/specialoffer', 'MailController@firstTimeCoupon')->name('firstTimeCoupon');
Route::post('/beta/signup', 'MailController@requestBetaEmail')->name('requestBetaEmail');
//... Reports ...
Route::post('/ordernhd/{transactionID}/{reportType?}/{requesterRole?}', 'ReportController@snapNHD')->name('order.snapnhd');
//Route::post('/ordernhd', 'MailController@requestSnapNHD')->name('order.snapnhd');
Route::get('/verifyreport/{transactionID}/{reportSource}', 'ReportController@lookupReport')->name('verifyReport');
//Route::get('/verifynhd/{transactionsID}', 'MailController@ajaxVerifyNHDReport')->name('verify.snapnhd');

// ... Viewers ...
Route::get('/util/viewer/{$urlFile}',
    function () { return view(\App\Library\Utilities\_LaravelTools::addVersionToViewName('tools.pdfViewer'));})->name('viewer.pdf');

//======================================================================================================================
// ... Administrator Tools
Route::get('/admin/menu', 'AdminController@viewMenu')->name('admin.menuView');
Route::post('/admin/process/menu', 'AdminController@processToolMenu')->name('admin.menuProcess');
Route::match(['get', 'post'], '/admin/edit-emailTemplate', 'AdminController@editEmailTemplate')->name('admin.editEmailTemplate');
Route::match(['get', 'post'], '/admin/save-emailTemplate', 'AdminController@saveEmailTemplate')->name('admin.saveEmailTemplate');


Route::get('/admin/ic/{taid?}/{role?}/{roleID?}/{userID?}/{shareRoomID?}', 'AdminController@getInviteCode');
Route::view('/admin/dashboard',\App\Library\Utilities\_LaravelTools::addVersionToViewName('dashboard.adminDashboard'))->name('enterAdmin');
Route::post('/admin/defaults', 'DocumentAccessController@documentAccessTransfers')->name('documentAccessTransfers');
Route::post('/access/decoder', 'DocumentAccessController@decodeAccess')->name('decodeAccess');
Route::post('/access/levels', 'DocumentAccessController@accessLevels')->name('accessLevels');

Route::get('/admin/phpinfo', function () { return view('tools.phpInfo'); })
    ->middleware('is_admin')
    ->name('phpinfo');


//======================================================================================================================
// ... OTC Staff Tools
Route::get('/staff/menu/{path?}', 'StaffToolsController@viewMenu')->name('staff.menuView');
Route::match(['get', 'post'],'/staff/process/menu/{path?}', 'StaffToolsController@processToolMenu')->name('staff.menuProcess');
Route::match(['get', 'post'],'/staff/approve-request/{memReqID?}', 'InvitationController@approveRequest')->name('staff.approveRequest');
Route::match(['get', 'post'],'/staff/reject-requestReason/{memReqID?}', 'InvitationController@rejectRequestReason')->name('staff.rejectRequestReason');

Route::post('/invite/send', 'InvitationController@sendInvitationFromRequest')->name('invite.send');
Route::post('/request/reject', 'InvitationController@rejectRequest')->name('request.reject');
Route::get('/mark/request/spam/{ID}', 'InvitationController@markRequestSpam')->name('markRequestSpam');
//======================================================================================================================
// ... Developer Tools
Route::get('/dt/db/hackSchema', 'DevToolsController@schemaHack')->name('devTools.schemaHack');

Route::get('/dev/menu', 'DevToolsController@viewMenu')->name('dev.menuView');

//Route::get('/dev/menu', 'DevToolsController@viewMenu')->name('all.menuView');
Route::get('tool/menu', function () { return view('2a.dashboard.masterTools'); })->name('all.menuView');

Route::post('/dev/process/menu', 'DevToolsController@processToolMenu')->name('dev.menuProcess');

Route::post('/dev/tools/', 'DevToolsController@transactionRoles')->name('dev.transactionRoles');
Route::post('/dev/tools/', 'DevToolsController@transactionDocs')->name('dev.transactionDocs');
Route::post('/dev/tools/', 'DevToolsController@transactionFiles')->name('dev.transactionFiles');
Route::post('/dev/tools/', 'DevToolsController@users')->name('dev.users');
Route::post('/dev/tools/', 'DevToolsController@userTransactions')->name('dev.userTransactions');


Route::post('/ts/invite/choose', 'InvitationController@chooseInvitees')->name('invite.choose');
Route::post('/ts/invite/confirm', 'InvitationController@confirmInvitation')->name('invite.confirm');
Route::post('/ts/questionnaires/updateAnswer', 'QuestionnairesController@ajaxUpdateAnswer')->name('update.questionnaireAnswer');
Route::get('/ts/questionnaire/answer/{questionnaire}/{transactionID}', 'QuestionnairesController@questionnaire')->name('answer.questionnaire');
Route::post('/ts/questionnaires/getQuestionnaire', 'QuestionnairesController@ajaxAnswersDisplaysAndValues')->name('get.questionnaireQuestions');
Route::post('/ts/questionnaires/checkAnswer', 'QuestionnairesController@ajaxCheckAnswer')->name('check.questionnaireAnswer');


/*
* Registration ( to route a user to registration, just use 'register' )
**/

//Route::match(['get', 'post'],'/register/{role?}/{inviteCode?}', 'HomeController@getForms')->name('reg');
Route::group(['prefix' => 'register'],
    function ()
    {
        Route::post('/request', 'Auth\RegisterController@requestMembership')->name('requestMembership');
        Route::post('/submit', 'Auth\RegisterController@submitMembership')->name('submitMembershipRequest');
        Route::get('/{role?}/{inviteCode?}', 'Auth\RegisterController@showRegistrationForm')->name('otc.register.invite');
        Route::post('validate-form', 'HomeController@validateForm');
        Route::post('store', 'HomeController@storeRegistration');
        Route::get('success/{user?}', 'HomeController@success')->name('register.success');

        // fb registration
        Route::get('{user}/fb-register', 'HomeController@fbRegister')->name('register.fb-register');
    });

//Routes for Timeline
Route::get('/transaction-timeline/', 'TimelineController@create')->name('timeline.create');
Route::post('/transaction-timeline/', 'TimelineController@store')->name('timeline.store');
Route::get('/transaction-timeline/edit/{link}', 'TimelineController@edit')->name('timeline.edit');
Route::get('/transaction-timeline/{address}/{link}', 'TimelineController@show')->name('timeline.show');
Route::post('/transaction-timeline/update/{link}', 'TimelineController@update')->name('timeline.update');
Route::post('/transaction-timeline/recalculate', 'TimelineController@recalculateTimeline')->name('timeline.recalculate');

//Routes for Admin  *****************************************************
Route::get('/admin', 'AdminController@admin')
     ->middleware('is_admin')
     ->name('admin');

Route::post('admin/authorize', 'HomeController@adminAuthorize')->name('admin.authorize');
Route::get('admin/login', 'HomeController@adminLogin')->name('admin.login');
Route::get('admin/logout', 'AdminController@adminLogout')
    ->middleware('is_admin')
    ->name('admin.logout');


///////////////////////////////////////////////////////////////////////////////////////////////////
/// ... Don't change these routes
///
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('otc.logout');

Route::get('/login/{inviteCode?}', 'HomeController@userLogin')->name('otc.login');
Route::get('/login', 'HomeController@userLogin')->name('otc.login');

Route::post('/login/rr', 'HomeController@recordRole')->name('login.recordRole');
Route::get('/login/cr/{roles?}', 'HomeController@chooseRole')->name('login.chooseRole2');
Route::get('/chooseRole', 'HomeController@chooseRole')->name('login.chooseRole');
Route::get('/user/new', 'UserController@userNew')->name('user.index');

// Route::get('admin/login', 'HomeController@adminLogin')->name('admin.login');


// ... route to Log Viewer
//Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('/vd/documentSets/{taID?}/{type?}', 'ViewDataController@documents');


Route::group(['domain'=>'otcadmin.net'], function(){});
//Route::group(array('domain' => Request::server('HTTP_HOST')), function() {
//
//    $value1 = 'mobile';
//    $value2 ='admin';
//    $url = Request::server('HTTP_HOST');
//    $check1 = str_contains($url, $value1);
//    $check2 = str_contains($url, $value2);
//
//    if($check1 == true){
//        // Routes
//    }
//
//    if($check2 == true){
//        // Routes
//    }else{
//        // Routes
//    }
//}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Routes To go to Static pages

Route::get('/get-started', function () { return Redirect::to(URL::to('/get-started/index.html')); });

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Catch-all Routes

Route::get('{page}/{a?}/{b?}/{c?}/{d?}', function ($slug) {
    $page = \App\Models\Page::findBySlug($slug);

    return view()->first([
        "pages/{$page->slug}",
        'default-page',
        ], compact('page'));
});
