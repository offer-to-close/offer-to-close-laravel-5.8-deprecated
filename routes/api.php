<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\apiAccess;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// ***************************************************************************************
//  Agents
// ***************************************************************************************

// ... get all records (paginated)
Route::get('/agents/{apiUserID}/{state?}/{pageLength?}/{conditions?}', [
    'middleware' => 'App\Http\Middleware\apiAccess',
    'uses'       => 'AgentController@index',
]);

// ... get specific agent by id
Route::get('agent/{apiUserID}/{id}/{conditions?}', [
    'middleware' => 'App\Http\Middleware\apiAccess',
    'uses'       => 'AgentController@show',
]);

// ... get list of agents by name search
Route::get('agent-name/{apiUserID}/{name}/{state?}/{pageLength?}/{conditions?}', [
    'middleware' => 'App\Http\Middleware\apiAccess',
    'uses'       => 'AgentController@byName',
]);

// ... get specific agent by license #
Route::get('agent-license/{apiUserID}/{license}/{state?}/{pageLength?}/{conditions?}', [
    'middleware' => 'App\Http\Middleware\apiAccess',
    'uses'       => 'AgentController@byLicense',
]);

// ... update (or insert) record. If ID column present attempt update, if not insert
Route::post('agent-upsert/{apiUserID}/{data}', [
    'middleware' => 'App\Http\Middleware\apiAccess',
    'uses'       => 'AgentController@upsert',
]);

// ... update (or insert) record. If ID column present attempt update, if not insert
Route::get('agent-upsert/{apiUserID}/{data}', [
    'middleware' => 'App\Http\Middleware\apiAccess',
    'uses'       => 'AgentController@cannotGet',
]);

// ... insert record. If ID column present it is ignored
Route::post('agent-insert/{apiUserID}/{data}', [
    'middleware' => 'App\Http\Middleware\apiAccess',
    'uses'       => 'AgentController@insert',
]);

// ... insert record. If ID column present it is ignored

Route::get('agent-insert/{apiUserID}/{data}', [
//    'middleware' => 'App\Http\Middleware\apiAccess',
    'uses'       => 'AgentController@cannotGet',
]);


// ... delete a agent
//Route::delete('agent/{apiUserID}/{id}',
//    ['middleware'=>'App\Http\Middleware\apiAccess',
//     'uses'=>'AgentController@destroy',]);

// ...  create new agent
//Route::post('agent',
//   ['middleware'=>'App\Http\Middleware\apiAccess',
//     'uses'=>'AgentController@store',]);


// ***************************************************************************************
//  Communication with otcAdmin
// ***************************************************************************************

// ... Set Member Request Answer from otcAdmin

Route::match(['get', 'post'], 'otcAdmin/ping', [
    'uses'       => 'OtcAdminController@ping',
])->name('api.admin.ping');


Route::match(['get', 'post'], 'otcAdmin/{securityID}/member-request/{mrID}/approve', [
    'middleware' => 'App\Http\Middleware\apiAdminAccess',
    'uses'       => 'OtcAdminController@approveMemberRequest',
])->name('api.admin.request.approve');

Route::match(['get', 'post'], 'otcAdmin/{securityID}/member-request/{mrID}/reject', [
    'middleware' => 'App\Http\Middleware\apiAdminAccess',
    'uses'       => 'OtcAdminController@rejectMemberRequest',
]);

