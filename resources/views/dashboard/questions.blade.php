@extends('layouts.app')


@section('content')
<div class="mc-wrapper">
    <form>
        <div id="mc-steps">
            <h3>1</h3>
            <section class="mc-steps-contents">
                <h2 class="question">Try the keyboard navigation by clicking arrow left or right!?</h2>
                <div class="form-items">
                    <div class="form-group">
                        <input class="form-control" placeholder="First Name">
                    </div>
                     <div class="form-group">
                        <input class="form-control" placeholder="Last Name">
                    </div>
                     <div class="form-group">
                        <input class="form-control" placeholder="Address">
                    </div>
                </div>
            </section>
            <h3>2</h3>
            <section class="mc-steps-contents">
                <h2 class="question">Wonderful transition effects?</h2>
            </section>
            <h3>3</h3>
            <section class="mc-steps-contents">
                <h2 class="question">The next and previous buttons help you to navigate through your c13123ontent?</h2>
            </section>
             <h3>4</h3>
            <section class="mc-steps-contents">
                <h2 class="question">The next and previous buttons help you to navigate throug321321h your content?</h2>
            </section>
             <h3>5</h3>
            <section class="mc-steps-contents">
                <h2 class="question">The next and previous buttons help you to navigate th312321rough your content?</h2>
            </section>
             <h3>6</h3>
            <section class="mc-steps-contents">
                <h2 class="question">The
                 next and previous buttons help you to navigate through y31231our content?</h2>
            </section>
             <h3>7</h3>
            <section class="mc-steps-contents">
                <h2 class="question">The next and previous buttons help you to navigate321321 through your content?</h2>
            </section>
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
            

        $("#mc-steps").steps({
            headerTag: "h3",
            bodyTag: "section",
             contentContainerTag: "div",
            transitionEffect: "slideLeft",
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span>',
            transitionEffect : 1
        });


    });
</script>
@endsection