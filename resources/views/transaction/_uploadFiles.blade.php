<div class="kp-content-area">

        <div class="mc-wrapper " id="uploadfiles">
            <div class="mc-heading">
                <h1>Upload File</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger alert-dismissable text-center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    All of the field are required
                </div>
            @endif

            <div class="mc-box">

                <form  action="{{ route('saveFile') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}

                    <input type="hidden" name="transactionID" value="{{ $transactionID ?? 0 }}">

                    <div class="form-groups">

                        <div class="form-group">

                            <div class="wraper">
                                <input type="file" name="document" />
                            </div>

                            <br/>
<?php
    $parameters = ['displayPattern'   => '{ShortName} - {Description}', // what to display in dropdown
                   'storePattern'     => '{ID}',   // what value gets stored
                   'sort' => 'ShortName',
                   'firstRowLabel' => '** Select a document **',
                   'firstRowValue' => '*'
    ];
?>
                            <div class="form-group{{ $errors->has('documentID') ? ' has-error' : '' }} with-label">
                                <label for="documentID" class="label-control">Document </label>
                                <select name="documentID">
                                    {!! \App\Library\Utilities\FormElementHelpers::buildComplexOptionListFromTable('Documents', $parameters) !!}
                                </select>
                            </div>

                        </div>

                        <div class="form-group radio-boxes">
                            <h2>Signed By</h2>
                            @foreach($buyers as $buyer)
                                <label class="label-control checkboxes radio"><input type="checkbox" name="signedBy[]" value="b+{{$buyer['ID'] ?? null}}">Buyer: {{$buyer['NameFull'] ?? '?'}}<span></span></label>
                            @endforeach
                            @foreach($sellers as $seller)
                                <label class="label-control checkboxes radio"><input type="checkbox" name="signedBy[]" value="s+{{$seller['ID'] ?? null}}">Seller: {{$seller['NameFull'] ?? '?'}}<span></span></label>
                            @endforeach
                            <label class="label-control checkboxes radio"><input type="checkbox" name="signedBy[]" value="ba+0">Buyer's Agent<span></span></label>
                            <label class="label-control checkboxes radio"><input type="checkbox" name="signedBy[]" value="sa+0">Seller's Agent<span></span></label>
                        </div>
                        <div class="form-ctrls text-center">
                            <button class="btn red">SUBMIT</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

</div>
