@extends('layouts.app')


@section('content')
<div class="mc-wrapper questionslist">
    <form>
        <div id="mc-steps">
            <h3>1</h3>
            <section class="mc-steps-contents">
                <h2 class="question">What is the purchase price?</h2>
                <div class="form-items">
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="">
                    </div>
                </div>
               
            </section>
            <h3>2</h3>
            <section class="mc-steps-contents">
                <h2 class="question">What is the type of property being sold?</h2>
                 <div class="form-items">
                     <div class="form-group">
                        <label class="label-control checkboxes radio"><input type="radio" name="property-type" value="Condominium">Condominium<span></span></label>
                        <label class="label-control checkboxes radio"><input type="radio" name="property-type" value="Townhouse">Townhouse<span></span></label>
                        <label class="label-control checkboxes radio"><input type="radio" name="property-type" value="Single-Family">Single-Family<span></span></label>
                        <label class="label-control checkboxes radio"><input type="radio" name="property-type" value="Multi-family / Income Property">Multi-family / Income Property<span></span></label>
                    </div>
                </div>
            </section>
            <h3>3</h3>
            <section class="mc-steps-contents">
                <h2 class="question">'What year was the property built?</h2>
                 <div class="form-items">
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="">
                    </div>
                </div>
            </section>
             <h3>4</h3>
            <section class="mc-steps-contents">
                <h2 class="question">What type of loan is the buyer intending to obtain?</h2>
                <div class="form-items">
                    <!--<div class="form-group form-select">
                        <select class="form-control" type="text">
                            <option value="FHA">FHA</option>
                            <option value="VA">VA</option>
                            <option value="Conventional">Conventional</option>
                        </select>
                    </div> -->
                     <div class="form-group">
                        <label class="label-control checkboxes radio"><input type="radio" name="loan-type" value="FHA">FHA<span></span></label>
                        <label class="label-control checkboxes radio"><input type="radio" name="loan-type" value="VA">VA<span></span></label>
                        <label class="label-control checkboxes radio"><input type="radio" name="loan-type" value="Conventional">Conventional<span></span></label>
                    </div>
                </div>
            </section>
             <h3>5</h3>
            <section class="mc-steps-contents">
                <h2 class="question">Type of sale</h2>
                <div class="form-items">

                     <div class="form-group">
                        <label class="label-control checkboxes radio"><input type="radio" name="sale-type" value="normal">normal<span></span></label>
                        <label class="label-control checkboxes radio"><input type="radio" name="sale-type" value="reo">reo<span></span></label>
                        <label class="label-control checkboxes radio"><input type="radio" name="sale-type" value="shortSale">shortSale<span></span></label>
                        <label class="label-control checkboxes radio"><input type="radio" name="sale-type" value="probate">probate<span></span></label>
                    </div>
                </div>
            </section>
             <h3>6</h3>
            <section class="mc-steps-contents">
                <h2 class="question">Will the seller be renting back the property to the buyer?</h2>
                <div class="form-items">

                    <div class="form-group">
                        <label class="label-control checkboxes radio"><input type="radio" name="sale-type" value="No">No<span></span></label>
                        <label class="label-control checkboxes radio"><input type="radio" name="sale-type" value="Yes, for less than 30 days">Yes, for less than 30 days<span></span></label>
                        <label class="label-control checkboxes radio"><input type="radio" name="sale-type" value="Yes, for more than 30 days">Yes, for more than 30 days<span></span></label>

                    </div>
                 
                </div>
            </section>
             <h3>7</h3>
            <section class="mc-steps-contents">
                <h2 class="question">Do any of the following apply? (check all that apply)</h2>
                <div class="form-items">
                    <div class="form-group">
                        <label class="label-control checkboxes"><input type="checkbox" value="The sale is a short-sale">The sale is a short-sale<span></span></label>
                        <label class="label-control checkboxes"><input type="checkbox" value="The seller is a trust">The seller is a trust<span></span></label>
                        <label class="label-control checkboxes"><input type="checkbox" value="The buyer is a trust">The buyer is a trust<span></span></label>
                        <label class="label-control checkboxes"><input type="checkbox" value="This a distressed or court-ordered sale (e.g. REO, Short-Sale, or probate)">This a distressed or court-ordered sale (e.g. REO, Short-Sale, or probate)<span></span></label>
                        <label class="label-control checkboxes"><input type="checkbox" value="The home has an HOA">The home has an HOA<span></span></label>
                        <label class="label-control checkboxes"><input type="checkbox" value="The purchase is contingent on the buyer selling their home">The purchase is contingent on the buyer selling their home<span></span></label>
                        <label class="label-control checkboxes"><input type="checkbox" value="The home has either a septic tank or well">The home has either a septic tank or well<span></span></label>
                        <label class="label-control checkboxes"><input type="checkbox" value="The buyer has waived their inspection contingency">The buyer has waived their inspection contingency<span></span></label>
                    </div>
                </div>
            </section>
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
            

        $("#mc-steps").steps({
            headerTag: "h3",
            bodyTag: "section",
             contentContainerTag: "div",
            transitionEffect: "slideLeft",
            autoFocus: true,
            titleTemplate: '<span class="number">#index#</span>',
            transitionEffect : 1
        });


    });
</script>
@endsection