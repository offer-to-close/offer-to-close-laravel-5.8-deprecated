@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Transaction Summary: Documents</h2>
            </div>

            <div class="pull-left">
                <div class="table-responsive">



                    <table class="table">
                        <thead>
                        <tr>
                            <th>isIncluded</th>
                            <th>Document</th>
                            <th>Due Date</th>
                            <th>Date Completed</th>
                            <th>SignaturesNeeded</th>
                            <th>Docs Uploaded</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        @foreach($collection as $doc)
                            <tr>
                                <!-- <td>{{$doc->ID}}</td> -->
                                <td>{!! $doc->isIncluded ? '<i class="fas fa-check-circle"></i>' : ''!!}</td>
                                <td>{{$doc->ShortName}}&nbsp;-&nbsp;{{$doc->Description}}</td>
                                    <td>{{ date('M j, y', strtotime($doc->DateDue)) ?? '?'}}</td>
                                    <td>{{ is_null($doc->DateCompleted) ? '-' : date('M j, y', strtotime($doc->DateCompleted)) }}</td>
                                <td>{{$doc->RequiredSignatures ?? '?'}}</td>
                                    <?php
                                       if(isset($files[$doc->ID]))
                                           {
                                               foreach($files[$doc->ID] as $file)
                                               {
                                                    $f[] = pathinfo($file['url'], PATHINFO_BASENAME);
                                               }
                                           }
                                           else $f = [];
                                    ?>
                                <td>{!! implode('<br/>', $f) !!}</td>
                                <td>&nbsp;</td>
                            </tr>
                        @endforeach
                    </table>

                </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('transaction.index') }}"> Back</a>
            </div>
        </div>
    </div>


@endsection