@extends('2a.layouts.master')
<?php




?>
@section('custom_css')
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/disclosures/radio.css') }}" rel="stylesheet">



@endsection

<style>
    .row{
        margin-left:5% !important;
    }
    .q-a{
        padding: 10px;
        padding-top: -20px;
        float: left;
        width: 33%;
        height: 300px;
        word-wrap: normal;
    }
    .q-a:hover{
        cursor: pointer;
        border: solid 1px #dfdfdf;
        background: #00e2da;
        color: white;
    }

    .question-number{
        font-weight: bold;
    }

    .q-a:hover .question-number{
        color: white;
    }

    .q-a:hover .answer{
        color: white;
        opacity: .8;
    }

    .q-a:hover .question-text{
        color: white;
        opacity: .8;
    }

    .answer{
        font-style: italic;
        font-weight: bold;
        font-size: 15px;
        padding: 10px;
        color: black;
        text-align: center;
    }

    .answer-display{
        text-decoration: underline;
    }

    .edit{
        color: white;
        font-size: 20px;
        text-align: center;
    }
</style>
@section('content')
    <section class="appendMe">

    </section>
    <section class="main-details dashboard clearfix">
        <div class="wrap">
            <div class="top">
                <!-- todo temporary styling on the div below-->
                <div class="container-fluid" style="height:200px;width: 100%;">
                    <div class="tb-wrap">
                        <div class="tb-layout">
                            <div class="tb-item left">

                            </div>
                            <div class="tb-item right">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container-fluid">
                    <!-- MAIN TAB -->
                    <div id="timeline" class="tab-content">
                        <div class="tb-wrap">
                            <div class="tb-layout">
                                <div class="tb-item center">
                                    <div class="review-information"></div>
                                    <div class="questionnaireContainer container">
                                        <div id="signArea" style="width: 200px;height: 200px;"></div>
                                        <canvas id="myCanvas" width="500" height="500" style="border: solid 1px black;"></canvas>
                                    </div>
                                    <button class="saveToPng">Save as Png</button>
                                    <button class="saveToJpg">Save as Jpg</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        var signaturePad = null;
        $(document).ready(function() {
            var canvas = document.querySelector("canvas");

            signaturePad = new SignaturePad(canvas, {
                backgroundColor: 'rgb(255, 255, 255)', // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
            });
        });

        $(document).on('click', '.saveToPng', function(){
            console.log('clicked save to png');
            //signaturePad.toDataURL(); // save image as PNG
            // Returns signature image as an array of point groups
            const data = signaturePad.toDataURL('image/png');
            console.log(data);
            window.open(data);
            // Draws signature image from an array of point groups
                        signaturePad.fromData(data);

            // Clears the canvas
                        signaturePad.clear();

            // Returns true if canvas is empty, otherwise returns false
                        signaturePad.isEmpty();

            // Unbinds all event handlers
                        signaturePad.off();

            // Rebinds all event handlers
                        signaturePad.on();
        });

        $(document).on('click', '.saveToJpg', function(){
            console.log('clicked save to jpg');
            signaturePad.toDataURL("image/jpeg"); // save image as JPEG
        });

    </script>


@endsection