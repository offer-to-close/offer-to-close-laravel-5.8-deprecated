<div class="" style="margin-top: 20px;">    
    <h3> {{ $question }} </h3>    
    <a href="#"><p style="color: #21d7d1;">What does this mean?</p></a>
    <div class="answer-section" style="margin-top: 50px;">
        @foreach ($answers as $a)
            <label class="rectangular-radio">
                <input type="radio" name="{{ $fieldName }}[]" value="{{ $a['value'] }}" >
                <span class="rectangular-radio-value">{{ $a['display'] }}</span>
            </label>
        @endforeach
    </div>
</div>