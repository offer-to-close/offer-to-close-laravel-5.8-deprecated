@extends('2a.layouts.master')
<?php
    $userID = $userID ?? \App\Http\Controllers\CredentialController::current()->ID();
    $transactionID = $transactionID ?? 1;
    $title = $title ?? 'Shared Documents for ' . $transactionID;
    $address = $address ?? 'No Address Provided';
    $documentCount = \App\Models\ShareRoom::documentCount($transactionID);
    $roomDir = \App\Models\ShareRoom::view($transactionID);
dd(['doc count'=>$documentCount, 'room dir'=>$roomDir]);
$dirByUser = \App\Http\Controllers\ShareRoomController::directoryByUser($transactionID, $userID);

$documents = $documents ?? [];
$roomCount = \App\Combine\ShareRoomCombine::roomCountByUser($userID);
?>
@section('custom_css')
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="container-left col-xs-10">
        <div class="row">
            <div class="add-new-transaction-title col-xs-10">
                <div class="col-xs-12">
                    <h1>{!! $title !!}</h1>
                </div>
            </div>

        @if(Session::has('success'))
            @include('2a.partials._successMessage', [
                '_Message' => Session::get('success'),
            ])
        @elseif(Session::has('failMessage'))
            @include('2a.partials._failureMessage', [
                '_Message' => Session::get('failMessage'),
            ])
        @elseif( Session::has('fail') )
            @include('2a.partials._overrideAddress', [
               '_Message' => Session::get('fail'),
               '_Address' => Session::get('address'),
               '_TransactionId' => Session::get('transactionId'),
            ])
        @endif


            <div class="row" style="margin-left: 0; margin-right: 0">

                <div class="two-by-two two-by-two-white col-xs-12">
                You can see {{$roomCount}} rooms. But do you really want to?
                </div>
                <!-- ############################ -->

                <div class="one-by-one col-xs-12">
                </div>

                <div class="one-by-one-white col-xs-12">

                </div>
            </div>
        </div>

        <!-- MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMOODEL -->
        <div class="container-two col-xs-4"></div>
    </div>
@endsection