@extends('2a.layouts.master')

<?php
    $sides = ['b'=>'Buyer', 's'=>'Seller', ];
    $bigdata = $data;

    @$dateContract=$data['transaction']['DateOfferPrepared'];
    @$dateAccept=$data['transaction']['DateAcceptance'];
    @$transaction_id=$data['transaction']['ID'];
    $side = $data['transaction']['Side'];

    $taid = isServerLive() ? null : (' <span style="color: '. config('otc.orange') . ';">' . ($transaction_id ?? null)) . '</span>';
        $info = null;
    $ownershipInfo = \App\Combine\TransactionCombine::getOwners($transaction_id);
// ... When the cursor hovers over the header words a tool tip displays and lists the name data in $ownershipInfo
    $title = ($firstTime ?? false) ? ('Add New ' . ($sides[$side]??null) . ' Transaction') : ('Edit ' . ($sides[$side]??null) . ' Transaction' . $info . $taid);
    $mode =  array_sum($_isCreated) == 0 ? 'create' : 'edit';
    if ($_isCreated['p'])
    {
        $address = \App\Library\otc\AddressVerification::formatAddress($data['property'][0], 1);
    }
    else $address = null;
    $is_readonly = !($address == '') ? ' READONLY ' : null;
?>
@section('content')
    <div class="container-left col-xs-10">
        <div class="row">
            <div class="add-new-transaction-title col-xs-10">
                <div class="col-xs-12">
                    <h1>{!! $title !!}</h1>
                </div>
            </div>
             @if(Session::has('success'))
                    @include('2a.partials._successMessage', [
                        '_Message' => Session::get('success'),
                    ])
             @elseif(Session::has('failMessage'))
                    @include('2a.partials._failureMessage', [
                        '_Message' => Session::get('failMessage'),
                    ])
             @elseif( Session::has('fail') )
                     @include('2a.partials._overrideAddress', [
                        '_Message' => Session::get('fail'),
                        '_Address' => Session::get('address'),
                        '_TransactionId' => Session::get('transactionId'),
                     ])
             @endif
            <!-- HEY DEVELOPER!! -->
            <!-- When the page is in create mode (see code above) then this div needs to be a form -->
            <!-- When an address is typed into the field it will use the SmartyStreets api to auto-complete -->
            <!--   and when the user hits Enter, then the address will be saved into the property record -->
            <!--   and the user will come back to this same page. This time the page will be in edit mode -->
            <!--   and the field will be readonly. -->


            <div class="div-address-search col-xs-7 col-xs-offset-2">
                 @include('2a.partials._addressAutoComplete', [
                    'o_action' => route("saveProperty"),
                    'o_placeholder' => 'Add Property Address',
                    'Table'    => 'properties',
                    'ID'       => @$data['property'][0]['ID'],
                    'default_address_value' => @$address,
                ])
            </div>


           <div class="row" style="margin-left: 0; margin-right: 0">
            <div class="col-md-12 text-right" style="margin-top:10px;">

                @if (\App\Combine\DocumentCombine::checkForDocListData($transaction_id) && \App\Combine\TaskCombine::checkForTaskListData($transaction_id) )
                    <a href="{{route('transactionSummary.timeline', ['transactionID'=>$transaction_id])}}">
                    <button href="" id="ta_show_summary" class="btn btn-teal" style="text-transform: uppercase;">Show Summary</button>
                    </a>

                     @if($data['transaction']['Status'] == 'draft' || $data['transaction']['Status'] == 'closed' )
                            <button href="#" id="ta_status_open" class="btn btn-teal"> OPEN TRANSACTION</button>
                     @else
                             <button href="#" id="ta_status_close" class="btn btn-teal"> CLOSE TRANSACTION </button>
                     @endif
                @endif
           </div>


            <div class="two-by-two two-by-two-white col-xs-12">
                @if($_isCreated['d'])
                    @include('2a.transaction.home._info_d')
                @else
                <a class="open-tdn-modal transaction_details_data-class">
                    <div onclick="window.location.href = '#';" class="col-xs-12 col-md-6">
                        <div class="red red-blue-2 col-xs-12">
                            <div class="img"></div>
                            <div class="transaction-placeholder">
                                <h4>Add New</h4>
                                <h2>Transaction Details</h2>
                            </div>
                        </div>
                    </div>
                </a>
                @endif
                @include('2a.transaction.home.modals._modal_d')

                @if ($_isCreated['p'])
                    @include('2a.transaction.home._info_p')
                @else
                <a href="#" class="view-property-modal">
                    <div class="col-xs-12 col-md-6">
                            <div class="blue red-blue-2 col-xs-12">
                                <div class="img"></div>
                                <div class="transaction-placeholder">
                                    <h4>Add New</h4>
                                    <h2>Property Details</h2>
                                </div>
                            </div>
                    </div>
                 </a>
                @endif
                @include('2a.transaction.home.modals._modal_p')
            </div>
            <!-- ############################ -->

            <div class="one-by-one col-xs-12">
                @if ($_isCreated['b'])
                    @include('2a.transaction.home._info_h', ['role'=>'buyer'])
                @endif
                @if(!$_isCreated['b'])
                <a class="get-h-data-input empty-buyer-box affterr" data-h-url-input="{{route('inputBuyer',['transactionID'=>$transaction_id])}}" data-form-type="buyers">
                    <div onclick="window.location.href = '#';" class="col-xs-12 col-md-6 col-lg-3">
                        <div class="red red-blue-4 col-xs-12">
                            <div class="img"></div>
                            <div class="transaction-placeholder">
                                <h4>Add New</h4>
                                <h2>Buyer</h2>
                            </div>
                        </div>
                    </div>
                </a>
                @endif

                @if ($_isCreated['s'])
                    @include('2a.transaction.home._info_h', ['role'=>'seller'])
                @endif
                @if(!$_isCreated['s'])
                <a class="get-h-data-input empty-seller-box" data-h-url-input="{{route('inputSeller',['transactionID'=>$transaction_id])}}" data-form-type="sellers">
                    <div onclick="window.location.href = '#';" class="col-xs-12 col-md-6 col-lg-3">
                        <div class="blue red-blue-4 col-xs-12">
                            <div class="img"></div>
                            <div class="transaction-placeholder">
                                <h4>Add New</h4>
                                <h2>Seller</h2>
                            </div>
                        </div>
                    </div>
                </a>
                @endif

                @include('2a.transaction.home.modals._modal_a')
                @include('2a.transaction.home.modals._modal_h')


                @if ($_isCreated['ba'] || $data['transaction']['BuyersAgent_ID'] === 0)
                    @include('2a.transaction.home._info_a', ['role'=>'buyer'])
                @elseif(!$_isCreated['ba'] && $data['transaction']['BuyersAgent_ID'] !== 0)
                    <div onclick="window.location.href = '#';" data-model-for="ba" data-a-url="{{ route('inputBuyerAgent', $data['transaction']['ID'] ) }}" class="col-xs-12 col-md-6 col-lg-3 view-agent">
                        <div class="red red-blue-4 col-xs-12">
                            <div class="img"></div>
                            <div class="transaction-placeholder">
                                <h4>Add New</h4>
                                <h2>Buyer's Agent</h2>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($_isCreated['sa'] || $data['transaction']['SellersAgent_ID'] === 0)
                    @include('2a.transaction.home._info_a', ['role'=>'seller'])
                @elseif(!$_isCreated['sa'] && $data['transaction']['SellersAgent_ID'] !== 0)
                    <div onclick="window.location.href = '#';" data-model-for="sa" data-a-url="{{ route('inputSellerAgent', $data['transaction']['ID'] ) }}" class="col-xs-12 col-md-6 col-lg-3 view-agent">
                        <div class="blue red-blue-4 col-xs-12">
                            <div class="img"></div>
                            <div class="transaction-placeholder">
                                <h4>Add New</h4>
                                <h2>Seller's Agent</h2>
                            </div>
                        </div>
                    </div>
                @endif
                </div>

                <div class="one-by-one-white col-xs-12">
                    @if($_isCreated['btc'] or $_isCreated['stc'])
                        @include('2a.transaction.home._info_tc')
                    @else
                        <div onclick="window.location.href = '#';" data-model-for="tc" class="col-xs-12 col-md-6 col-lg-3 view-tc">
                            <div class="white col-xs-12">
                                <div class="img"></div>
                                <div class="transaction-placeholder">
                                    <div class="transaction-placeholder-add-text" style="margin-top: 13%;"><h4>Add a</h4></div>
                                    <div  class="transaction-placeholder-role"><h4>Transaction Coordinator</h4></div>
                                </div>
                            </div>
                        </div>
                    @endif
                    @include('2a.transaction.home.modals._newmodal_tc')


                    @if($_isCreated['e'])
                        @include('2a.transaction.home._info_aux', ['role'=>'escrow'])
                    @endif
                    @if(!$_isCreated['e'])
                        <div onclick="window.location.href = '#';" data-modal-for="escrow" data-a-url="{{ route('inputEscrow', $data['transaction']['ID']) }}" class="col-xs-12 col-md-6 col-lg-3 view-aux-modal">
                            <div class="white col-xs-12">
                                <div class="img"></div>
                                <div class="transaction-placeholder">
                                    <div class="transaction-placeholder-add-text" style="margin-top: 10%;"><h4>Add New</h4></div>
                                    <div class="transaction-placeholder-role"><h4>Escrow Agent</h4></div>
                                </div>
                            </div>
                        </div>
                        @endif

                        @if($_isCreated['t'])
                            @include('2a.transaction.home._info_aux', ['role'=>'title'])
                        @endif
                        @if(!$_isCreated['t'])
                        <div onclick="window.location.href = '#';" data-modal-for="title" data-a-url="{{ route('inputTitle', $data['transaction']['ID']) }}" class="col-xs-12 col-md-6 col-lg-3 view-aux-modal">
                            <div class="white col-xs-12">
                                <div class="img"></div>
                                <div class="transaction-placeholder">
                                    <div class="transaction-placeholder-add-text" style="margin-top: 10%;"><h4>Add New</h4></div>
                                    <div class="transaction-placeholder-role"><h4>Title Agent</h4></div>
                                </div>
                            </div>
                        </div>
                        @endif

                        @if($_isCreated['l'])
                            @include('2a.transaction.home._info_aux', ['role'=>'loan'])
                        @endif
                        @if(!$_isCreated['l'])
                            <div onclick="window.location.href = '#';" data-modal-for="loan" data-a-url="{{ route('inputLoan', $data['transaction']['ID']) }}" class="col-xs-12 col-md-6 col-lg-3 view-aux-modal">
                                <div class="white col-xs-12">
                                    <div class="img"></div>
                                    <div class="transaction-placeholder">
                                        <div class="transaction-placeholder-add-text" style="margin-top: 10%;"><h4>Add New</h4></div>
                                        <div class="transaction-placeholder-role"><h4>Loan Agent</h4></div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    @include('2a.transaction.home.modals._modal_aux')
             </div>
        </div>

        <!-- MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMOODEL -->
        <div class="container-two col-xs-4"></div>

@endsection


@section('scripts')


    <script>
    var clientRole                  = '{{$data['transaction']['clientRole']}}';
    var userRole                    = '{{session('userRole')}}';
    var escrowRoute                 = '{{route('assignRole.Escrow')}}';
    var loanRoute                   = '{{route('assignRole.Loan')}}';
    var titleRoute                  = '{{route('assignRole.Title')}}';
    var route                       = null;
    var there_is_a_change_in_TD     = false;
    var there_is_a_change_in_TDN    = false;
    var transaction_details_data    = {};
    var token                       = "{{Session::token()}}";
    var savedNameFirst              = null;
    var savedNameLast               = null;

    /**
     * TC MODAL OPENING FUNCTION
     * This function opens the modal, the logic for inside the modal is inside of the modal template.
     * */
    //var clientRole = '$clientRole}}';
    //var roleCode = '$roleCode}}';
    //var roleDisplay = '\App\Combine\RoleCombine::getDisplayByRole($roleCode) ?? $roleCode}}';
    var transactionID = '{{$data['transaction']['ID']}}';
    var ajax =  true;
    $('.view-tc').on('click', function(){

        var el = $(this);
        var modal = $('.tcs-modal');
        $('.tcs-modal #_roleCode').val('btc');
        $('.tcs-modal #tc_ajax').val(ajax);
        //$('.tcs-modal .modal-title').html(roleDisplay+' Details');
        modal.modal({backdrop: 'static', keyboard: false});
        CHANGES_MADE = false;
        modal.find('.modal-content').removeClass('dim');
    });

    //END TC MODAL OPENING FUNCTION

    $(document).on('click','.open-tds-modal', function(){
        $('#TDS-model').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $('.the-next-model').click(function(){
        var the_next_step_button=$(this);
        var you_can_go_next=true;
        var this_button=$(this);
        $('.errors').hide();
        if($(this).parents().hasClass('step-1')){
            if($("#TDN-model input[name='PurchasePrice']").val().length==0){
                $("#TDN-model input[name='PurchasePrice']").parents('.red-bottom').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
            if($("#TDN-model input[name='DateOfferPrepared']").val().length==0){
                $("#TDN-model input[name='DateOfferPrepared']").parents('.red-bottom').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
            if($("#TDN-model input[name='EscrowLength']").val().length==0){
                $("#TDN-model input[name='EscrowLength']").parents('.red-bottom').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
            else if($("#TDN-model input[name='EscrowLength']").val()>180){
                $("#TDN-model input[name='EscrowLength']").parents('.red-bottom').siblings('.errors').show().text("The escrow length may not be greater than 180.");
                you_can_go_next=false;
            }
            if($("#TDN-model input[name='DateAcceptance']").val().length==0){
                $("#TDN-model input[name='DateAcceptance']").parents('.red-bottom').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
        }
        if($(this).parents().hasClass('step-2')){
            if($('#TDN-model select[name="ClientRole"]').val().length==0){
                $('#TDN-model select[name="ClientRole"]').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
            if($('#TDN-model select[name="SaleType"]').val().length==0){
                $('#TDN-model select[name="SaleType"]').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
            if($('#TDN-model select[name="LoanType"]').val().length==0){
                $('#TDN-model select[name="LoanType"]').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
        }
        if(you_can_go_next === true)
        {
            if(!the_next_step_button.parents('.model-step').hasClass('step-3')){ // if this is step 1 and 2 only
                $(the_next_step_button).parents('.model-step').attr('hidden',"");
                $(the_next_step_button).parents('.model-step').next('.model-step').removeAttr('hidden');
            }
            if(the_next_step_button.parents('.model-step').hasClass('step-3') && the_next_step_button.parents('#TDN-model').length>0){

                $('.spinner-kero').show();
                var opj ={
                            "ID":                       "{{$bigdata['transaction']['ID']}}",
                            "PurchasePrice"             : $("#TDN-model input[name='PurchasePrice']").val(),
                            "DateAcceptance"            : $("#TDN-model input[name='DateAcceptance']").val(),
                            "DateOfferPrepared"         : $("#TDN-model input[name='DateOfferPrepared']").val(),
                            "EscrowLength"              : $("#TDN-model input[name='EscrowLength'] ").val(),
                            "willRentBack"              : $("#TDN-model input[name='willRentBack1']:checked").val(),
                            "ClientRole"                : $("#TDN-model select[name='ClientRole'] option:selected").val(),
                            "SaleType"                  : $("#TDN-model select[name='SaleType'] option:selected").val(),
                            "LoanType"                  : $("#TDN-model select[name='LoanType'] option:selected").val(),
                            "hasBuyerSellContingency"   : $("#TDN-model input[name='hasBuyerSellContingency1']:checked").val(),
                            "hasSellerBuyContingency"   : $("#TDN-model input[name='hasSellerBuyContingency1']:checked").val(),
                            "hasInspectionContingency"  : $("#TDN-model input[name='hasInspectionContingency1']:checked").val() || 0,
                            "hasAppraisalContingency"   : $("#TDN-model input[name='hasAppraisalContingency1']:checked").val() || 0,
                            "hasLoanContingency"        : $("#TDN-model input[name='hasLoanContingency1']:checked").val() || 0,
                            "needsTermiteInspection"    : $("#TDN-model input[name='needsTermiteInspection1']:checked").val(),
                            "willTenantsRemain"         : $("#TDN-model input[name='willTenantsRemain1']:checked").val(),
                            "_screenMode"               : "create",
                            "Notes"                     : $('#TDN-model .td-note-field').val(),
                            "_token"                    : token,
                            'recalculateDates'          : true,
                        };
                    $.ajax({
                    url : "{{ route('saveDetails') }}",
                    method: "POST",
                    data: opj,
                    error:function(response){
                        $('.spinner-kero').show();

                    },
                    success:function(str){
                        $('.spinner-kero').fadeOut(1000);
                        transaction_details_data=opj;
                        transaction_D_box();
                        there_is_a_change_in_TDN=false;
                        if(this_button.hasClass('save-exit-button-td')){
                            $('#TDN-model').modal('hide');
                        }
                        if(this_button.hasClass('next-continue-btn')){
                            $('#TDN-model').modal('hide');
                            location += 'opm';
                            location.reload();
                        }
                    },
                });
            }
        }
    });
    $('.the-previous-model').click(function(){
        $(this).parents('.model-step').attr('hidden',"");
        $(this).parents('.model-step').prev('.model-step').removeAttr('hidden');
    });
    $('input:radio').click( function(){
            var el = $(this);
            var name = el.attr("name");
            var selected = $('input[name='+ name + ']:checked');
            var unselected = $('input[name='+ name + ']:not(":checked")');
            $(unselected).each(function(){
            var unchecked = $(this);
            unchecked.parent('.o-radio-container').removeClass('true-checked');
            unchecked.parent('.o-radio-container').removeClass('false-checked');
            });
            if(selected.val()== 0){
                selected.parent('.o-radio-container').addClass('false-checked');
            }
            else if(selected.val() > 0){
                selected.parent('.o-radio-container').addClass('true-checked');
            }
    });
    $('input[type=radio]:checked').each(function(){
        el = $(this);
        if (el.val() == 0){
            el.parent('.o-radio-container').addClass('false-checked');
        }
        else if(el.val() > 0){
            el.parent('.o-radio-container').addClass('true-checked');
        }
    });
    $('.masked-phone').mask('(000) 000-0000');
    var CHANGES_MADE = false;
    $(document).ready(function(){
        $('.view-aux-modal').on('click', function(){
            var el     = $(this);
            var modal  = $('.aux-modal');
            modal.modal({backdrop: 'static', keyboard: false});
            var modalFor     = el.attr('data-modal-for');
            var url   = el.attr('data-a-url');
            fillModal(url, modal, modalFor);
            CHANGES_MADE = false;
        });
        $('.aux-modal :input[name]').on('input', function(){
            CHANGES_MADE = true;
        });
    });
    function fillModal(url, modal, modalFor)
    {
            $.ajax({
            type: "GET",
            url: url,
                async: false,
            success: function(response) {
                if( response['_screenMode'] == "edit" )
                {
                    var fields = modal.find(':input[name]');
                        fields.each(function(){
                            el   = $(this);
                            if ( el.attr('name') == 'Address' ) {
                                var Address = '';
                                    Address =  ( response['data']['Street1'] === "" || response['data']['Street1'] === null ) ?  "" : response['data']['Street1'] + ", ";
                                    Address += ( response['data']['Street2'] === "" || response['data']['Street2'] === null ) ?  "" : response['data']['Street2'] + ", ";
                                    Address += ( response['data']['City']    === "" || response['data']['City']    === null ) ?  "" : response['data']['City']    + ", ";
                                    Address += ( response['data']['State']   === "" || response['data']['State']   === null ) ?  "" : response['data']['State']   + ", ";
                                    Address += ( response['data']['Zip']     === "" || response['data']['Zip']     === null ) ?  "" : response['data']['Zip']     + ", ";
                                el.val(Address);
                            }
                            if( el.attr('name') == 'Notes' && response['data']['Notes'] ) {
                                self = $(this);
                                self.closest('div').siblings('div').find('.add-notes').text('NOTES');
                                self.addClass('active');
                            }
                            else if(el.attr('name') == 'Notes'){
                                self = $(this);
                                self.closest('div').siblings('div').find('.add-notes').text('+ ADD A NOTE');
                            }
                            if( response['data'][el.attr('name')] ) {
                                el.val(response['data'][el.attr('name')]);
                            }
                        });
                        savedNameFirst = response['data']['NameFirst'];
                        savedNameLast  = response['data']['NameLast'];
                        modal.find('.modal-title').text("Edit " + response['_role']['display'] + " Details");
                        modal.find('input[name="_roleCode"]').val(response['_role']['code']);
                        /**
                         * The two lines below are added to the responses inside of the following functions
                         * inside of TransactionController:
                         * inputEscrow,inputLoan,inputTitle
                         * */
                        modal.find('input[name="_table"]').val(response['_role']['table']);
                        modal.find('input[name="_model"]').val(response['_role']['model']);
                        /* Need to assign the route based on what aux role is being used
                        *  Routes are set at the top of this script
                        *
                        * */
                        if(response['_role']['code'] === 'e')
                        {
                            route = escrowRoute;
                        }
                        if(response['_role']['code'] === 'l')
                        {
                            route = loanRoute;
                        }
                        if(response['_role']['code'] === 't')
                        {
                            route = titleRoute;
                        }
                        modal.find($('#searchForm')).attr('action', route);
                }
                else {
                    var fields = modal.find(':input[name]');
                        fields.each(function(){
                            el   = $(this);
                            if ( el.attr('name') == 'Address' ) {
                                var Address = '';
                                Address =  ( response['data']['Street1'] === "" || response['data']['Street1'] === null ) ?  "" : response['data']['Street1'] + ", ";
                                Address += ( response['data']['Street2'] === "" || response['data']['Street2'] === null ) ?  "" : response['data']['Street2'] + ", ";
                                Address += ( response['data']['City']    === "" || response['data']['City']    === null ) ?  "" : response['data']['City']    + ", ";
                                Address += ( response['data']['State']   === "" || response['data']['State']   === null ) ?  "" : response['data']['State']   + ", ";
                                Address += ( response['data']['Zip']     === "" || response['data']['Zip']     === null ) ?  "" : response['data']['Zip']     + ", ";
                                el.val(Address);
                            }
                            if ( el.attr('name') == 'ID' && response['data']['ID'] == null  ) {
                                    el.prop('disabled', true);
                            }
                            if( response['data'][el.attr('name')] ) {
                                el.val(response['data'][el.attr('name')]);
                            }
                        });
                    if ( modalFor == 'escrow' ) {
                        modal.find('.modal-title').text("NEW ESCROW'S DETAILS");
                            modal.find('input[name="_roleCode"]').val(response['_role']['code']);
                    }
                    else if( modalFor == 'loan' ){
                        modal.find('.modal-title').text("New Loan Details");
                            modal.find('input[name="_roleCode"]').val(response['_role']['code']);
                    }
                    else if( modalFor == 'title' ){
                        modal.find('.modal-title').text("New Title Details");
                            modal.find('input[name="_roleCode"]').val(response['_role']['code']);
                    }
                    /**
                     * The two lines below are added to the responses inside of the following functions
                     * inside of TransactionController:
                     * inputEscrow,inputLoan,inputTitle
                     * */
                    modal.find('input[name="_table"]').val(response['_role']['table']);
                    modal.find('input[name="_model"]').val(response['_role']['model']);
                    /* Need to assign the route based on what aux role is being used
                    *  Routes are set at the top of this script
                    *
                    * */
                    if(response['_role']['code'] === 'e') route = escrowRoute;
                    if(response['_role']['code'] === 'l') route = loanRoute;
                    if(response['_role']['code'] === 't') route = titleRoute;
                    modal.find($('#searchForm')).attr('action', route);
                }
            }
        });
    }

    $('.open-tdn-modal').click(function(){
        $('#TDN-model').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $('input[name="NameFirst"], input[name="NameLast"]').keyup(function(){
        this.value = this.value.replace(/[^A-Za-z0-9 ,.'-]/g, '');
    });

    function transaction_D_box(){
            var monthNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
            var date=new Date(transaction_details_data['DateOfferPrepared']);
            var DateOfferPrepared="";
        DateOfferPrepared+= monthNames[date.getMonth()] +"&nbsp;";
        DateOfferPrepared+= date.getDate()+'&nbsp;,&nbsp;';
        DateOfferPrepared+= date.getFullYear();

            function addComma(num) {
                return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            var date=new Date(transaction_details_data['DateAcceptance']);
            var DateAcceptance="";
        DateAcceptance+= monthNames[date.getMonth()] + "&nbsp;";
        DateAcceptance+= date.getDate()+'&nbsp;,&nbsp;';
        DateAcceptance+= date.getFullYear();
            var TD_box='<div class="col-xs-12 col-md-6 transaction_details_data-class">';
                TD_box+=     '<div class="white col-xs-12">';
                TD_box+=         '<div class="img"></div>';
                TD_box+=         '<div class="transaction-details text-left">';
                TD_box+=              '<h2>Transaction Details</h2>';
                TD_box+=              '<div class="info">';
                TD_box+=                   '<div class="purchase-price col-lg-5 col-xs-12">';
                TD_box+=                        '<span>Purchase Price</span>';
                TD_box+=                        '<h1>$'+addComma(transaction_details_data['PurchasePrice'])+'</h1>';
                TD_box+=                    '</div>';
                TD_box+=                     '<div class="right col-xs-7">';
                TD_box+=                          '<div class="contract-date col-xs-12 ">';
                TD_box+=                                '<span>Contract Date</span>';
                TD_box+=                                '<h6>'+DateOfferPrepared+'</h6>';
                TD_box+=                           '</div>';
                TD_box+=                            '<div class="contract-date col-xs-12">';
                TD_box+=                                '<span>Accepted Date</span>';
                TD_box+=                                '<h6>'+DateAcceptance+'</h6>';
                TD_box+=                            '</div>';
                TD_box+=                      '</div>';
                TD_box+=                     '<a class="open-tds-modal">Edit & View</a>';
                TD_box+=               '</div>';
                TD_box+=           '</div>';
                TD_box+=      '</div>';
                TD_box+= '</div>';
            $('.two-by-two-white .transaction_details_data-class').remove();
            TD_box=$(TD_box).clone(true,true);
            $('.two-by-two-white').prepend(TD_box);
            // ####___The modals data___####
            $('#TD-model input[name="PurchasePrice"]').val(transaction_details_data['PurchasePrice']);
            $('#TD-model input[name="DateAcceptance"]').val(transaction_details_data['DateAcceptance']);
            $('#TD-model input[name="DateOfferPrepared"]').val(transaction_details_data['DateOfferPrepared']);
            $('#TD-model input[name="EscrowLength"]').val(transaction_details_data['EscrowLength']);
            $('#TD-model select[name="ClientRole"]').val(transaction_details_data['ClientRole']);
            $('#TD-model select[name="SaleType"]').val(transaction_details_data['SaleType']);
            $('#TD-model select[name="LoanType"]').val(transaction_details_data['LoanType']);
            $('#TD-model select[name="hasBuyerSellContingency"]').val(transaction_details_data['hasBuyerSellContingency']);
            $('#TD-model select[name="hasSellerBuyContingency"]').val(transaction_details_data['hasSellerBuyContingency']);
            $('#TD-model select[name="hasInspectionContingency"]').val(transaction_details_data['hasInspectionContingency']);
            $('#TD-model select[name="hasLoanContingency"]').val(transaction_details_data['hasLoanContingency']);
            $('#TD-model select[name="hasAppraisalContingency"]').val(transaction_details_data['hasAppraisalContingency']);
            $('#TD-model select[name="needsTermiteInspection"]').val(transaction_details_data['needsTermiteInspection']);
            $('#TD-model select[name="willRentBack"]').val(transaction_details_data['willRentBack']);
            $('#TD-model select[name="willTenantsRemain"]').val(transaction_details_data['willTenantsRemain']);

            $('#TDS-model .ClientRole').text(transaction_details_data['ClientRole']);
            $('#TDS-model .DateAcceptance').text(transaction_details_data['DateAcceptance']);
            $('#TDS-model .DateOfferPrepared').text(transaction_details_data['DateOfferPrepared']);
            $('#TDS-model .EscrowLength').text(transaction_details_data['EscrowLength']);
            $('#TDS-model .LoanType').text(transaction_details_data['LoanType']);
            $('#TDS-model .PurchasePrice').text(transaction_details_data['PurchasePrice']);
            $('#TDS-model .SaleType').text(transaction_details_data['SaleType']);
            $('#TDS-model .hasBuyerSellContingency').text( (transaction_details_data['hasBuyerSellContingency']===1)?'Yes':'No');
            $('#TDS-model .hasInspectionContingency').text( (transaction_details_data['hasInspectionContingency']>0)?'Yes':'No');
            $('#TDS-model .hasLoanContingency').text( (transaction_details_data['hasLoanContingency']>0)?'Yes':'No');
            $('#TDS-model .hasAppraisalContingency').text( (transaction_details_data['hasAppraisalContingency']>0)?'Yes':'No');
            $('#TDS-model .needsTermiteInspection').text( (transaction_details_data['needsTermiteInspection']===1)?'Yes':'No');
            $('#TDS-model .willTenantsRemain').text( (transaction_details_data['willTenantsRemain']===1)?'Yes':'No');
            $('#TDS-model .Notes').text(transaction_details_data['Notes']);
        }


        $(document).ready(function(){
            var CHANGES_MADE = false;
            $('.view-property-modal').click( function(){
                var el = $(this);
                var modal = $('.property-modal');
                modal.modal({backdrop: 'static', keyboard: false});
                CHANGES_MADE = false;
            });

            $('.aux-modal :input[name]').on('input', function(){
                CHANGES_MADE = true;
            });

            $('.view-agent').on('click', function(){
                var el = $(this);
                var modal   = $('.agents-modal');
                modal.modal({backdrop: 'static', keyboard: false});
                var spinner = modal.find('.spinner');
                spinner.show();
                modal.find('.modal-content').addClass('dim');
                var source   = el.attr('data-model-for');
                var url = el.attr('data-a-url');
                if(url)
                {
                    $.ajax({
                        type: "GET",
                        url: url,
                        success: function(response) {
                            var fields = $('.agents-modal').find(':input[name]');
                            fields.each(function(){
                                el   = $(this);
                                if ( el.attr('name') == 'Address' ) {
                                    var Address = '';
                                        Address =  ( response['data']['Street1'] ) ? response['data']['Street1'] + ", " : " ";
                                        Address += ( response['data']['Street2'] ) ? response['data']['Street2'] + ", " : " ";
                                        Address += ( response['data']['City']    ) ? response['data']['City']    + ", " : "";
                                        Address += ( response['data']['State']   ) ? response['data']['State']   + " " : "";
                                        Address += ( response['data']['Zip']     ) ? response['data']['Zip']    : "";
                                    el.val(Address);
                                }
                                 if( el.attr('name') == 'Notes' && response['data']['Notes'] ) {
                                   self = $(this);
                                    self.closest('div').siblings('div').find('.add-notes').text('NOTES');
                                    self.addClass('active');
                                }
                                else if(el.attr('name') == 'Notes'){
                                    self = $(this);
                                    self.closest('div').siblings('div').find('.add-notes').text('+ ADD A NOTE');
                                }
                                if( response['data'][el.attr('name')] ) {
                                    el.val(response['data'][el.attr('name')]);
                                }
                            });
                            savedNameFirst = response['data']['NameFirst'];
                            savedNameLast = response['data']['NameLast'];
                            $('#BrokerageOffices_ID').trigger('change');
                            $('.agents-modal').find('input[name="_roleCode"]').val(response['_role']['code']);
                            spinner.hide();
                            modal.find('.modal-content').removeClass('dim');

                            $('.agents-modal').find('.modal-title').text(response['_role']['display'] + " Details");

                            /**
                             * Route to TransactionCombine@isSameSide
                             * @type {string}
                             */
                            isSameSideURL = '{{route('isSameSide', [
                                'transactionID' => $data['transaction']['ID'],
                                'source' => ':source',
                                'ajax' => true
                                ])}}';
                            isSameSideURL = isSameSideURL.replace(':source', source);
                            $.ajax({
                                type: 'GET',
                                url: isSameSideURL,
                                success: function(response){
                                    var isSameSide = response.answer;
                                    if ( source == 'ba' ) {
                                        if (isSameSide)
                                        {
                                            modal.find('.cellphone-required').text("*");
                                        }
                                        else
                                        {
                                            modal.find('.cellphone-required').text("");
                                        }

                                    }
                                    else if( source == 'sa' )
                                    {
                                        if (isSameSide)
                                        {
                                            modal.find('.cellphone-required').text("*");
                                        }
                                        else
                                        {
                                            modal.find('.cellphone-required').text("");
                                        }
                                    }
                                }
                            });
                            /**
                             * End of ajax call for isSameSide
                             */

                        },
                    });
                }
                else{

                }
                spinner.hide();
                modal.find('.modal-content').removeClass('dim');
            });

            $('#ta_status_open').on('click', function(){
                el     = $(this);
                taID   = "{{ $transactionID }}";
                status = true;
                update_ta_status(taID, status);
            });

            $('#ta_status_close').on('click', function(){
                el     = $(this);
                taID   = "{{ $transactionID }}";
                status = false;
                update_ta_status(taID, status);
            });

            function update_ta_status(transaction_id, status)
            {
                $.ajax({
                    type: "POST",
                    url:  "{{ route('transaction.updateStatus') }}",
                    data: {'transactionID': transaction_id, 'status': status},
                    success: function(response)
                    {
                       if( response['status'] == 'success' && response['currentStatus'] == 'open')
                       {
                           swal("Success", "Transaction opened successfully", "success", {timer: 4000});
                            location.reload();
                       }
                       else if( response['status'] == 'success' && response['currentStatus'] == 'closed')
                       {
                           swal("Success", "Transaction Has Been Closed Successfully", "success", {timer: 4000});
                          location.reload();
                       }
                       else  swal("Oops...", "Something went wrong!", "error", {timer: 4000});

                    }
                });
            }


            function fillModal(url, modal, modalFor)
            {
                $.ajax({
                    type: "GET",
                    url: url,
                    success: function(response) {
                        if( response['_screenMode'] == "edit" )
                        {
                            var fields = modal.find(':input[name]');
                            fields.each(function(){
                                el   = $(this);
                                if ( el.attr('name') == 'Address' ) {
                                    var Address = '';
                                        Address =  ( response['data']['Street1'] ) ? response['data']['Street1'] + ", " : " ";
                                        Address += ( response['data']['Street2'] ) ? response['data']['Street2'] + ", " : " ";
                                        Address += ( response['data']['City']    ) ? response['data']['City']    + ", " : "";
                                        Address += ( response['data']['State']   ) ? response['data']['State']   + " " : "";
                                        Address += ( response['data']['Zip']     ) ? response['data']['Zip']     : "";
                                    el.val(Address);
                                }
                                if( response['data'][el.attr('name')] ) {
                                    el.val(response['data'][el.attr('name')]);
                                }
                            });
                            modal.find('.modal-title').text("Edit " + response['_role']['display'] + " Details");
                            modal.find('textarea[name="Notes"]').text(response['data']['Notes']);
                            modal.find('input[name="_roleCode"]').val(response['_role']['code']);
                        }
                        else {
                            var fields = modal.find(':input[name]');
                                fields.each(function(){
                                    el   = $(this);
                                    if ( el.attr('name') == 'Address' ) {
                                        var Address = '';
                                            Address =  ( response['data']['Street1'] ) ? response['data']['Street1'] + ", " : " ";
                                            Address += ( response['data']['Street2'] ) ? response['data']['Street2'] + ", " : " ";
                                            Address += ( response['data']['City']    ) ? response['data']['City']    + ", " : "";
                                            Address += ( response['data']['State']   ) ? response['data']['State']   + " " : "";
                                            Address += ( response['data']['Zip']     ) ? response['data']['Zip']     : "";
                                        el.val(Address);
                                    }
                                    if ( el.attr('name') == 'ID' && response['data']['ID'] == null  ) {
                                            el.prop('disabled', true);
                                    }
                                    if( response['data'][el.attr('name')] ) {
                                        el.val(response['data'][el.attr('name')]);
                                    }
                                });
                            if ( modalFor == 'escrow' ) {
                                modal.find('.modal-title').text("NEW ESCROW'S DETAILS");
                                    modal.find('input[name="_roleCode"]').val(response['_role']['code']);
                            }
                            else if( modalFor == 'loan' ){
                                modal.find('.modal-title').text("New Loan Details");
                                    modal.find('input[name="_roleCode"]').val(response['_role']['code']);
                            }
                            else if( modalFor == 'title' ){
                                modal.find('.modal-title').text("New Title Details");
                                    modal.find('input[name="_roleCode"]').val(response['_role']['code']);
                            }
                        }
                    }
                });
            }
        });

        //------Search Code
        var routeUrl = '{{route('search.Person')}}';
        searchPersonAjax(routeUrl);

</script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4J1w4-TUMxlpNYl_NZDl6_Dg2cPhrKS8&libraries=places&callback=initAutocomplete"
                    async defer>

            </script>
@append

