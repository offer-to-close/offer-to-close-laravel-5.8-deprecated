<?php
$boxColor = ['buyer'=>'red', 'seller'=>'blue'];
$people = array_slice($data[$role]->toArray(), 0, 2);
$suffixPerson = count($people) > 1 ? 's' : null;
$hPerson = count($people) > 1 ? 'h5' : 'h4';
?>
@if($_isCreated[substr($role,0,1)])
    <div class="col-xs-12 col-md-6 col-lg-3 fill-buyer-box affterr">
        <div class="{{$boxColor[$role]}} no-cursur red-blue-4 col-xs-12">
            <div class="img"></div>
            <div class="transaction-info text-left">
                <h3>{{ title_case(str_singular($role).$suffixPerson) }}</h3>
                @foreach ($people as $person)
                    <{{$hPerson}}>{{$person['NameFull']}}</{{$hPerson}}>
            <{{$hPerson}}>{{$person['PrimaryPhone'] ? $person['PrimaryPhone'] : ''}}</{{$hPerson}}>
        @endforeach
        <div class="hrefs">
            <a  class="view get-h-data-input" data-h-url-input="{{route('input'.title_case($role),['transactionID'=>$transaction_id, $role.'ID'=>$people[0]['ID']])}}" data-form-type="{{str_plural($role)}}">VIEW</a>
            <!-- <a  class="info">ADD NEW</a> ToDO-->
        </div>
    </div>
    </div>
    </div>
@endif