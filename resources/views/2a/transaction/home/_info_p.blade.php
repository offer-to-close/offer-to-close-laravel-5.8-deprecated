<?php
$address = \App\Library\otc\AddressVerification::formatAddress($data['property'][0], 2);
$yearBuilt =  $data['property'][0]['YearBuilt'];
$HOA       = ($data['property'][0]['hasHOA']) ? 'No' : 'YES';
$HOA       = (is_null($data['property'][0]['hasHOA']) or $data['property'][0]['hasHOA'] == 0) ? 'No' : 'Yes';
$Septic    = (is_null($data['property'][0]['hasSeptic']) or $data['property'][0]['hasSeptic'] == 0) ? 'No' : 'Yes';
$propertyType = \App\Models\lu_PropertyTypes::getDisplay($data['property'][0]['PropertyType']);
?>
<div class="col-xs-12 col-md-6">
    <div class="white col-xs-12">
        <div class="img"></div>
        <div class="property-details text-left">
            <h2>Property Details</h2>
            <div class="info">
                <div class="data col-xs-6">
                    <span>Property Type</span>
                    <h6 class="o-PropertyType">{{ $propertyType }}</h6>
                </div>
                <div class="data col-xs-6">
                    <span>Built In</span>
                    <h6 class="o-YearBuilt">{{$yearBuilt}}</h6>
                </div>
                <div class="data col-xs-6 address">
                    <span>Address</span>
                    <h6 class="o-address">{!!  $address !!}</h6>
                </div>
                <div class="data col-xs-6">
                    <h6 class="o-hasHOA"><span>HOA: </span>{{title_case($HOA)}}</h6>
                    <h6 class="o-hasSeptic"><span>Septic: </span>{{title_case($Septic)}}</h6>
                </div>
                <a href="#" class="view-property-modal"  style="text-transform: uppercase;">Edit & View</a>
            </div>
        </div>
    </div>
</div>
    {{--  @include('2a.transaction.home.modals._modal_p')  --}}
