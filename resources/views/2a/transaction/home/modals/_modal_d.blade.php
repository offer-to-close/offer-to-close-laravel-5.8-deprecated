<?php
    $bigdata=$data;
    $closeOfEscrow = NULL;
    if (!empty($milestones = \App\Library\Utilities\_Convert::toArray($bigdata['timeline'])))
    {
        $milestones                     = \App\Library\Utilities\_Convert::toArray($milestones);
        $closeOfEscrow                  = array_search('Close Escrow', array_column($milestones,'MilestoneName'));
        $inspectionContingencyMilestone = array_search('Inspection Contingency', array_column($milestones,'MilestoneName'));
        $appraisalContingencyMilestone  = array_search('Appraisal Completed', array_column($milestones,'MilestoneName'));       //  Deprecated
        $appraisalContingencyMilestone  = array_search('Appraisal Contingency Completed', array_column($milestones,'MilestoneName'));
        $loanContingencyMilestone       = array_search('Loan Contingency', array_column($milestones,'MilestoneName'));
        $closeOfEscrow                  = $milestones[$closeOfEscrow];
        $inspectionContingencyMilestone = $milestones[$inspectionContingencyMilestone];
        $appraisalContingencyMilestone  = $milestones[$appraisalContingencyMilestone];
        $loanContingencyMilestone       = $milestones[$loanContingencyMilestone];
    }
    else
    {
        $closeOfEscrow                  = NULL;
        $inspectionContingencyMilestone = NULL;
        $appraisalContingencyMilestone  = NULL;
        $loanContingencyMilestone       = NULL;
    }

    $transactionDetails = json_encode($data['transaction']->toArray());
    $dateContract= date('F j, Y', strtotime($data['transaction']['DateOfferPrepared']));
    @$dateContract2=$data['transaction']['DateOfferPrepared'];
    $dateAccept=date('F j, Y', strtotime($data['transaction']['DateAcceptance']));
?>
<?php
function updateList($list, $side)
{
    if ($side != 'bs')
    {
        foreach($list as $dsp=>$code)
        {
            if (substr($code, 0, 1) != substr($side, 0, 1)) unset($list[$dsp]);
        }
    }
    if (substr(session('userRole'), -1) == 'a')
    {
        foreach($list as $dsp=>$code) if (substr($code, -1) == 'a') unset($list[$dsp]);
    }
    return $list;
}

?>

<!-- #################################################################################### -->
<!--    TDN = Transaction Data New
        TD = Transaction Data
        TDS = Transaction Data Summary
-->
<div id="TDS-model" class="TD-modal modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body transaction-details-form">
                <button type="button" class="close" data-dismiss="modal" style="">
                    {!! config('otc.modalClose') !!}
                </button>
                <div class="intro">
                    <div class="title col-sm-12">
                        <h1>Transaction Details Overview</h1>
                    </div>

                    <div class="line-pa col-sm-12">
                        <div class="blue-black col-sm-3">
                            <h3 style="text-transform: uppercase;">Purchase Price</h3>
                            <h4 class="PurchasePrice">{{\App\Library\Utilities\_Convert::formatDollars($bigdata['transaction']['PurchasePrice']) }}</h4>
                        </div>
                        <div class="blue-black col-sm-3">
                            <h3 style="text-transform: uppercase;">Will Seller Rent after close</h3>
                            <h4 class="willRentBack">{{($bigdata['transaction']['willRentBack']==0)?'No':(($data['transaction']['willRentBack']==1)?'Yes, for less than 30 days':'Yes, for more than 30 days')}}</h4>
                        </div>
                        <div class="blue-black col-sm-6">
                            <h3 style="text-transform: uppercase;">Buyer selling another home?</h3>
                            <h4 class="hasBuyerSellContingency">{{($bigdata['transaction']['hasBuyerSellContingency'])==1?'Yes':'No'}}</h4>
                        </div>
                    </div>

                    <div class="line-pa col-sm-12">
                        <div class="blue-black col-sm-3">
                            <h3 style="text-transform: uppercase;">Property Type</h3>
                            <h4>{{\App\Models\lu_PropertyTypes::getDisplay($bigdata['property'][0]['PropertyType'] ?? null)}}</h4>
                        </div>
                        <div class="blue-black col-sm-3">
                            <h3 style="text-transform: uppercase;">Client's  Role</h3>
                            <h4 class="ClientRole">{{str_replace('_', ' ', title_case(\App\Models\lu_UserRoles::getDisplay($bigdata['transaction']['ClientRole'])))}}</h4>
                        </div>
                        <div class="blue-black col-sm-6">
                            <h3 style="text-transform: uppercase;">Seller Buying another home?</h3>
                            <h4 class="hasSellerBuyContingency">{{($bigdata['transaction']['hasSellerBuyContingency'])==1?'Yes':'No'}}</h4>
                        </div>
                    </div>

                    <div class="line-pa col-sm-12">
                        <div class="blue-black col-sm-3">
                            <h3 style="text-transform: uppercase;">Contract Date</h3>
                            <h4 class="DateOfferPrepared">{{ \App\Library\Utilities\_Time::formatDate($bigdata['transaction']['DateOfferPrepared'], 4)}}</h4>
                        </div>
                        <div class="blue-black col-sm-3">
                            <h3 style="text-transform: uppercase;">Sale Type</h3>
                            <h4 class="SaleType">{{str_replace('_', ' ', title_case(\App\Models\lu_SaleTypes::getDisplay($bigdata['transaction']['SaleType'])))}}</h4>
                        </div>
                        <div class="blue-black col-sm-6">
                            <h3 style="text-transform: uppercase;">Is there an inspection contingency?</h3>
                            <h4 class="hasInspectionContingency">{{($bigdata['transaction']['hasInspectionContingency'])>0?'Yes':'No'}}</h4>
                        </div>
                        <div class="blue-black col-sm-6">
                            <h3 style="text-transform: uppercase;">Is there an appraisal contingency?</h3>
                            <h4 class="hasAppraisalContingency">{{($bigdata['transaction']['hasAppraisalContingency'])>0?'Yes':'No'}}</h4>
                        </div>
                        <div class="blue-black col-sm-6">
                            <h3 style="text-transform: uppercase;">Is there a loan contingency?</h3>
                            <h4 class="hasLoanContingency">{{($bigdata['transaction']['hasLoanContingency'])>0?'Yes':'No'}}</h4>
                        </div>
                    </div>

                    <div class="line-pa col-sm-12">
                        <div class="blue-black col-sm-3">
                            <h3 style="text-transform: uppercase;">Acceptance Date</h3>
                            <h4 class="DateAcceptance">{{\App\Library\Utilities\_Time::formatDate($bigdata['transaction']['DateAcceptance'],4)}}</h4>
                        </div>
                        <div class="blue-black col-sm-3">
                            <h3 style="text-transform: uppercase;">Loan Type</h3>
                            <h4 class="LoanType">{{str_replace('_', ' ', title_case(\App\Models\lu_LoanTypes::getDisplay($bigdata['transaction']['LoanType'])))}}</h4>
                        </div>
                        <div class="blue-black col-sm-6">
                            <h3 style="text-transform: uppercase;">Will Seller Rent After Close?</h3>
                            <h4 class="willTenantsRemain">{{($bigdata['transaction']['willTenantsRemain'])==1?'Yes':'No'}}</h4>
                        </div>
                    </div>

                    <div class="line-pa col-sm-12">
                        <div class="blue-black col-sm-10">
                            <h3 style="text-transform: uppercase;">Length of Escrow</h3>
                            <h4 class="EscrowLength">{{$bigdata['transaction']['EscrowLength']}} days</h4>
                        </div>
                    </div>
                    <div class="line-pa col-sm-12">
                        <div class="blue-black col-sm-10">
                            <h3 style="text-transform: uppercase;">Notes</h3>
                            <h6 class="Notes">{{ $bigdata['transaction']['Notes'] }}</h6>
                        </div>
                    </div>

                    <div class="line-pa col-sm-12 text-right">
                        <a href="#" class="edit-delatls-btn" style="text-transform: uppercase;">Edit Details</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ####################### This is the Modal for Editing ############################### -->
<div id="TD-model" class="TD-modal TDN-modal modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body transaction-details-form">
                <div class="spinner-kero"><i class="fas fa-spinner fa-spin"></i></div>
                <button type="button" class="close close-td-model" style="">
                    {!! config('otc.modalClose') !!}
                </button>
                <!-- #################################################################### -->
                <section id="TD-modal-step-1" class="model-step step-1" >
                    <header class="header" class="container">
                        <div class="col-sm-6">
                            <h1>TRANSACTION DETAILS</h1>
                        </div>
                        <div class="col-sm-6 text-right">
                            <h1>STEP <span class="text-danger">1</span> OF <span class="text-danger">3</span></h1>
                        </div>
                    </header>
                    <section class="input-lable">
                        <div class="col-md-3">
                            <h6>Purchase Price</h6>
                        </div>
                        <div class="col-md-3">
                            <h6>Contact Date</h6>
                        </div>
                        <div class="col-md-3">
                            <h6>ACCEPTANCE Date</h6>
                        </div>
                        <div class="col-md-3">
                            <h6>LENGTH OF Escrow</h6>
                        </div>
                    </section>
                    <section class="input-data">
                        <div class="col-md-3 input-and-image">
                            <div class="red-bottom">
                                <div class="img" style="background-image: url({{asset('images/short-icon/$.png')}})"></div>
                                <div class="the-input">
                                    <input class="purchase_price_input" type="text" name="PurchasePrice" id="PurchasePrice" value="{{number_format($data['transaction']['PurchasePrice'],0)}}">
                                </div>
                            </div>
                            <span class=" col-md-12 text-center errors text-left"></span>
                        </div>
                        <div class="col-md-3 input-and-image">
                            <div class="red-bottom">
                                 <div class="the-input">
                                    <input type="date" class="datepicker-date" name="DateOfferPrepared" id="DateOfferPrepared" value="{{date('m/d/Y', strtotime($dateContract))}}">
                                </div>
                                <i class="fa fa-calendar-alt datepicker-image"></i>
                            </div>
                            <span class=" col-md-12 text-center errors text-left"></span>
                        </div>
                        <div class="col-md-3 input-and-image text-left">
                            <div class="red-bottom text">
                                <div class="the-input">
                                    <input type="date" class="datepicker-date" name="DateAcceptance" id="DateAcceptance" value="{{date('m/d/Y', strtotime($dateAccept))}}">
                                </div>
                                <i class="fa fa-calendar-alt datepicker-image"></i>
                            </div>
                            <span class=" col-md-12 text-center errors text-left"></span>
                        </div>
                        <div class="col-md-3 input-and-image">
                            <div class="red-bottom">
                                <div class="the-input">
                                    <input type="number" placeholder="Days" name="EscrowLength" id="EscrowLength" value="{{$data['transaction']['EscrowLength']}}">
                                </div>
                            </div>
                            <span class=" col-md-12 text-center errors text-left"></span>
                        </div>
                    </section>
                    <div class="col-sm-12 seller-rent">
                        <h6>Will seller rent after close</h6>
                    </div>
                    @php
                        $name='willRentBack';
                        $default=$bigdata['transaction']['willRentBack'];
                    @endphp
                    <section class="check-box-section col-sm-12">
                        <label class="o-radio-container pull-right check-false">NO
                            <input name="{{$name}}" type="radio" {{$default == 0 ? 'checked':''}} value="0" >
                            <i class="fas fa-times"></i>
                        </label>
                        <label class="o-radio-container pull-right check-true">YES, FOR LESS THAN 30 DAYS
                            <input name="{{$name}}"  type="radio" {{$default == 1 ? 'checked':''}} value="1">
                            <span><i class="fas fa-check"></i></span>
                        </label>
                        <label class="o-radio-container pull-right check-true">YES, FOR 30 DAYS OR MORE
                            <input name="{{$name}}"  type="radio" {{$default == 31 ? 'checked':''}} value="31">
                            <span><i class="fas fa-check"></i></span>
                        </label>
                    </section>
                    <div class="col-sm-12">
                        <button type="button" class="the-next-model">
                            <div class="text-left">
                                NEXT <div class="right-arrow" style="background-image: url({{asset('images/short-icon/right-arrow.png')}})"></div>
                            </div>
                        </button>
                        <button type="button" class="save-old-user save-exit-button-td">
                            <div class="text-left">
                                SAVE & EXIT<div class="right-arrow"></div>
                            </div>
                        </button>
                    </div>
                </section>
                <!-- ###################################################################################### -->
                <section id="TD-modal-step-2" class="model-step step-2" hidden>
                    <header class="header" class="container">
                        <div class="col-sm-6">
                            <h1 style="text-transform: uppercase;">TRANSACTION DETAILS</h1>
                        </div>
                        <div class="col-sm-6 text-right">
                            <h1>STEP <span class="text-danger">2</span> OF <span class="text-danger">3</span></h1>
                        </div>
                    </header>
<?php
                    $textFields = [
                        ['col' => 'ClientRole', 'label' => 'Client\'s Role', 'default' => null, 'required' => true, 'luTable' => 'lu_UserRoles',],
                    ];

                    foreach($textFields as $field)
                    {
                        $list = \App\Library\Utilities\FormElementHelpers::getKeyValuePairListFromTable($field['luTable'],
                            ['sortByOrder'      => 'a', 'removeSortOrder0' => true, ]);
                        $list = updateList($list, $side);

                        $transactionID=$bigdata['transaction']['ID'];
                        $data = App\Combine\TransactionCombine::getDetails($transactionID);
                        $data = App\Combine\TransactionCombine::collectionToArray($data);
                        $data=$data[0];
                        $col = $field['col'];
                        $label = $field['label'];
                        $ph = $field['placeholder'] ?? $label;
                        $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
                        if (count($list) == 1) $default = reset($list);

                    $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
                        $table = $field['luTable'];
?>

                    <div class=" col-md-4 form-group with-label">
                        <label for="{{$col}}xx" class="label-controlxx" style="text-transform: uppercase;">{{$label}} {!! $star !!}</label>
                        <select name="{{$col}}xx" id="ClientRolexx" class="o-select">
                            {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($default, $list, 'value', 'index', false, '** Select', null) !!}
                        </select>
                        <span class=" col-md-12 text-center errors text-left"></span>
                    </div>
                    <?php   } ?>
                    <?php
                    $textFields = [
                        ['col' => 'SaleType', 'label' => 'Sale Type', 'default' => null, 'required' => true, 'luTable' => 'lu_SaleTypes',],
                        ['col' => 'LoanType', 'label' => 'Loan Type', 'default' => null, 'required' => true, 'luTable' => 'lu_LoanTypes',],
                    ];

                    foreach($textFields as $field)
                    {
                    $col = $field['col'];
                    //    if (!array_key_exists($col, $data)) continue;

                    $label = $field['label'];
                    $ph = $field['placeholder'] ?? $label;
                    $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
                    $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
                    $table = $field['luTable'];
                    ?>

                    <div class=" col-sm-4 form-group with-label">
                        <label for="{{$col}}" class="label-control" style="text-transform: uppercase;">{{$label}} {!! $star !!}</label>
                        <select name="{{$col}}" id="{{$field['col']}}" class="o-select">
                            {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromLookupTable($default, $table, 'Display', 'Value', false, '** Select', null) !!}
                        </select>
                        <span class=" col-md-12 text-center errors text-left"></span>
                    </div>
                    <?php   } ?>
                    <div class="col-sm-12 row">
                        <div class="col-md-2">
                            <a class="add-notes text-danger" style="color: #cd6563">
                                {{ $bigdata['transaction']['Notes'] ? 'NOTES' : '+ ADD A NOTE' }}
                            </a>
                        </div>
                        <div class="col-md-8">
                            <textarea  style="border-radius: 5px; margin-top: 25px"
                                       class="col-sm-8 modal-notes"
                                       cols="85"
                                       rows="2">
                            </textarea>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <a class="the-previous-model">PREVIOUS</a>
                        <button type="button" class="the-next-model">
                            <div class="text-left">
                                Next<div class="right-arrow"></div>
                            </div>
                        </button>
                        <button type="button" class="save-old-user save-exit-button-td">
                            <div class="text-left">
                                SAVE & EXIT <div class="right-arrow" style="background-image: url({{asset('images/short-icon/right-arrow.png')}})"></div>
                            </div>
                        </button>
                    </div>
                </section>
                <!-- ###################################################################################### -->
                <section id="TD-modal-step-3" class="model-step step-3 final-step" hidden>
                    <header class="header" class="container">
                        <div class="col-sm-6">
                            <h1 style="text-transform: uppercase;">TRANSACTION DETAILS</h1>
                        </div>
                        <div class="col-sm-6 text-right">
                            <h1>STEP <span class="text-danger">3</span> OF <span class="text-danger">3</span></h1>
                        </div>
                    </header>
                    <div class="sale-conti col-sm-12">
                        <h4>Is Sale Contingent on Any of the Following?</h4>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <?php
                            $textFields = [
                                ['col' => 'hasBuyerSellContingency', 'label' => 'Is the sale contingent on the buyer selling another home', 'default' => 0, 'required' => true, 'yes' => 1],
                                ['col' => 'hasSellerBuyContingency', 'label' => 'Is the sale contingent on the seller buying another home', 'default' => 0, 'required' => true, 'yes' => 1],
                                ['col' => 'hasInspectionContingency', 'label' => 'Is there an inspection contingency', 'default' => 0, 'required' => true, 'yes' => $bigdata['transaction']['hasInspectionContingency']],
                                ['col' => 'hasAppraisalContingency',  'label' => 'Is there an appraisal contingency?', 'default' => 0, 'required' => true, 'yes' => $bigdata['transaction']['hasAppraisalContingency']],
                                ['col' => 'hasLoanContingency',       'label' => 'Is there a loan contingency?', 'default' => 0, 'required' => true, 'yes' => $bigdata['transaction']['hasLoanContingency']],

                                ['col' => 'needsTermiteInspection', 'label' => 'Is a termite inspection needed', 'default' => 0, 'required' => true, 'yes' => 1],
                                ['col' => 'willTenantsRemain', 'label' => 'Will there be tenants remaining in the property after close of escrow', 'default' => 0, 'required' => true, 'yes' => 1],
                            ];

                            $specialContingencies = [
                                'hasInspectionContingency' => true,
                                'hasAppraisalContingency' => true,
                                'hasLoanContingency' => true
                            ];

                            $transactionID=$bigdata['transaction']['ID'];
                            $data = App\Combine\TransactionCombine::getDetails($transactionID);
                            $data = App\Combine\TransactionCombine::collectionToArray($data);
                            $data=$data[0];
                            foreach($textFields as $field)
                            {
                            $col = $field['col'];
                            // ddd($data['transaction'][$field['col']],$field['col']);
                            if (!array_key_exists($col, $data))
                            {
                                \Illuminate\Support\Facades\Log::debug(['Array Key '. $col . ' does not exist!', 'textFields' => $textFields, 'col' => $col, 'data' => $data, $view_name=>__LINE__]);
                                continue;
                            }
                            $label = $field['label'];
                            $ph = $field['placeholder'] ?? $label;
                            $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
                            $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
                            ?>

                            <div class="form-group with-label">
                                <label class="label-control long_label">
                                    <div class="blue-ball" style="background-image: url({{asset('images/short-icon/blue-ball.png')}})"></div>
                                    {{$label}} {!! $star !!}

                                </label>
                                @if(isset($specialContingencies[$col]))
                                    <label id="{{$col}}_edit" class="o-radio-container height-30 pull-right check-true"> YES
                                        <input name="{{$col}}" class="form-control" type="radio" value="{{$field['yes']}}" {{ $default ? 'CHECKED' : '' }}>
                                        <i class="fas fa-check"></i>
                                    </label>
                                    <label class="o-radio-container height-30 pull-right check-false"> NO
                                        <input name="{{$col}}" class="form-control" type="radio" value="0" {{ !$default ? 'CHECKED' : '' }}>
                                        <span><i class="fas fa-times"></i></span>
                                    </label>
                                    @if($field['yes'] > 0)
                                        <label class="text-input">How many days after acceptance does this contingency need to be removed?
                                            <input class="form-control {{$col}}-text" type="text" value="{{$field['yes']}}" style="width: 60px;">
                                        </label>
                                    @endif
                                @else
                                    <label class="o-radio-container height-30 pull-right check-true"> YES
                                        <input name="{{$col}}" class="form-control" type="radio" value="{{$field['yes']}}" {{ $default ? 'CHECKED' : '' }}>
                                        <i class="fas fa-check"></i>
                                    </label>
                                    <label class="o-radio-container height-30 pull-right check-false"> NO
                                        <input name="{{$col}}" class="form-control" type="radio" value="0" {{ !$default ? 'CHECKED' : '' }}>
                                        <span><i class="fas fa-times"></i></span>
                                    </label>
                                @endif
                            </div>
                            <hr>
                            <?php   } ?>
                            <a class="the-previous-model important">PREVIOUS</a>
                            <button type="button" class="save-exit-button save-exit-button-td save-old-user">
                                <div class="text-left">
                                    SAVE & EXIT
                                    <div class="right-arrow" style="background-image: url({{asset('images/short-icon/right-arrow.png')}})"></div>
                                </div>
                            </button>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<!-- ####################### This is the Modal for First Time Create ########################### -->
<div id="TDN-model" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body transaction-details-form">
                <button type="button" class="close close-tdn-model" style="">
                    <span  class="text-danger" style="font-size: 50px">&times;</span>
                </button>
                <!-- #################################################################### -->
                <section id="TDN-modal-step-1" class="model-step step-1">
                    <header class="header" class="container">
                        <div class="col-sm-6">
                            <h1>TRANSACTION DETAILS</h1>
                        </div>
                        <div class="col-sm-6 text-right">
                            <h1>STEP <span class="text-danger">1</span> OF <span class="text-danger">3</span></h1>
                        </div>
                    </header>
                    <section class="input-lable">
                        <div class="col-md-3">
                            <h6>purchase price</h6>
                        </div>
                        <div class="col-md-3">
                            <h6>CONTRACT DATE</h6>
                        </div>
                        <div class="col-md-3">
                            <h6>ACCEPTANCE DATE</h6>
                        </div>
                        <div class="col-md-3">
                            <h6>LENGTH OF ESCROW</h6>
                        </div>
                    </section>
                    <section class="input-data">
                        <div class="col-md-3 input-and-image">
                            <div class="red-bottom">
                                <div class="img" style="background-image: url({{asset('images/short-icon/$.png')}})"></div>
                                <div class="the-input">
                                    <input class="purchase_price_input" type="text" name="PurchasePrice" id="PurchasePrice" value="{{@number_format($data['transaction']['PurchasePrice'],0) ? @number_format($data['transaction']['PurchasePrice'],0) : ''}}">
                                </div>
                            </div>
                            <span class=" col-md-12 text-center errors text-left"></span>
                        </div>
                        <div class="col-md-3 input-and-image">
                            <div class="red-bottom text">
                                <div class="the-input">
                                    <input required="required" type="date" class="datepicker-date" name="DateOfferPrepared" id="DateOfferPrepared1" value="{{date('m/d/Y', strtotime($dateContract))}}">
                                </div>
                                <i class="fa fa-calendar-alt datepicker-image"></i>
                            </div>
                            <span class=" col-md-12 text-center errors text-left"></span>
                        </div>
                        <div class="col-md-3 input-and-image text-left">
                            <div class="red-bottom text">
                                <div class="the-input">
                                    <input required="required" type="date" class="datepicker-date" name="DateAcceptance" id="DateAcceptance1" value="{{date('m/d/Y', strtotime($dateAccept))}}">
                                </div>
                                <i class="fa fa-calendar-alt datepicker-image"></i>
                            </div>
                            <span class=" col-md-12 text-center errors text-left"></span>
                        </div>
                        <div class="col-md-3 input-and-image">
                            <div class="red-bottom">
                                <div class="the-input">
                                    <input type="number" placeholder="0 Days" name="EscrowLength" id="EscrowLength" value="">
                                </div>
                            </div>
                            <span class=" col-md-12 text-center errors text-left"></span>
                        </div>
                    </section>
                    <div class="col-sm-12 seller-rent">
                        <h6>Will seller rent after close</h6>
                    </div>
                    @php
                        $name='willRentBack';
                    @endphp
                    <section class="check-box-section col-sm-12">
                        <label class="o-radio-container pull-right check-false">NO
                            <input name="{{$name}}1" type="radio" checked value="0" >
                            <i class="fas fa-times"></i>
                        </label>
                        <label class="o-radio-container pull-right check-true">YES FOR LESS THAN 30 DAYS
                            <input name="{{$name}}1"  type="radio" value="1">
                            <span><i class="fas fa-check"></i></span>
                        </label>
                        <label class="o-radio-container pull-right check-true">YES FOR 30 DAYS OR MORE
                            <input name="{{$name}}1"  type="radio" value="31">
                            <span><i class="fas fa-check"></i></span>
                        </label>
                    </section>
                    <div class="col-sm-12">
                        <button type="button" class="the-next-model">
                            <div class="text-left">
                                NEXT <div class="right-arrow" style="background-image: url({{asset('images/short-icon/right-arrow.png')}})"></div>
                            </div>
                        </button>
                    </div>
                </section>
                <!-- ###################################################################################### -->
                <section id="TDN-modal-step-2" class="model-step step-2" hidden>
                        <header class="header" class="container">
                            <div class="col-sm-6">
                                <h1>TRANSACTION DETAILS</h1>
                            </div>
                            <div class="col-sm-6 text-right">
                                <h1>STEP <span class="text-danger">2</span> OF <span class="text-danger">3</span></h1>
                            </div>
                        </header>
<?php
                        $textFields = [
                            ['col' => 'ClientRole', 'label' => 'Client\'s Role', 'default' => null, 'required' => true, 'luTable' => 'lu_UserRoles',],
                        ];

                        foreach($textFields as $field)
                        {
                            $list = \App\Library\Utilities\FormElementHelpers::getKeyValuePairListFromTable($field['luTable'],
                                ['sortByOrder'      => 'a', 'removeSortOrder0' => true, ]);
                            $list = updateList($list, $side);
                            $col = $field['col'];
                            $label = $field['label'];
                            $ph = $field['placeholder'] ?? $label;
                            $default = null;//old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
                            if (count($list) == 1) $default = reset($list);
                            $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
                            $table = $field['luTable'];
?>

                        <div class=" col-md-4 form-group with-label">
                            <label for="{{$col}}" class="label-control">{{$label}} {!! $star !!}</label>
                            <select name="{{$col}}" id="ClientRoleyy" class="o-select">
                                {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($default, $list, 'value', 'index', false, '** Select', null) !!}
                            </select>
                            <span class=" col-md-12 text-center errors text-left"></span>
                        </div>
                <?php   } ?>
                <?php
                        $textFields = [
                            ['col' => 'SaleType', 'label' => 'Sale Type', 'default' => null, 'required' => true, 'luTable' => 'lu_SaleTypes',],
                            ['col' => 'LoanType', 'label' => 'Loan Type', 'default' => null, 'required' => true, 'luTable' => 'lu_LoanTypes',],
                        ];

                        foreach($textFields as $field)
                        {
                        $col = $field['col'];
                        //    if (!array_key_exists($col, $data)) continue;

                        $label = $field['label'];
                        $ph = $field['placeholder'] ?? $label;
                        $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
                        $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
                        $table = $field['luTable'];
?>

                        <div class=" col-sm-4 form-group with-label">
                            <label for="{{$col}}" class="label-control">{{$label}} {!! $star !!}</label>
                            <select name="{{$col}}" id="{{$field['col']}}" class="o-select">
                                {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromLookupTable($default, $table, 'Display', 'Value', false, '** Select', null) !!}
                            </select>
                            <span class=" col-md-12 text-center errors text-left"></span>
                        </div>
                        <?php   } ?>
                        <div class="col-sm-12 row">
                            <div class="col-md-2">
                                <a class="add-notes text-danger" style="color: #cd6563">
                                    {{ $bigdata['transaction']['Notes'] ? 'NOTES' : '+ ADD A NOTE' }}
                                </a>
                            </div>
                            <div class="col-md-8">
                            <textarea  style="border-radius: 5px; margin-top: 25px"
                                       class="col-sm-8 modal-notes"
                                       cols="85"
                                       rows="2">
                                {{$bigdata['transaction']['Notes']}}
                            </textarea>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <a class="the-previous-model">PREVIOUS</a>
                            <button type="button" class="the-next-model">
                                <div class="text-left">
                                    NEXT<div class="right-arrow" style="background-image: url({{asset('images/short-icon/right-arrow.png')}})"></div>
                                </div>
                            </button>
                        </div>
                </section>
                <!-- ###################################################################################### -->
                <section id="TDN-modal-step-3" class="model-step step-3 final-step" hidden>
                    <header class="header" class="container">
                        <div class="col-sm-6">
                            <h1>TRANSACTION DETAILS</h1>
                        </div>
                        <div class="col-sm-6 text-right">
                            <h1>STEP <span class="text-danger">3</span> OF <span class="text-danger">3</span></h1>
                        </div>
                    </header>
                    <div class="sale-conti col-sm-12">
                        <h4>IS Sale Contingent on Any of the following ?</h4>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <?php
                            $textFields = [
                                ['col' => 'hasBuyerSellContingency', 'label' => 'Is the sale contingent on the buyer selling another home', 'default' => 0, 'required' => true, 'yes' => 1],
                                ['col' => 'hasSellerBuyContingency', 'label' => 'Is the sale contingent on the seller buying another home', 'default' => 0, 'required' => true, 'yes' => 1],
                                ['col' => 'hasInspectionContingency', 'label' => 'Is there an inspection contingency', 'default' => 0, 'required' => true, 'yes' => \App\Models\Timeline::getByCode('ic')['DaysOffset']],
                                ['col' => 'hasAppraisalContingency',  'label' => 'Is there an appraisal contingency?', 'default' => 0, 'required' => true, 'yes' => \App\Models\Timeline::getByCode('app')['DaysOffset']],
                                ['col' => 'hasLoanContingency',       'label' => 'Is there a loan contingency?', 'default' => 0, 'required' => true, 'yes' => \App\Models\Timeline::getByCode('lc')['DaysOffset']],

                                ['col' => 'needsTermiteInspection', 'label' => 'Is a termite inspection needed', 'default' => 0, 'required' => true, 'yes' => 1],
                                ['col' => 'willTenantsRemain', 'label' => 'Will there be tenants remaining in the property after close of escrow', 'default' => 0, 'required' => true, 'yes' => 1],
                            ];
                            //$transactionID=$bigdata['transaction']['ID'];
                            //$data = App\Combine\TransactionCombine::getDetails($transactionID);
                            //$data = App\Combine\TransactionCombine::collectionToArray($data);
                            //$data=$data[0];
                            //dd($data);
                            foreach($textFields as $field)
                            {
                            $col = $field['col'];
                            // ddd($data['transaction'][$field['col']],$field['col']);
                            //if (!array_key_exists($col, $data))
                           // {
                            //    \Illuminate\Support\Facades\Log::debug(['textFields' => $textFields, 'col' => $col, 'data' => $data]);
                           //     continue;
                           // }
                            $label = $field['label'];
                            $ph = $field['placeholder'] ?? $label;
                            $default =null; //old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
                            $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
                            ?>

                            <div class="form-group with-label">
                                <label class="label-control long_label">
                                    <div class="blue-ball" style="background-image: url({{asset('images/short-icon/blue-ball.png')}})"></div>
                                    {{$label}} {!! $star !!}

                                </label>

                                <label class="o-radio-container height-30 pull-right check-true" id="{{$col}}"> YES
                                    <input name="{{$col}}1" class="form-control" type="radio" value="{{$field['yes']}}" >
                                    <i class="fas fa-check"></i>
                                </label>
                                <label class="o-radio-container height-30  pull-right check-false"> NO
                                    <input name="{{$col}}1" class="form-control" type="radio" value="0" checked>
                                    <span><i class="fas fa-times"></i></span>
                                </label>
                            </div>
                            <hr>
                            <?php   } ?>
                            <a class="the-previous-model important">PREVIOUS</a>
                            <button type="button" class="the-next-model next-continue-btn">
                                <div class="text-left">
                                    SAVE & CONTINUE
                                    <div class="right-arrow" style="background-image: url({{asset('images/short-icon/right-arrow.png')}})"></div>
                                </div>
                            </button>
                            <button type="button" class="save-exit-button save-exit-button-td the-next-model refresh">
                                    <div class="text-left">
                                        SAVE & EXIT
                                        <div class="right-arrow" style="background-image: url({{asset('images/short-icon/right-arrow.png')}})"></div>
                                    </div>
                            </button>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
@section('scripts')
<script>
    var tid = '{{$bigdata['transaction']['ID']}}';
    let transactionDetails = JSON.parse('{!! $transactionDetails !!}');
    var token="{{Session::token()}}";

    let timelineChangeRoutes = {
        listAffectedTasksRoute  : '{{route('listAffectedTasks')}}',
        approveTaskChangeRoute  : '{{route('approveTaskChange')}}',
    };

    $(document).ready(function(){

        var contingencies = {
            hasInspectionContingency: '{{$bigdata['transaction']['hasInspectionContingency'] ?? 0}}',
            hasAppraisalContingency: '{{$bigdata['transaction']['hasAppraisalContingency'] ?? 0}}',
            hasLoanContingency: '{{$bigdata['transaction']['hasLoanContingency'] ?? 0}}',
        };

        $('.purchase_price_input').keyup(function () {
            $(this).val(currencyFormat($(this).val()));
        });

        $(document).on('keyup','.hasInspectionContingency-text, .hasAppraisalContingency-text, .hasLoanContingency-text',function () {
            let parent      = $(this).parents('.text-input');
            let actualValue = parent.siblings('.check-true');
            this.value      = this.value.replace(/[^0-9\.]/g,''); //prevent the user from typing in non numeric values
            actualValue.children('input').val($(this).val());
        });

        $('.check-true').click(function(evt){
           var self = $(this);
           $.each(contingencies, function(index,value){
               let val;
               let textInput;
               if(self.is('#'+index) || self.is('#'+index+'_edit'))
               {
                   val = self.children('input').val();
                   self.siblings('.check-false input').prop('checked', false);
                   self.children('input').prop('checked', true);
                   if(self.siblings('.text-input').length === 0)
                       self.parents('.form-group').append('<label class="text-input">' +
                           'How many days after acceptance does this contingency need to be removed?' +
                           '<input class="form-control '+index+'-text" type="text" value="'+val+'" style="width: 60px;"></label>');
                   self.children('input').val(val);
                   textInput = $('.'+index+'-text');
                   textInput.keyup(function(){
                       this.value = this.value.replace(/[^0-9\.]/g,''); //prevent the user from typing in non numeric values
                       self.children('input').val(textInput.val());
                   });
               }
           });
        });

        $('.check-false').click(function(){
            var self = $(this);
            self.siblings('.text-input').remove();
            self.children('input').prop('checked',true);
            self.siblings('.check-true input').prop('checked', false);
        });

        $('#TD-model .add-notes, #TDN-model .add-notes').click(function(){
            let el = $(this);
            let noteField = el.closest('.row').find('.modal-notes');
            if (el.text() == '+ ADD A NOTE') {
                el.text('NOTES');
                noteField.show();
            }
            else {
                el.text('+ ADD A NOTE');
                noteField.hide();
            }
        });
    });

    //we are replacing the type date into text, then assigning jquery datepicker to those inputs.
    //This will make it work the same across all major browsers.
    $('input[type="date"]').datepicker({
        onSelect: function(){
            $(this).val(this.value);
        }
    }).attr('type','text');

    /**
     * Added a class to the input 'datepicker-image' so that we can reference it when the calendar icon is clicked.
     * */
    $('.datepicker-image').click(function(){
       $(this).siblings('.the-input').children('.datepicker-date').datepicker("show");
    });

    /**
     * Initialize the dates for offer prepared, contract start, etc.
     */
    $('#DateOfferPrepared,#DateOfferPrepared1').val('{{date('m/d/Y', strtotime($dateContract))}}');
    $('#DateAcceptance,#DateAcceptance1').val('{{date('m/d/Y', strtotime($dateAccept))}}');

    $('#TDS-model .edit-delatls-btn').click(function(){
        $('.spinner-kero').hide();
        $("#TDS-model").modal('toggle');
        setTimeout(function(){
            $('#TD-model').modal({
                backdrop: 'static',
                keyboard: false
            });
        },500);
    });
    $('.close-td-model').click(function(){
        //$('#TD-model').modal('hide');
        if(there_is_a_change_in_TD){
            swal({
                    title: "Are you sure?",
                    text: "Changes will not be saved!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,

                    })
                    .then((willDelete) => {
                    if (willDelete)
                    {
                        $('#TD-model').modal('toggle');
                        there_is_a_change_in_TD=false;
                    }
                });
        }else{
            $('#TD-model').modal('toggle');
        }
    });
    $('.close-tdn-model').click(function(){
        //$('#TD-model').modal('hide');
        if(there_is_a_change_in_TDN){
            swal({
                    title: "Are you sure?",
                    text: "Changes will not be saved!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,

                    })
                    .then((willDelete) => {
                    if (willDelete)
                    {
                        $('#TDN-model').modal('toggle');
                        there_is_a_change_in_TDN=false;
                    }
                });
        }else{
            $('#TDN-model').modal('toggle');
        }
    });
    $('#TD-model').on('input',function(e){
        there_is_a_change_in_TD=true;
    });
    $('#TD-model select').on('change', function (e) {
        there_is_a_change_in_TD=true;
    });

    $('#TDN-model').on('input',function(e){
        there_is_a_change_in_TDN=true;
    });
    $('#TDN-model select').on('change', function (e) {
        there_is_a_change_in_TDN=true;
    });

        $('#TD-model .the-next-model').click(function(){
            if($(this).parents('.model-step').next('.model-step').length > 0)
            {
                $(this).parents('.model-step').attr('hidden',"");
                $(this).parents('.model-step').next('.model-step').removeAttr('hidden');
            }
            else
            {
                $('#TDN-model').attr('hidden',"");
            }
        });
        $('.the-previous-model').click(function(){
            $(this).parents('.model-step').attr('hidden',"");
            $(this).parents('.model-step').prev('.model-step').removeAttr('hidden');
        });

        $('.save-old-user').click(function()
        {
            /**
             * This is the date of acceptance received from the server.
             * @type {string}
             */
            let dateOfAcceptance    = '{{date('Y-m-d', strtotime($dateAccept))}}';
            /**
             * This is the date of acceptance element that holds a new value (if applicable)
             * @type {*|jQuery.fn.init|jQuery|HTMLElement}
             */
            let dateAcceptance      = $("#TD-model input[name='DateAcceptance']");

            let oldInspectionContingency    = '{{$bigdata['transaction']['hasInspectionContingency']}}';
            let oldAppraisalContingency     = '{{$bigdata['transaction']['hasAppraisalContingency']}}';
            let oldLoanContingency          = '{{$bigdata['transaction']['hasLoanContingency']}}';
            let inspectionContingency       = $("#TD-model input[name='hasInspectionContingency']:checked").val();
            let appraisalContingency        = $("#TD-model input[name='hasAppraisalContingency']:checked").val();
            let loanContingency             = $("#TD-model input[name='hasLoanContingency']:checked").val();
            let escrowLength                = $("#TD-model input[name='EscrowLength']");
            let queuedFunctions             = [];

            if (parseInt(oldLoanContingency) != parseInt(loanContingency))
            {
                let proposedLoanContingency = moment(addOffset(dateOfAcceptance,loanContingency)+' 00:00').format('YYYY-MM-DD');
                let loanMilestoneObject = {
                    ID: parseInt('{{$loanContingencyMilestone['ID']}}'),
                    Description: '{{$loanContingencyMilestone['MilestoneName']}}',
                    DateDue: proposedLoanContingency,
                };

                queuedFunctions.push(function () {
                    timelineDateChangeDialog(
                        loanMilestoneObject,
                        timelineChangeRoutes,
                        tid,
                        queuedFunctionsCallback,
                        token,
                        'By changing the length of Loan Contingency, you will be moving' +
                        ' Loan Contingency to this date. Please confirm that is what you wish to do.',
                        true,
                        parseInt('{{$loanContingencyMilestone['mustBeBusinessDay']}}')
                    );
                });
            }
            if (parseInt(oldAppraisalContingency) != parseInt(appraisalContingency))
            {
                let proposedAppraisalContingency = moment(addOffset(dateOfAcceptance,appraisalContingency)+' 00:00').format('YYYY-MM-DD');
                let appraisalMilestoneObject = {
                    ID: parseInt('{{$appraisalContingencyMilestone['ID']}}'),
                    Description: '{{$appraisalContingencyMilestone['MilestoneName']}}',
                    DateDue: proposedAppraisalContingency,
                };

                queuedFunctions.push(function () {
                    timelineDateChangeDialog(
                        appraisalMilestoneObject,
                        timelineChangeRoutes,
                        tid,
                        queuedFunctionsCallback,
                        token,
                        'By changing the length of Appraisal Contingency, you will be moving' +
                        ' Appraisal Contingency to this date. Please confirm that is what you wish to do.',
                        true,
                        parseInt('{{$appraisalContingencyMilestone['mustBeBusinessDay']}}')
                    );
                });
            }
            if (parseInt(oldInspectionContingency) != inspectionContingency)
            {
                let proposedInspectionContingency  = moment(addOffset(dateOfAcceptance,inspectionContingency)+' 00:00').format('YYYY-MM-DD');
                let inspectionMilestoneObject = {
                    ID: parseInt('{{$inspectionContingencyMilestone['ID']}}'),
                    Description: '{{$inspectionContingencyMilestone['MilestoneName']}}',
                    DateDue: proposedInspectionContingency,
                };
                queuedFunctions.push(function () {
                    timelineDateChangeDialog(
                        inspectionMilestoneObject,
                        timelineChangeRoutes,
                        tid,
                        queuedFunctionsCallback,
                        token,
                        'By changing the length of Inspection Contingency, you will be moving' +
                        ' Inspection Contingency to this date. Please confirm that is what you wish to do.',
                        true,
                        parseInt('{{$inspectionContingencyMilestone['mustBeBusinessDay']}}')
                    );
                });
            }
            if(parseInt(escrowLength.val()) != transactionDetails.EscrowLength)
            {
                let newLength           = parseInt(escrowLength.val());
                let dateOfAcceptance    = '{{date('Y-m-d', strtotime($dateAccept))}}';
                let proposedEscrowDate  = addOffset(dateOfAcceptance,newLength)+' 00:00';
                let isBusinessDay       = isBusinessDayFunction(proposedEscrowDate);
                if(!isBusinessDay) proposedEscrowDate = moveToBusinessDay(proposedEscrowDate);
                proposedEscrowDate = new Date(proposedEscrowDate).toISOString().substring(0, 10);
                let milestoneData = {
                    ID: '{{$closeOfEscrow['ID']}}',
                    Description: '{{$closeOfEscrow['MilestoneName']}}',
                    DateDue: proposedEscrowDate,
                    Notes: '',
                };
                queuedFunctions.push( function(){
                    timelineDateChangeDialog(
                        milestoneData,
                        timelineChangeRoutes,
                        tid,
                        queuedFunctionsCallback,
                        token,
                        'By changing the length of Escrow, you will change the Close of Escrow ' +
                        'date to the date below. Please confirm you wish to save this new date.',
                        true,
                        parseInt('{{$closeOfEscrow['mustBeBusinessDay']}}')
                    );
                });
            }


            let functionIterator = new FunctionIterator(queuedFunctions,verifyDateOfAcceptance);
            let queuedFunctionsCallback = function(){
                functionIterator.executeQueuedFunctions();
            };
            functionIterator.executeQueuedFunctions();

            function verifyDateOfAcceptance()
            {
                let oldDate = new Date(transactionDetails.DateAcceptance+' 00:00');
                let newDate = new Date(dateAcceptance.val()+' 00:00');
                newDate = JSON.stringify(newDate);
                oldDate = JSON.stringify(oldDate);
                if(newDate != oldDate)
                {
                    let modalHTML =
                        '<div class="row">' +
                        '    <h3 style="margin-bottom: 20px; font-weight: bold;">Confirm Changes</h3>' +
                        '    <div style="margin-bottom: 20px;" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Changing <i>Acceptance Date</i> will recalculate your timeline ' +
                        '        and task due dates. Are you sure you want to proceed?' +
                        '    </div>' +
                        '    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
                        '        <button id="proceedConfirm" class="btn-v2 btn-teal">Proceed</button>' +
                        '    </div>' +
                        '    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">' +
                        '        <button id="cancelConfirm" class="btn-v2 btn-red">Cancel</button>' +
                        '    </div>' +
                        '</div>';
                    Swal2({
                        html: modalHTML,
                        showConfirmButton: false,
                        onBeforeOpen: () => {
                            const content   = Swal2.getContent();
                            const $s        = content.querySelector.bind(content);
                            let cancel      = $s('#cancelConfirm');
                            let proceed     = $s('#proceedConfirm');
                            cancel.addEventListener('click', () => {
                                Swal2.close();
                            });
                            proceed.addEventListener('click', () => {
                                Swal2.showLoading();
                                saveData(true);
                            });
                        },
                    });
                }
                else
                {
                    Swal2({
                        title:  'Success',
                        type:   'success',
                        text:   'Saving changes..',
                        timer: 3000,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        onBeforeOpen: () => {
                            saveData();
                        },
                    });
                }
            }

            function saveData(recalculateDates = false)
            {
                var opj ={
                    "ID"                        :"{{$bigdata['transaction']['ID']}}",
                    "PurchasePrice"             :  $('#PurchasePrice').val(),
                    "DateAcceptance"            :  dateAcceptance.val(),
                    "DateOfferPrepared"         :  $("#TD-model input[name='DateOfferPrepared']").val(),
                    "EscrowLength"              :  escrowLength.val(),
                    "willRentBack"              :  $("#TD-model input[name='willRentBack']:checked").val(),
                    "ClientRole"                :  $("#TD-model select[name='ClientRole'] option:selected").val(),
                    "SaleType"                  :  $("#TD-model select[name='SaleType']  option:selected").val(),
                    "LoanType"                  :  $("#TD-model select[name='LoanType']  option:selected").val(),
                    "hasBuyerSellContingency"   :  $("#TD-model input[name='hasBuyerSellContingency']:checked").val(),
                    "hasSellerBuyContingency"   :  $("#TD-model input[name='hasSellerBuyContingency']:checked").val(),
                    "hasInspectionContingency"  :  inspectionContingency,
                    "hasAppraisalContingency"   :  appraisalContingency,
                    "hasLoanContingency"        :  loanContingency,
                    "needsTermiteInspection"    :  $("#TD-model input[name='needsTermiteInspection']:checked").val(),
                    "willTenantsRemain"         :  $("#TD-model input[name='willTenantsRemain']:checked").val(),
                    "Notes"                     :  $('#TD-model .modal-notes').val(),
                    "_screenMode"               : "edit",
                    "_token"                    : token,
                    "recalculateDates"          : recalculateDates,
                };
                $.ajax({
                    url : "{{route('saveDetails')}}",
                    method: "POST",
                    data: opj,
                    error:function(response){
                        there_is_a_change_in_TD=true;
                        for(x in response['responseJSON']['errors']){
                            if(x=='DateAcceptance'||x=='DateOfferPrepared'||x=='EscrowLength'||x=='PurchasePrice'){
                                el = $('.transaction-details-form').find('input[name = "'+ x +'"]');
                                el.parents('.red-bottom').siblings('.errors').text(response['responseJSON']['errors'][x][0]).show();
                                $('#TD-modal-step-1').removeAttr('hidden');
                                $('#TD-modal-step-2').attr('hidden',"");
                                $('#TD-modal-step-3').attr('hidden',"");

                            }
                            else if(x=='ClientRole'||x=='LoanType'||x=='SaleType'){
                                el = $('.transaction-details-form').find('select[name = "'+ x +'"]');
                                el.siblings('.errors').text(response['responseJSON']['errors'][x][0]).show();
                                $('#TD-modal-step-2').removeAttr('hidden');
                                $('#TD-modal-step-1').attr('hidden',"");
                                $('#TD-modal-step-3').attr('hidden',"");
                            }
                        }
                    },
                    success:function(data){
                        transaction_details_data=opj;
                        transaction_D_box();
                        location.reload();
                    },
                });//end of ajax
            }//end of saveData() function

        });

    $('.refresh').click(function(){
        setTimeout(function () {
            location.reload();
        },800);
    });

    $('#TDN-model .the-next-model').click(function(){
        var the_next_step_button=$(this);
        var you_can_go_next=true;
        var this_button=$(this);
        $('.errors').hide();
        if($(this).parents().hasClass('step-1')){
            if($("#TDN-model input[name='PurchasePrice']").val().length===0){
                $("#TDN-model input[name='PurchasePrice']").parents('.red-bottom').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
            if($("#TDN-model input[name='DateOfferPrepared']").val().length===0){
                $("#TDN-model input[name='DateOfferPrepared']").parents('.red-bottom').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
            if($("#TDN-model input[name='EscrowLength']").val().length===0){
                $("#TDN-model input[name='EscrowLength']").parents('.red-bottom').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
            else if($("#TDN-model input[name='EscrowLength']").val()>180){
                $("#TDN-model input[name='EscrowLength']").parents('.red-bottom').siblings('.errors').show().text("The escrow length may not be greater than 180.");
                you_can_go_next=false;
            }
            if($("#TDN-model input[name='DateAcceptance']").val().length===0){
                $("#TDN-model input[name='DateAcceptance']").parents('.red-bottom').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
        }
        if($(this).parents().hasClass('step-2')){
            if($('#TDN-model select[name="ClientRole"]').val().length===0){
                $('#TDN-model select[name="ClientRole"]').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
            if($('#TDN-model select[name="SaleType"]').val().length===0){
                $('#TDN-model select[name="SaleType"]').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
            if($('#TDN-model select[name="LoanType"]').val().length===0){
                $('#TDN-model select[name="LoanType"]').siblings('.errors').show().text('this field is required');
                you_can_go_next=false;
            }
        }
        if(you_can_go_next)
        {
            $(this).parents('.model-step').attr('hidden',"");
            $(this).parents('.model-step').next('.model-step').removeAttr('hidden');
        }
    });
</script>
@append