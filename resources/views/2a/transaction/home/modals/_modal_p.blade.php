<div id="property-modal" class="modal fade property-modal" role="dialog">
    <div class="modal-dialog modal-lg">
            <div class="modal-content">
                 <div class="modal-header">
                    <button type="button" class="close">
                        {!! config('otc.modalClose') !!}
                    </button>
                </div>
                <div class="modal-body">
                    <div class="success_msg"></div>
                    <div class="spinner"><i class="fas fa-spinner fa-spin"></i></div>
                    <form class="property_form_1" action="{{ route('saveProperty') }}" method="POST">
                            <input type="hidden" name="Transactions_ID"
                                value="{{ ($transactionID) ? $transactionID : '' }}">
                            <input type="hidden" name="ID" {{ !empty(@$data['property'][0]['ID']) ? @$data['property'][0]['ID'] : 'disabled'}} value="{{ (!empty(@$data['property'][0]['ID']) ? @$data['property'][0]['ID']: '') }}">
                        <div class="container-fluid">
                            <div class="row field-row" style="margin-top: -20px;">
                                    <div class="col-sm-12 col-md-9 col-lg-9">
                                        <label for="" class="address-label">PROPERTY DETAILS FOR </label>
                                        <input id="pModalSearchAddress" type="text" name="PropertyAddress" class="address-field" placeholder="Add Property Address" value="{{ @$address }}">
                                        <span class=" col-md-12 text-center errors"></span>
                                    </div>
                                    <!-- The class pModalSearchAddress is referred to in _addressAutoComplete.blade.php in partials.
                                    The google places autcomplete api needs to use the id there. -->
                                    <div class="col-sm-12 col-md-3 col-lg-3">
                                        <img src="{!! $data['propertyImageURL'] !!}" style="max-width: 200px; display: inline-block" id="uploadPropertyImage">Property Image
                                    </div>
                            </div>
                            <div class="row field-row">
                                <?php
                                    $default = !empty(@$data['property'][0]['PropertyType']) ? @$data['property'][0]['PropertyType']: '' ;
                                ?>
                                <div class="col-md-4">
                                    <label for="" class="modal-label">PROPERTY TYPE<span class="important">*</span></label>
                                    <select name="PropertyType" class="o-select" id="">
                                        {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromLookupTable($default, 'lu_PropertyTypes', 'Display', 'Value', false, '** Select Property Type', null, ['sortField'=>'Display']) !!}
                                    </select>
                                    <span class="errors"></span>
                                </div>
                                <div class="col-md-2 col-md-offset-1">
                                   <?php
                                        $default = !empty( @$data['property'][0]['ParcelNumber'] ) ? @$data['property'][0]['ParcelNumber']: '' ;
                                   ?>
                                    <label for="" class="modal-label" value="">APN</label>
                                    <input type="text" name="ParcelNumber" class="text-field" value="{{ $default }}">
                                    <span class="errors"></span>
                                </div>
                                <div class="col-md-2 col-md-offset-1">
                                    <?php
                                        $default = !empty($data['property'][0]['YearBuilt']) ? @$data['property'][0]['YearBuilt']: '' ;
                                   ?>
                                    <label for="" class="modal-label">YEAR BUILT <span class="important">*</span></label>
                                    <input type="text" name="YearBuilt" class="text-field" value="{{ $default }}">
                                    <span class="errors"></span>
                                </div>
                            </div>

                            <div class="row field-row">
                                <div class="col-md-4 col-xs-12">
                                    <?php
                                        $fname = 'hasHOA';
                                        $label = 'Has HOA';
                                        $default = !empty( @$data['property'][0]['ParcelNumber']) ? @$data['property'][0][$fname] : '';
                                    ?>
                                    <label style="margin-top: 10px; text-transform: uppercase;" for="" class="modal-label pull-left">{{ $label }}<span class="important">*</span></label>
                                    <label class="o-radio-container pull-right check-false">NO
                                        <input type="radio" {{ ($default == 0) ? 'CHECKED' : ''}} name="{{ $fname }}" value="0" >
                                        <i class="fas fa-check"></i>
                                    </label>
                                    <label class="o-radio-container pull-right check-true">YES
                                        <input name="{{ $fname }}" value="1" {{ ($default == 1) ? 'CHECKED' : ''}} type="radio">
                                        <span><i class="fas fa-times"></i></span>
                                    </label>
                                </div>
                                <?php
                                    $fname = 'hasSeptic';
                                    $label = 'Has Septic Tank';
                                    $default = !empty( @$data['property'][0]['ParcelNumber']) ? @$data['property'][0][$fname] : '';
                                ?>
                                <div class="col-md-5 col-xs-offset-1">
                                    <label for="" style="margin-top: 10px; text-transform: uppercase;" class="modal-label pull-left">{{ $label }}<span class="important">*</span></label>
                                    <label class="o-radio-container pull-right check-false">NO
                                        <input type="radio" name="{{ $fname }}" {{ ($default == 0) ? 'CHECKED' : ''}} value="0" >
                                        <i class="fas fa-check"></i>
                                    </label>
                                    <label class="o-radio-container pull-right check-true">YES
                                        <input name="{{ $fname }}" {{ ($default == 1) ? 'CHECKED' : ''}} value="1" type="radio">
                                        <span><i class="fas fa-times"></i></span>
                                    </label>
                                </div>
                            </div>
                            <div class="field-row row">
                                @php
                                    $notes      = @$data['property'][0]['Notes'];
                                    $notesClass = ( $notes ) ? 'show-notes' : '';
                                @endphp
                                <div class="col-md-2">
                                    <a href="#" class="add-notes text-danger" id="add_notes" style="color: #cd6563">{{ $notes ? 'NOTES' : '+ ADD A NOTE' }}
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <textarea name="Notes" id="" class="modal-notes {{ $notesClass }}" cols="85" rows="1">
                                        {{ !empty($data['property'][0]['Notes']) ? @$data['property'][0]['Notes']: ''  }}
                                    </textarea>
                                </div>
                            </div>
                             <div class="o-modal-footer text-left">
                                <a href="#" type="submit" id="submit_button" class="btn btn-teal">SAVE <span><i class="fas fa-angle-right"></i></span></a>
                                <a href="#" id="submit_exit" class="btn btn-red">SAVE & EXIT <span><i class="fas fa-angle-right"></i></span></a>
                             </div>
                        </div>
                    </form>
                </div>

            </div>
    </div>
</div>


@section('scripts')
    <script>
        var savedAddress = '{{$address}}';
        var SAVED_PROPERTY_DATA = false;
        var SKIP_ACTIVE_PROPERTY_CHECK = false;

        $(document).ready(function () {
            let command = window.location.hash;
            if(command === '#opm')
            {
                $('#property-modal').modal();
                window.location.hash = '';
            }
        });

        $(document).on('click', '#uploadPropertyImage', function(){
            async function uploadImage()
            {
                const {value: file} = await Swal2({
                    title: 'Select Image',
                    input: 'file',
                    inputAttributes: {
                        'accept': 'image/*',
                        'aria-label': 'Upload your profile picture'
                    }
                });
                if (file)
                {
                    const reader = new FileReader;
                    reader.onload = (e) => {
                        Swal2({
                            title: 'Selected Picture.',
                            imageUrl: e.target.result,
                            imageAlt: 'The selected picture.'
                        }).then(()=>{
                            Swal2({
                                text: 'Uploading...',
                                showConfirmButton: false,
                                onBeforeOpen: () => {
                                    Swal2.showLoading();
                                },
                            });
                            fetch(e.target.result).then(res => res.blob()).then((blob) => {
                                let formData = new FormData();
                                formData.append('image',blob);
                                formData.append('transactionID', '{{$transactionID}}');
                                formData.append('_token', '{{csrf_token()}}');

                                let request = new XMLHttpRequest();
                                request.open('POST', '{{route('property.uploadImage')}}');
                                request.send(formData);
                                request.onreadystatechange = function () {
                                    if (request.readyState !== 4 || request.status !== 200) return;
                                    let data = JSON.parse(request.response);
                                    $('#uploadPropertyImage').attr('src', data.url+'?timestamp='+new Date().getTime());

                                    Swal2({
                                        toast: true,
                                        showConfirmButton: false,
                                        text: 'Property image saved!',
                                        type: 'success',
                                        position: 'top-end',
                                        timer: 2000,
                                    });
                                }
                            });


                        });
                    };
                    reader.readAsDataURL(file)
                }
            }
            uploadImage();
        });

       var CHANGES_MADE_P = false;
       $(document).ready(function(){
           $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.property-modal .close').on('click', function(){
                modal = $('.property-modal');
                if( CHANGES_MADE_P === true ){
                    swal({
                        title: "Are you sure?",
                        text: "Changes will not be saved!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                        })
                        .then((willDelete) => {
                        if (willDelete)
                        {
                            CHANGES_MADE_P = false;
                            fields = modal.find(':input[name]');
                            errors = modal.find('.errors');
                            errors.each(function(){
                                $(this).html("");
                            });
                            modal.modal('toggle');
                        } else {

                        }
                    });
                }
                else if(SAVED_PROPERTY_DATA){
                    modal.modal('toggle');
                    location.reload();
                }
                else
                {
                    errors = modal.find('.errors');
                    errors.each(function(){
                            $(this).html("");
                    });
                    modal.modal('toggle');
                }
            });

            $('#submit_button, #submit_exit').click(function(){
                $('.errors').hide();
                var btn       = $(this);
                var modal     = btn.closest('.modal-content');
                modal.addClass('dim');
                $('.spinner').show();
                var propertyAddress = modal.find('input[name="PropertyAddress"]').val();
                var formData  = btn.closest('form').serialize();
                /*
                Before submitting to save the property. Do an active property check.
                */
                var url = '{{route('checkProperty')}}';
                var executePropertyCheck = true;
                if(propertyAddress === savedAddress || SKIP_ACTIVE_PROPERTY_CHECK) executePropertyCheck = false;
                if (executePropertyCheck) {
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: formData,
                        error: function (err) {
                            if (err['responseText'].split(".")[1] === ' This means the address is not valid')
                            {
                                el = $('input[name="PropertyAddress"]');
                                el.siblings('.errors').text('The Address is not valid.').show();

                                $('.spinner').fadeOut(500);
                                modal.removeClass('dim');

                                swal({
                                    type: 'warning',
                                    text: 'The address \''+propertyAddress+'\' could not be verified. Please check the spelling. If you believe the address is correct as is, you can override.',
                                    showConfirmButton: true,
                                    showCancelButton: true,
                                    confirmButtonText: 'Override',
                                }).then((value) => {
                                    let override = value.value;
                                    if(override) addressOverride('{{route('override.address')}}',token,propertyAddress,'{{$transactionID}}');
                                });

                                return false;
                            }
                            else
                            {
                                swal("Oops...", "An error has occurred.", "error", {timer: 3000});
                            }
                        },
                        success: function (response) {

                            if (response.proceed === false) {

                                function canceledPropertyCheckModal()
                                {
                                    $('.property-modal .modal-content').removeClass('dim');
                                    $('.spinner').fadeOut(500);
                                }

                                function proceedPropertyCheckModal()
                                {
                                    saveProperty();
                                    SKIP_ACTIVE_PROPERTY_CHECK = true;
                                }

                                activePropertyCheckFailModal(canceledPropertyCheckModal,proceedPropertyCheckModal,cancelTransaction, response.message);
                            }
                            else {
                                saveProperty();
                            }
                        },
                    });
                }
                else
                {
                    saveProperty();
                }

                /*
                End property check.
                */
                function cancelTransaction()
                {
                    $.ajax({
                        url: '{{route('cancelTransaction', ['transactionID' => $transactionID])}}',
                        method: 'GET',
                        error: function(){
                            $('.spinner').fadeOut(500);
                            swal("Oops...", "An error has occurred.", "error", {timer: 3000});
                        },
                        success: function(response){
                            swal("Successfully Canceled Transaction",{
                                type: "success",
                                timer:2000,
                            }).then(function(){
                               window.location.href = '{{route('dash.ta.list')}}';
                            });
                        },
                    });
                }

                function saveProperty()
                {
                    url = "{{ route('saveProperty') }}";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: formData,
                        error: function (response) {
                            if (response['statusText'] == 'timeout') {
                                swal("Oops...", "Something went wrong!", "error", {timer: 3000});
                                $('.spinner').fadeOut(1000);
                                modal.removeClass('dim');
                                return false;
                            }

                            if (response['responseText'].split(".")[1] === ' This means the address is not valid') {

                                swal({
                                    type: 'warning',
                                    text: 'The address \''+propertyAddress+'\' could not be verified. Please check the spelling. If you believe the address is correct as is, you can override.',
                                    showConfirmButton: true,
                                    showCancelButton: true,
                                    confirmButtonText: 'Override',
                                }).then((value) => {
                                    let override = value.value;
                                    if(override) addressOverride('{{route('override.address')}}',token,propertyAddress,'{{$transactionID}}');
                                });
                            }

                            else {
                                for (x in response['responseJSON']['errors']) {
                                    if (x === 'PropertyType') {
                                        el = $('.property_form_1').find('select[name = "' + x + '"]');
                                        el.siblings('.errors').text(response['responseJSON']['errors'][x]).show();
                                    }
                                    el = $('.property_form_1').find('input[name = "' + x + '"]');
                                    el.siblings('.errors').text(response['responseJSON']['errors'][x]).show();
                                }
                            }
                            $('.spinner').fadeOut(1000);
                            modal.removeClass('dim');

                        },
                        success: function (response) {
                            $('.spinner').fadeOut(2000, function () {
                                $('#submit_button').attr('disabled', false);
                            });
                            $('.spinner').fadeOut(1000);
                            if (btn.attr('id') === 'submit_exit') {
                                CHANGES_MADE_P = false;
                                btn.closest('.modal').modal('toggle');
                                $('.o-address').text(response['address']);
                                $('#search_address').val(response['address']);
                                $('.o-PropertyType').text(response['PropertyType']);
                                $('.o-hasHOA').text(response['hasHOA']);
                                $('.o-hasSeptic').text(response['hasSeptic']);
                                $('.o-YearBuilt').text(response['YearBuilt']);
                                Swal2({
                                   title: 'Success',
                                    type: 'success',
                                   text: 'Property saved successfully.',
                                   timer: 3000,
                                   allowOutsideClick: false,
                                   showConfirmButton: false,
                                });
                                location.reload()
                            }
                            else {
                                savedAddress = response['address'];
                                CHANGES_MADE_P = false;
                                SAVED_PROPERTY_DATA = true;
                                modal.removeClass('dim');
                                $('.o-address').text(response['address']);
                                $('#search_address').val(response['address']);
                                $('.o-PropertyType').text(response['PropertyType']);
                                $('.o-hasHOA').text(response['hasHOA']);
                                $('.o-hasSeptic').text(response['hasSeptic']);
                                $('.o-YearBuilt').text(response['YearBuilt']);
                                swal("Success", "Data saved Successfully.", "success", {timer: 3000});
                            }

                        },
                        timeout: 7000,
                    });
                }
                return false;
            });

            $('.property-modal .add-notes').on('click', function(){
                let el = $(this);
                el.closest('.row').find('.modal-notes').toggleClass('show-notes');
                if ( el.text() == '+ ADD A NOTE' ) {
                    el.text('NOTES')
                }
                else {
                    el.text('+ ADD A NOTE')
                }
            });

            $('.property-modal :input[name]').on('input', function(){
                CHANGES_MADE_P = true;
            })
       });
    </script>
@append
