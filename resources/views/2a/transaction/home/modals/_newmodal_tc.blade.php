<div id="tcs-modal" class="tcs-modal modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="dim">
                <div class="spinner-kero"><i class="fas fa-spinner fa-spin"></i></div>
            </div>
            <div class="modal-body">
                <div class="row o-field-row">
                    <div class="col-sm-9">
                        <h1 class="tc-title"></h1>
                    </div>
                    <div class="col-sm-3 text-right">
                        <!-- <h1>STEP <span class="text-danger">1</span> FROM <span class="text-danger">1</span></h1> -->
                        <button type="button" class="close close-tcs-model" style="">
                            {!! config('otc.modalClose') !!}
                        </button>
                    </div>
                </div>
                <div class="row o-field-row">
                    <!-- Search Div -->

                <div class="col-md-12 search-container col-xs-12 col-sm-12 text-left">
                    <form action="{{ route('assignRole.TC') }}" id="formSearchBS">

                        <div class="col-md-1 text-right" style="padding-right: 2px;padding-top: 7px;"><i class="fas fa-search" style="color: #c94a49"></i></div>
                        <div class="col-md-12 col-xs-12 field-box">
                            <input type="hidden" name="Transactions_ID" value=" @$data['transaction']['ID'] }}">
                            <input type="hidden" name="_table" value="TransactionCoordinators">
                            <input type="hidden" name="_model" value="\App\Models\TransactionCoordinator">
                            <input type="hidden" name="_pickedID" value="" id="_pickedID">
                            <input type="hidden" name="_roleCode" value="btc" id="_roleCode">
                            <input name="search_query" class="search_person" autocomplete="off" placeholder="Search existing transaction coordinators" type="text" id="search_person" >
                            <ul class="col-md-12 col-sm-12 col-xs-12 search_list" id="search_list"> </ul>
                            <i id="search_person_spinner" class="search_spinner fas fa-circle-notch fa-spin"></i>
                        </div>


                        <button type="submit" class="o-teal-btn deactivated select-button" id="select-button" disabled>SELECT</button>

                    </form>
                </div>
                    <!-- End Search Div -->
                </div>
                <div class="row o-field-row">
                    <div class=" col-md-4 form-group with-label ">
                        <label for="NameFirst" class="label-control" style="text-transform: uppercase;">First Name <span class="important">*</span></label>
                        <input name="NameFirst" class="padding-left-0 text-field" placeholder="Enter your first Name">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-4 form-group with-label">
                        <label for="NameLast" class="label-control" style="text-transform: uppercase;">Last Name <span class="important">*</span></label>
                        <input name="NameLast" class="text-field padding-left-0" placeholder="Enter your last Name">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-4 form-group with-label">
                        <label for="Company" class="label-control" style="text-transform: uppercase;">Company Name<span class="important">*</span></label>
                        <input name="Company" class="text-field padding-left-0" placeholder="">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                </div>
                <div class="row o-field-row">
                    <div class=" col-md-4 form-group with-label ">
                        <label for="PrimaryPhone" class="label-control" style="text-transform: uppercase;">Cell #<span class="important">*</span></label>
                        <input name="PrimaryPhone" class="padding-left-0 text-field masked-phone" placeholder="### ### ####">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-4 form-group with-label">
                        <label for="License" class="label-control" style="text-transform: uppercase;">License<span class=""> #</span></label>
                        <input name="License" class="text-field padding-left-0" placeholder="#">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-4 form-group with-label">
                        <label for="Email" class="label-control" style="text-transform: uppercase;">Email<span class="important">*</span></label>
                        <input name="Email" class="text-field padding-left-0" placeholder="">
                        <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                </div>
                <div class="row o-field-row">
                    <div class=" col-md-6 form-group with-label">
                        <label for="Address" class="label-control" style="text-transform: uppercase;">Address <span class="important"></span></label>
                        <input name="Address" class="padding-left-0 text-field" placeholder="Enter Current Address">
                        <span class="address_errors col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                    <div class=" col-md-3 form-group with-label">
                        <label for="Unit" class="label-control" style="text-transform: uppercase;">Unit <span class="important"></span></label>
                        <input name="Unit" class="padding-left-0 text-field" placeholder="">
                        <span class="col-md-12 text-center errors text-left" style="display: none;"></span>
                    </div>
                </div>
                <div class="col-sm-12 row" style="margin-bottom: 10px !important;">
                    <div class="col-md-12">
                        <a class="add-notes text-danger col-sm-2" style="color: #cd6563">+ ADD A NOTE</a>
                    </div>
                    <div class="col-md-12">
                        <textarea class="form-control z-depth-1 display-none modal-notes" name="Notes" rows="1" cols="85" placeholder="Write something here..."></textarea>
                    </div>
                </div>
                <div class="row o-field-row text-left">
                    <div class="col-sm-12">
                        <button  type="button" class="save-add-edit-tc btn btn-teal" data-tc-url-input="" data-id="">
                            <div id="save-add-edit-tc-div" class="text-left hold-text" style="text-transform: uppercase;">

                                <div class="right-arrow" style="background-image: url({{asset('images/short-icon/right-arrow.png')}})"></div>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        var save_and_edit=false;
        var exit_or_save_before=false;
        var change_flag=false;

        var _screenMode;
        var _roleCodeTC = $('.tcs-modal #_roleCode');
        var roleCode;
        var _roleDisplay;
        var seller_buyer_data;
        var SPACE = "";
        tcModal = $('.tcs-modal');

        $(document).ready(function(){
            executeChanges();
        });
        

        function executeChanges(){
            all_current_tc_data = null;
            $('#_pickedID').val(null);
            var the_url = '{{route('inputBuyerTC',
            [
            'transactionID' => $transactionID,
            'ajax' => true,
            'tcType' => ':tctype'
            ])}}';

            roleCode                = $('.tcs-modal #_roleCode').val();
            var roleID              = $('.tcs-modal #_pickedID').val();
            var saveEditTCButton    = $('#tcs-modal .save-add-edit-tc');
            var dim                 = $('.modal-content .dim');
            if(roleCode === 'btc') the_url = the_url.replace(':tctype', 'btc');
            else the_url = the_url.replace(':tctype', 'stc');

            $.ajax({ // get tc data
                url : the_url,
                method: "GET",
                error:function(response){
                    dim.hide(500);
                },
                success:function(str){
                    dim.hide(500);
                    var modal = $('#tcs-modal');
                    all_current_tc_data = str;
                    //If there is an existing transaction coordinator
                    if(all_current_tc_data.data['ID'] !== null && all_current_tc_data.data['ID'] !== undefined)
                    {
                        $('.tcs-modal #_pickedID').val(all_current_tc_data.data['ID']);
                        if(roleCode === 'btc')
                        {
                            $('#tcs-modal .tc-title').text('Edit Buyer\'s Transaction Coordinator');
                            $('#save-add-edit-tc-div').text('Save and Edit Seller\'s Transaction Coordinator');
                        }
                        else
                        {
                            $('#tcs-modal .tc-title').text('Edit Seller\'s Transaction Coordinator');
                            $('#save-add-edit-tc-div').text('Save and Edit Buyers\'s Transaction Coordinator');
                        }
                    }
                    else
                    {
                        if(roleCode === 'btc')
                        {
                            $('#tcs-modal .tc-title').text('Add Buyer\'s Transaction Coordinator');
                            $('#save-add-edit-tc-div').text('Save and Edit Seller\'s Transaction Coordinator');
                        }
                        else
                        {
                            $('#tcs-modal .tc-title').text('Add Seller\'s Transaction Coordinator');
                            $('#save-add-edit-tc-div').text('Save and Edit Buyers\'s Transaction Coordinator');
                        }
                    }
                    let allInputs = $('#tcs-modal *').filter(':input');
                    let tc_DATA   = all_current_tc_data['data'];
                    allInputs.each((i,el) => {
                        if(tc_DATA[el.name] !== undefined)
                            el.value = tc_DATA[el.name] || '';
                    });

                    savedNameFirst = modal.find('input[name="NameFirst"]').val();
                    savedNameLast  = modal.find('input[name="NameLast"]').val();

                    var address = null;
                    if(all_current_tc_data.data['Street1']) address = all_current_tc_data.data['Street1'];
                    if(all_current_tc_data.data['Street2']) address = address + ' ' + all_current_tc_data.data['Street2'];
                    if(all_current_tc_data.data['State']) address = address + ' ' + all_current_tc_data.data['State'];
                    if(all_current_tc_data.data['Zip']) address = address + ' ' + all_current_tc_data.data['Zip'];

                    $('#tcs-modal input[name="Address"]').val(address);
                    //assign the next role to edit
                    if (roleCode === 'btc') saveEditTCButton.data({next:'stc'});
                    else saveEditTCButton.data({next:'btc'});

                    saveEditTCButton.data({theurl:the_url});

                    dim.hide(500);
                },
                async:false,
            });

            exit_or_save_before=true;
        }

        function save_TC_to_DB(){
            var modal = $('.tcs-modal');
            if(change_flag === false){
                if(roleCode === 'btc')
                {
                    _roleCodeTC.val('stc');
                    executeChanges();
                    return true;
                }
                else
                {
                    _roleCodeTC.val('btc');
                    executeChanges();
                    return true;
                }
            }

            $('.modal-content .dim').show();
            $('.errors').hide();

            var you_can_go_next2=true;
            var function_saved_succesfully=true;
            var error_inputs={
                "NameFirst":$("#tcs-modal input[name='NameFirst']").val(),
                "NameLast":$("#tcs-modal input[name='NameLast']").val(),
                //"Address":$("#tcs-modal input[name='Address']").val(),
            };
            for(err in error_inputs){
                if(error_inputs[err].length==0){
                    $("#tcs-modal input[name='"+err+"']").siblings('.errors').show().text('this field is required');
                    you_can_go_next2=false;
                    function_saved_succesfully=false;
                    save_and_edit=false;
                }
            }

            var nameFirst = modal.find('input[name="NameFirst"]').val();
            var nameLast = modal.find('input[name="NameLast"]').val();

            var data={
                "ajax": 1,
                "Transactions_ID"   :'{{$transactionID}}',
                "ID"                :$("#tcs-modal input[name='_pickedID']").val(),
                "Users_ID"          :all_current_tc_data.data['Users_ID'],
                "NameFirst"         :nameFirst,
                "NameLast"          :nameLast,
                "License"           :$("#tcs-modal input[name='License']").val(),
                "Company"           :$("#tcs-modal input[name='Company']").val(),
                "PrimaryPhone"      :$("#tcs-modal input[name='PrimaryPhone']").val(),
                "SecondaryPhone"    :$("#tcs-modal input[name='SecondaryPhone']").val(),
                "Email"             :$("#tcs-modal input[name='Email']").val(),
                "Unit"              :$("#tcs-modal input[name='Unit']").val(),
                "Notes"             :$("#tcs-modal textarea[name='Notes']").val(),
                "Address"           :$("#tcs-modal input[name='Address']").val(),
            };

            //savedNameFirst and savedNameLast are variables declared in home
            if(nameFirst !== savedNameFirst || nameLast !== savedNameLast) delete data['ID'];

            $.ajax({
                url : '{{route('saveRole.TC')}}',
                method: "POST",
                data: data,
                error:function(response){
                    if( response['responseText'].split(".")[1] === ' This means the address is not valid'){
                        el = $('input[name="Address"]');
                        el.siblings('.errors').text('The Address is not valid.').show();

                    }
                    else
                    {
                        $.each(response.responseJSON.errors, function(key,err){
                            $('#tcs-modal :input[name="'+key+'"]').siblings('.errors').text(err);
                            $('#tcs-modal :input[name="'+key+'"]').siblings('.errors').css('display', 'block');
                        });
                    }


                    save_and_edit=false;
                    function_saved_succesfully=false;
                },
                success:function(str){
                    swal({
                        title: "Success",
                        text: "Transaction Coordinator Saved!",
                        icon: "success",
                        timer:2000
                    });

                    var data = {
                        "_pickedID":str.tc_ID,
                        "_roleCode":roleCode,
                    };

                    $.ajax({
                        url: '{{route('assignRole.TC')}}',
                        method: 'POST',
                        data: data,
                        success:function(response){

                        },

                    });

                    if(roleCode === 'btc')
                    {
                        _roleCodeTC.val('stc');
                        executeChanges();
                    }
                    else
                    {
                        _roleCodeTC.val('btc');
                        executeChanges();
                    }

                    $('.modal-content .dim').hide(500);
                    save_and_edit=true;
                    function_saved_succesfully=true;
                    change_flag=false;
                },
                timeout: 7000,
                async: false,

            });
            $('.modal-content .dim').hide(500);
            return function_saved_succesfully;
        }

        //This function needs to be edited
        $('.save-add-edit-tc').click(function(){
            $('.modal-content .dim').show();
            setTimeout(function(){
                save_TC_to_DB();//running
                $('.modal-content .dim').hide();
            }, 30);

        });


        $('.close-tcs-model').click(function(){
            if(change_flag){
                swal({
                    title: "Are you sure?",
                    text: "Changes will not be saved!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            $('.modal-content .dim').hide();
                            $('#tcs-modal').modal('hide');
                            change_flag=false;
                        } else {

                        }
                    });
            }
            else if(save_and_edit){
                $('#tcs-modal').modal('hide');
                location.reload();
            }
            else $('#tcs-modal').modal('hide');
        });

        $('#tcs-modal').on('input',function(e){
            change_flag=true;
        });


        $('.tcs-modal .add-notes').on('click', function() {
            var el = $(this);
            let noteField = $("#tcs-modal textarea[name='Notes']");
            if ( el.text() == '+ ADD A NOTE' ) {
                el.text('NOTES');
                noteField.show();
            }
            else {
                el.text('+ ADD A NOTE');
                noteField.hide();
            }
        });

    </script>
@append