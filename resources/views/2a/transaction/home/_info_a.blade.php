<?php
$boxColor = ['buyer'=>'red', 'seller'=>'blue'];
?>
@if($data['transaction'][title_case($role).'sAgent_ID'] === 0)
    <div class="col-xs-12 col-md-6 col-lg-3 agents-div">
        <div class="{{$boxColor[$role]}} no-cursur red-blue-4 col-xs-12">
            <div class="img"></div>
            <div class="transaction-info text-left">
                <h3>{{title_case($role)}}'s Agent</h3>
                <h4>None</h4>
                <div class="hrefs">
                    <a href="#" class="view view-agent" data-a-url="{{ route('input'.title_case($role).'Agent', $data['transaction']['ID'] ) }}" data-model-for="{{strtolower($role[0].'a')}}" data-id="" data-backdrop="static" data-keyboard="false">VIEW</a>
                    <!-- <a  class="info">ADD NEW</a> ToDO-->
                </div>
            </div>
        </div>
    </div>
@else
    @foreach ($data[$role.'sAgent'] as $agent)
        <div class="col-xs-12 col-md-6 col-lg-3 agents-div">
            <div class="{{$boxColor[$role]}} no-cursur red-blue-4 col-xs-12">
                <div class="img"></div>
                <div class="transaction-info text-left">
                    <h3>{{title_case($role)}}'s Agent</h3>
                    <h4>{{$agent['NameFull'] ?? $agent['NameFirst'].' '.$agent['NameLast']}}</h4>
                    <h4>{{$agent['PrimaryPhone']}}</h4>
                    <div class="hrefs">
                        <a href="#" class="view view-agent" data-a-url="{{ route('input'.title_case($role).'Agent', $data['transaction']['ID']) }}" data-model-for="{{strtolower($role[0].'a')}}" data-id="{{ $agent['ID'] }}" data-backdrop="static" data-keyboard="false">VIEW</a>
                        <!-- <a  class="info">ADD NEW</a> ToDO-->
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif


@section('scripts')

@append