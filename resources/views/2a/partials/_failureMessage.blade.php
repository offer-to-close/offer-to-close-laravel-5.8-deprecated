@section('custom_style')
    <style>
        .error-icon{
            color: #E7002A;
        }
    </style>
@endsection
<div style="" id="o-failure-message">
    <div class="row">
        <div class="col-md-12 text-center failure-icon"><span><i class="fas fa-exclamation-triangle red"></i></span></div>
        <div class="col-md-12 failure-text text-center">
            <h4>{{ @$_Message }}</h4>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $(document).ready(function(){
            setTimeout(function(){
                $('#o-failure-message').fadeOut(1000);
            }, 2000);
        });
    </script>
@append