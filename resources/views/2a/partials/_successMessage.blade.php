

<div style="" id="o-success-message">
    <div class="row">
        <div class="col-md-12 text-center success-icon error-icon"><span><i class="fas fa-check-circle"></i></span></div>
        <div class="col-md-12 success-text text-center ">
            <h4>{{ @$_Message }}</h4>
        </div>
    </div>
</div>

@section('scripts')
    <script>
        $(document).ready(function(){
            setTimeout(function(){
                $('#o-success-message').fadeOut(1000);
            }, 2000);
        });
    </script>
@append