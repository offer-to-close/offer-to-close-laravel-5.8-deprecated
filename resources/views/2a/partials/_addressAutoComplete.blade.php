
<style>
    .search_address_partial #search_address{
        border-radius: 0px;
        outline: none;
        border-width: 0;
        border-color: none;
        padding-left:25px;
        background-color: #F9F9F9;
        border-bottom-width: 1px;
        border-bottom-color: #c94543;
        font-size: 20px;
        color: #333;
    }
    .search_address_partial .search_address_list{
        position: absolute;
        left: 0px;
        list-style: none;
        padding-left: 5px;
        background: white;
        max-height: 170px;
        overflow-y: scroll;
        box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        z-index: 5;
        color: 	#333 !important;
    }

    .search_address_partial #search_address_list li{
        letter-spacing: 1px;
        margin-top: 5px;
        margin-bottom: 5px;
        padding-top: 5px;
        padding-bottom: 5px;

    }
    .search_address_partial #search_address_list li:hover{
        background: #f2f2f2;
        cursor: pointer;
    }
    .search_address_partial #search_address_list a{
        list-style: none;
        color: 	#333 !important;
    }
    .search_address_partial #search_address_list{
        font-weight: 1000;
        color: #333;
        display: none;
    }

    .search_address_partial #search_address_spinner{
        font-size: 20px;
        position: absolute;
        top: 20%;
        left: 78%;
        display: none;
    }
    .list-address{
        font-weight: 500;
    }
    .outter-div{
        padding: 0 !important;
    }
    .search_address_partial .select_btn{
        border-radius: 3px;
        outline: none;
        border: none;
        background-color: #00C3B9;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    .search_address_partial .select_btn i{
        color: white !important;
    }
    .search_address_partial .outter-div{
        position: relative;
        z-index: 5;
    }
    .search_address_partial .location-icon{
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
    }
    .search_address_partial .select_btn{
        font-weight: 700;
        color: white;
        letter-spacing: 1px;
    }

    /*
     This is done due to the modal having a high z-index value.
     You can edit how the container showing the addresses looks.
     */
    .pac-container{
        z-index: 100000; /*necessary to appear in front of modal,
            need to check other z-indexes to see if other devs have been using extreme values */
    }

    /* Always set the map height explicitly to define the size of the div
    * element that contains the map. */
    #map {
        height: 100%;
    }
    /* Optional: Makes the sample page fill the window. */
    html, body {
        height: 100%;
        margin: 0;
        padding: 0;
    }
</style>

<form class="search_address_form" role="form" action="{{ @$o_action }}" method="POST">
    <div class="spinner teal"><i class="fas fa-spinner fa-spin"></i></div>
    <div class="outter-div search_address_partial {{ @$o_classes }}">
        <div class="location-icon" style="background-image: url({{ asset('images/location-icon.png') }})"></div>
        {!! csrf_field() !!}
        <input type="hidden" name="_table" value="{{ @$Table }}">
        <input type="hidden" name="ID" value="{{ @$ID }}">
        <input type="hidden" name="transactionID" value="{{ @$transactionID }}">
        <input name="search_query" id="search_address" style="width: 85%" autocomplete="off" placeholder="{{  @$o_placeholder }}" type="text"
               {{ @$is_readonly }}
               value="{{ @$default_address_value ? @$default_address_value : Request::old('search_query')}}">
        @if( @$default_address_value == '' )
        <!--<button name="" style="width: 14%" class="select_btn" type="button">SELECT</button>-->
        @endif
        <div class="col-md-12">
            <ul class="col-md-12 col-sm-12 col-xs-12 search_list text-left" id="search_address_list"> </ul>
        </div>

    </div>
</form>

@section('scripts')
    <script>
        var spinner = $('.spinner');
        let selectProperty;
        var placeSearch, autocompleteTransactionHome,autocompletePropertyModal;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        /**
         * The api key for google.places.autocomplete is set in home.
         * */


        // This displays an address form, using the autocomplete feature
        // of the Google Places API to help users fill in the information.

        // This requires the Places library. Include the libraries=places
        // parameter when you first load the API.

        function initAutocomplete()
        {
            // Create the auto complete object, restricting the search to geographical
            // location types. This auto complete is attached to the top address bar inside of transaction home.
            autocompleteTransactionHome = new google.maps.places.Autocomplete(
                (document.getElementById('search_address')), //top of transaction home
                {types: ['geocode']});
            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocompleteTransactionHome.addListener('place_changed', fillInAddressTopBar);

            /**
             * This is the auto complete attached to the property modal.
             * */
            autocompletePropertyModal = new google.maps.places.Autocomplete(
                (document.getElementById('pModalSearchAddress')), //inside of property modal
                {types: ['geocode']});
            autocompletePropertyModal.addListener('place_changed', fillInAddressPropertyModal);

            /**
             * This is the auto complete attached to the homesumer modals
             * */
            autocompleteHomesumerModal = new google.maps.places.Autocomplete(
                (document.getElementById('homesumer_address')),
                {types: ['geocode']});
            autocompleteHomesumerModal.addListener('place_changed', fillInHomesumerAddress);
        }

        function fillInAddressTopBar()
        {
            // Get the place details from the autocomplete object.

            function formatAddress(object,elementId)
            {
                var place = object.getPlace();
                place.formatted_address = place.formatted_address.substr(0,place.formatted_address.length-5);
                $('#'+elementId).val(place.formatted_address);

            }

            try {
                formatAddress(autocompletePropertyModal, 'pModalSearchAddress');
            } catch (e) {
                //this try to avoid crashing future code if this object is empty.
            }
            try {
                formatAddress(autocompleteTransactionHome, 'search_address');
            } catch (e) {
                //this try to avoid crashing future code if this object is empty.
            }

            selectProperty();
        }

        function fillInAddressPropertyModal()
        {
            // Get the place details from the autocomplete object.

            function formatAddress(object,elementId)
            {
                var place = object.getPlace();
                place.formatted_address = place.formatted_address.substr(0,place.formatted_address.length-5);
                $('#'+elementId).val(place.formatted_address);

            }

            try {
                formatAddress(autocompletePropertyModal, 'pModalSearchAddress');
            } catch (e) {
                //this try to avoid crashing future code if this object is empty.
            }
            try {
                formatAddress(autocompleteTransactionHome, 'search_address');
            } catch (e) {
                //this try to avoid crashing future code if this object is empty.
            }
        }

        $(document).ready(function(){

            $(document).keypress(function(event) {
                if(event.keyCode === 13)
                {
                    event.preventDefault();
                }
            });

            selectProperty = function selectProperty()
            {
                function executePropertyCheck()
                {
                    spinner.show();
                    var propertyAddress = $('.search_address_form input[name="search_query"]').val();
                    /*
                    Before submitting to save the property. Do an active property check.
                    */
                    var url = '{{route('checkProperty')}}';
                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {
                            "PropertyAddress" : propertyAddress,
                            "transactionID": '{{$transactionID}}',
                            "search_query" : propertyAddress,
                        },
                        error: function(err){
                            spinner.fadeOut(500);
                            if (err['responseText'].split(".")[1] === ' This means the address is not valid')
                            {
                                $('.spinner').fadeOut(500);

                                swal({
                                    type: 'warning',
                                    text: 'The address \''+propertyAddress+'\' could not be verified. Please check the spelling. If you believe the address is correct as is, you can override.',
                                    showConfirmButton: true,
                                    showCancelButton: true,
                                    confirmButtonText: 'Override',
                                }).then((value) => {
                                    let override = value.value;
                                    if(override)
                                    {
                                        addressOverride('{{route('override.address')}}',token,propertyAddress,'{{$transactionID}}');
                                        executePropertyCheck();
                                    }
                                });

                                return false;
                            }
                            else
                            {
                                swal("Oops...", "An error has occurred", "error", {timer: 3000});
                            }
                        },
                        success: function(response){
                            if(response.proceed === false)
                            {
                                function canceledPropertyCheckModal() {spinner.fadeOut(500);}
                                function proceedPropertyCheckModal() {saveProperty();}

                                activePropertyCheckFailModal(canceledPropertyCheckModal,proceedPropertyCheckModal,cancelTransaction,response.message);
                            }
                            else
                            {
                                saveProperty();
                            }
                        },
                    });

                    /*
                    End property check.
                    */
                    function cancelTransaction()
                    {
                        $.ajax({
                            url: '{{route('cancelTransaction', ['transactionID' => $transactionID])}}',
                            method: 'GET',
                            error: function(){
                                swal("Oops...", "An error has occurred.", "error", {timer: 3000});
                                spinner.fadeOut(500);
                            },
                            success: function(response){
                                spinner.fadeOut(500);
                                swal("Successfully Canceled Transaction",{
                                    type: "success",
                                    timer:2000,
                                }).then(function(){
                                    window.location.href = '{{route('dash.ta.list')}}';
                                });
                            },
                        });
                    }

                    function saveProperty()
                    {
                        url = "{{ route('saveProperty') }}";
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {
                                "Transactions_ID": '{{$transactionID}}',
                                "search_query" : propertyAddress,
                                "singleInput"  : true,
                            },
                            error: function (response) {
                                if (response['responseText'].split(".")[1] === ' This means the address is not valid' || response['responseJSON']['status'] === 'AddressError')
                                {
                                    swal({
                                        type: 'warning',
                                        text: 'The address \''+propertyAddress+'\' could not be verified. Please check the spelling. If you believe the address is correct as is, you can override.',
                                        showConfirmButton: true,
                                        showCancelButton: true,
                                        confirmButtonText: 'Override',
                                    }).then((value) => {
                                        let override = value.value;
                                        if(override)
                                        {
                                            addressOverride('{{route('override.address')}}',token,propertyAddress,'{{$transactionID}}');
                                            saveProperty();
                                        }
                                    });
                                }
                                else {
                                    swal("Oops...", "An error has occurred.", "error", {timer: 3000});
                                }
                                spinner.fadeOut(500);

                            },
                            success: function (response) {
                                spinner.fadeOut(500);
                                swal("Success", "Data saved Successfully.", "success", {timer: 3000});
                                location.reload();
                            },
                            timeout: 7000,
                        });
                    }

                    return false;
                }

                executePropertyCheck();
            }
        }); //end of document ready function
    </script>
@append
