@if(Session::has('data_saved'))
    @include('2a.partials._successMessage', [
        '_Message' => Session::get('data_saved'),
    ])
@endif
@extends('2a.layouts.master')
@section('content')
    <?php
    /* ... Expected variables ...
            $transactionID
            $users :: The list of users that can be invited by TC
            $senderRole :: Is the sender TC the 'btc' or the 'stc'
    */
        $action = $action ?? route('invite.send');
    ?>
    @if (\Session::has('failure'))
        <div class="alert alert-danger">
            <h3 class="alert-danger-h3">{!! \Session::get('failure') !!}</h3>
        </div>
    @endif

    <div class="col-sm-offset-1 col-sm-10 offer_page clearfix">
        <div class="col-sm-6">

            <span class="offer_page_gen_title otc-red"><img class="img-responsive" src="{{asset('images/icons/invitation-send.png')}}" alt="Mail" width="35" height="35"> Invite User</span> </div>
        <form method="post" action="{{$action}}" enctype="multipart/form-data">
            {{csrf_field()}}

            <input type="hidden" name="inviteCode" value="{{$inviteCode}}" />
            <input type="hidden" name="address" value="{{$address}}" />

            <div class="col-sm-12 offer_page_main clearfix">
                <div class="col-lg-offset-3 col-lg-6  col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">


                    <h3>Are you sure you want to send an invitation to the {{$role ?? 'random role'}}, <strong>{{$invitee ?? 'random invitee'}}</strong>,
                        with regard to the property at {{$address ?? 'random address'}}?
                    </h3>

                    <button name="inviteButton" value="sendInvitation" type="submit">Yes</button>
                    <button name="inviteButton" value="cancelInvitation" type="submit">No</button>

                    <div style="clear: both"></div>
                </div>
            </div>
        </form>
    </div>
@endsection