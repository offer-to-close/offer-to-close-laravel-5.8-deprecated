 @if(Session::has('data_saved'))
        @include('2a.partials._successMessage', [
            '_Message' => Session::get('data_saved'),
        ])
@endif
@extends('2a.layouts.master')
@push('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endpush
@section('content')

    <style>
        .offer_page {margin-top:60px;}
        .offer-icon{position: relative; font-size: 25px; padding-top: 5px; }
        .offer-icon i{
            position: absolute;
            transform: rotateY(180deg)
        }
        .offer_page_gen_icon {float:left; line-height: 40px; margin-top: 10px; line-height: 10; transform: rotate(180de)}
        .offer_page_gen_title {display: block;float:left; padding-top: 2px; line-height: 40px; margin-bottom:0; padding-left: 40px; font-size: 25px; font-weight: 600; letter-spacing: 1px;}
        .offer_page_add_button{background-color: #01beb8; color: white; font-size:11px; border: 0;  margin-bottom:0;  border-radius: 6px; padding: 15px; float: right}
        .offer_page_save_icon_box {text-align: center;}
        .offer_page_save_icon_box.with_padding {padding-top:30px; padding-bottom: 100px;}
        .offer_page_save_title {color:#21d7d1;  font-size: 16px; font-weight: 600; text-decoration: underline; text-align: center; padding-left: 3px; padding-top:2px; }
        .offer_page_save_icon {margin-top: -4px;}
        .offer_page_main {background-color: white; border-radius: 10px; width: 100%; clear: both; margin-top: 20px; margin-bottom: 100px;}
        .offer_page_row {background-color: #f9f9f9; clear: both; padding: 10px 0 0 0;margin-top: 25px;}
        .offer_page_row.padding_20 {margin-top: 5px!important;}
        .offer_page_title {
            font-size: 15px;
            font-weight: 700;
        }
        .offer_page_title a{
            font-size: 13px;
            font-weight: 600;
        }
        .offer_page_right_text {text-align: right; padding-right: 0}
        .offer_page_left_text {text-align: left; padding-left: 0}
        .offer_page_green_button {width: 100%; margin-top: 10px; text-align: center; padding: 15px; background-color: #21d7d1; font-size: 15px}
        .offer_page_red_button {width: 100%; margin-top: 10px; text-align: center; padding: 15px; background-color: #cd6564; font-size: 15px}
        .form-control {padding:22px; margin:0 0  0 0}
        .form-select {    height: 46px;    padding: 12px;}
        .form-date {    height: 46px;  padding: 12px;}
        textarea {  resize: none;  }
        .offer_page_button_box {margin-top: 40px;}
        .transparent_button{background: transparent; border: 0; outline: 0}
        .error_input {height:10px; font-size:13px; color:red; font-weight: 100;  display: inherit!important}
        .long_label{margin-top: 10px;}
        .currency{
            font-size: 20px;
            /*padding: 0 10px 0 15px;*/
            color: #999;
            line-height: 2.0;
            border-radius: 7px 0 0 7px;
            background: transparent;
            top: 3px;
            left: 12px;
        }
        .currency_format{
            padding-left: 30px;
        }
        .hide{
            opacity: 0;
        }
        .show{
            opacity: 1;
        }
        #uploaded_offers_and_counter_offers span, #uploaded_counter_offers span{
            display: block;
        }

        #uploaded_counter_offers span:hover a{
            color: {{config('otc.teal')}} !important;
            text-decoration: underline;
        }

        #uploaded_offers_and_counter_offers span:hover a{
            color: {{config('otc.teal')}} !important;
            text-decoration: underline;
        }

        @media screen and (max-width: 768px) {
            .main-content {padding-left: 40px;}
            .offer_page {margin-top: 10px;}
            .offer_page_main {margin-bottom: 50px;}
            .offer_page_right_text {text-align: left}
            .offer_page_button_box {margin-top: 20px;}
            .offer_page_save_icon_box.with_padding {padding-top:30px; padding-bottom: 40px;}
            .offer_page_main{margin-top:10px;}
            .offer_page_gen_icon {margin-bottom:10px}
            .offer_page_gen_title {margin-bottom:10px; }
            .offer_page_add_button {margin-bottom:10px}
        }
    </style>

    <?php
            if (isset($_mode) && $_mode == 'edit') $titlePrefix = 'EDIT';
            else $titlePrefix = '+ ADD NEW';

            if($data['isFee'] == NULL) $data['isFee'] = 0;
            if($data['isPaid'] == NULL) $data['isPaid'] = 0;
            $data['EscrowLength'] = 45;
            $data['AcceptedDate'] = date('Y-m-d');
    ?>
    @if (\Session::has('failure'))
        <div class="alert alert-danger">
                <h3 class="alert-danger-h3">{!! \Session::get('failure') !!}</h3>
        </div>
    @endif
<?php
    if (isset($data['ID'])) $title = 'Offer ' . $data['ID'];
    else $title = 'New Offer';

    $documents = \App\Http\Controllers\OfferController::getOfferDocuments($data['ID']??0)
?>
    <div class="col-sm-offset-1 col-sm-10 offer_page clearfix">
        <div class="col-sm-6"><span class="offer-icon pull-left otc-red"><i class="fas fa-tags"></i></span> <span class="offer_page_gen_title otc-red">{{$title}}</span> </div>
        <div class="col-sm-6"><a class="btn offer_page_add_button" href="{{url('offer')}}">{{ $titlePrefix }} OFFER</a></div>
        <form action="{{route('save.Offer')}}" method="POST" enctype="multipart/form-data" id="offer_form">
            {{csrf_field()}}
            <input type="hidden" name="Status" value="open">
            <input class="form-control" type="hidden" name="ID" value="@if($data['ID']){{$data['ID']}}@endif">
            <div class="col-sm-12 offer_page_save_icon_box">
                <button type="submit" class="transparent_button" name="saveoffer">
                    <img class="offer_page_save_icon" src="{{asset('/images/icons/offer-save-icon.png')}}"> <span class="offer_page_save_title" style="text-transform: uppercase;">Save</span>
                </button>
            </div>

            <div class="col-sm-12 offer_page_main clearfix">
                <div class="col-lg-offset-3 col-lg-6  col-md-offset-2 col-md-8 col-sm-offset-1 col-sm-10">

                    <div class="offer_page_row clearfix">
                        <div class="col-sm-6 offer_page_title offer_page_right_text" style="text-transform: uppercase;">Offer Date</div>
                        <div class="col-sm-6">
                            <input type="date" class="form-control form-date" name="DateOffer" value="@if(old('DateOffer')){{old('DateOffer')}}@elseif($data['DateOffer']){{$data['DateOffer']}}@else{{date('Y-m-d')}}@endif">
                            <label class="error_input" for="DateOffer">{{$errors->first('DateOffer')}}</label>
                        </div>
                    </div>
                    <!--
                    Start Property Segment
                    -->
                    <div class="warning">
                        <h4 class="alert-danger-h4"></h4>
                    </div>
                    <div class="offer_page_row clearfix clearfix">
                        <div class="col-sm-12 offer_page_title" style="text-transform: uppercase;">Property address</div>
                        <div class="col-sm-8">
                            <input id="street_address" class="form-control street" value="@if(old('Street1')){{old('Street1')}}@elseif($data['Street1']){{$data['Street1']}}@endif" placeholder="Address Line 1" name="Street1">
                            <span class=" col-md-12 text-center errors"></span>
                            <label class="error_input" for="Street1">{{$errors->first('Street1')}}</label>
                        </div>
                        <div class="col-sm-4">
                            <input class="form-control" value="@if(old('Unit')){{old('Unit')}}@elseif($data['Unit']){{$data['Unit']}}@endif" placeholder="Unit #" name="Unit">
                            <label class="error_input" for="Unit">{{$errors->first('Unit')}}</label>
                        </div>
                        <div class="col-sm-4">
                            <input id="locality" class="form-control city" value="@if(old('City')){{old('City')}}@elseif($data['City']){{$data['City']}}@endif" placeholder="City" name="City">
                            <span class=" col-md-12 text-center errors"></span>
                            <label class="error_input" for="City">{{$errors->first('City')}}</label>
                        </div>
                        <div class="col-sm-4">
                            <?php $default = old('State') ?? ($data['State'] ?? 'CA');?>
                            <div class="form-group{{ $errors->has('State') ? ' has-error' : '' }} with-label">
                                <select id="administrative_area_level_1" name="State" class="form-control form-select">
                                    {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($default, Config::get('otc.states'), 'index', 'value') !!}
                                </select>
                                @if($errors->has('State'))
                                    <span class="help-block"><strong>{{ $errors->first('State') }}</strong></span>
                                @endif
                                <label class="error_input" for="State">{{$errors->first('State')}}</label>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <input id="postal_code" class="form-control zip" value="@if(old('Zip')){{old('Zip')}}@elseif($data['Zip']){{$data['Zip']}}@endif" placeholder="Zip code" name="Zip">
                            <span class=" col-md-12 text-center errors"></span>
                            <label class="error_input" for="Zip">{{$errors->first('Zip')}}</label>
                        </div>
                    </div>
                    <!--
                    END Property Segment
                    -->
                    <div class="offer_page_row clearfix">
                        <div class="col-sm-12 offer_page_title" style="text-transform: uppercase;">Client Name</div>
                        <div class="col-sm-6">
                            <input class="form-control" value="@if(old('NameFirst')){{old('NameFirst')}}@elseif($data['NameFirst']){{$data['NameFirst']}}@endif" placeholder="First Name" name="NameFirst">
                            <label class="error_input" for="NameFirst">{{$errors->first('NameFirst')}}</label></div>
                        <div class="col-sm-6">
                            <input class="form-control" value="@if(old('NameLast')){{old('NameLast')}}@elseif($data['NameLast']){{$data['NameLast']}}@endif" placeholder="Last Name" name="NameLast">
                            <label class="error_input" for="NameLast">{{$errors->first('NameLast')}}</label>
                        </div>
                    </div>

                    <div class="offer_page_row clearfix">
                        <div class="col-sm-6">
                            <div class="offer_page_title" style="text-transform: uppercase;">Listing Price</div>
                            <div style="position: relative;">
                                <span class="currency" style="position: absolute;">$</span>
                                <input class="form-control currency_format" value="@if(old('PriceListing')){{old('PriceListing')}}@elseif($data['PriceListing']){{$data['PriceListing']}}@endif" placeholder="0.00" name="PriceListing">
                            </div>
                            <label class="error_input" for="PriceListing">{{$errors->first('PriceListing')}}</label>
                        </div>
                        <div class="col-sm-6">
                            <div class="offer_page_title" style="text-transform: uppercase;">Offer Price</div>
                            <div style="position: relative;">
                                <span class="currency" style="position: absolute;">$</span>
                                <input class="form-control currency_format" value="@if(old('PriceAsking')){{old('PriceAsking')}}@elseif($data['PriceAsking']){{$data['PriceAsking']}}@endif" placeholder="0.00" name="PriceAsking">
                            </div>
                            <label class="error_input" for="PriceAsking">{{$errors->first('PriceAsking')}}</label>
                        </div>
                    </div>

                    <div class="offer_page_row clearfix">
                        <div class="col-sm-6 offer_page_title offer_page_right_text" style="text-transform: uppercase;">Inspection Contingency</div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <input class="form-control" value="@if(old('ContingencyInspection')){{old('ContingencyInspection')}}@elseif($data['ContingencyInspection']){{$data['ContingencyInspection']}}@else 17 @endif" placeholder="0" name="ContingencyInspection">
                            <label class="error_input" for="Zip">{{$errors->first('ContingencyInspection')}}</label>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 offer_page_title" style="padding-left: 0">
                            <b>Days</b>
                        </div>
                    </div>

                    <div class="offer_page_row clearfix padding_20">
                        <div class="col-sm-6 offer_page_title offer_page_right_text" style="text-transform: uppercase;"> Appraisal Contingency</div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <input value="@if(old('ContingencyAppraisal')){{old('ContingencyAppraisal')}}@elseif($data['ContingencyAppraisal']){{$data['ContingencyAppraisal']}}@else 17 @endif" class="form-control" placeholder="0" name="ContingencyAppraisal">
                            <label class="error_input" for="ContingencyAppraisal">{{$errors->first('ContingencyAppraisal')}}</label>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 offer_page_title" style="padding-left: 0">
                            <b>Days</b>
                        </div>
                    </div>

                    <div class="offer_page_row clearfix padding_20">
                        <div class="col-sm-6 offer_page_title offer_page_right_text" style="text-transform: uppercase;">Loan Contingency</div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <input value="@if(old('ContingencyLoan')){{old('ContingencyLoan')}}@elseif($data['ContingencyLoan']){{$data['ContingencyLoan']}}@else 21 @endif" class="form-control" placeholder="0" name="ContingencyLoan">
                            <label class="error_input" for="ContingencyLoan">{{$errors->first('ContingencyLoan')}}</label>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 offer_page_title" style="padding-left: 0">
                            <b>Days</b>
                        </div>
                    </div>

                    <div class="offer_page_row clearfix padding_20">
                        <div class="col-sm-12 offer_page_title" style="text-transform: uppercase;">Any Additional Details</div>
                        <div class="col-sm-12">
                            <textarea name="AdditionalDetails" cols="40" rows="5" class="form-control" placeholder="Additional Details">@if(old('AdditionalDetails')){{old('AdditionalDetails')}}@elseif($data['AdditionalDetails']){{$data['AdditionalDetails']}}@endif</textarea>
                            <label class="error_input" for="AdditionalDetails">{{$errors->first('AdditionalDetails')}}</label>
                        </div>
                    </div>
                    <!--

                    START FEE SEGMENT
                    Currently experiencing issues with images not showing up. Also, the fas classes which are supposed to apply different graphics to isFee and isPaid inputs are not working.

                    -->
<?php
    if (\App\Http\Controllers\AccessController::hasAccess('s', \App\Http\Controllers\CredentialController::current()->ID())) {
?>
                    <div class="offer_page_row clearfix padding_20">
                        <div class="col-sm-12 offer_page_title" style="text-transform: uppercase;">Fees</div>
                        <div class="col-sm-12">
                            <div class="form-group with-label col-md-12 col-sm-12 col-xs-12">
                                <label class="label-control long_label">
                                    <div class="blue-ball" style="background-image: url(http://localhost/OfferToClose/public/images/short-icon/blue-ball.png)"></div>
                                    Is There a Fee for Preparing This Offer ?
                                </label>
                                <label class="o-radio-container pull-right check-true"> YES
                                    <input name="isFee" class="form-control" mama type="radio" value="1" {{ $data['isFee'] == 1 ? ' checked ': NULL}} >
                                    <i class="fas fa-check"></i>
                                </label>
                                <label class="o-radio-container pull-right check-false"> NO
                                    <input name="isFee" class="form-control" type="radio" value="0" {{ $data['isFee'] == 0 ? ' checked ': NULL }}>
                                    <span><i class="fas fa-times"></i></span>
                                </label>
                            </div>
                            <div class="form-group with-label col-md-12 col-sm-12 col-xs-12">
                                <label class="label-control long_label">
                                    <div class="blue-ball" style="background-image: url(http://localhost/OfferToClose/public/images/short-icon/blue-ball.png)"></div>
                                    Is Fee Paid?
                                </label>
                                <label class="o-radio-container height-30 pull-right check-true"> YES
                                    <input name="isPaid" class="form-control" mama type="radio" value="1" {{ $data['isPaid'] == 1 ? ' checked ': NULL }}>
                                    <i class="fas fa-check"></i>
                                </label>
                                <label class="o-radio-container height-30 pull-right check-false"> NO
                                    <input name="isPaid" class="form-control" type="radio" value="0" {{ $data['isPaid'] == 0 ? ' checked ': NULL }}>
                                    <span><i class="fas fa-times"></i></span>
                                </label>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="fee_amount">Fee Amount</label>
                                <div style="position: relative;">
                                    <span class="currency" style="position: absolute;">$</span>
                                    <input class="form-control currency_format" name="Fee" placeholder="0.00" value="@if(old('Fee')){{old('Fee')}}@elseif($data['Fee']){{$data['Fee']}}@endif">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="invoice_number">Invoice Number</label>
                                <input class="form-control" name="Invoices_ID" placeholder="705" value="@if(old('Invoices_ID')){{old('Invoices_ID')}}@elseif($data['Invoices_ID']){{$data['Invoices_ID']}}@endif">
                            </div>
                        </div>
                    </div>
<?php
    }
?>
                <!--

                        END FEE SEGMENT

                    -->




                    <div class="offer_page_row clearfix padding_20">
                        <div class="col-sm-12 padding_20" style="padding:0 20px;">
                            <div class="col-sm-12 padding_20">
                                <div class="offer_page_title" style="text-transform: uppercase;">
                                    <div style="padding-left: 0; text-decoration: underline;">Offers and Counter Offers</div>
                                    <ul id="uploaded_offers_and_counter_offers" style="list-style: square;">
                                    @foreach($uploads['offers'] as $offer)
                                        <li><span>Offer: <a href="{{\App\Library\otc\_Locations::url('public',"_uploaded_documents/offers/$data->ID/offer/$offer")}}" target="_blank">{{ $offer }}</a></span></li>
                                    @endforeach
                                    @foreach($uploads['counterOffers'] as $counterOffer)
                                        <li><span>Counter: <a href="{{\App\Library\otc\_Locations::url('public',"_uploaded_documents/offers/$data->ID/counter_offer/$counterOffer")}}" target="_blank">{{ $counterOffer }}</a></span></li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Offers/Counter Offers End here-->


                    <div class="offer_page_button_box clearfix">
                        <div class="col-sm-6 offer_page_title ">
                            <button type="button" id="upload-offer-button" class="btn offer_page_green_button"><img src="{{asset('/images/icons/icon-cloud.png')}}" style="text-transform: uppercase;"> Upload Offer</button>
                        </div>
                        <div class="col-sm-6 offer_page_title">
                            <button type="button" id="upload-counter-offer-button" class="btn offer_page_red_button"><img src="{{asset('/images/icons/icon-cloud.png')}}" style="text-transform: uppercase;"> Upload Counter Offer</button>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 offer_page_save_icon_box with_padding">
                        <button type="submit" class="transparent_button" name="saveoffer">
                            <img class="offer_page_save_icon" src="{{asset('/images/icons/offer-save-icon.png')}}"> <span class="offer_page_save_title" style="text-transform: uppercase;">Save</span>
                        </button>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 offer_page_save_icon_box with_padding">
                        <button type="button" data-toggle="modal" data-target="#transaction-offer-modal" class="transparent_button convert">
                            <img class="offer_page_save_icon" src="{{asset('/images/icons/offer-save-icon.png')}}"> <span class="offer_page_save_title" style="text-transform: uppercase;">Convert to Transaction</span>
                        </button>
                    </div>
                </div>
            </div>

            <!-- Make Transaction Modal -->
            <div class="modal" id="transaction-offer-modal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">{!! config('otc.modalClose') !!}</button>
                            <div class="col-sm-6 offer_page_title offer_page_left_text" ><h4>Additional Details</h4></div>
                        </div>
                        <div class="modal-body">
                            <div class="col-sm-6 offer_page_title offer_page_left_text" >What date was the offer accepted?</div>
                                <input type="date" class="form-control form-date" name="AcceptedDate" value="@if(old('AcceptedDate')){{old('AcceptedDate')}}@elseif($data['AcceptedDate']){{$data['AcceptedDate']}}@else {{date('Y-m-d')}} @endif">
                            <label class="error_input" for="AcceptedDate">{{$errors->first('AcceptedDate')}}</label>

                            <div class="col-sm-6 offer_page_title offer_page_left_text" >Length of Escrow</div>
                                <input class="form-control" name="EscrowLength" placeholder="45" value="@if(old('EscrowLength')){{old('EscrowLength')}}@elseif($data['EscrowLength']){{$data['EscrowLength']}}@else 45 @endif">
                            <label class="error_input" for="EscrowLength">{{$errors->first('EscrowLength')}}</label>
                        </div>
                        <div class="modal-footer">
                            <!-- The Transaction Button -->
                            <!-- the name of the button is makeTransaction which is sent to the saveOffer method in OfferController -->
                            <button type="submit" class="transparent_button" name="makeTransaction" value="true">
                                <img class="offer_page_save_icon" src="{{asset('/images/icons/offer-save-icon.png')}}"> <span class="offer_page_save_title" style="text-transform: uppercase;">Convert to Transaction</span>
                            </button>
                            <!-- End the Transaction Button -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Transaction Modal -->

            <!-- Modal -->
            <div class="modal fade" id="upload-offer-modal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">{!! config('otc.modalClose') !!}</button>
                            <h4 class="modal-title">Upload Offer</h4>
                        </div>
                        <div class="modal-body">
                            <input type="file" id="offer">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default btn-teal" name="upload_offer" disabled>Upload</button>
                            <button type="button" class="btn btn-default btn-red" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="upload-counter-modal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">{!! config('otc.modalClose') !!}</button>
                            <h4 class="modal-title">Upload Counter Offer</h4>
                        </div>
                        <div class="modal-body">
                            <input type="file" id="counter_offer">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default btn-teal" name="upload_counter_offer" disabled>Upload</button>
                            <button type="button" class="btn btn-default btn-red" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div style="clear: both"></div>
    <script>
        var offerID = '{{$data['ID'] ?? NULL}}';
        var date = new Date();
        var dateString = date.getFullYear() + "" + (date.getMonth()+1) + "" + date.getDate() + "-" + date.getHours() + date.getMinutes() + date.getSeconds();
        var counterOfferViewRoute = '{{\App\Library\otc\_Locations::url('public',"_uploaded_documents/offers/:oID/counter_offer/:counterOfferFileName")}}';
        var offerViewRoute = '{{\App\Library\otc\_Locations::url('public',"_uploaded_documents/offers/:oID/offer/:offerFileName")}}';
        var placeSearch, autocomplete;

        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            postal_code: 'short_name'
        };

        let streetNumberPlusRoute = '';

        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('street_address'), {types: ['geocode']});
            autocomplete.setFields('address_components');
            autocomplete.addListener('place_changed', fillInAddress);
        }

        function fillInAddress() {
            streetNumberPlusRoute = '';
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            /*
            for (var component in componentForm) {
                if(component !== 'street_number' || component !== 'route')
                {
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = false;
                }
            }
            */
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if(addressType !== 'street_number' && addressType !== 'route')
                {
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(addressType).value = val;
                    }
                }
                else
                {
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        streetNumberPlusRoute += i === 0 ? val + ' ': val;
                    }
                }
                document.getElementById('street_address').value = streetNumberPlusRoute;
            }
        }

        function formatFileName(filename)
        {
            filename = filename.split('.');
            return filenameString = filename[0]+'+'+dateString+'.'+filename[1];
        }

        $(document).ready(function (){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#upload-counter-offer-button').click(function(){
                 $('#upload-counter-modal').modal();
            });

            $('#upload-offer-button').click(function(){
                $('#upload-offer-modal').modal();
            });

            $('#upload-offer-modal #offer, #upload-counter-modal #counter_offer').change(
                function(){
                    let parentModal = $(this).parents('.modal').attr('id');
                    let submitButton = $('#'+parentModal+' button[type="submit"]');
                    if ($(this).val()) submitButton.prop('disabled',false);
                    else submitButton.prop('disabled',true);
                }
            );

            $(document).keypress(function (event) {
                if(event.keyCode === 13)
                {
                    event.preventDefault();
                }
            });

            var url       = "{{ route('offer.checkProperty') }}";
            //$('.street,.city,.zip').keyup(keyup);
            $('.convert').click(function()
            {
                var street = $('.street').val();
                var city = $('.city').val();
                var zip = $('.zip').val();
                if($('.street').val() != '' && $('.city').val() != '' && $('.zip').val() != '')
                {
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {street:street,city:city,zip:zip},
                            error: function(response){
                                $('.warning').addClass('alert alert-danger');
                                $('.alert-danger-h4').html('The address you entered is not valid');

                                $('#transaction-offer-modal').modal('hide');
                                $('.modal-backdrop').remove();
                                $('.street').focus();
                            },
                            success: function(response) {
                            },
                            timeout: 2000,
                        }); //end of ajax
                } //end of if statement
            }); //end of keyup function

            //Currency Format
            $('input.currency_format').keyup(function(event)
            {
                // skip for arrow keys
                if(event.which >= 37 && event.which <= 40) return;
                // format number
                $(this).val(currencyFormat($(this).val()));
            });

            $(document).on("click","button[name=upload_offer]",function(){
                var file_name = document.getElementById("offer").files[0].name;
                var form_data = new FormData();
                form_data.append("file", document.getElementById('offer').files[0]);
                form_data.append("offerID",offerID);
                form_data.append("time",dateString);
                url = "{{ route('offer.uploadOffer') }}";
                $.ajax({
                    type:'POST',
                    url:url,
                    data:form_data,
                    dataType:'json',
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if(response.saved === 'failed')
                        {
                            if(response.offerExists === false)
                            {
                                swal({
                                    title: "Unable to Upload",
                                    text: "You may only upload a file once the offer has been saved.",
                                    icon: "error",
                                    type: "error",
                                    buttons:false,
                                    timer: 3000
                                });
                            }
                        }
                        else
                        {
                            swal({
                                title: "Success!",
                                text: "File Uploaded Successfully",
                                icon: "success",
                                type: "success",
                                showConfirmButton:false,
                                timer: 2000
                            });
                            offerViewRoute = offerViewRoute.replace(':oID', offerID);
                            offerViewRoute = offerViewRoute.replace(':offerFileName',formatFileName(file_name));
                            $("#uploaded_offers_and_counter_offers").append("<li><span>Offer: <a target='_blank' href='"+offerViewRoute+"'>"+formatFileName(file_name)+"</a></span></li>");
                            $('#upload-offer-modal').modal('hide');
                        }



                    },
                    error: function(response) {
                        swal("Oops...", "Something went wrong!", "error", {timer: 3000});
                    }
                });
                return false;
            });

            $(document).on("click","button[name=upload_counter_offer]",function(){
                var file_name = document.getElementById("counter_offer").files[0].name;
                var form_data = new FormData();
                form_data.append("file", document.getElementById('counter_offer').files[0]);
                form_data.append("offerID",offerID);
                form_data.append("time",dateString);
                url = "{{ route('offer.uploadCounterOffer') }}";
                $.ajax({
                    type:'POST',
                    url:url,
                    data:form_data,
                    dataType:'json',
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if(response.saved === 'failed')
                        {
                            if(response.offerExists === false)
                            {
                                swal({
                                    title: "Unable to Upload",
                                    text: "You may only upload a file once the offer has been saved.",
                                    icon: "error",
                                    type: "error",
                                    buttons:false,
                                    timer: 3000
                                });
                            }
                        }
                        else
                        {
                            swal({
                                title: "Success!",
                                text: "File Uploaded Successfully",
                                icon: "success",
                                type: "success",
                                showConfirmButton:false,
                                timer: 2000
                            });
                            counterOfferViewRoute = counterOfferViewRoute.replace(':oID', offerID);
                            counterOfferViewRoute = counterOfferViewRoute.replace(':counterOfferFileName',formatFileName(file_name));
                            $("#uploaded_offers_and_counter_offers").append("<li><span>Counter: <a target='_blank' href='"+counterOfferViewRoute+"'>"+formatFileName(file_name)+"</a></span></li>");
                            $('#upload-counter-modal').modal('hide');
                        }


                    },
                    error: function(response) {
                        swal("Oops...", "Something went wrong!", "error", {timer: 3000});
                    }
                });
                return false;
            });

            $(document).on('submit', '#offer_form', function(){
                var data = $(this).serialize();
            });
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4J1w4-TUMxlpNYl_NZDl6_Dg2cPhrKS8&libraries=places&callback=initAutocomplete"
            async defer>

    </script>
@endsection