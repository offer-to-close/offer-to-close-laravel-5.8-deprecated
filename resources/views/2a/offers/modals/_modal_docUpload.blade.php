@section('custom_css')
  <style>
    .modal-content.dim:after{
        content: "";
        position: absolute;
        top:0;
        left:0;
        right:0;
        bottom:0;
        display: block;
        z-index: 1;
        background:rgba(0, 0, 0, 0.7);
    }
    .spinner{
        display: none;
        font-size: 80px;
        color: #ffff;
        position: absolute;
        left: 47%;
        top: 50%;
        z-index: 6;
        transform: translate(-50%, -50%);
      }
      .errors {
          display: none;
          text-align: left;
          font-size: 12px;
          color: red;
      }
      label{
        font-weight: 600;
        font-size: 14px;
      }
      .form-group{
        margin-top: 25px;
      }
      .radio-button{
        margin-left: 25px;
      }
    [type="radio"]:checked, [type="radio"]:not(:checked) {
        position: absolute;
        left: -9999px;
    }
    [type="radio"]:checked + label, [type="radio"]:not(:checked) + label
    {
        position: relative;
        padding-left: 28px;
        cursor: pointer;
        line-height: 20px;
        display: inline-block;
        color: #666;
    }
    [type="radio"]:checked + label:before, [type="radio"]:not(:checked) + label:before {
        content: '';
        position: absolute;
        left: 0;
        top: 0;
        width: 18px;
        height: 18px;
        border: 1px solid #ddd;
        border-radius: 100%;
        background: #fff;
    }
    [type="radio"]:checked + label:after, [type="radio"]:not(:checked) + label:after {
        content: '';
        width: 12px;
        height: 12px;
        background: #01bdb7;
        position: absolute;
        top: 3px;
        left: 3px;
        border-radius: 100%;
        -webkit-transition: all 0.2s ease;
        transition: all 0.2s ease;
    }
    [type="radio"]:not(:checked) + label:after {
        opacity: 0;
        -webkit-transform: scale(0);
        transform: scale(0);
    }
    [type="radio"]:checked + label:after {
        opacity: 1;
        -webkit-transform: scale(1);
        transform: scale(1);
    }
</style>
@append

<div id="doc-upload" class="modal fade doc-upload" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close">{!! config('otc.modalClose') !!}</button>
        <div class="spinner"><i class="fas fa-spinner fa-spin"></i></div>
        <h4 class="modal-title"><i style="color: #cd6564" class="fas fa-cloud-upload-alt"></i>&nbsp;&nbsp;Upload Document</h4>
      </div>
      <div class="modal-body">
        <form action="upload.php" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="ID">
             <div class="form-group" style="margin-top: 20px; margin-bottom: 20px; padding-left: 10px;">
              <input type="radio" id="offer1" CHECKED name="UploadType" value="1">
              <label for="offer1" style="margin-right: 20px; color: #7878c1">Offer</label>

              <input type="radio" id="offer2" CHECKED name="UploadType" value="2">
              <label for="offer2" style="color: #cd6564">Counter Offer</label>
            </div>

            <div class="form-group " style="margin-top: 40px; margin-bottom: 40px;">
             <label for="">Select a file to upload:</label>
              <input type="file" name="offerFile" class="form-control" id="fileToUpload">
               <span class=" col-md-12 text-center errors text-left" style="display: none;"></span>
            </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-default" id="submit_button">UPLOAD FILE</button>
          </div>
         </form>
      </div>
    </div>
  </div>
</div>
@section('scripts')
    <script>
      $(document).ready(function(){
          $('#doc-upload form').submit( function(e){
              e.preventDefault();
              var el = $(this);
              var modal = $('#doc-upload');
              modal.find('.modal-content').addClass('dim');
              spinner= modal.find('.o-spinner');
              formData = new FormData($(this)[0]);
              spinner.show();
              url = "{{ route('offer.uploadDocument') }}";
              $.ajax({
                        type: "POST",
                        url:   url,
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(response) {
                            if(response == 'Success'){
                              swal("Success", "File Uploaded Successfully", "success", {timer: 4000});
                            }
                            modal.find('.modal-content').removeClass('dim');
                            spinner.hide();
                            el.trigger('reset');
                            $('.errors').hide();
                        },
                        error: function(response) {
                            swal("Oops...", "Something went wrong!", "error", {timer: 3000});
                            for(x in response['responseJSON']['errors'])
                            {
                                el = modal.find('input[name = "'+ x +'"]');
                                el.siblings('.errors').text(response['responseJSON']['errors'][x]).show();
                            }
                            modal.find('.modal-content').removeClass('dim');
                            spinner.hide();
                            CHANGES_MADE = false;
                        }
              });
              modal.find('.modal-content').removeClass('dim');
              spinner.hide();
              CHANGES_MADE = false;
          });

          $('#doc-upload .close').click( function() {
             modal = $('#doc-upload');
             form  = modal.find('form');
             form.trigger('reset');
             modal.modal('toggle');
             $('.errors').hide();
          });

      });
    </script>
@append
