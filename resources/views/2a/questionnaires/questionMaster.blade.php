@extends('2a.layouts.master')
<?php
$answeredSize = $answers['AnsweredSize'];
$size = $answers['Size'];
$lk_ID = $answers[0]->lk_ID;
/*
 * Passed in disclosure ID, questions, and transaction ID.
 */
$answers = array();

?>
@section('custom_css')
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/disclosures/radio.css') }}" rel="stylesheet">

@endsection


<style>
    .questionnaireContainer{
        background: white !important;
        padding: 40px;
        min-height: 500px;
    }
    .list-buttons{
        position: absolute;
        bottom: 0px;
        min-height: 100px;
    }

    .row{
        margin-left:5% !important;
    }

    .signatureContainer{
        background: #f8f8f8;
        padding: 20px;
        min-height: 250px;
    }

    .signatureOptions{
        text-align: center;
    }

    .sigOption{
        background: white;
        border-radius: 3px;
        border: solid 1px #dedede;
        color: #6f6f6f;
        padding: 10px;
    }

    .sigOption:hover{
        background: #00e2da;
        color: white;
        transition: .4s;
        cursor: pointer;
    }

    #sigCanvas{
        width: 100%;
        height: 225px;
        padding-top: 1px;
        padding-bottom: 1px;
    }
</style>
@section('content')
    <section class="main-details dashboard clearfix">
        <div id="closemodal" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Exit Questionnaire</h5>
                    </div>
                    <div class="modal-body">
                        <p>Your changes to the current question have not been saved. All your other changes have been saved, you can continue where you left off next time!</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-teal" data-dismiss="modal">Resume</button>
                        <button id="exit-disclosure" type="button" class="btn btn-teal">Exit</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="wrap">
            <div class="top">
                <!-- todo temporary styling on the div below-->
                <div class="container-fluid" style="height:200px;width: 100%;">
                    <div class="tb-wrap">
                        <div class="tb-layout">
                            <div class="tb-item left">

                            </div>
                            <div class="tb-item right">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container-fluid">
                    <!-- MAIN TAB -->
                    <div id="timeline" class="tab-content">
                        <div class="tb-wrap">
                            <div class="tb-layout">
                                <div class="tb-item center">
                                    <div class="container">
                                        <div class="questionnaire-top">
                                            <div class="modal-header" style="border:none; background: white;">
                                                <button type="button" class="close"><span>{!! config('otc.modalClose') !!}</span></button>
                                            </div>
                                        </div>
                                        <div class="questionnaireContainer container" style="width: 100%;">
                                            <div></div> <!-- this div is here for functionality do not remove -->
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>

        var token="{{Session::token()}}";
        var questionnairesID = '{{$questionnairesID}}';
        var transactionsID = '{{$transactionsID}}';
        var flag = true;
        var totalSize = '{{$size}}';
        var title = '{{$title}}';
        var answeredSize = null;
        var isAnswered = false;
        var questionsLeft = null;
        var questionsLeftPercent = null;
        var isComplete = false;
        var theKey = '{{$key}}';

        $(document).ready(function(){
            if(theKey >= 0)
            {
                drawQuestionnaire(theKey,false,false,true);
            }
            else
            {
                drawQuestionnaire();
            }
        });

        function update(DQ_ID,lk_ID,addInfo,answer)
        {
            if($.isArray(answer)) if(answer.length == 0) answer = false;
            if(answer || addInfo)
            {
                $.ajax({
                    url : '{{route('update.questionnaireAnswer')}}',
                    type: "post",
                    tryCount : 0,
                    retryLimit : 3,
                    async: false,
                    data: {
                        'lk_ID':lk_ID,
                        'DQ_ID':DQ_ID,
                        "addInfo":addInfo,
                        "answer":answer,
                        "_token":token
                    },
                    success: function(data){
                        //this is empty because there is no usage for the call back at the moment.
                    },
                    error : function(xhr, textStatus, errorThrown ) {
                        if (textStatus == 'timeout') {
                            this.tryCount++;
                            if (this.tryCount <= this.retryLimit) {
                                $.ajax(this);
                                return;
                            }
                            return;
                        }
                    },
                    timeout: 0
                });
            }
        }


        function drawQuestionnaire(key = 0, moveToUnanswered = true, doNotDraw = false, mustDraw = false) {
            Swal2({
               title: 'Loading...',
               onBeforeOpen: function () {
                   Swal2.showLoading();
               }
            });
            $.ajax({
                url : "{{route('get.questionnaireQuestions')}}",
                type: "post",
                tryCount : 0,
                retryLimit : 3,
                data: {'questionnairesID':questionnairesID,'transactionsID':transactionsID, "_token":token},
                success: function(data){
                    Swal2.close();
                    allquestions = data.questions;
                    answeredSize = data.answerMetaData['AnsweredSize'];
                    size = allquestions.length;
                    questionsLeft = size - answeredSize;
                    questionsPercent = ((answeredSize/size)*100);
                    //if the questionnaire is fully answered
                    if(moveToUnanswered && questionsLeft == 0) mustDraw = false;
                    if(questionsLeft == 0 && mustDraw === false)
                    {
                        if(allquestions[key])update(null,allquestions[key]['ID'],'DateCompleted',null);
                        if(theKey >= 0)
                        {
                            window.location.href = "{{route('review.questionnaire', ['transactionID' => $transactionsID, 'lk_ID' => $lk_ID])}}";
                        }
                        else
                        {
                            window.location.href = "{{route('review.questionnaire', ['transactionID' => $transactionsID, 'lk_ID' => $lk_ID])}}";
                        }
                        doNotDraw = true;
                    }
                    var section = allquestions[key].Section;
                    if(!section) section = "";
                    eaquestions = data.displays_and_values;
                    var thisquestion = eaquestions[key];
                    if(doNotDraw)
                    {
                        return;
                    }

                    var item_html_post =
                        '<div class="questionContainer">\n' +
                        '<div class="disclosure-title">'+title+'</div>\n' +
                        '    <div class="question-section">'+section+'</div>\n' +
                        '    <div class="question-text"><span class="">'+allquestions[key]['Question']+'</span><span class="day"></span></div>\n' +
                        '        <div class="description">\n' +
                        '            <div class="answers answer-section" id="">\n' +
                        '            </div>\n' +
                        '            <div class="list-buttons row" data-id="">\n' +
                        '                <button class="prev-question update-button col-md-2 col-lg-2 col-sm-3 ladda-button" data-style="expand-right" data-id="'+key+'"><span class="ladda-label"><span class="angle-icon"><i class="fas fa-angle-left"></i></span> Previous</span></button>\n' +
                        '                <div class="col-md-3 col-lg-3"></div>\n'+
                        '                <div class="col-md-6 col-lg-6 col-sm-6"><div class="questionAmt">'+questionsLeft+' of '+totalSize+' Possible Questions Left</div><div class="progress"><div class="progress-bar" role="progressbar" style="width: '+questionsPercent+'%;" aria-valuemin="0" aria-valuemax="100"></div></div></div>\n'+
                        '                <div class="col-md-3 col-lg-3"></div>\n'+
                        '                <button class="next-question update-button btn btn-teal col-md-2 col-lg-2 col-sm-3 ladda-button" data-style="expand-left" data-id="'+key+'"><span class="ladda-label">Continue <span class="angle-icon"><i class="fas fa-angle-right"></i></span></span></button>\n' +
                        '                <button class="next-unanswered-question update-button btn btn-red col-md-2 col-lg-2 col-sm-3 ladda-button" data-style="slide-down" data-id="'+key+'"><span class="ladda-label">Next Unanswered Question</span></button>' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '    </div>\n' +
                        '</div>';

                    if(questionsLeft !== 0 && theKey >= 0)
                    {
                        window.location.href = "{{route('answer.questionnaire', ['questionnaireID' => $questionnairesID, 'transactionID' => $transactionsID])}}";
                    }
                    if(moveToUnanswered)
                    {
                        let unansweredKey = searchArrayOfObjectsForFirstValue(allquestions,'Answer',null);
                        drawQuestionnaire(unansweredKey,false,false,true);
                    }
                    else
                    {
                        $(".questionnaireContainer").children().replaceWith(item_html_post);
                        $('.update-button').data({
                            "lk_ID":allquestions[key]['ID'],
                            "DQ_ID":allquestions[key]['QuestionnaireQuestions_ID'],
                            "addInfo":$('.additionalInfo').val(),
                            "questionsLeft":questionsLeft
                        });
                        if(allquestions[key]['Hint'])
                        {
                            info_html = '<div class="question-info" data-id="'+key+'"><div class="hint-title">What does this mean?</div></div>';
                            $(info_html).insertAfter(".question-text");
                        }
                        questionTypeDisplay(allquestions[key],thisquestion);
                    }
                },
                error : function(xhr, textStatus, errorThrown ) {
                    if (textStatus == 'timeout') {
                        this.tryCount++;
                        if (this.tryCount <= this.retryLimit) {
                            $.ajax(this);
                            return;
                        }
                        return;
                    }
                },
                timeout: 0,
                statusCode: {
                    500: function () {
                        location.reload();
                    }
                }
            });
        } //end of draw questionnaire


        $(document).on('click', '.answer-choice', function()
        {
            var type = $(this).data("type");
            if($(this).data('status') === true)
            {
                var about   = $(this).data("about");
                var val     = $(this).data("val");
                html_text = '<textarea id="addInfo-'+val+'" class="additionalInfo" rows="2" cols="50" placeholder="Explanation (if applicable)"></textarea>';
                if(type === 'radio')
                {
                    var element = $('.additionalInfo');
                    if(element.length <= 0)
                    {
                        $('.answer-section').append(html_text);
                    }
                    else
                    {
                        element.replaceWith(html_text);
                    }
                }
                if(type === 'checkbox')
                {
                    var elementID = $('#addInfo-'+val);
                    if($('.additionalInfoSection textarea').length <= $(this).data("amt"))
                    {
                        if(elementID.length === 0)
                        {
                            $('.checkbox-group').append(html_text);
                        }
                        else
                        {
                            elementID.toggle();
                        }
                    }
                }
            }
        });

        $(document).on('click','.update-button',function () {
            let laddaButton = Ladda.create(this);
            laddaButton.start();
        });


        $(document).on('click', '.next-question, .next-unanswered-question', function(){
            var self = $(this);
            var key = $(this).data('id');
            DQ_ID = self.data("DQ_ID");
            lk_ID = self.data("lk_ID");
            var questionsLeft = self.data("questionsLeft");
            var infoElement = $('.additionalInfo');
            if(infoElement.length > 0)
            {
                addInfo = {}; //must be an associative array
                $('.additionalInfo').each(function (i) {
                    var string = $(this).attr('id');
                    var index = string.replace('addInfo-','');
                    if(index === undefined)
                    {
                        addInfo = 0;
                    }
                    else
                    {
                        addInfo[index] = $(this).val();
                    }
                });
            }
            else addInfo = null;
            type = self.data("type");
            var answer = filterType(type);
            update(DQ_ID,lk_ID,addInfo, answer);
            if(key !== size - 1)
            {
                var nextKey = key+1;
                let unansweredKey = searchArrayOfObjectsForFirstValue(allquestions,'Answer',null);
                if(self.hasClass('next-unanswered-question')) drawQuestionnaire(unansweredKey, true);
                else drawQuestionnaire(nextKey,false);
            }
            else
            {
                let unansweredKey = searchArrayOfObjectsForFirstValue(allquestions,'Answer',null);
                drawQuestionnaire(unansweredKey,true,false,true);
            }
        });


        $(document).on('click', '.prev-question', function(){
            var self = $(this);
            var id = $(this).data('id');
            DQ_ID = self.data("DQ_ID");
            lk_ID = self.data("lk_ID");
            type = self.data("type");
            var answer = filterType(type);
            //update(DQ_ID,lk_ID, answer); //Uncomment this to make back button save
            if(id !== 0)
            {
                var prevKey = id-1;
                drawQuestionnaire(prevKey,false);
            }
        });

        $(document).on('click', '.close', function(){
            var self = $(this);
            $('#closemodal').modal();
        });

        $(document).on('click', '#exit-disclosure', function(){
            var exitURL = '{{route("transactionSummary.disclosures", ['transactionID' => ':tid'])}}';
            exitURL = exitURL.replace(':tid', transactionsID);
            window.location.href = exitURL;
        });


        $(document).on('click', '.question-info', function(){
            var self = $(this);
            var key = $(this).data('id');
            var replace_html = '<div class="question-info">'+allquestions[key]['Hint']+'</div>';
            self.replaceWith(replace_html);
        });

    </script>

@endsection