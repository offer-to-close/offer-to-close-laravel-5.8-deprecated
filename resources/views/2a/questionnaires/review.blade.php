@extends('2a.layouts.master')
<?php


/*
 * Passed in questionnaire ID, link table ID, transaction ID, and answers.
 */

$answers = $answers->sortBy('DisplayOrder'); //sort the collection based on display order
unset($answers['AnsweredSize']);
unset($answers['Size']);
foreach ($answers as $key => $a) if($a->Answer == NULL) unset($answers[$key]);
$answers = $answers->values(); //re-index the collection
?>
@section('custom_css')
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/disclosures/radio.css') }}" rel="stylesheet">



@endsection

<style>
    .row{
        margin-left:5% !important;
    }
    .q-a{
        padding: 10px;
        word-wrap: normal;
    }
    .q-a:hover{
        cursor: pointer;
        border: solid 1px #dfdfdf;
        background: {{\Illuminate\Support\Facades\Config::get('otc.teal')}};
        color: white;
    }

    .question-number{
        font-weight: bold;
    }

    .q-a:hover .question-number{
        color: white;
    }

    .q-a:hover .answer{
        color: white;
        opacity: .8;
    }

    .q-a:hover .question-text{
        color: white;
        opacity: .8;
    }

    .answer{
        font-style: italic;
        font-weight: bold;
        font-size: 15px;
        color: black;
        text-align: left;
    }

    .answer-display{
        text-decoration: underline;
    }

    .answer-elaboration{
        text-decoration: none;
    }

    .edit{
        color: white;
        font-size: 20px;
        text-align: center;
    }
</style>
@section('content')
    @if(Session::has('fail'))
        @include('2a.partials._failureMessage', [
            '_Message' => Session::get('fail'),
        ])
    @endif
    <section class="main-details dashboard clearfix">
        <div class="wrap">
            <div class="top">
                <!-- todo temporary styling on the div below-->
                <div class="container-fluid" style="height:200px;width: 100%;">
                    <div class="tb-wrap">
                        <div class="tb-layout">
                            <div class="tb-item left">

                            </div>
                            <div class="tb-item right">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom">
                <div class="container-fluid">
                    <div>
                        <button>
                            <a href="{{route('transactionSummary.disclosures', ['transactionID' => $transactionID])}}">
                                Return to Disclosures
                            </a>
                        </button>
                    </div>
                    <div>
                        <button>
                            <a href="{{route('review.questionnaire', [
                            'transactionID' => $transactionID,
                            'lk_ID'         => $lk_ID,
                            'returnJSON'    => TRUE,
                            'returnHTML'    => TRUE ])}}">
                                Print or View this Document
                            </a>
                        </button>
                    </div>
                    <div>
                        <button id="sign" type="button">
                            <a href="{{route('review.questionnaire', [
                            'transactionID' => $transactionID,
                            'lk_ID'         => $lk_ID,
                            'returnJSON'    => TRUE,
                            'returnHTML'    => TRUE,
                            'sign'          => TRUE])}}">
                                Sign
                            </a>
                        </button>
                    </div>
                    <!-- MAIN TAB -->
                    <div id="timeline" class="tab-content">
                        <div class="tb-wrap">
                            <div class="tb-layout">
                                <div class="tb-item center">
                                    <div class="review-information"></div>
                                    <div class="questionnaireContainer container">
                                        <?php

                                            for ($i = 0; $i < count($answers); $i++)
                                                {
                                                    try
                                                        {
                                                            $display = \App\Combine\QuestionnaireCombine::lookupCommonAnswerValues($answers[$i]->Answer);
                                                            $notes =  \App\Combine\QuestionnaireCombine::parseNotes($answers[$i]->Notes);
                                                            if(!$display) $display = $answers[$i]->Answer ?? NULL; //if there is no lookup, its a text answer
                                                            if($display)
                                                                {
                                                        ?>
                                                        <div class="q-a" id="q-{{$i}}">
                                                            <div class="question-number">{{$answers[$i]->DisplayOrder}}</div>
                                                            <div class="question-text" style="font-size: 20px; margin-top: 0 !important;">{{$answers[$i]->Question}}</div>
                                                            <div class="answer">
                                                                Answer:
                                                                <span class="answer-display">{{$display}}</span><br>
                                                                <span class="answer-elaboration">{{$notes[$answers[$i]->Answer] ?? NULL}}</span>
                                                            </div>
                                                            <div class="edit">Edit</div>
                                                        </div>
                                                        <?php
                                                                } //end if
                                                        }catch(Exception $e)
                                                        {

                                                        }
                                                } //end for loop

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>
        $('.q-a').click(function(){
            var id = $(this).prop('id');
            id = id.split('-');
            let type = 'disclosure'; //change this to get other types of questionnaires
            var key = id[1];
            var editURL = '{{route("editanswer.questionnaire", ['questionnaireID' => ':did', 'transactionID' => ':tid', 'type' => ':type','key' => ':k'])}}';
            editURL = editURL.replace(':did' , {{$questionnaireID}});
            editURL = editURL.replace(':tid' , {{$transactionID}});
            editURL = editURL.replace(':k' , key);
            editURL = editURL.replace(':type', type);

            window.location.href = editURL;
        });
    </script>


@endsection