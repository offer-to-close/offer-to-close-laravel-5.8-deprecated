@extends('2a.emails.layouts.master')
@section('content')
    @component('2a.emails.emailTemplateComponents.greeting')
        @slot('greeting')
            {{$data['u'] ? 'Hello'.$data['u'].',':'Hello,'}}
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Thank you for registering for Offer To Close – a transaction management solution with a
            mission to make home buying simple and transparent. We have built an easy-to-use
            transaction management software powered not only by technology but also by the best
            transaction coordinators. We are also working on some proprietary mobile applications
            to further improve the experience of real estate agents, home buyers, and home sellers.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            If you are interested in hearing more about ways Offer To Close can help you,
            please email (
            @include('2a.emails.emailTemplateComponents.link', [
                'href' => 'mailto:support@offertoclose.com',
                'display' => 'support@offertoclose.com',
            ])
            )or call us at <strong>@include('2a.emails.emailTemplateComponents.link',[
                'href' => 'tel:8336333782',
                'display' => '1-(833)-OFFER-TC'
            ])</strong>
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => 'The Offer To Close Team'
    ])
@endsection