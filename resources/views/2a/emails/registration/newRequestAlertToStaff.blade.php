@extends('2a.emails.layouts.master')
<?php
$role = $data['RequestedRole'];
$role = title_case(str_replace('_', ' ', \App\Models\lu_UserRoles::getDisplay($role)));
$states = config('otc.states');
$state = $states[$data['State']] ?? null;
?>
@section('content')
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => title_case($data['NameFirst'] .' '. $data['NameLast']).' has requested membership',
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Here are the details:
        <strong><br>'.$data['NameFirst'].' '.$data['NameLast'].'<br>'.$data['Email'].'</strong>
        <br><br>Transaction: '.($data['TransactionID'] == 0     ? '-' : $data['TransactionID']).'
        <br>Role:        '.(empty($role)                    ? '-' : $role).'
        <br>Company:     '.(empty($data['Company'])         ? '-' : $data['Company']).'
        <br>State:       '.(empty($state)                   ? '-' : $state).'
        <br>Address:     '.(empty($data['PropertyAddress']) ? '-' : $data['PropertyAddress']).'
        <br>Reason:      '.(empty($data['ReasonRequested']) ? '-' : $data['ReasonRequested'])
    ])
@endsection
