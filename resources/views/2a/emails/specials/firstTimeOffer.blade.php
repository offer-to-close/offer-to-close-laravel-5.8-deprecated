@extends('2a.emails.layouts.master')
@section('content')
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            <strong>Subject: </strong>{{$data['subject'] ?? 'First Transaction Special'}}<br>
            <strong>Email: </strong>{{$data['Email' ?? 'Undefined']}}
        @endslot
    @endcomponent
@endsection