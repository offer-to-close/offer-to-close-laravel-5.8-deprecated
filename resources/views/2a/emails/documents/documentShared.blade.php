@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['Name'] ? 'Hi '.$data['Name'] : 'Hi,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            A version of <b><i>{{$data['DocumentName']}}</i></b> has been shared with you {{ isset($data['SharingUser']) ? 'by '.$data['SharingUser'] : '' }}.
            <br><br>To view the new document please click @include('2a.emails.emailTemplateComponents.link', [
                'href' => route('openDocumentBag', ['transactionID' => $data['TransactionID'], 'documentCode' => $data['DocumentCode']]),
                'display' => 'here'
            ]).
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            If you have any questions
            @if(isset($data['SharingUser']))
                you may reply to {{$data['SharingUser']}} at {{$data['replyTo']}} or
            @endif
            contact us at @include('2a.emails.emailTemplateComponents.link', [
                'href' => 'https://offertoclose.com',
                'display' => 'Offer To Close',
            ]).
        @endslot
    @endcomponent
@endsection
