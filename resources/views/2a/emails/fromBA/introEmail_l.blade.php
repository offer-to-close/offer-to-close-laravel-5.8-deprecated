
@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['l'] ? 'Hello '.$data['l'].',':'Hello,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            I am the real estate agent for {{$data['b']??'_(Buyer)_'}}. We will be working with you on the
            file for: {{$data['b']??'_(Buyer)_'}} {{$data['p']??'_(Property Address)_'}}
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.unorderedList')
        @slot('ul')
            <ul>
                <li>{{$data['ma']??'_(Acceptance of Offer)_'}} - Offer Accepted</li>
                <li>{{$data['emdDueDate']??'_(Initial Deposit Due Date)_'}} - Earnest Money Deposit Due</li>
                <li>{{$data['disToBDueDate']??'_(Disclosures To Buyer)_'}} - Seller Disclosures Due</li>
                <li>{{$data['crInspectionDueDate']??'_(Inspection Contingency Removal Due Date)_'}} - Inspection Contingency</li>
                <li>{{$data['AppraisalDueDate']??'_(Appraisal Contingency Removal Due Date)_'}} - Appraisal Contingency</li>
                <li>{{$data['crLoanDueDate']??'_(Loan Contingency Removal Due Date)_'}} - Loan Contingency</li>
                <li>{{$data['pCloseEscrow']??'_(Property Close Escrow)_'}} - Close of Escrow</li>
            </ul>
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            <strong>Attached documents:</strong> Purchase & Sale Agreement
            <br><strong>Escrow has been set up with:</strong> {{$data['eCompany']??'_(Escrow Agent Company)_'}}
            <br><strong>Estimated Close Date:</strong> {{$data['pCloseEscrow']??'_(Property Close Escrow)_'}}
            <br><strong>The loan commitment/approval letter is due on:</strong> {{$data['crLoanDueDate']??'_(Loan Contingency Removal Due Date)_'}}
            <br><strong>The appraisal is due on:</strong> {{$data['AppraisalDueDate']??'_(Appraisal Contingency Removal Due Date)_'}}
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            ** Please let us know once the appraisal is scheduled. If the appraiser will need an agent
            to let them in the property, please have them contact the seller’s agent for access.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Seller’s agent's contact info:
            <br>{{$data['sa']??'_(Seller\'s Agent)_'}}
            <br>{{$data['saEmail']??'_(Seller\'s Agent Email)_'}}
            <br>{{$data['saPhone']??'_(Seller\'s Agent Phone)_'}}
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Finally, if you could provide me with a contact for processing/funding, that will help
            me stay on top of things as closing approaches.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Thank you. We look forward to working with you. Please do not hesitate to contact me if you have any questions.
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['ba']??'_(Buyer\'s Agent)_'
    ])
@endsection