@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Please see the attached request for repairs for your review on {{$data['p']??'_(Property Address)_'}}.
            Please let me know if you have any questions regarding the contents.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Thank you,
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['ba'] ?? '_(Buyer\'s Agent)_'
    ])
@endsection