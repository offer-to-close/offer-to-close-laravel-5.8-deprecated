
@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello '.($data['sa'] ?? '_(Seller\'s Agent)_')
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'You are invited to open an account on Offer To Close to service your new escrow!
        My name is '.($data['stc'] ?? '_(Seller\'s Transaction Coordinator)_').' I am a Transaction Coordinator with Offer To Close.
            To facilitate the all scheduling and paperwork for your transaction, please use the following link to register at Offer To Close.
            When you do, you will be automatically attached to the transaction for '.($data['p'] ?? '_(Property Address)_').':'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            @include('2a.emails.emailTemplateComponents.link', [
                'href' => 'https://offertoclose.com/register',
                'display' => 'https://offertoclose.com/register'
            ])
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Looking forward to working with you.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['stc'] ?? '_(Seller\'s Transaction Coordinator)_'
    ])
@endsection