@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello '.($data['ba'] ?? '_(Buyer\'s Agent)_').','
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            You are invited to open an account on Offer To Close to service your new escrow!
            My name is {{$data['stc'] ?? '_(Seller\'s Transaction Coordinator)_'}}. I am a Transaction Coordinator with Offer To Close and I am working with {{$data['sa'] ?? '_(Seller\'s Agent)_'}}.
            I will be your main contact for all scheduling and paperwork for the transaction.
            Please use the following link to register at Offer To Close. When you do, you will be automatically attached to the transaction for {{$data['p'] ?? '_(Property Address)_'}}:
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph',[
        'text' => $data['url'] ?? '_(Registration Link)_'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Please let me know if you are working with a TC as well so I can be sure to provide them with an account as well.
            If you don’t have a TC and would like us to help you with your side of the file, we would welcome the opportunity.
            Looking forward to working with you.
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.paragraph',[
        'text' => 'Thank you,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName',[
        'sender' => $data['stc'] ?? '_(Seller\'s Transaction Coordinator)_'
    ])
@endsection