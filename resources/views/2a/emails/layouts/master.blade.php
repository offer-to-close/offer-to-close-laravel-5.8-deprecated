<html xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if gte mso 9]>
    <style id="ol-styles">
        /* OUTLOOK-SPECIFIC STYLES */
        li {
            text-indent: -1em;
            padding: 0;
            margin: 0;
            line-height: 1.2;
        }
        ul, ol {
            padding: 0;
            margin: 0 0 0 40px;
        }
        p {
            margin: 0;
            padding: 0;
            margin-bottom: 0;
        }
        sup {
            font-size: 85% !important;
        }
        sub {
            font-size: 85% !important;
        }
    </style>
    <![endif]-->
    <style id="template-styles-head" data-premailer="ignore">
        .footer-main-width {
            width: 690px!important;
            max-width: 690px;
        }
        table {
            border-collapse: collapse;
            table-layout: fixed;
        }
        .bgimage {
            table-layout: auto;
        }
        .preheader-container {
            color: transparent;
            display: none;
            font-size: 1px;
            line-height: 1px;
            max-height: 0px;
            max-width: 0px;
            opacity: 0;
            overflow: hidden;
        }
        /* LIST AND p STYLE OVERRIDES */
        .editor-text p {
            margin: 0;
            padding: 0;
            margin-bottom: 0;
        }
        .editor-text ul,
        .editor-text ol {
            padding: 0;
            margin: 0 0 0 40px;
        }
        .editor-text li {
            padding: 0;
            margin: 0;
            line-height: 1.2;
        }
        /* ==================================================
        CLIENT/BROWSER SPECIFIC OVERRIDES
        ================================================== */
        /* IE: correctly scale images with w/h attbs */
        img {
            -ms-interpolation-mode: bicubic;
        }
        /* Text Link Style Reset */
        a {
            text-decoration: underline;
        }
        /* iOS: Autolink styles inherited */
        a[x-apple-data-detectors] {
            text-decoration: underline !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
            color: inherit !important;
        }
        /* FF/Chrome: Smooth font rendering */
        .editor-text, .MainTextFullWidth {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        /* Gmail/Web viewport fix */
        u + .body .template-body {
            width: 690px;
        }
        @media only screen and (max-width:480px) {
            u + .body .template-body {
                width: 100% !important;
            }
        }
        /* Office365/Outlook.com image reset */
        [office365] button, [office365] .divider-base div, [office365] .spacer-base div, [office365] .editor-image div { display: block !important; }
    </style>
    <style>@media only screen and (max-width:480px) {
            table {
                border-collapse: collapse;
            }
            .main-width {
                width: 100% !important;
            }
            .mobile-hidden {
                display: none !important;
            }
            td.OneColumnMobile {
                display: block !important;
            }
            .OneColumnMobile {
                width: 100% !important;
            }
            td.editor-col .editor-text {
                padding-left: 20px !important; padding-right: 20px !important;
            }
            td.editor-col .editor-image.editor-image-hspace-on td {
                padding-left: 20px !important; padding-right: 20px !important;
            }
            td.editor-col .editor-button-container {
                padding-left: 20px !important; padding-right: 20px !important;
            }
            td.editor-col .editor-social td {
                padding-left: 20px !important; padding-right: 20px !important;
            }
            td.editor-col .block-margin {
                padding-left: 20px !important; padding-right: 20px !important;
            }
            td.editor-col td.block-margin .editor-text {
                padding-left: 0px !important; padding-right: 0px !important;
            }
            td.editor-col td.block-margin .editor-image.editor-image-hspace-on td {
                padding-left: 0px !important; padding-right: 0px !important;
            }
            td.editor-col td.block-margin .editor-button-container {
                padding-left: 0px !important; padding-right: 0px !important;
            }
            td.editor-col td.block-margin .editor-social td {
                padding-left: 0px !important; padding-right: 0px !important;
            }
            .editor-button td > table tr > td {
                padding: 0px 0px 0px 0px !important;
            }
            .editor-button td > table tr > td td {
                padding: 9px 15px 10px 15px !important;
            }
            .layout {
                padding: 15px 9px 15px 9px !important;
            }
            .layout-container-border {
                padding: 0px 0px 0px 0px !important;
            }
            .layout-container {
                padding: 0px 0px 0px 0px !important;
            }
            .editor-image img {
                width: auto !important; margin-left: auto !important; margin-right: auto !important;
            }
            .editor-image .image-cell {
                padding-bottom: 15px !important;
            }
            .section-headline-text {
                font-size: 24px !important;
            }
            .headline-text {
                font-size: 24px !important;
            }
            .subheadline-text {
                font-size: 20px !important;
            }
            .feature {
                padding-top: 0px !important; padding-bottom: 0px !important;
            }
            .layout-outer {
                padding: 0px 20px !important;
            }
            .feature-heading-text {
                font-size: 20px !important;
            }
            .feature-text {
                font-size: 16px !important;
            }
            .split.editor-col {
                margin-top: 0px !important;
            }
            .split.editor-col ~ .split.editor-col {
                margin-top: 10px !important;
            }
            .split-layout-margin {
                padding: 0px 20px !important;
            }
            .article {
                padding-top: 0px !important; padding-bottom: 0px !important;
            }
            .article-heading-text {
                font-size: 20px !important;
            }
            .article-text {
                font-size: 16px !important;
            }
            .social-container {
                text-align: center !important;
            }
            .social-text {
                font-size: 14px !important;
            }
            .cpn-heading-text {
                font-size: 28px !important;
            }
            .editor-cpn-heading-text {
                font-size: 28px !important;
            }
            td.col-divided .editor-col {
                border-right: 0px solid #56a6ad !important; border-bottom: 1px solid #56a6ad !important;
            }
            td.col-divided td.editor-col:last-of-type {
                border-bottom: 0 !important;
            }
            .col-divided {
                padding: 0 20px !important;
            }
            td.col-divided .editor-col .editor-text {
                padding-left: 0px !important; padding-right: 0px !important;
            }
            td.col-divided .editor-col .editor-image.editor-image-hspace-on td {
                padding-left: 0px !important; padding-right: 0px !important;
            }
            td.col-divided .editor-col .editor-button-container {
                padding-left: 0px !important; padding-right: 0px !important;
            }
            td.col-divided .editor-col .editor-social td {
                padding-left: 0px !important; padding-right: 0px !important;
            }
            td.col-divided .editor-col .block-margin {
                padding-left: 0px !important; padding-right: 0px !important;
            }
            .sidebar-left-margin {
                padding-left: 20px !important; padding-right: 20px !important;
            }
            .sidebar-right-margin {
                padding-left: 20px !important; padding-right: 20px !important;
            }
            .layout-outer {
                padding-left: 20px !important; padding-right: 20px !important;
            }
            .feature-margin {
                padding-left: 20px !important; padding-right: 20px !important;
            }
            .layout-margin {
                padding-left: 20px !important; padding-right: 20px !important;
            }
            .action-block .poll-answer {
                width: 100% !important; display: block !important;
            }
            .action-block .poll-button {
                width: 100% !important;
            }
            div.MobileFooter {
                font-size: 11px !important;
            }
            td.FooterMobile {
                padding: 0px 10px 0px 10px !important;
            }
            td.MainCenter {
                width: 100% !important;
            }
            table.MainSide {
                display: none !important;
            }
            img.MainSide {
                display: none !important;
            }
            td.MainSide {
                display: none !important;
            }
            .rsvp-button-inner {
                padding: 0px 0px 10px 0px !important;
            }
            .rsvp-button-outer {
                width: 100% !important; max-width: 100% !important;
            }
            .footer-main-width {
                width: 100% !important;
            }
            .footer-mobile-hidden {
                display: none !important;
            }
            .footer-mobile-hidden {
                display: none !important;
            }
            .footer-column {
                display: block !important;
            }
            .footer-mobile-stack {
                display: block !important;
            }
            .footer-mobile-stack-padding {
                padding-top: 3px;
            }
        }
        @media only screen and (max-width:320px) {
            .layout {
                padding: 0px 0px 0px 0px !important;
            }
        }
        @media screen {
            @font-face {
                font-family: 'Roboto'; font-style: normal; font-weight: 400; src: local('Roboto'), local('Roboto-Regular'), url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2) format('woff2'); unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215;
            }
        }

        .li{
            font-size: 14px;
            color: #000 !important;
        }
    </style>
    <title>@yield('title')</title>
</head>
<body class="body" align="center" style="width: 100%; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; background-color: #e6e6e6; margin: 0px; padding: 0px;" bgcolor="#e6e6e6">
<table class="template-body" border="0" cellpadding="0" cellspacing="0" style="text-align: center; min-width: 100%;" width="100%">
    <tbody>
    <tr>
        <td class="preheader-container">
            <div>
                <div id="preheader" style="display: none; font-size: 1px; color: transparent; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;">
                    <span data-entity-ref="preheader">You don't want to miss this.</span>
                </div>
            </div> </td>
    </tr>
    <tr>
        <td class="template-shell-container" align="center">
            <div class="bgcolor" style="background-color: #e6e6e6;">
                <table class="bgimage" width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color: #e6e6e6;" bgcolor="#e6e6e6">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table class="main-width" width="690" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 690px;">
                                <tbody>
                                <tr>
                                    <td class="layout" align="center" valign="top" style="padding: 15px 5px;">
                                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                            <tr>
                                                <td class="layout-container-border" align="center" valign="top" style="background-color: #56a6ad; padding: 0px;" bgcolor="#56a6ad">
                                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="background-color: #56a6ad;" bgcolor="#56a6ad">
                                                        <tbody>
                                                        <tr>
                                                            <td class="layout-container" align="center" valign="top" style="background-color: #ffffff; padding: 0;" bgcolor="#ffffff">
                                                                <div class="">
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="editor-logo editor-col OneColumnMobile" width="100%" align="left" valign="top">
                                                                                <div class="gl-contains-spacer">
                                                                                    <table class="editor-spacer" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="spacer-container" align="center" valign="top">
                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td class="spacer-base" style="padding-bottom: 20px; height: 1px; line-height: 1px;" width="100%" align="center" valign="top">
                                                                                                            <div>
                                                                                                                <img alt="" width="5" height="1" border="0" hspace="0" vspace="0" src="images/otc.gif" style="display: block; height: 1px; width: 5px;">
                                                                                                            </div> </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <div class="gl-contains-image">
                                                                                    <table class="editor-image logo-container editor-image-vspace-on" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td align="center" valign="top" style="padding-top: 10px; padding-bottom: 10px;">
                                                                                                <div class="publish-container">
                                                                                                    <img alt="" class="" style="display: block; height: auto; max-width: 100%;" width="243" border="0" hspace="0" vspace="0" src="http://media.releasewire.com/photos/show/?id=140275&amp;size=large">
                                                                                                </div> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <div class="gl-contains-divider">
                                                                                    <table class="editor-divider" width="100%" cellpadding="0" cellspacing="0" border="0" style="min-width: 100%;">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="" align="center" valign="top">
                                                                                                <table width="100%" class="galileo-ap-content-editor" style="cursor: default; min-width: 100%;">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td class="divider-base divider-solid" width="100%" align="center" valign="top" style="padding: 9px 0;">
                                                                                                            <table cellpadding="0" cellspacing="0" border="0" align="center" style="height: 1px; width: 94%; min-width: 94%;">
                                                                                                                <tbody>
                                                                                                                <tr>
                                                                                                                    <td height="1" align="center" style="border-bottom-style: none; height: 1px; line-height: 1px; padding-bottom: 0px; background-color: #56a6ad;" bgcolor="#56a6ad">
                                                                                                                        <div>
                                                                                                                            <img alt="" width="5" height="1" border="0" hspace="0" vspace="0" src="images/otc.gif" style="display: block; height: 1px; width: 5px;">
                                                                                                                        </div> </td>
                                                                                                                </tr>
                                                                                                                </tbody>
                                                                                                            </table> </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
@yield('content')
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class=" editor-col OneColumnMobile" width="100%" align="" valign="top">
                                                                                <div class="gl-contains-spacer">
                                                                                    <table class="editor-spacer" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="spacer-container" align="center" valign="top">
                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td class="spacer-base" style="padding-bottom: 13px; height: 1px; line-height: 1px;" width="100%" align="center" valign="top">
                                                                                                            <div>
                                                                                                                <img alt="" width="5" height="1" border="0" hspace="0" vspace="0" src="images/otc.gif" style="display: block; height: 1px; width: 5px;">
                                                                                                            </div> </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="divider-container editor-col OneColumnMobile" width="100%" align="left" valign="top">
                                                                                <div class="gl-contains-divider">
                                                                                    <table class="editor-divider" width="100%" cellpadding="0" cellspacing="0" border="0" style="min-width: 100%;">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="divider-container" align="center" valign="top">
                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-content-editor" style="cursor: default; min-width: 100%;">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td class="divider-base divider-solid" width="100%" align="center" valign="top" style="padding: 9px 0px;">
                                                                                                            <table style="width: 100%; min-width: 100%; height: 1px;" cellpadding="0" cellspacing="0" border="0" align="center">
                                                                                                                <tbody>
                                                                                                                <tr>
                                                                                                                    <td height="1" align="center" style="background-color: rgb(214, 214, 214); border-bottom-style: none; height: 1px; line-height: 1px; padding-bottom: 0px;" bgcolor="D6D6D6">
                                                                                                                        <div>
                                                                                                                            <img alt="" width="5" height="1" border="0" hspace="0" vspace="0" src="images/otc.gif" style="display: block; height: 1px; width: 5px;">
                                                                                                                        </div> </td>
                                                                                                                </tr>
                                                                                                                </tbody>
                                                                                                            </table> </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="content editor-col OneColumnMobile" width="100%" align="left" valign="top" style="background-color: rgb(76, 76, 76);" bgcolor="4C4C4C">
                                                                                <div class="gl-contains-text">
                                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="editor-text content-text" align="left" valign="top" style="font-family: Verdana,Geneva,sans-serif; font-size: 14px; color: #2e2f2f; text-align: left; display: block; word-wrap: break-word; line-height: 1.2; padding: 10px 40px;">
                                                                                                <div></div>
                                                                                                <div class="text-container galileo-ap-content-editor">
                                                                                                    <div>
                                                                                                        <div style="text-align: center;" align="center">
                                                                                                            <span style="font-size: 12px; color: rgb(255, 255, 255);">833-OFFER-TC | support@offertoclose.com</span>
                                                                                                        </div>
                                                                                                        <div style="text-align: center;" align="center">
                                                                                                            <span style="font-size: 12px; color: rgb(255, 255, 255); font-weight: bold;">Offer To Close</span>
                                                                                                            <span style="color: rgb(255, 255, 255); font-weight: bold; font-family: Arial, Verdana, Helvetica, sans-serif;">™</span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="galileo-ap-layout-editor" style="min-width: 100%;">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td class="spacer editor-col OneColumnMobile" width="100%" align="left" valign="top">
                                                                                <div class="gl-contains-spacer">
                                                                                    <table class="editor-spacer" width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <td class="spacer-container" align="center" valign="top">
                                                                                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                                                    <tbody>
                                                                                                    <tr>
                                                                                                        <td class="spacer-base" style="padding-bottom: 10px; height: 1px; line-height: 1px;" width="100%" align="center" valign="top">
                                                                                                            <div>
                                                                                                                <img alt="" width="5" height="1" border="0" hspace="0" vspace="0" src="images/otc.gif" style="display: block; height: 1px; width: 5px;">
                                                                                                            </div> </td>
                                                                                                    </tr>
                                                                                                    </tbody>
                                                                                                </table> </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div> </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div> </td>
                                                        </tr>
                                                        </tbody>
                                                    </table> </td>
                                            </tr>
                                            </tbody>
                                        </table> </td>
                                </tr>
                                </tbody>
                            </table> </td>
                    </tr>
                    </tbody>
                </table>
            </div> </td>
    </tr>
    <tr>
        <td></td>
    </tr>
    </tbody>
</table>
</body>
<html>
