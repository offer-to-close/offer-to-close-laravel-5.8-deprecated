@include('2a.emails.common.viewPath')
<?php
$plan       = $data['order-type'];
$address    = $data['address'] ?? false;
if($data['e'] == NULL) $e = false;
else $e = true;
if($data['t'] == NULL) $t = false;
else $t = true;
if($data['l'] == NULL) $l = false;
else $l = true;
if(!$address) return false;
?>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; color: #74787E; height: 100%; hyphens: auto; line-height: 1.4; margin: 0; -moz-hyphens: auto; -ms-word-break: break-all; width: 100% !important; -webkit-hyphens: auto; -webkit-text-size-adjust: none; word-break: break-word;">
<style>
    @media  only screen and (max-width: 600px) {
        .inner-body {
            width: 100% !important;
        }

        .footer {
            width: 100% !important;
        }
    }

    @media  only screen and (max-width: 500px) {
        .button {
            width: 100% !important;
        }
    }



</style>

<table class="wrapper" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #D8283A; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
    <tr>
        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
            <table class="content" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                <tr>
                    <td class="header" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 25px 0; text-align: center;">
                        <a href="http://localhost/OfferToClose" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: white; font-size: 19px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;">
                            Offer To Close
                        </a>
                    </td>
                </tr>




                <!-- Email Body -->
                <tr>
                    <td class="body" width="100%" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; border-bottom: 1px solid #EDEFF2; border-top: 1px solid #EDEFF2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                        <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; margin: 0 auto; padding: 0; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
                            <!-- Body content -->
                            <tr>
                                <td class="content-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"> </p>
                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;"> </p>
                                    <!-- start-of-msg -->
                                    <h2>NHD Request</h2>
                                    <br>
                                    <h3>Property: {{$address}}</h3>
                                    <h3>Plan: {{$plan}}</h3>
                                    @if($data['agent'] != NULL)
                                        <h4>Agent Information</h4>
                                        <p>Name: {{$data['agent'] ?? 'None Provided'}}</p>
                                        <p>Email: {{$data['agentEmail'] ?? 'None Provided'}}</p>
                                        <p>Phone: {{$data['agentPhone'] ?? 'None Provided'}}</p>
                                        <p>Company: {{$data['agentCompany'] ?? 'None Provided'}}</p>
                                    @else
                                        <h4>Agent Information</h4>
                                        <p>None Provided or No Agent</p>
                                    @endif
                                    @if($t)
                                    <h4>Title Agent Information</h4>
                                    <p>Name: {{$data['t'] ?? 'None Provided'}}</p>
                                    <p>Email: {{$data['tEmail' ?? 'None Provided']}}</p>
                                    @endif
                                    @if($l)
                                        <h4>Loan Agent Information</h4>
                                        <p>Name: {{$data['l'] ?? 'None Provided'}}</p>
                                        <p>Email: {{$data['lEmail'] ?? 'None Provided'}}</p>
                                    @endif
                                    @if($e)
                                        <h4>Escrow Agent Information</h4>
                                        <p>Name: {{$data['e'] ?? 'None Provided'}}</p>
                                        <p>Email: {{$data['eEmail'] ?? 'None Provided'}}</p>
                                    @endif
                                    <!-- end-of-msg -->

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0 auto; padding: 0; text-align: center; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
                            <tr>
                                <td class="content-cell" align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: white; font-size: 12px; text-align: center;">© 2018 Offer To Close. All rights reserved.</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
