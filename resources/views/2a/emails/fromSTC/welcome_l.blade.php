
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <p>Hello {{$data['l'] ?? '_(Loan Agent)_'}},</p>
        <p> My name is {{$data['stc'] ?? '_(Seller\'s TC)_'}}. I am a Transaction Coordinator with Offer To
            Close and I am working with {{$data['sa'] ?? '_(Seller\'s Agent)_'}}. We will be working with you
            on the file for:</p>
        <p>{{$data['b'] ?? '_(Buyer)_'}}</p>
        <p>{{$data['p'] ?? '_(Property Address)_'}}</p>

        <p> Attached to this email is the Purchase Agreement which has an estimated close date of
            {{$data['pCloseEscrow'] ?? '_(Property Close Escrow)_'}}. Escrow will be handled by
            {{$data['e'] ?? '_(Escrow Agent)_'}} with {{$data['eCompany'] ?? '_(Escrows Company)_'}}.</p>
        <p> To ensure we are able to help {{$data['b'] ?? '_(Buyer)_'}} successfully close on the purchase
            of their new home:</p>
        <ul>
            <li><strong>The loan commitment/approval letter is due on</strong>:
                {{$data['crLoanDueDate'] ?? '_(Loan Contingency Removal Due Date)_'}}</li>
            <li><strong>The appraisal is due on</strong>:
                {{$data['AppraisalDueDate'] ?? '_(Appraisal Contingency Removal Due Date)_'}}</li>
        </ul>
        <p>By this point you should have received and already approved the following:</p>
        <ol>
            <li>Loan Application</li>
            <li>Employment Verification</li>
            <li>Income Verification (pay-stubs, W-2, Tax Returns, etc.)</li>
            <li>Asset Verification (bank statements, retirement funds)</li>
            <li>Credit Report (credit score, debts, bankruptcy, fraud alerts)</li>
            <li>Preliminary Title Report for the property</li>
            <li>Purchase Contract</li>
            <li>Appraisal (value, issues with property)</li>
        </ol>
        <p>As soon as you have it, please email us the commitment letter and let us know when you have
            scheduled the appraisal.</p>
        <p>If you need to reach {{$data['b'] ?? '_(Buyer)_'}}’s listing agent. I have included their contact
            info below.</p>
        <blockquote>
            {{$data['sa'] ?? '_(Seller\'s Agent)_'}}
            {{$data['saEmail'] ?? '_(Seller\'s Agent Email)_'}}
            {{$data['saPhone'] ?? '_(Seller\'s Agent Phone)_'}}
        </blockquote>
        <p> Finally, if you could provide me with a contact for processing/funding, that will help me stay on
            top of things as closing approaches.</p>
        <p> Thank you. We look forward to working with you. Please do not hesitate to contact me if you have
            any questions.</p>
        <p>Regards,</p>
        <p>{{$data['stc'] ?? '_(Seller\'s TC)_'}}</p>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello '.($data['l'] ?? '_(Loan Agent)_').','
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'My name is '.($data['stc'] ?? '_(Seller\'s TC)_').' I am a Transaction Coordinator with Offer To
            Close and I am working with '.($data['sa'] ?? '_(Seller\'s Agent)_'). ' We will be working with you
            on the file for: '.($data['b'] ?? '_(Buyer)_').' '.($data['p'] ?? '_(Property Address)_')
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Attached to this email is the Purchase Agreement which has an estimated close date of '
            .($data['pCloseEscrow'] ?? '_(Property Close Escrow)_'). ' Escrow will be handled by '
            .($data['e'] ?? '_(Escrow Agent)_').' with '.($data['eCompany'] ?? '_(Escrows Company)_').
            ' To ensure we are able to help '.($data['b'] ?? '_(Buyer)_').' successfully close on the purchase
            of their new home:'
    ])
    @include('2a.emails.emailTemplateComponents.unorderedList', [
        'ul' => '<ul>
            <li><strong>The loan commitment/approval letter is due on </strong>:
                '.($data['crLoanDueDate'] ?? '_(Loan Contingency Removal Due Date)_').'</li>
            <li><strong>The appraisal is due on </strong>:
                '.($data['AppraisalDueDate'] ?? '_(Appraisal Contingency Removal Due Date)_').'</li>
        </ul>'
    ])
    @include('2a.emails.emailTemplateComponents.unorderedList', [
        'ul' => 'By this point you should have received and already approved the following:
        <ol>
            <li>Loan Application</li>
            <li>Employment Verification</li>
            <li>Income Verification (pay-stubs, W-2, Tax Returns, etc.)</li>
            <li>Asset Verification (bank statements, retirement funds)</li>
            <li>Credit Report (credit score, debts, bankruptcy, fraud alerts)</li>
            <li>Preliminary Title Report for the property</li>
            <li>Purchase Contract</li>
            <li>Appraisal (value, issues with property)</li>
        </ol>'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'As soon as you have it, please email us the commitment letter and let us know when you have
            scheduled the appraisal.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'If you need to reach '.($data['b'] ?? '_(Buyer)_').' listing agent. I have included their contact
            info below.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => '<br>
            '.($data['sa'] ?? '_(Seller\'s Agent)_').'<br>'.
            ($data['saEmail'] ?? '_(Seller\'s Agent Email)_').'<br>'.
            ($data['saPhone'] ?? '_(Seller\'s Agent Phone)_')
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Finally, if you could provide me with a contact for processing/funding, that will help me stay on
            top of things as closing approaches. Thank you. We look forward to working with you. Please do not hesitate to contact me if you have
            any questions.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Regards,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['stc'] ?? '_(Seller\'s TC)_'
    ])
@endsection
