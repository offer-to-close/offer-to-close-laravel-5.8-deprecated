
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <p style="font-size: 15px;">
                Hello! I am the transaction coordinator for the seller’s side of the transaction on {{$data['p']??'_(Property Address)_'}}.
                Can you please order a warranty on {{$data['p']??'_(Property Address)_'}}? Contract paperwork is
                attached and escrow info is below. Thank you!!
            </p>
            <p style="font-size: 15px;">
                {{$data['e']??'_(Escrow Agent)_'}}
                {{$data['eEmail']??'_(Escrow Agent Email)_'}}
            </p>
            <p>Thanks,</p><br><br>
            <p>{{$data['stc']??'_(Seller\'s TC)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'I am the transaction coordinator for the seller’s side of the transaction on '.($data['p']??'_(Property Address)_').
                ' Can you please order a warranty on '.($data['p']??'_(Property Address)_').'? Contract paperwork is
                attached and escrow info is below.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => $data['e']??'_(Escrow Agent)_'.' '.$data['eEmail']??'_(Escrow Agent Email)_'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thanks,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['stc']??'_(Seller\'s TC)_'
    ])
@endsection
