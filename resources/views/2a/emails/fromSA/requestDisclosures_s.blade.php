
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <h5 class="font-weight-bold mb-4" style="color: #c14145;">Hello,</h5>
        <div class="row pt-4 px-4 pb-0 ">
            <p> In preparation for the next steps we’ve prepared the disclosures that will need to be
                completed/returned, this is your opportunity to tell the buyer what you know about the
                property, so it is very important to fill them in with as much detailed information as possible.</p><br>
            <p style="font-size: 15px;">
                The disclosures can be
            </p>
            <ul style="list-style: none;">
                <li>
                    1)	Disclosure Information Advisory – This is a form that explains the importance of several
                    disclosures including the Transfer Disclosure Statement and Seller Property Questionnaire.
                    There is also another section that explains which sellers are exempt from filling out specific
                    forms.

                    @if($data['dia_QuestionnaireLink'])
                        <br><br>You can read and sign this disclosure here: <br>
                        <a href="{{$data['dia_QuestionnaireLink']}}">DIA Review</a>
                    @endif
                </li>
                <li>
                    2)	Transfer Disclosure Statement – we will need all YES/NO questions answered to the
                    best of your ability and for any answer that is YES please give an explanation.
                    HOA- since there is an HOA make sure any item regarding HOA, common area, and any
                    other fields are correctly marked.

                    @if($data['tds_QuestionnaireLink'])
                        <br><br>You can complete this form here:<br>
                        <a href="{{$data['tds_QuestionnaireLink']}}">TDS Questionnaire</a>
                    @endif
                </li>
                <li>
                    3)	Seller Property Questionnaire- we will need all YES/NO questions answered to the best
                    of your ability and for any answer that is YES please give an explanation. HOA- since there
                    is an HOA make sure any item regarding HOA, common area, and any other fields are correctly
                    marked.

                    @if($data['spq_QuestionnaireLink'])
                        <br><br>You can complete this form here:<br>
                        <a href="{{$data['spq_QuestionnaireLink']}}">SPQ Questionnaire</a>
                    @endif
                </li>
            </ul>
            <p style="font-size: 15px;">
                Please let us know when you’ll be able to provide those disclosures.
            </p><br>
            <p>Sincerely,</p><br><br>
            <p>{{$data['sa'] ?? '_(Seller\'s Agent)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'In preparation for the next steps we’ve prepared the disclosures that will need to be
                completed/returned, this is your opportunity to tell the buyer what you know about the
                property, so it is very important to fill them in with as much detailed information as possible.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'The disclosures can be:'
    ])
    <?php
    $paragraph = '1)	Disclosure Information Advisory – This is a form that explains the importance of several
                disclosures including the Transfer Disclosure Statement and Seller Property Questionnaire.
                There is also another section that explains which sellers are exempt from filling out specific
                forms.';
        if($data['dia_QuestionnaireLink'])
        {
            $paragraph .= '<br><br>You can read and sign this disclosure here: <br>
            <a href="'.$data['dia_QuestionnaireLink'].'" target="_blank" style="font-size: 14px; color: rgb(76, 76, 76); font-weight: normal; font-family: Roboto, sans-serif; font-style: normal; text-decoration: underline;">DIA Review</a>';
        }

        $paragraph .= '<br><br>2)	Transfer Disclosure Statement – we will need all YES/NO questions answered to the
                    best of your ability and for any answer that is YES please give an explanation.
                    HOA- since there is an HOA make sure any item regarding HOA, common area, and any
                    other fields are correctly marked.';
        if($data['tds_QuestionnaireLink'])
        {
            $paragraph .= '<br><br>You can complete this form here: <br>
                <a href="'.$data['tds_QuestionnaireLink'].'" target="_blank" style="font-size: 14px; color: rgb(76, 76, 76); font-weight: normal; font-family: Roboto, sans-serif; font-style: normal; text-decoration: underline;">TDS Questionnaire</a>';
        }
        $paragraph .= '<br><br>3)	Seller Property Questionnaire- we will need all YES/NO questions answered to the best
                    of your ability and for any answer that is YES please give an explanation. HOA- since there
                    is an HOA make sure any item regarding HOA, common area, and any other fields are correctly
                    marked.';
        if($data['spq_QuestionnaireLink'])
        {
            $paragraph .= '<br><br>You can complete this form here: <br>
                    <a href="'.$data['spq_QuestionnaireLink'].'" target="_blank" style="font-size: 14px; color: rgb(76, 76, 76); font-weight: normal; font-family: Roboto, sans-serif; font-style: normal; text-decoration: underline;">SPQ Questionnaire</a>';
        }
    ?>
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => $paragraph
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Please let us know when you’ll be able to provide those disclosures.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Sincerely,'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['sa'] ?? '_(Seller\'s Agent)_'
    ])
@endsection
