
@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <p style="font-size: 15px;">
                Hello! I am the transaction coordinator for the seller’s side of the transaction on
                {{$data['pStreet']??'_(Property Street Address)_'}}. Can you please tell me the status of
                the warranty purchase on {{$data['p']??'_(Property Address)_'}}?
            </p>
            <p style="font-size: 15px;">
                {{$data['e']??'_(Escrow Agent)_'}}
                {{$data['eEmail']??'_(Escrow Agent Email)_'}}
            </p>
            <p>Thanks you!</p><br><br>
            <p>{{$data['sa']??'_(Seller\'s Agent)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'I am the transaction coordinator for the seller’s side of the transaction on
                '.($data['pStreet']??'_(Property Street Address)_'). ' Can you please tell me the status of
                the warranty purchase on '.($data['p']??'_(Property Address)_').'?'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => ($data['e']??'_(Escrow Agent)_').' '.($data['eEmail']??'_(Escrow Agent Email)_')
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you!'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['sa']??'_(Seller\'s Agent)_'
    ])
@endsection
