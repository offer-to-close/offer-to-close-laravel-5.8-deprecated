@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <h5 class="font-weight-bold mb-4" style="color: #c14145;">Hi,</h5>
            <p style="font-size: 15px;">
                This is a just a quick reminder that as a part of your purchase agreement you have the
                following contingencies:
            </p><br>
            <div>
                <ul>
                    @if((int)$data['crInspectionLength'] > 0) <li>Inspection Contingency</li>@endif
                    @if((int)$data['needsTermiteInspection'] > 0) <li>Termite Contingency</li>@endif
                    @if((int)$data['AppraisalLength'] > 0) <li>Appraisal Contingency</li>@endif
                    @if((int)$data['crLoanLength'] > 0) <li>Loan Contingency</li>@endif
                </ul>
            </div><br>
            <p>Thank you!</p><br><br>
            <p>{{$data['btc'] ?? '_(Buyer\'s TC)_'}}</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting',[
        'greeting' => 'Hi,'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'This is a just a quick reminder that as a part of your purchase agreement you have the
                following contingencies:'
    ]);
    <?php
        $ul = '<ul>';
        if((int)$data['crInspectionLength'] > 0)        $ul .= '<li>Inspection Contingency</li>';
        if((int)$data['needsTermiteInspection'] > 0)    $ul .= '<li>Termite Contingency</li>';
        if((int)$data['AppraisalLength'] > 0)           $ul .= '<li>Appraisal Contingency</li>';
        if((int)$data['crLoanLength'] > 0)              $ul .= '<li>Loan Contingency</li>';
        $ul .= '</ul>';

    ?>
    @include('2a.emails.emailTemplateComponents.unorderedList', [
        'ul' => $ul
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Thank you,'
    ]);
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc'] ?? '_(Buyer\'s TC)_'
    ])
@endsection