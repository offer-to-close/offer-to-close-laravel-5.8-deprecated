@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hi,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            I wanted to check and see if you have received the earnest money deposit from
            {{$data['b']??'_(Buyers)_'}} for {{$data['pStreet']??'_(Property Street Address)_'}}.
            If so, please send a receipt for our records. If not, please let us know and we’ll
            follow-up to see why do not yet have it.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Thank you!
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc'] ?? '_(Buyer\'s TC)_'
    ])
@endsection