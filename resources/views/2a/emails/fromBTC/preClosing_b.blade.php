@extends('2a.emails.layouts.master')
@section('content')
    <!--
    <div class="container my-5 pt-2" style="max-width: 530px;">
        <div class="row pt-4 px-4 pb-0 ">
            <p>Hi {{$data['b'] ?? '_(Buyer)_'}},</p>
            <p>Closing is just around the corner. Here is a list of things that need to be taken care of:</p>
            <p><strong>Utility Transfers</strong></p>
            <p> Now is the time to start contacting the utility companies to have them put into your name. The transfer date
                should be the closing date.</p>
            <p><strong>Final Inspection</strong></p>
            <p> The final verification of property condition before closing is to make sure any agreed upon repair requests have
                been completed, and that the property is in basically the same condition at closing as it was when you made
                your original offer.The final verification of property condition before closing is to make sure any repair
                items requested and agreed upon have been completed, and that the property is in basically the same condition
                at closing as it was when you made your original offer.</p><br>
            <p> Please let your agent know right away if you would like to schedule a final inspection and we will work to
                get that scheduled for you.</p><br>
            <p><strong>Loan Documents and Signing</strong></p>
            <p> Over the next few days, your loan documents should be delivered to escrow. Once the loan documents are received,
                escrow will calculate your final figures and contact you to schedule the appointment. You will want to review
                the settlement statement to verify key info.</p><br>
            <p> Pay specific attention to:</p>
            <ul>
                <li>Correct names and spelling</li>
                <li>Property address</li>
                <li>Closing date</li>
                <li>Sales price</li>
                <li>Earnest money deposit</li>
                <li>Seller credit, if any</li>
            </ul><br>
            <p> With regards to the loan amounts and fees, please refer to the good faith estimate provided to you by your
                loan officer. If you have questions about the fees, your loan officer is the best person to answer
                those questions.</p>
            <p>Feel free to contact us with any questions.</p>
        </div>
    </div>
    -->
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hi '.$data['b'] ?? '_(Buyer)_'.','
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Closing is just around the corner. Here is a list of things that need to be taken care of:'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => '<strong>Utility Transfers</strong>
             Now is the time to start contacting the utility companies to have them put into your name. The transfer date
                should be the closing date.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => '<strong>Final Inspection</strong>
             The final verification of property condition before closing is to make sure any agreed upon repair requests have
                been completed, and that the property is in basically the same condition at closing as it was when you made
                your original offer.The final verification of property condition before closing is to make sure any repair
                items requested and agreed upon have been completed, and that the property is in basically the same condition
                at closing as it was when you made your original offer.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Please let your agent know right away if you would like to schedule a final inspection and we will work to
                get that scheduled for you.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => '<strong>Loan Documents and Signing</strong>
             Over the next few days, your loan documents should be delivered to escrow. Once the loan documents are received,
                escrow will calculate your final figures and contact you to schedule the appointment. You will want to review
                the settlement statement to verify key info.'
    ])
    @include('2a.emails.emailTemplateComponents.unorderedList', [
        'ul' => 'Pay specific attention to:</p>
            <ul>
                <li>Correct names and spelling</li>
                <li>Property address</li>
                <li>Closing date</li>
                <li>Sales price</li>
                <li>Earnest money deposit</li>
                <li>Seller credit, if any</li>
            </ul>'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'With regards to the loan amounts and fees, please refer to the good faith estimate provided to you by your
                loan officer. If you have questions about the fees, your loan officer is the best person to answer
                those questions.'
    ])
    @include('2a.emails.emailTemplateComponents.paragraph', [
        'text' => 'Feel free to contact us with any questions.'
    ])
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc']??'_(Buyer\'s TC)_'
    ])
@endsection