@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['u'] ? 'Hello '.$data['u'].',':'Hello,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            I just wanted to pop in and introduce myself as the Transaction Coordinator for {{$data['ba']??'_(Buyer\'s Agent)_'}}.
            I will be your main contact for all scheduling and paperwork for the transaction.
            Please send ALL paperwork via email directly to us. We look forward to a smooth transaction.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            I am also including key dates and deadlines below. I hope you find it helpful.
            Please let me know if you calculate any of the key dates differently.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.unorderedList')
        @slot('ul')
            <ul>
                <li>{{$data['ma']??'_(Acceptance of Offer)_'}} - Offer Accepted</li>
                <li>{{$data['emdDueDate']??'_(Initial Deposit Due Date)_'}} - Earnest Money Deposit Due</li>
                <li>{{$data['disToBDueDate']??'_(Disclosures To Buyer)_'}} - Seller Disclosures Due</li>
                <li>{{$data['crInspectionDueDate']??'_(Inspection Contingency Removal Due Date)_'}} - Inspection Contingency</li>
                <li>{{$data['AppraisalDueDate']??'_(Appraisal Contingency Removal Due Date)_'}} - Appraisal Contingency</li>
                <li>{{$data['crLoanDueDate']??'_(Loan Contingency Removal Due Date)_'}} - Loan Contingency</li>
                <li>{{$data['pCloseEscrow']??'_(Property Close Escrow)_'}} - Close of Escrow</li>
            </ul>
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Thank You,
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc'] ?? '_(Buyer\'s TC)_'
    ])
@endsection