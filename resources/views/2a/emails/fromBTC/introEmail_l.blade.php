@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => $data['l'] ? 'Hello '.$data['l'].',':'Hello,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            I am the Transaction Coordinator for the buyer's agent. We will be working with you on the
            file for: {{$data['b']??'_(Buyer)_'}} {{$data['pStreet']??'_(Property Street Address)_'}}
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            <b>Attached documents:</b> Purchase & Sale Agreement
            <br><b>Escrow has been set up with:</b> {{$data['eCompany']??'_(Escrow Agent Company)_'}}
            <br><b>Estimated Close Date:</b> {{$data['pCloseEscrow']??'_(Property Close Escrow)_'}}
            <br><b>The loan commitment/approval letter is due on:</b> {{$data['crLoanDueDate']??'_(Loan Contingency Removal Due Date)_'}}
            <br><b>The appraisal is due on:</b> {{$data['AppraisalDueDate']??'_(Appraisal Contingency Removal Due Date)_'}}
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            ** Please let us know once the appraisal is scheduled. If the appraiser will need an agent
            to let them in the property, please have them contact the seller’s agent for access.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Seller’s agent's contact info:
            <br>{{$data['sa']??'_(Seller\'s Agent)_'}}
            <br>{{$data['saEmail']??'_(Seller\'s Agent Email)_'}}
            <br>{{$data['saPhone']??'_(Seller\'s Agent Phone)_'}}
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Finally, if you could provide me with a contact for processing/funding, that will help
            me stay on top of things as closing approaches.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Thank you. We look forward to working with you. Please do not hesitate to contact me if you have any questions.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Regards,
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc']??'_(Buyer\'s TC)_'
    ])
@endsection