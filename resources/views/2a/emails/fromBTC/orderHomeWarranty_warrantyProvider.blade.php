@extends('2a.emails.layouts.master')
@section('content')
    @include('2a.emails.emailTemplateComponents.greeting', [
        'greeting' => 'Hello,'
    ])
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            I am the transaction coordinator for the buyer’s side of the transaction on
            {{$data['pStreet']??'_(Property Street Address)_'}}.
            Can you please order a warranty on {{$data['p']??'_(Property Address)_'}}? Contract paperwork is
            attached and escrow info is below.
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            {{$data['e']??'_(Escrow Agent)_'}}
            {{$data['eEmail']??'_(Escrow Agent Email)_'}}
        @endslot
    @endcomponent
    @component('2a.emails.emailTemplateComponents.paragraph')
        @slot('text')
            Thanks,
        @endslot
    @endcomponent
    @include('2a.emails.emailTemplateComponents.senderName', [
        'sender' => $data['btc']??'_(Buyer\'s TC)_'
    ])
@endsection