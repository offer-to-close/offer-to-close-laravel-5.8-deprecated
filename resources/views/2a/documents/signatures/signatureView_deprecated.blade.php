@extends('2a.layouts.master')
@section('content')
    <div>
        @if(isset($url))
            <div id="docuSignContainer">
                <iframe id="docuSign" src="{{$url}}" height="1000" width="90%"></iframe>
            </div>
        @endif
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        /*
            Kludge if you want to use iframe and then redirect
            add this to iframe: onload="redirectToLanding(this.contentWindow.location)"
         */
        /*
        function redirectToLanding(location)
        {
            if(window.location.href = location)
            {
                $('#docuSignContainer').remove();
            }
        }
        */

    </script>
@endsection