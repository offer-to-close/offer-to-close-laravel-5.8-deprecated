<!DOCTYPE html>
<html>
<head>
    <title>TRANSACTION COORDINATOR SERVICE AGREEMENT</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .wrapper{
            width: 1000px;
            margin: 0 auto;
            margin-top: 20px;
            margin-bottom: 20px;
            padding: 0 20px;
        }

        .wrapper h2{
            text-align: center;
        }

        .wrapper p{
            font-size: 22px;
            line-height: 28px;
        }

        .wrapper input{
            border: none;
            border-bottom: 1px solid black;
            font-size: 18px;
            margin: 0 2px;
            padding-left: 4px;
            font-weight: bold;
        }

        .wrapper input:focus{
            outline: none;
        }

        .wrapper li{
            font-size: 22px;
            line-height: 28px;
        }

        .wrapper #parrent-list li{
            margin-bottom: 20px;
            padding-left: 15px;
            margin-left: 30px;
        }

        .wrapper #parrent-list .child-list li{
            margin-bottom: 10px;
            padding-left: 8px;
            margin-left: 10px;
            list-style-type: a;
        }

        .wrapper #parrent-list .child-list .grand-child-list li{
            margin-bottom: 10px;
            padding-left: 8px;
            margin-left: 5px;
        }

        /*----------------- Wrapper-2 CSS---------------------*/

        .wrapper-2 #parrent-list li{
            margin-bottom: 20px;
            padding-left: 15px;
            margin-left: 30px;
            line-height: 30px;
        }

        .wrapper-2 #parrent-list .child-list .grand-child-list li{
            margin-bottom: 10px;
            margin-top: 10px;
            padding-left: 8px;
            margin-left: 5px;
            line-height: 30px;
        }

        .wrapper table td{
            padding-right: 150px;
            padding-bottom: 80px;
            font-size: 20px;
        }

        .wrapper table span{
            font-weight: bold;
            font-size: 20px;
            line-height: 32px;
        }
    </style>
    <?php
        //dd($data);
    $buyersAgent    = $data['BuyersAgent']  ?? '*Not Defined.*';
    $sellersAgent   = $data['SellersAgent'] ?? '*Not Defined.*';
    $buyersAgentBrokerage   = $data['RealEstateBrokerBuyer'] ?? '*Not Defined.*';
    $sellersAgentBrokerage  = $data['RealEstateBrokerSeller'] ?? '*Not Defined.*';
    $tcName = auth()->user()->name ?? auth()->user()->NameFirst.' '.auth()->user()->NameLast;
    $side = $data['UserSides'][0] ?? NULL;
    $services = explode('|',$data['OtherServices']->Answer);
    $additionalAgreements = explode('|', $data['AdditionalAgreementTerms']->Answer);
    foreach ($services as $key => $service)
    {
        $sp = explode('`',$service);
        $s = $sp[0];
        $p = $sp[1];
        $services[$key] = [
            'Service' => $s,
            'Price'   => $p,
        ];
    }
    if($side)
    {
        if($side == 'b')
        {
            $agent = $buyersAgent->NameFull ?? $buyersAgent->NameFirst.' '.$buyersAgent->NameLast;
            $brokerageName = $buyersAgentBrokerage;
        }
        elseif($side == 's')
        {
            $agent = $sellersAgent->NameFull ?? $sellersAgent->NameFirst.' '.$sellersAgent->NameLast;
            $brokerageName = $sellersAgentBrokerage;
        }
        else
        {
            $agent = 'Not Defined.';
            $brokerageName = 'Not Defined.';
        }
    }
    ?>
</head>
<body>
<div class="wrapper">
    <h2>TRANSACTION COORDINATOR SERVICE AGREEMENT</h2>

    <p>
        This Transaction Coordinator Service Agreement (the “Agreement”) is made and effective as of {{date('F d, Y')}} (“Effective Date”) by and between Home By Home, Inc., which is also known as Offer To Close, a Delaware Corporation (the “Contractor”) and {!! $agent !!} , a {!! $brokerageName !!} (the “Agent”). The Contractor and the Agent may be referred to individually as a “Party” or collectively as the “Parties.”
    </p>

    <h2>RECITALS</h2>

    <p>
        <b>WHEREAS</b> the Agent wishes to engage the Contractor as a service provider to perform certain Work (as defined below) for the Agent and, when applicable, Agent’s clients as described in Exhibit A hereto and on the terms and conditions set forth below; and
    </p>
    <p>
        <b>WHEREAS,</b> each Party is duly authorized and capable of entering into this Agreement.
    </p>
    <p>
        <b>NOW THEREFORE,</b> in consideration of the above recitals and the mutual promises and benefits contained herein, the Parties hereby agree as follows:
    </p>

    <ol id="parrent-list">
        <li>
            <b>SCOPE OF WORK.</b> The scope of work outlined by Contractor is expressly limited to those of a Transaction Coordinator and are not intended to supplement, replace or otherwise augment the duties of the Agent pursuant to California Civil Code §§2079 et seq., or California Business and Professions Code Division 4 Chapter 3. Furthermore, Contractor agrees to perform all work as defined in Exhibit A, unless specifically excluded by written agreement with Agent or Agent’s broker.
        </li>
        <li>
            <b>CONTRACT PRICE.</b> The Agent shall pay to the Contractor, as full payment for the Work and materials to be provided under this Agreement, the sum of ${{$data['RateInquiry']->Answer ?? '(Not Defined)'}} (the “Contract Price”) with a surcharge of ${{$data['ExtraCharge']->Answer ?? 100}} for Dual Agency transactions.
        </li>
        <li>
            <b>ADDITIONAL OPTIONAL SERVICES.</b> From time to time, Agent may wish to use Contractor to help with the following services:
            <ol class="child-list" type="a">
                <li>Preparing Residential Purchase Agreements (e.g. offers) – $35</li>
                <li>Input listing details into Agent’s multiple listing service (for agent to approve) – $40</li>
                <li>Attend appointments, including inspections, showings, and others, in lieu of Agent – To Be Determined (based on location of appointment)</li>
                <li>Sending printed copy or flash drive of completed broker file – $30</li>
                @foreach($services as $service)
                    <li>{!! $service['Service'] !!} - ${!! $service['Price'] !!}</li>
                @endforeach
            </ol>
        </li>
        <li>
            <b>REPRESENTATIONS AND WARRANTIES.</b>
            <ol class="child-list" type="a">
                <li>
                    Both parties represent and warrant as follows:
                    <ol class="grand-child-list" type="i">
                        <li><b>Duly Authorized.</b> Each party is duly authorized and capable of entering into this Agreement.</li>
                    </ol>
                </li>
                <?php
                    $continueRepsAndWarr = $data['PreventAgentClientClause']->Answer == 'yes' ? 3:2;
                ?>
                @if($continueRepsAndWarr == 3)
                    <li>
                        The Contractor hereby represents and warrants as follows:
                        <ol class="grand-child-list" type="i">
                            <li><b>Non-Solicitation of Clients.</b> During the period starting on the Effective Date and ending twenty-four months (24 months) after the termination or expiration of this agreement (the "Non-Solicitation Period"), Contractor will not directly or indirectly, on its own behalf or in the service or on behalf of others, in any capacity attempt to solicit, the business of any customer of Agent without express written permission to the contrary.</li>
                        </ol>
                    </li>
                @endif
            </ol>
        </li>
    </ol>

    <div style="margin-top: 30px;">
        <p style="font-style: italic; color: #808080; font-size: 16px; line-height: 0;">Transaction Coordinator Service Agreement <span style="float: right; font-style: normal; font-size: 20px;">1</span></p>
    </div>
</div>
<div class="wrapper wrapper-2" style="padding: 30px 20px 0; ">
    <ol id="parrent-list" start="5">
        @if($data['PreventAgentSolicitingEmployees']->Answer == 'yes' || $data['RequireAgentCarryErrors']->Answer == 'yes')
        <ol class="child-list" type="a" start="{{$continueRepsAndWarr}}" style="margin-left: 50px;">
            <li>
                The Agent hereby represents and warrants as follows:
                <ol class="grand-child-list" type="i">
                    @if($data['PreventAgentSolicitingEmployees']->Answer == 'yes')
                        <li><b>Non-Solicitation of Employees.</b> During the period starting on the Effective Date and ending twenty-four months (24 months) after the termination or expiration of this agreement (the "Non-Solicitation Period"), Agent will not directly or indirectly, on its own behalf or in the service or on behalf of others, in any capacity induce or attempt to induce any officer, director, or employee to leave the other party.</li>
                    @endif
                    @if($data['RequireAgentCarryErrors']->Answer == 'yes')
                        <li><b>Errors and Omissions Insurance.</b> The Agent shall, either directly or through their Broker, maintain an errors and omissions insurance policy, in form and substance reasonably acceptable (including applicable coverage), of the type customarily in force by entities engaged, with at all times a principal face amount of no less than ${{$data['HowMuchInsurance']->Answer ?? 1000000}} under which, with respect to the Agent (or any Agent that is an Affiliate thereof), the Trustee, for the benefit of the Secured Parties, will be named as the loss payee and additional insured. The initial Agent shall, no less than annually, provide written evidence of policy renewal and payment of premiums.</li>
                    @endif
                </ol>
            </li>
        </ol>
        @endif
        <li>
            <b>INDEMNIFICATION.</b> Broker and Agent are responsible for acquiring the appropriate signatures of all parties involved in the transaction and returning the documentation to Contractor. Agent agrees that it will abide by any and all state and federal laws as applicable to this type of transaction and that they are duly licensed by the appropriate entities. Agent further agrees to release Contractor from any and all liabilities that may arise by virtue of this real estate transaction and broker further agrees to indemnify, defend and hold Contractor harmless from all claims, disputes, litigation, judgments, and attorney fees that may arise in connection with this transaction and also from any incorrect information supplied by third parties to Contractor, or from any material facts that third parties know but fail to disclose to Contractor.
        </li>
        <li>
            <b>TIME IS OF THE ESSENCE.</b> Time is of the essence of this Agreement; provided, however, that notwithstanding anything to the contrary in this Agreement, two days prior to the close of escrow, Agent and Agent’s Clients must provide all paperwork to Contractor, otherwise, Agent accepts any and all liability and commit to paying Contractor in the event the failure to perform causes the buyer or seller to cancel the agreement. Additionally, should Agent’s Broker refuse to pay Agent as a result of an incomplete file, where agent failed to provide necessary documents in a timely and complete manner, Agent will be responsible for directly paying Contractor their full fee.
        </li>
        <li>
            <b>TERMINATION.</b> This Agreement may be terminated by either Party on 30 days’ written notice to the other Party, with or without cause. In the event of termination, the Company shall promptly pay the Contractor for services rendered before the effective date of the termination including full payment of fees for any files the Contractor has performed work on as though they had closed.
        </li>
        <li>
            <b>ADDITIONAL AGREEMENT TERMS.</b> Agent and Contractor agree to the following terms:
            <ol class="grand-child-list" type="a">
                @foreach($additionalAgreements as $a)
                    <li>{!!  $a !!}</li>
                @endforeach
            </ol>
        </li>
        <li>
            <b>ASSIGNMENT.</b> The rights and the duties of the Contractor under this Agreement are personal, and may not be assigned or delegated without the prior written consent of the Agent. The Agent may assign its rights and duties under this Agreement with the prior written consent of the Contractor.
        </li>
    </ol>

    <div style="margin-top: 50px;">
        <p style="font-style: italic; color: #808080; font-size: 16px; line-height: 0;">Transaction Coordinator Service Agreement <span style="float: right; font-style: normal; font-size: 20px;">2</span></p>
    </div>
</div>
<div class="wrapper" style="padding-top: 30px;">
    <ol id="parrent-list" start="10">
        <li>
            <b>NO IMPLIED WAIVER.</b> The failure of either Party to insist on strict performance of any covenant or obligation under this Agreement, regardless of the length of time for which such failure continues, shall not be deemed a waiver of such Party’s right to demand strict compliance in the future. No consent or waiver, express or implied, to or of any breach or default in the performance of any obligation under this Agreement shall constitute a consent or waiver to or of any other breach or default in the performance of the same or any other obligation.
        </li>
        <li>
            <b>NATURE OF RELATIONSHIP.</b> The Contractor is not an employee of the Agent; the Contractor is working in its capacity as an independent contractor. The Contractor agrees to hold the Agent harmless and indemnify the Agent for any claims, including (but not limited to) liability insurance, workers’ compensation, and tax withholding for the Contractor’s employees.
        </li>
        <li>
            <b>GOVERNING LAW.</b> This agreement shall be governed, construed, and enforced in accordance with the laws of the State of California, without regard to its conflict of laws rules.
        </li>
        <li>
            <b>COUNTERPARTS/ELECTRONIC SIGNATURES.</b> This Agreement may be executed in one or more counterparts, each of which shall be deemed an original but all of which shall constitute one and the same instrument. For purposes of this Agreement, use of a facsimile, e-mail, or other electronic medium shall have the same force and effect as an original signature.
        </li>
        <li>
            <b>ENTIRE AGREEMENT.</b> This Agreement, constitutes the final, complete, and exclusive statement of the agreement of the Parties with respect to the subject matter hereof, and supersedes any and all other prior and contemporaneous agreements and understandings, both written and oral, between the Parties. If any part of this agreement is declared unenforceable or invalid, the remainder will continue to be valid and enforceable.
        </li>
    </ol>
    <p>
        No amendment, addendum, change, or modification of this Agreement shall be valid unless in writing and signed by both Parties. Headings used in this Agreement are provided for convenience only and shall not be used to construe meaning or intent.
    </p>
    <p>
        <b>IN WITNESS WHEREOF,</b> the Parties have executed this Agreement as of the date first above written.
    </p>

    <table>
        <tr>
            <td><b>AGENT</b></td>
            <td style="padding-top: 50px;">
                <span>By:</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" style="width: 300px;"><br>
                <span>Name:</span> &nbsp;<input value="{!! $agent !!}" type="text" style="width: 300px;"><br>
            </td>
        </tr>
        <tr>
            <td><b>CONTRACTOR</b></td>
            <td style="padding-top: 50px;">
                <span>By:</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" style="width: 300px;"><br>
                <span>Name:</span> &nbsp;<input value="{!! $tcName !!}" type="text" style="width: 300px;"><br>
            </td>
        </tr>
    </table>

    <div style="margin-top: 10px; margin-bottom: 10px;">
        <p style="font-style: italic; color: #808080; font-size: 16px; line-height: 0;">Transaction Coordinator Service Agreement <span style="float: right; font-style: normal; font-size: 20px;">3</span></p>
    </div>
</div>

<div class="wrapper" style="margin-bottom: 0; padding-top: 30px;">
    <h2>EXHIBIT A <br>SCOPE OF WORK</h2>

    <ul style="margin: 30px 130px 830px;">
        <li>Thoroughly review documents to ensure each document is fully executed</li>
        <li>Coordinate the opening of escrow and confirm receipt of earnest money deposit</li>
        <li>Ensure seller disclosures are provided or received within contract timelines</li>
        <li>Request required Escrow documents and communicate with Escrow</li>
        <li>Order home warranty and NHD reports</li>
        <li>Ensure that Escrow is in receipt of commission instructions and, as applicable, home warranty invoice</li>
        <li>Track timelines for purchase agreement</li>
        <li>Maintain regular contact with all parties involved to report status updates</li>
        <li>Review the file prior to closing, to ensure completeness</li>
        <li>Fully complete and deliver a digital copy of the completed file to broker, agent and client</li>
        <li>Schedule inspections</li>
    </ul>

    <div style="margin-top: 30px;">
        <p style="font-style: italic; color: #808080; font-size: 16px; line-height: 0;">Transaction Coordinator Service Agreement <span style="float: right; font-style: normal; font-size: 20px;">4</span></p>
    </div>
</div>

</body>
</html>