<!-- #### {{\App\Library\Utilities\_LaravelTools::pageContext($view_name)}} #### -->
<?php
  $isAdmin = \App\Http\Controllers\AccessController::hasAccess('a');
  if ($isAdmin && session('isAdmin')) $isAdminLoggedIn = true;
  else $isAdminLoggedIn = false;
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PK9VQPK');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name='ir-site-verification-token' value='246367830' />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Transaction Coordinator Service from Offer To Close</title>
    <meta name="keywords" content="Transaction Coordinators, TC, real estate assistant, offer to close, offertoclose.com, Get a TC advantage" />
    <meta name="description" content="Offer To Close is a transaction coordinator service and platform that helps to simplify the process of buying and selling real estate by making it more transparent and driven by our proprietary technology. Each member of our team of transaction coordinators are either licensed by the California Bureau of Real Estate as a real estate salesperson or have been certified by the California Association of Realtors as transaction coordinators." />

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('LaddaBootstrap/dist/ladda-themeless.css')}}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app_2a_SASS.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/lightcase.css')}}">
    <link href="{{ asset('css/notification.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' rel="stylesheet">
    <link rel="icon" href="{{asset('images/favicon-32x32.png')}}" sizes="32x32" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">


@yield('custom_css')
    <!-- Scripts -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script type="text/javascript" src="https://s3.amazonaws.com/eversign-embedded-js-library/eversign.embedded.latest.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/upload/core.js') }}"></script>
    <script src="{{ asset('js/upload/upload.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('js/questionTypes/questionType.js') }}"></script>
    <script src="{{ asset('js/notification.js') }}"></script>
    <script src="{{ asset('js/ajaxSearch/ajaxSearch.js') }}"></script>
    <script src="{{asset('js/Utilities/utilities.js')}}"></script>
    <script src="{{asset('js/Utilities/modals.js')}}"></script>
    <script src="{{asset('js/Utilities/functionIterator.js')}}"></script>
    <script src="{{asset('LaddaBootstrap/dist/spin.js')}}"></script>
    <script src="{{asset('LaddaBootstrap/dist/ladda.js')}}"></script>
</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PK9VQPK"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div  id="body-wrapper">
    <header id="header">
        <div class="container-fluid">
            @if(Session::has('error'))
                <script>genericError('{{Session::get('error')}}')</script>
            @elseif(Session::has('success'))
                <script>genericSuccess('{{Session::get('success')}}')</script>
            @elseif(Session::has('info'))
                <script>genericInfo('{{Session::get('info')}}','{{Session::get('infoTitle') ?? NULL}}')</script>
            @elseif(Session::has('warning'))
                <script>genericWarning('{{Session::get('warning')}}','{{Session::get('confirmButtonText') ?? NULL}}','{{Session::get('warningTitle') ?? NULL}}')</script>
            @endif
            <div class="row">
                <div class="col-xs-12">
                    <div class="header-wrapper">
                        <div class="header-items">
                            <div class="item">
                                <div class="logo-wrapper">
                                    <a class="logo" href="{{ route(config('otc.DefaultRoute.dashboard')) }}"><img src="{{ asset('images/logo.png') }}"></a>
                                </div>
                            </div>

<?php
     $dspColor = '#ffffff';
     if (!isServerLive()) $dspColor = config('otc.orange');

                            $userID = \App\Http\Controllers\CredentialController::current()->ID();
?>

<?php if (!isServerLive()) { ?>
                            <div class="item">
                                <div class="logo-wrapper">
                                    <span style="color: {{config('otc.orange')}};">{{ date('D, M j, Y g:i a') }}</span>
                                </div>
                            </div>

                            <div class="item">
                                <div class="logo-wrapper">
                                    <span style="color: {{config('otc.orange')}}">Server: {{ env('APP_ENV') }}</span>
                                </div>
                            </div>
<?php } ?>

                            <div class="item">
                                <div class="logo-wrapper">
                                    <span style="color: {{$dspColor}}">Release: {{ \App\Models\lu_UserTypes::getDisplay(session('userType')) }}</span>
                                </div>
                            </div>

                            <div class="item">
                                <div class="settings-links-wrapper">
                                    <div class="settings-links">

                                        @if(\App\Http\Controllers\AccessController::hasAccess('s'))
<?php
// ... Contact Request Logic ...
                                            $requestCount = \App\Combine\AccountCombine::contactRequestCount();
                                            if ($requestCount > 0)
                                            {
                                                $icon = '<span style="color:' . config('otc.color.teal') . ';" ><i class="far fa-envelope"></i> ' . $requestCount . '</span>';
                                            }
                                            else $icon = false;
?>
                                            @if($icon)
                                                @include('2a.layouts._contactRequestDisplay')
                                            @endif
                                        @endif

                                        @if(\App\Http\Controllers\AccessController::hasAccess('s'))
<?php
// ... Membership Request Logic ...
                                            $requestCount = \App\Combine\AccountCombine::memberRequestCount();
                                            if ($requestCount > 0)
                                            {
                                                $icon = '<span style="color:' . config('otc.color.teal') . ';" ><i class="fas fa-hand-paper"></i> ' . $requestCount . '</span>';
                                            }
                                            else $icon = false;
?>
                                            @if($icon)
                                                @include('2a.layouts._requestDisplay')
                                            @endif
                                        @endif

<?php
// ... Alert Logic ...
                                        $userController = new \App\Http\Controllers\UserController();
                                        $alerts = $userController->getAlertsByUserId();
                                        if (!is_array($alerts)) return $alerts;  // Kludge - if the return is not an array it is because within the previous method a redirect was attempted.
                                        $alertCount = count($alerts);
                                        $alertCountUnread = 0;
                                        foreach ($alerts as $alert)
                                        {
                                            if(is_null($alert['dateRead'])) $alertCountUnread++;
                                        }
                                        if ($alertCountUnread > 0)
                                        {
                                            $color = config('otc.color.red');
                                            $icon = '<span style="color:' . $color . ';" ><i class="fas fa-bell"></i>' . $alertCountUnread . '</span>';
                                        }
                                        else if($alertCountUnread === 0 && $alertCount > 0)
                                        {
                                            $icon = '<i style="color:' . config('otc.color.disabled') . ';" class="fas fa-bell"></i>';
                                        }
                                        else $icon = '<i style="color:' . config('otc.color.disabled') . ';" class="far fa-bell"></i>';
?>
                                            @include('2a.layouts._alertDisplay')

                                            <div class="item uinfo" id="user-info">
                                            <div class="user-info-wrapper">
                                                    <span class="avatar">
                                                        <?php
                                                        $img = Auth::user()->image;
                                                        $imgFile = str_replace(['public/public', 'public\\public'], 'public', asset($img));
                                                        $img = str_replace(['public/public', 'public\\public'], 'public', asset($img));
                                                        ?>
                                                        <img style="height: 50px; width: 50px; border-radius: 50%;"src="{{ $img  . '?otc=' .date('is') }}">
                                                    </span>

                                                    <span class="name">
                                                         @guest
                                                           Hello Guest
                                                        @else
                                                            Hello {{ App\Combine\AccountCombine::getFirstName($userID)}}
                                                        @endif

                                                        <a href="#"><span><i class="fas fa-caret-down" id="drop-down-toggler" style="color: #D8283A; padding-left: 10px;"></i></span></a>
                                                        <div class="col-sm-12 user-info-drop-down" id="user-info-drop-down">
                                                          <ul>
<?php
                                                                  $roleCode = $usrRole = session('userRole');
                                                                  if (substr($usrRole, -1) == 'a') $usrRole = 'a';
                                                                  $usrRole = title_case(str_replace('_', ' ', array_search($usrRole, config('constants.USER_ROLE'))));
                                                                  $userRoles = \App\Models\lk_Users_Roles::rolesByUserId($userID);
                                                                  $altRole = [];
                                                                  if (count($userRoles) > 1)
                                                                  {
                                                                      foreach ($userRoles as $role)
                                                                      {
                                                                          if ($role->Role == $roleCode) continue;
                                                                          $altRole[] = $role->Role;
                                                                      }
                                                                  }
?>
                                                              <li><span style="text-align: center; color:{{config('otc.teal')}};">{{$usrRole}}</span></li>
                                                              @if (count($userRoles) > 1)

@foreach($altRole as $role)
                                                                 <a class="user-role" data-role="{{$role}}" data-url="{{route('switch.Role')}}" data-redirect-url="{{route(config('otc.DefaultRoute.dashboard'))}}"><li>{{title_case(str_replace('_', ' ', array_search($role, config('constants.USER_ROLE'))))}}</li></a>
@endforeach
                                                              @endif


@if($isAdminLoggedIn)
                                                              <a href="{{route('admin.logout')}}"><li>Admin Logout</li></a>
@endif
@if($isAdmin && !$isAdminLoggedIn)
                                                              <a href="{{route('admin.login')}}"><li>Admin Login</li></a>
@endif
                                                              <a href="{{route('userProfile')}}"><li>My Profile</li></a>
                                                              <a href="{{route('userProfile')}}"><li>Settings</li></a>
                                                              <a href="{{route('otc.logout')}}"><li><button class="btn btn-xs btn-block btn-ofc">LOGOUT</button></li></a>
                                                          </ul>
                                                        </div>
                                                    </span>
                                            </div>
                                        </div>
                                        <div class="item ql">
                                            <div class="quicklinks-wrapper">
                                                <div class="quicklinks">
                                                    <span class="item"><a href=""><i class="fas fa-question qm"></i></a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- main body -->
    <div id="main-body">
        <div class="container-fluid">
            <div class="row">
                <div class="left-menu">
                    <div class="menu-toggler">
                        <span id="left-menu-toggler"><i class="fas fa-bars"></i></span>
                    </div>

<?php
// .. This code is the standard left nav menu everyone sees
?>
                    @include('2a.layouts._leftNav')


                </div>
            </div>
        </div>

        <div class="main-content">
            @yield('content')
        </div>

        <footer class="footer col-xs-12">
            <div class="col-xs-12">
                <div class=" left">
                    <div class="img"></div>
                </div>
                <div class="right">
                    <div class="copy">

                    </div>
                    <div class="links">
                        <a href="#">&copy; 2019 Offer To Close</a>
                        <a href="{{route('prelogin.termsConditions')}}">Terms & Conditions</a>
                        <a href="{{route('prelogin.privacyPolicy')}}">Privacy Policy</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/upload/core.js') }}"></script>
<script src="{{ asset('js/upload/upload.js') }}"></script>

<script>
$(document).ready(function(){
    $('#drop-down-toggler').on('click', function(){
        $('#user-info-drop-down').toggleClass("show");
    });

    $('.user-role').click(function () {
       switchRoles('user-role');
    });

    $('.left-menu .item .main').on('click', function(){
        el = $(this);
        $('.menu-icon').removeClass('selected');
        $('.menu-title').removeClass('selected');
        $('.left-menu').addClass("active");
        $('.item .main .menu-title').addClass("active");
        $('.left-menu .item .main').removeClass("active");
        el.toggleClass("active");
        $('.main-content').addClass('shrink');
        $('.left-menu .menu-toggler').addClass("active");
        el.find('.menu-title').toggleClass("selected");
        el.find('.menu-icon').toggleClass("selected");
        el.siblings('.left-menu .item .sub').toggleClass("active");
    });
    $('.left-menu .item').on('mouseover', function(){
        var self = $(this);
        self.find('.menu-icon').addClass('white')
        self.find('.menu-title').addClass('white');

    });
     $('.left-menu .item').on('mouseout', function(){
        var self = $(this);
        self.find('.menu-icon').removeClass('white')
        self.find('.menu-title').removeClass('white');

    });
    $('#left-menu-toggler').on('click', function(){
        el = $('.left-menu .menu-toggler');
        el.toggleClass("active");
        $('.main-content').toggleClass('shrink');
        $('.left-menu').toggleClass("active");
        $('.item .main .menu-title').toggleClass("active");
        $('.item .main .menu-title').removeClass("selected");
        $('.menu-icon').removeClass("selected");
        $('.left-menu .item .sub').removeClass("active");
        $('.left-menu .item .main').removeClass("active");
    });
    var topLimit = $('.left-menu').offset().top;
    $(window).scroll(function() {
        if (topLimit <= $(window).scrollTop()) {
            $('.left-menu').addClass('stickIt')
        } else {
            $('.left-menu').removeClass('stickIt')
        }
    })

    $('input:radio').click( function(){
            var el = $(this);
            var name = el.attr("name");
            var selected = $('input[name='+ name + ']:checked');
            var unselected = $('input[name='+ name + ']:not(":checked")');
            $(unselected).each(function(){
            var unchecked = $(this);
            unchecked.parent('.o-radio-container').removeClass('true-checked');
            unchecked.parent('.o-radio-container').removeClass('false-checked');
            })
            if(selected.val()== 0){
                selected.parent('.o-radio-container').addClass('false-checked');
            }
            else if(selected.val() > 0){
                selected.parent('.o-radio-container').addClass('true-checked');
            }
    });
    $('input[type=radio]:checked').each(function(){
        el = $(this);
        if (el.val() == 0){
            el.parent('.o-radio-container').addClass('false-checked');
        }
        else if(el.val() > 0){
            el.parent('.o-radio-container').addClass('true-checked');
        }
    });
});
</script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
@yield('scripts')
</body>
</html>