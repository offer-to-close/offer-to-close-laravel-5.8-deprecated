<?php
// .. This code is the standard left nav menu everyone sees
$isTC = \App\Combine\AccountCombine::isThisRole(session('userRole'), ['tc', 'stc', 'btc']);

?>
<div class="menu-item-wrapper">
    <div class="item">
        <a class="main" href="{{ route('dashboard') }}"> <span class="menu-icon"><i class="fas fa-cog"></i></span> <span class="menu-title upper-case">Dashboard</span></a>
    </div>

    <div class="item">
        <a class="main"> <span class="menu-icon"><i class="fas fa-exchange-alt"></i></span> <span class="menu-title upper-case">Transactions</span></a>
        <div class="sub">
            <a href="{{ route('dash.ta.list') }}">All Transactions</a>
            <a href="#" id="addNewTransactionLeftNav">Add Transaction</a>
        </div>
    </div>

    @if($isTC)
    <div class="item">
        <a class="main"> <span class="menu-icon"><i class="fas fa-users"></i></span> <span class="menu-title upper-case">People</span></a>
        <div class="sub">
            <a href={{route('add.New')}}>Add New Person</a>
        </div>
    </div>
    @endif

    @if($isTC)
    <div class="item">
        <a class="main"> <span class="menu-icon"><i class="fas fa-tags"></i></span> <span class="menu-title upper-case">Offers</span></a>
        <div class="sub">
            <a href="{{ route('offers.byTC' ) }}">All Offers</a>
            <a href="{{route('input.Offer')}}">Create Offer</a>
        </div>
    </div>
    @endif

    @if($isTC)
    <div class="item">
        <a class="main"> <span class="menu-icon"><i class="fas fa-envelope-open"></i></span> <span class="menu-title upper-case">Invoices</span></a>
        <div class="sub">
            <a href="{{route('invoices.byUserID' )}}">All Invoices</a>
            <a href="{{route('invoice.input')}}">Create Invoice</a>
        </div>
    </div>
    @endif

    <div class="item">
        <a class="main"  style="min-height: 80px;"> <span class="menu-icon"><i class="fas fa-file-contract"></i></span> <span class="menu-title upper-case">Disclosures<br>&nbsp&nbsp&nbsp- Coming Soon</span></a>
    </div>

<?php
// .. This code is for creating sub-navs based on certain user types
// .. This code should always be the last elements in the left nav
?>
    @include('2a.layouts._specialLeftNav')

</div>
<script>
    $('#addNewTransactionLeftNav').click(function(){
        addNewTransactionModal('{{route('transaction.create', ['side' => ':side'])}}');
    });
</script>