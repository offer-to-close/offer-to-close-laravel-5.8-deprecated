@extends('2a.layouts.master')

@section('content')
<?php
$title = 'Choose Email to Send';
$table = 'MailTemplates';
$header = null;
$userTransactionRoles = [];
$countUserRoles = count($userRoles);

if (isset($transactionID)) session(['transactionID'=>$transactionID]);

$property = \App\Combine\TransactionCombine::getProperty($transactionID);
$address = \App\Library\otc\AddressVerification::formatAddress($property->first(), 2);

foreach ($userRoles as $r)
{
    $userTransactionRoles[] = $r['role'];
}


?>
@section('custom_css')
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
@endsection
@section('content')
    <div class="container-left col-xs-10">
        <div class="row">

            <h1>{{$title}}</h1>

            @if(Session::has('email_sent'))
                @include('2a.partials._successMessage', [
                    '_Message' => Session::get('email_sent'),
                ])
            @elseif( Session::has('fail') )
                @include('2a.partials._failureMessage', [
                   '_Message' => Session::get('fail'),
                ])
            @elseif( Session::has('missingInfo') )
                @include('2a.partials._failureMessage', [
                   '_Message' => Session::get('missingInfo'),
                ])
            @elseif( Session::has('errors') )
                @include('2a.partials._failureMessage', [
                   '_Message' => Session::get('errors'),
                ])
            @endif


            <div class="row choose-template-container" style="margin-left: 0; margin-right: 0">
                <br/>

                <form class="choose-template-form" name="chooseForm" method="post" action="{{route('editEmail', ['mailTemplate'=>0, 'transactionID'=>$transactionID,'editFirst'=>true])}}"   enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input  type="hidden" name="transactionID" value="{{$transactionID}}">
                    @if (count($userRoles) == 1)
                        <?php $v = is_array(reset($userRoles)) ? json_encode(reset($userRoles)) : reset($userRoles);   \Illuminate\Support\Facades\Log::debug(['v'=>$v,__LINE__]);?>
                        <input type="hidden" name="fromRole" value="{{$v}}">
                    @endif

                    @if (count($userRoles) > 1)
                        <?php $uRoles = Config::get('constants.USER_ROLE'); ?>
                        <h2>From which of your roles are you sending this email?</h2>
                        @foreach ($userRoles as $r)
                            <input class="role-choice template-radio" data-id="{{$r['role']}}" type="radio" name="fromRole"
                                   <?php $v = is_array($r) ? json_encode($r) : $r;?>
                                   value="{{$v}}"> {{str_replace('_', ' ', title_case(array_search($r['role'], $uRoles)))}}<br/>
                        @endforeach
                        <hr/>
                    @endif



                    @foreach($collection as $template)

                        @if(in_array($template->FromRole, $userTransactionRoles))
                            <div class="{{$template->FromRole}} hide template">
                            @if ($template->Category != $header)
                                <h3>{{title_case($template->Category)}}</h3>
                                <?php $header = $template->Category ?>
                            @endif
                            <input class="template-radio" type="radio" name="mailTemplate"
                                   value="{{$template->ID}}" {{$template->ID==$templateID ? ' CHECKED ' : null }}><span>{{$template->DisplayDescription ?? 'No Display'}}</span><br/>
                            </div>
                        @endif
                    @endforeach
                    <div class="col-md-12 text-right">
                        <button type="submit" id="ta_status_open" class="btn btn-teal"> Choose Template</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <script>
        var countUserRoles = '{{$countUserRoles}}';
        $(document).on('click', '.role-choice', function(){
            if ($(this).is(':checked'))
            {
                var role = $(this).data('id');
                $('.template').removeClass('show');
                $('.'+role).addClass('show');
            }

        });

        if(countUserRoles < 2)
        {
            $('.template').removeClass('hide');
        }
    </script>

@endsection


