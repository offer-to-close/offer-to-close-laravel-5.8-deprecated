<?php //  Admin Main Menu   // ?>
@extends('2a.layouts.master')
@section('css')
@endsection
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="add-new-transaction-title col-xs-10">
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.red')}}">Administrator's Menu</h1>

                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    @include('2a.dashboard.admin.justMenu')
                </div>
            </div>
        </div>
    </div>
@endsection