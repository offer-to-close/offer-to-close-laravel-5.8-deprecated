@extends('2a.layouts.master')
<?php
$viewNames = explode('.', $view_name);
$title = \App\Library\Utilities\_Convert::camelToTitleCase(array_pop($viewNames));
$showData = (isset($data) && !empty($data));
$showForm = (isset($formData) && !empty($formData));
$table = 'broken';

$excludeCols = ['TaskSets_Value', 'State', 'ShortName', 'IncludeWhen', 'DateCreated',
                'deleted_at', 'DateUpdated', 'isTest'];
if ($showData) $table = \App\Library\Utilities\DisplayTable::getBasic($data, ['excludeColumns'=>$excludeCols]);
//if ($showData) $table = \App\Library\Utilities\DisplayTools::editChildTable($data, ['parentFields'=>$excludeCols]);

?>
@section('custom_css')
    @include ('2a.css.basicTable')
@endsection
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="add-new-transaction-title col-xs-10">
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.red')}}">Administrator's Menu</h1>
                        <h2>{{$title}}</h2>
                        <hr/>
                    </div>
                </div>
            </div>
        </div>

        <!-- ***************************************************************************************************** -->
        <!-- ***************************************************************************************************** -->
        @if (true || $showForm)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="add-new-transaction-title col-xs-10">
                        <div class="col-xs-12">
                            <h1>Form</h1>
                            <form action="{{route($route)}}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="btnAction" value="{{$btnAction}}">
                                <label>State:</label>
                                <input type="text" name="state" value="CA">
                                <br/>
                                <label>User Role:</label>
                                <input type="text" name="role" value="btc">

                                <button type="submit">Get Tasks</button>
                            </form>
                            <hr/>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    <!-- ***************************************************************************************************** -->
        <!-- ***************************************************************************************************** -->
        @if ($showForm && !$showData)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="add-new-transaction-title col-xs-10">
                        <div class="col-xs-12">
                            <h2>{!! $sourceTitle !!}</h2>
                            <h1>State = {{$state ?? '?'}}, User Role = {{$role ?? '??'}}</h1>

                        </div>
                        <div class="col-xs-12">
                            <h1>No Data Found</h1>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    <!-- ***************************************************************************************************** -->
        <!-- ***************************************************************************************************** -->
        @if ($showData)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="add-new-transaction-title col-xs-10">
                        <div class="col-xs-12">
                            <h2>{!! $sourceTitle !!}</h2>
                            <h1>State = {{$state ?? '?'}}, User Role = {{$role ?? '??'}}</h1>
                        </div>
                        <div class="col-xs-12">
                            {!! $table !!}
                        </div>
                    </div>
                </div>
            </div>
    @endif

    <!-- ***************************************************************************************************** -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <hr>
                    <a href="{{route('all.menuView')}}"><button type="submit">Back to Tools Menu</button></a>
                </div>
            </div>
        </div>
    </div>
@endsection