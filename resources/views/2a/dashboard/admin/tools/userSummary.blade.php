@extends('2a.layouts.master')
<?php
$viewNames = explode('.', $view_name);
$title = \App\Library\Utilities\_Convert::camelToTitleCase(array_pop($viewNames));
$hasData = false;
if (is_array($data))
    {
        foreach($data as $datum)
            {
                if (count($datum) > 0) $hasData = true;
            }
    }

$showData = $hasData;
$showForm = (isset($formData) && !empty($formData));
$table = 'broken';

$excludeCols = ['TaskSets_Value', 'State', 'ShortName', 'IncludeWhen', 'DateCreated',
                'deleted_at', 'DateUpdated', 'isTest', 'Users_ID', 'Role_ID', 'isActive', 'DocumentPath',
                'OriginalDocumentName'];
if ($showData)
    {
        $tableLoginRoles = \App\Library\Utilities\DisplayTable::getBasic($data['loginRoles'], ['excludeColumns'=>$excludeCols]);
        $tableTransactionRoles = \App\Library\Utilities\DisplayTable::getBasic($data['transactionRoles'], ['excludeColumns'=>$excludeCols]);
        $tableTransactions = \App\Library\Utilities\DisplayTable::getBasic($data['transactions'], ['excludeColumns'=>$excludeCols]);
        $tableShareRooms = \App\Library\Utilities\DisplayTable::getBasic($data['shareRooms'], ['excludeColumns'=>$excludeCols]);
        $tableShareDocuments = \App\Library\Utilities\DisplayTable::getTwoLevel($data['shareDocuments'], ['caption-prefix'=>'Transaction ID: ',
                                                                                                          'excludeColumns'=>$excludeCols]);
    }

?>
@section('custom_css')
    @include ('2a.css.basicTable')
@endsection
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="add-new-transaction-title col-xs-10">
                    <div class="col-xs-12">
                        <h1 style="color: {{config('otc.color.red')}}">Administrator's Menu</h1>
                        <h2>{{$title}}</h2>
                        <hr/>
                    </div>
                </div>
            </div>
        </div>

        <!-- ***************************************************************************************************** -->
        <!-- ***************************************************************************************************** -->
        @if (true || $showForm)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="add-new-transaction-title col-xs-10">
                        <div class="col-xs-12">
                            <h1>Form</h1>
                            <form action="{{route($route)}}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="btnAction" value="{{$btnAction}}">
                                <label>User ID:</label>
                                <input type="text" name="userID" value="">

                                <button type="submit">Process</button>
                            </form>
                            <hr/>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    <!-- ***************************************************************************************************** -->
        <!-- ***************************************************************************************************** -->
        @if ($showForm && !$showData)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="add-new-transaction-title col-xs-10">
                        <div class="col-xs-12">
                            <h2>{!! $sourceTitle !!}</h2>
                            <h1>User ID = {{$userID ?? '?'}}, {{$userInfo ?? ''}}</h1>

                        </div>
                        <div class="col-xs-12">
                            <h1>No Data Found</h1>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    <!-- ***************************************************************************************************** -->
        <!-- ***************************************************************************************************** -->
        @if ($showData)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="add-new-transaction-title col-xs-10">
                        <div class="col-xs-12">
                            <h2>{!! $sourceTitle !!}</h2>
                            <h1>User ID = {{$userID ?? '?'}}, {{$userInfo ?? ''}}</h1>
                        </div>
                        <div class="col-xs-12">
                            <h1>Login Roles</h1>
                            {!! $tableLoginRoles !!}
                        </div>
                        <div class="col-xs-12">
                            <h1>Transaction Roles</h1>
                            {!! $tableTransactionRoles !!}
                        </div>
                        <!--
                        <div class="col-xs-12">
                            <h1>Share Rooms</h1>
                            {!! $tableShareRooms !!}
                        </div>

                        <div class="col-xs-12">
                            <h1>Share Documents</h1>
                            {!! $tableShareDocuments !!}
                        </div>
                        -->
                        <div class="col-xs-12">
                            <h1>Transactions</h1>
                            {!! $tableTransactions !!}
                        </div>
                    </div>
                </div>
            </div>
    @endif

    <!-- ***************************************************************************************************** -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <hr>
                    <a href="{{route('all.menuView')}}"><button type="submit">Back to Tools Menu</button></a>
                </div>
            </div>
        </div>
    </div>
@endsection