@extends('2a.layouts.master')

@section('custom_css')
    <style>
        body{
            background-color: white
        }
        .btn-teal{
            background-color: #01bdb7;
            font-weight: 500;
            padding-left: 5px;
            padding-right: 5px;
            padding-top: 15px;
            padding-bottom: 15px;
        }
        .search-text{
            font-size: 20px;
            font-weight: 200;
            width: 100%;
            margin-top: 50px;
            margin-bottom: 50px;
            border-top: none;
            border-left: none;
            border-right: none;
            border-bottom: 1px solid #01bdb7;
            outline: none;
            padding-left:20px;
            padding-right:20px;
            position: relative;
        }
        .search-text:focus{
            border-top: none !important;
            border-left: none;
            border-right: none;
        }
        .search-bar{
            position: relative;
        }
        .main-content{
            background-color: white;
        }
        .search_query{
            border: none;
            margin-bottom: 5px;
        }
        .search_query:focus{
            border: none;
            outline: none;
        }
        .address-col{
            text-align: left !important;
            padding:10px;
        }
        .client-name-col{
            text-align: left !important;
            padding: 10px;
        }
    </style>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 col-md-offset-3 search-bar">
                <input type="text" placeholder="Search" class="search-text" id="search_table">
                <span class="otc-red" style="position: absolute; top: 40%; left: -1%; font-size: 24px; "><i class="fas fa-map-marker-alt"></i></span>
            </div>
            @if ( @$_screenMode == 'Search' )
                <div class="col-md-3" style="padding-top: 50px">
                    <div class="col-md-12"><a  href="{{ route('dash.ta.list') }}"><button class="btn btn-red" style="background-color: #D52A39; font-weight: 600; letter-spacing:1px">Show All</button></a></div>
                </div>
            @endif
        </div>

        <div class="col-md-10 col-sm-12 col-md-offset-1">
            <div class="o-box">
                <div class="title">
                    <span></span> <strong>What Room would you like to enter?</strong>
                    <br/>
                </div>
                <table class="o-table table-responsive" id="transactionTable">
                    @if( $result )

                        <thead>
                        <tr>
                            <th>OTC ID</th>
                            <th>Listing Address</th>
                            <th>Offer Prepared</th>
                            <th>Acceptance of Offer</th>
                            <th>Opening Side</th>
                            <th>Document Count</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $result = \App\Library\Utilities\_Convert::toArray($result);
                        \Illuminate\Support\Facades\Log::info([$result, $view_name=>__LINE__]); ?>
                        @foreach( $result as $idx=>$record )
                            <?php
                                $record = $record->toArray();
                                $sides = ['s'=>'Seller', 'b'=>'Buyer'];
                            ?>
                            <tr>
                                <td> {{ $record['ID'] }} </td>
                                <td class="address-col"> {!!  $record['Street1'].'<br/>'.$record['City'].', '.$record['State'].' '. $record['Zip']!!} </td>
                                <td>
                                    @if (!empty($record['DateOfferPrepared']))
                                        {{ date("n/j/Y", strtotime( $record['DateOfferPrepared'])) }}
                                    @endif
                                </td>

                                <td>
                                    @if (!empty($record['DateAcceptance']))
                                        {{ date("n/j/Y", strtotime( $record['DateAcceptance'])) }}
                                    @endif

                                </td>
                                <td>
                                    @if (!empty($record["Side"]))
                                        {{ $sides[$record['Side']] }}
                                    @endif
                                </td>
                                <td>
                                        {{ \App\Models\ShareRoom::documentCount($record['ID']) }}
                                </td>
                                <td class=""><a class="otc-red view-icon" href="{{ route('transactionSummary.timeline', $record['ID']) }}"><i class="fas fa-door-open"></i></a>&nbsp;&nbsp;
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    @else
                        <div class="col-md-12 text-center text-danger" style="padding-bottom: 20px;"><h2>No Results found</h2></div>
                    @endif
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@endsection