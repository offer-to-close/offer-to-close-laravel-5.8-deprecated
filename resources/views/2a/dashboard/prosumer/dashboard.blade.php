@extends('2a.layouts.master')
<?php
\Illuminate\Support\Facades\Log::alert('test');
?>
@php

$results = $result['success']['info']['collection'];
$transactions = json_encode(array_values($results->toArray()));
@endphp
@section('content')
    <div id="app">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <v-app>
                    <prosumer-dashboard
                        transactions="{{$transactions}}"
                        transaction-create-route="{{route('transaction.create', ['side' => ':side'])}}"
                        transaction-home-route="{{route('transaction.home', ['taid' => ':id'])}}"
                        transaction-timeline-route="{{route('transactionSummary.timeline', ['transactionID' => ':id'])}}"
                    >

                    </prosumer-dashboard>
                </v-app>
            </div>
        </div>
    </div>
@endsection