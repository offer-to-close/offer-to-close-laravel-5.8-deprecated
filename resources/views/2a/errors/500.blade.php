@extends('2a.layouts.master')
@section('content')
    <?php
            $isLive = isServerLive();
    ?>
    <div class="container">
        @if($isLive)
            <h4>And error has been logged at about {{date('m/d/Y @ H:m:s')}}</h4>
        @else
            <h4 class="red">{{$message}}</h4>
        @endif
        <p>Don't worry, it <i>probably</i> isn't your fault! </p>
        <p>Please click <a href="{{route(config('otc.DefaultRoute.dashboard'))}}">here</a> to return to a time when things worked. </p>
    </div>
@endsection
