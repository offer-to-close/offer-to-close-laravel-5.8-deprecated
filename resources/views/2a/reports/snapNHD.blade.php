<script>
    $(document).ready(function(){

        let provider; //this value will be set if selecting a provider from a list.

        $("#btn-order-report").click(function() {
            $.ajax({
                url : "{{ route('verifyReport',['transactionsID' => $transactionID, 'reportSource' => 'snapNHD']) }}",
                type: "get",
                success: function(data){
                    let isComplete = data.status;
                    let orderType  = data.type || '';
                    provider = 'SnapNHD';
                    let snapNHD = {
                        standard: '',
                        premium: '',
                        types: {
                            standard: 'Standard',
                            premium: 'Premium'
                        },
                    };
                    let style = '';
                    if(isComplete || isComplete == null)
                    {
                        style = 'display: none;';
                        if(orderType === 'standard')
                        {
                            snapNHD.premium = '{{\App\Library\otc\_Locations::url('images','/backgrounds/PremiumSnapNHDDisabled.png')}}';
                            snapNHD.standard = '{{\App\Library\otc\_Locations::url('images','/backgrounds/StandardSnapNHDOrdered.png')}}';
                        }
                        else if(orderType === 'premium')
                        {
                            snapNHD.premium = '{{\App\Library\otc\_Locations::url('images','/backgrounds/PremiumSnapNHDOrdered.png')}}';
                            snapNHD.standard = '{{\App\Library\otc\_Locations::url('images','/backgrounds/StandardSnapNHDDisabled.png')}}';

                        }

                    }
                    else
                    {
                            snapNHD.premium = '{{\App\Library\otc\_Locations::url('images','/backgrounds/PremiumSnapNHD.png')}}';
                            snapNHD.standard = '{{\App\Library\otc\_Locations::url('images','/backgrounds/StandardSnapNHD.png')}}';
                    }
                    let modalHTML =
                        '<div class="row">\n' +
                        '    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">Order an NHD Report</div>\n' +
                        '    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">Provider: <span>'+provider+'</span></div>\n' +
                        '    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="position: relative;">' +
                        '       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">\n' +
                        '           <img src="'+snapNHD.premium+'" style="width: 100%;height:100%; border: solid 1px #e7e7e7;border-radius: 10px;">\n' +
                        '       </div>\n' +
                        '       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="position: absolute; bottom: -85%; left: 0;height: 100%;">' +
                        '           <button value="premium" id="orderPremium" style="'+style+' width: 60%;height:10%;margin: auto;" class="order-report">Order Report</button>' +
                        '       </div>' +
                        '    </div>' +
                        '    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="position: relative;">' +
                        '       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">\n' +
                        '           <img src="'+snapNHD.standard+'" style="width: 100%;height: 100%; border: solid 1px #e7e7e7; border-radius: 10px;">\n' +
                        '       </div>\n' +
                        '       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="position: absolute; bottom: -85%; left: 0; height: 100%;">' +
                        '           <button value="standard" id="orderStandard" style="'+style+' width: 60%;height:10%;margin: auto;" class="order-report">Order Report</button>' +
                        '       </div>' +
                        '    </div>' +
                        '</div>';

                    Swal2({
                        html:modalHTML,
                        width: '50%',
                        showConfirmButton: false,
                        showCancelButton: true,
                        onBeforeOpen: () =>{
                            const content   = Swal2.getContent();
                            const s$        = content.querySelector.bind(content);
                            let standard    = s$('#orderStandard');
                            let premium     = s$('#orderPremium');
                            function confirmOrder(orderType)
                            {
                                Swal2({
                                    title: 'Confirm Order',
                                    html: '' +
                                        '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">' +
                                        '    Order Type: '+orderType+'' +
                                        '</div>' +
                                        '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">' +
                                        '    Provider: '+provider+'' +
                                        '</div>',
                                    confirmButtonText: 'Yes, I want to order this report.',
                                    preConfirm: ()=>{
                                        Swal2.showLoading();
                                        let route   = '{{route('order.snapnhd', ['transactionID' => $transactionID, 'reportType' => ':ordertype'])}}';
                                        route       = route.replace(':ordertype', orderType);
                                        $.ajax({
                                            url: route,
                                            type: 'post',
                                            success: function (r){
                                                if(r.status === 'success')
                                                {
                                                    Swal2({
                                                        title: 'Order Complete!',
                                                        text: r.message,
                                                        type: 'success',
                                                        timer: 3000,
                                                    });
                                                }
                                                else if(r.status === 'fail')
                                                {
                                                    Swal2({
                                                        title: 'Oh no!',
                                                        text: r.message,
                                                        type: 'error',
                                                        timer: 3000,
                                                    });
                                                }
                                                else
                                                {
                                                    wal2({
                                                        title: 'Oh no!',
                                                        text: 'An error has occurred. Please try again.',
                                                        type: 'error',
                                                        timer: 3000,
                                                    });
                                                }

                                            },
                                            error: function(err){
                                                Swal2({
                                                    title: 'Oh oh...',
                                                    text: 'An error has occurred. Please try again.',
                                                    type: 'error',
                                                    timer: 3000,
                                                });
                                            },
                                        });
                                    },
                                });
                            }
                            standard.addEventListener('click',() =>{
                                confirmOrder(standard.value);

                            });
                            premium.addEventListener('click',() =>{
                                confirmOrder(premium.value);
                            });
                        },
                    });
                }
            });
        });
    });

</script>