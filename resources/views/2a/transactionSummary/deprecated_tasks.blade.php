<!-- {{$sort}} -->
@extends('2a.layouts.master')
@if(Session::has('successNHD'))
    @include('2a.partials._successMessage', [
        '_Message' => Session::get('successNHD'),
    ])
@endif
@if(Session::has('failNHD'))
    @include('2a.partials._failureMessage', [
        '_Message' => Session::get('failNHD'),
    ])
@endif

<?php
$btnCategoryClass = 'category-button-off';
$btnDateClass  = 'chronological-button';

if (strtolower($sort??null) == 'alpha')
{
    $btnCategoryClass = 'category-button-on';
    $btnDateClass  = 'chronological-button-off';
}
elseif (strtolower($sort??null) == 'chrono')
{
    $btnCategoryClass = 'category-button-off';
    $btnDateClass  = 'chronological-button-on';
}
?>

@section('content')
@php
    $data['taskDateDue'] = NULL;
    $data['taskDescription'] = NULL;
    $data['Notes'] = NULL;
    $FirstDay = date("Y-m-d", strtotime('sunday last week'));
    $LastDay = date("Y-m-d", strtotime('sunday this week'));
    $Date =  date("Y-m-d");
@endphp

@include('2a.transactionSummary.subViews._transactionSummaryBanner')

<section class="main-details dashboard clearfix">
    <div class="wrap">
        <div class="top">
            <div class="container-fluid">
                <div class="tb-wrap">
                    <div class="tb-layout">
                        <div class="tb-item left">
                            @include('2a.transactionSummary.subViews._subMenu')
                        </div>
                        <div class="tb-item right"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom">
            <div class="container-fluid">
                <!--  ADD TASK MODAL WINDOW SECTION START -->

                <div id="task-modal" class="modal task-modal-class">
                    <div class="modal-dialog modal-lg">
                    <form class="modal-content animate" method="post" action="{{url('/ts/todos/save')}}">
                        {{ csrf_field() }}
                        <div class="imgcontainer">
                            <span onclick="document.getElementById('task-modal').style.display='none'" class="close text-danger" title="Close Modal">&times;</span>
                        </div>
                        <div class="container">
                            <input type="hidden" name="transactionID" value="{{$transactionID}}">
                            <input type="hidden" name="userRole" value="{{session('userRole')}}">
                            <input type="hidden" name="sort" value="{{$sort}}">
                            <label for="desc"><b>Description <span class="important">*</span></b></label><br>
                            <textarea placeholder="Enter Description" required name="Description" class="col-sm-4 td-note-field tasks-textarea">{{$data['taskDescription']}}</textarea>
                            <br/>
                            <label class="error_input" for="Description">{{$errors->first('taskDescription')}}</label>

                            <div class="clearfix"></div>
                            <br>

                            <label for="due"><b>Due Date <span class="important">*</span></b></label>

                            <div class="tasks-red-bottom">
                                <div class="img"></div>
                                <div class="the-input">
                                    <input type="date" placeholder="Enter Due Date" name="DateDue" class="tasks-input" value="{{$data['taskDateDue']}}" required>
                                </div>
                            </div>
                            <br/>
                            <label class="error_input" for="DateDue">{{$errors->first('taskDateDue')}}</label>
                        </div>

                        <div class="container btngroup">
                            <button name="saveTask" value="true" class="btn btn-teal" type="submit">Save</button>
                        </div>
                    </form>
                    </div>
                </div>
                <!-- MODAL WINDOW SECTION END -->
                <!--  EDIT TASK MODAL WINDOW SECTION START -->

                <div id="task-edit-modal" class="modal task-modal-class">
                    <div class="modal-dialog modal-lg">
                    <form class="modal-content animate" method="post" action="{{route('update.tasks')}}">
                        {{ csrf_field() }}
                        <div class="imgcontainer">
                            <span onclick="document.getElementById('task-edit-modal').style.display='none'" class="close" title="Close Modal">&times;</span>
                        </div>
                        <div class="container edit-task-container">
                            <input type="hidden" name="transactionID" value="{{$transactionID}}">
                            <input type="hidden" name="id" id="task-id">
                            <input type="hidden" name="userRole" value="{{session('userRole')}}">
                            <input type="hidden" name="status" value="edit">

                            <h3 id="edit-task-description"></h3>

                            <div class="clearfix"></div>
                            <br>

                            <label for="due"><b>Due Date <span class="important">*</span></b></label>

                            <div class="tasks-red-bottom">
                                <div class="img"></div>
                                <div class="the-input">
                                    <input id="edit-date-due" type="date" placeholder="Enter Due Date" name="DateDue" class="tasks-input" value="{{$data['taskDateDue']}}" required>
                                </div>
                            </div>
                            <br/>
                            <label class="error_input" for="DateDue">{{$errors->first('taskDateDue')}}</label>

                            <label for="due"><b>Additional Details</b></label><br>
                            <textarea rows="4" id="tasks-notes" class="tasks-textarea" cols="50" value="{{$data['Notes']}}" name="Notes"></textarea>
                        </div>

                        <div class="container btngroup">
                            <button name="saveTask" value="true" class="tasks-save" type="submit">Save</button>
                        </div>
                    </form>
                    </div>
                </div>
                <!-- MODAL WINDOW SECTION END -->
                <!-- TASKS TAB -->
                <input id="transactionIDContainer" type="hidden" name="transactionID" value="{{$transactionID}}"/>
                <input id="clientRoleContainer" type="hidden" name="clientRole" value="{{$clientRole}}"/>
                <input id="sortContainer" type="hidden" name="sort" value="{{$sort}}"/>
                <div id="timeline" class="tab-content">
                    <div class="tb-wrap">
                        <div class="tb-layout">
                            <div class="tb-item left">
                                <div class="content-wrap">
                                    <div class="action-item view-buttons">
                                        <ul class="list-sort">
                                            <li class="sort-item">
                                                <a href="{{route('transactionSummary.tasks', ['transactionID'=>$transactionID, 'clientRole'=> $clientRole, 'sort' => 'Alpha'])}}">
                                                    <i class="fas fa-list-ul {{$btnCategoryClass}}"></i>
                                                </a>
                                            </li>
                                            <li class="sort-item">
                                                <a href="{{route('transactionSummary.tasks', ['transactionID'=>$transactionID, 'clientRole'=> $clientRole, 'sort' => 'Chrono'])}}">
                                                    <i class="far fa-calendar-alt {{$btnDateClass}}"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="section-item">
                                        <h3 class="title">Tasks</h3>
                                        <h4 class="no-tasks"></h4>
                                    </div>
                                    <div class="section-item section-top text-right">
                                        <a href="#" id='btn-addtask' class="btn-danger">+ ADD A TASK</a>
                                    </div>
                                    <div class="section-item pastdue-section">
                                        <h4 class="sub-title tasks-title task_past_due_header"></h4>
                                        <div class="list-dates timeline_past_due">

                                        </div>
                                    </div>
                                    <div class="section-item thisweek-section">
                                        <h4 class="sub-title task_this_week_header"></h4>
                                        <div class="list-dates timeline_this_week">
                                        </div>
                                    </div>
                                    <div class="section-item upcoming-section">
                                        <h3 class="sub-title task_upcoming_header"></h3>
                                        <div class="list-dates timeline_upcoming">
                                        </div>
                                    </div>
                                    <!-- THE FOLLOWING SEGMENT MUST BE INCORPORATED INTO THIS VIEW -->
                                    <div class="section-item accordion-section">
                                        <div class="accordions">
                                            <!-- START ACCORDION ITEMS -->



                                        </div>
                                    </div> <!-- END SECTION ACCORDION -->
                                    <div class="section-item completed-section">

                                        <h4 class="title timeline-show-completed" data-toggle="0"></h4>

                                        <h3 class="sub-title timeline-hide-toggle completed-milestone-h3 tasks-title"></h3>
                                        <div class="list-dates timeline_completed">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tb-item right">
                                <div class="details-content">
                                    @include('2a.transactionSummary.property_details')
                                </div>

                                <div class="details-content">
                                    @include('2a.transactionSummary.key_people')
                                </div>

                                @include('2a.transactionSummary.subViews._requestReports')

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

    var token="{{Session::token()}}";
    var tid = document.getElementById("transactionIDContainer").value;
    var clientRole = document.getElementById("clientRoleContainer").value;
    var userRole = '{{session('userRole')}}';
    var sort = document.getElementById("sortContainer").value;
    var isComplete = false;
    var noTasks = '{{$noTasks}}';

    function drawTasks(openContainers = []) {
        var id = $(location).attr('href').match(/\d+/);
        $(".timeline_past_due").html('');
        $(".timeline_upcoming").html('');
        $(".timeline_this_week").html('');
        $(".timeline_completed").html('');
        $(".accordions").html('');
        $(".accordion-item").html('');

        $.ajax({
            url : "{{route('get.tasks')}}",
            type: "post",
            tryCount : 0,
            retryLimit : 3,
            data: {"_token":token, "id":tid, "sort":sort},
            success: function(data){
                var items = data.tasks;
                if(items.length === 0) $('.no-tasks').text('You have no tasks associated with this transaction.');
                var accordions = true;
                $.each(items, function(idx, item) {
                    if(item.Display == null) item.Display = 'Other Tasks';
                    if(data.sort === 'Chrono') accordions = false;
                    let itemDateFormat  = 'YYYY-MM-DD';
                    let USDateFormat    = 'MM/DD/YYYY';
                    isComplete          = false;
                    let today           = (new Date()).toLocaleDateString('en-US').split(' ')[0];
                    let item_date       = item.DateDue.toLocaleString('en-US').split(' ')[0];
                    let weekNumber      = moment().week();
                    var month           = moment(item.DateDue, itemDateFormat).format("MMM");
                    var date            = moment(item.DateDue, itemDateFormat).format("DD");
                    var interval;
                    if ((weekNumber === moment(item.DateDue, itemDateFormat).week() && !moment(item.DateDue, itemDateFormat).isBefore()) || moment(today, USDateFormat).isSame(item_date, itemDateFormat)) interval = 'week';
                    else if(moment(item.DateDue, itemDateFormat).isBefore()) interval = 'past';
                    else interval = 'upcoming';
                    var description =item.Description;
                    var dateCompleted = item.DateCompleted;
                    if(dateCompleted != null) isComplete = true;
                    if(item.lk_Notes == null) item.lk_Notes = "";
                    var item_ID = item.lk_ID;
                    //var utc = new Date().toJSON().slice(0,10);
                    var item_html =
                        '<div class="date-item" id="'+item.Category+'-item-'+item_ID+'">\n' +
                        '    <div class="date"><span class="month" style="text-transform: uppercase">'+month+'</span><span class="day">'+date+'</span></div>\n' +
                        '        <div class="description">\n' +
                        '            <div class="desc-wrap" data-id="'+item_ID+'" data-status="'+isComplete+'">\n' +
                        '                <div class="desc">'+item.Description+'</div>\n' +
                        '                 <ul class="action-list">\n'+
                        '                    <li><a href="#"><i data-id="'+item_ID+'" class="fas fa-check completed"></i></a></li>\n'+
                        '                    <li><a href="#"><i id="edit-'+item_ID+'" data-id="'+item_ID+'" class="fas fa-edit edit"></i></a></li>\n'+
                        '                    <li><a href="#"><i data-id="'+item_ID+'" class="fas fa-trash-alt destroy"></i></a></li>\n'+
                        '                 </ul>\n'+
                        '            </div>\n' +
                        '             <div class="desc-note">\n'+item.lk_Notes+
                        '             </div>\n'+
                        '        </div>\n' +
                        '    </div>\n' +
                        '</div>';

                    var item_html_complete =
                        '<div class="date-item" id="'+item.Category+'-item-'+item_ID+'">\n' +
                        '    <div class="date"><span class="month" style="text-transform: uppercase">'+month+'</span><span class="day">'+date+'</span></div>\n' +
                        '        <div class="description">\n' +
                        '            <div class="desc-wrap desc-wrap-completed" data-id="'+item_ID+'" data-status="'+isComplete+'">\n' +
                        '                <span class="action"><i class="fas fa-check"></i></span>'+item.Description+' <a href="#" data-id="'+item_ID+'" class="mark-complete">Mark as Incomplete</a>\n' +
                        '            </div>\n' +
                        '            <div class="desc-note">\n'+item.lk_Notes+
                        '            </div>\n'+
                        '        </div>\n' +
                        '    </div>\n' +
                        '</div>';

                    var item_html_accordion =
                        '<div class="accordion-item '+item.Category+'-accordion-container">\n'+
                        '<div class="accordion-trigger" data-toggle="collapse" data-target="#'+item.Category+'" aria-expanded="false" aria-controls="collapseExample">\n'+
                        '<h4 class="acc-title">'+item.Display+'</h4>\n'+
                        '</div>\n'+
                        '<div class="collapse" id="'+item.Category+'">\n'+
                        '<div class="list-dates w-actions '+item.Category+'-accordion-parent">\n'+
                        '<div class="date-item" id="'+item.Category+'-item-'+item_ID+'">\n' +
                        '    <div class="date"><span class="month" style="text-transform: uppercase">'+month+'</span><span class="day">'+date+'</span></div>\n' +
                        '        <div class="description">\n' +
                        '            <div class="desc-wrap" data-id="'+item_ID+'" data-status="'+isComplete+'">\n' +
                        '                <div class="desc">'+item.Description+'</div>\n' +
                        '                 <ul class="action-list">\n'+
                        '                    <li><a href="#"><i data-id="'+item_ID+'" class="fas fa-check completed"></i></a></li>\n'+
                        '                    <li><a href="#"><i id="edit-'+item_ID+'" data-id="'+item_ID+'" class="fas fa-edit edit"></i></a></li>\n'+
                        '                    <li><a href="#"><i data-id="'+item_ID+'" class="fas fa-trash-alt destroy"></i></a></li>\n'+
                        '                 </ul>\n'+
                        '            </div>\n' +
                        '             <div class="desc-note">\n'+item.lk_Notes+
                        '             </div>\n'+
                        '        </div>\n' +
                        '    </div>\n' +
                        '</div>\n'+   //end of date-item
                        '</div>\n'+ //end of list dates-w-actions
                        '</div>\n'+ //end of collapse
                        '</div>\n'; //end of accordion-item


                    $('#edit'+item_ID).data({"notes":item.Notes}); //assign the notes to data('notes') for this element
                    if(item.Status != 'deleted')
                    {
                        if( isComplete === true) $(".timeline_completed").append(item_html_complete);
                        else {
                            if(accordions)
                            {
                                if(!($("."+item.Category+"-accordion-container").length) && accordions){
                                    $(".accordions").append(item_html_accordion);
                                }
                                else if(!($("#"+item.Category+"-item-"+item_ID).length) && accordions){
                                    $("."+item.Category+"-accordion-parent").append(item_html);
                                }
                                $('.pastdue-section, .thisweek-section, .upcoming-section').remove();
                            }
                            else
                            {
                                if(interval=='past') $(".timeline_past_due").append(item_html);
                                else if(interval=='week') $(".timeline_this_week").append(item_html);
                                else $(".timeline_upcoming").append(item_html);
                            }

                        }
                    }

                    if(moment(today, USDateFormat).isSame(item_date,itemDateFormat))
                    {
                        let date_identifier = '#'+item.Category+'-item-'+item_ID;
                        let due = '<div class="date due"><span class="quote-due">Due Today</span></div>';
                        $(date_identifier+' .date').remove();
                        $(date_identifier).prepend(due);
                    }


                    var identifier = "#edit-"+item_ID;
                    $(identifier).data("description",item.Description);
                    $(identifier).data("date-due",item_date);
                    $(identifier).data("notes",item.Notes);
                });

                var pastHeader = $('.task_past_due_header');
                var thisWeekHeader = $('.task_this_week_header');
                var upcomingHeader = $('.task_upcoming_header');
                var completedHeader = $('.timeline-show-completed');
                var completedHeader2 = $('.completed-milestone-h3');

                pastHeader.text('');
                thisWeekHeader.text('');
                upcomingHeader.text('');
                completedHeader.text('');
                completedHeader2.text('');

                if($('.timeline_past_due').children().length > 0) pastHeader.text('Past Due');
                if($('.timeline_this_week').children().length > 0) thisWeekHeader.text('This Week');
                if($('.timeline_upcoming').children().length > 0) upcomingHeader.text('Upcoming');
                if($('.timeline_completed').children().length > 0) {
                    completedHeader.html('Show Completed Tasks <i class="fa fa-caret-down"></i>');
                    completedHeader2.html('Completed Tasks<i class="fa fa-caret-up"></i>');
                }

                $.each(openContainers, function(i, el){
                    $('.'+el+' .collapse').collapse();
                });
            },
            error : function(xhr, textStatus, errorThrown ) {

                if (textStatus == 'timeout') {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) {
                        $.ajax(this);
                        return;
                    }
                    return;
                }
            },
            timeout: 0
        });
    }

    $(document).ready(function(){
        drawTasks();
        if(noTasks)
        {
            $('.no-tasks').text('You have no tasks associated with this transaction.');
        }
    });

    function keepAccordionsOpenDraw(){
        let parents = [];
        $('.in').each(function (obj) {
            let className = $(this).parents('.accordion-item').attr('class');
            try {
                className = className.replace('accordion-item ', '');
            }catch (e) {

            }
            parents.push(className);
        });
        drawTasks(parents);
    }

    //.mark-complete's functionality is actually mark-incomplete
    $(document).on('click', '.mark-complete', function(event){
        var self = $(this);
        var id = self.data('id');
        var status = 'incomplete';
        event.preventDefault();
        $.ajax({
            url : "{{route('update.tasks')}}",
            type: "post",
            data: {"_token":token, "id":id, "userRole":userRole,"status":status},
            success: function(data){
                keepAccordionsOpenDraw();
            }
        });

    });

    $(document).on('click', '.completed', function(event){
        var self = $(this);
        var id = self.data('id');
        var status = 'completed';
        event.preventDefault();
        $.ajax({
            url : "{{route('update.tasks')}}",
            type: "post",
            data: {"_token":token, "id":id, "userRole":userRole,"status":status},
            success: function(data){
                keepAccordionsOpenDraw();
            }
        });

    });

    $(document).on('click', '.destroy', function(event){
        var self = $(this);
        var id = self.data('id');
        var status = 'delete';
        $(this).parents('.date-item').remove();
        event.preventDefault();
        $.ajax({
            url : "{{route('update.tasks')}}",
            type: "post",
            data: {"_token":token, "id":id, "clientRole":clientRole,"status":status},
            success: function(data){
                keepAccordionsOpenDraw();
            }
        });
    });

    $(document).on('click', '.edit', function(){
        var self = $(this);
        var description = self.data('description');
        var dateDue = self.data('date-due');
        var id = self.data('id');
        var notes = self.data('notes');
        $("#task-edit-modal").css("display", "block");
        $("#task-edit-modal #edit-task-description").text(description);
        $("#task-edit-modal #edit-date-due").val(dateDue);
        $('#task-edit-modal #tasks-notes').val(notes);
        $("#task-edit-modal #task-id").val(id);
    });

    $(document).on('click', '.timeline-show-completed', function(){
        $(".timeline-show-completed").toggle();
        $(".timeline-hide-toggle").toggle();
        $(".timeline_completed").slideToggle();
    });
    $(document).on('click', '.timeline-hide-toggle', function(){

        $(".timeline-show-completed").toggle();
        $(".timeline-hide-toggle").toggle();
        $(".timeline_completed").slideToggle();
    });
    $("#btn-addtask").click(function() {
        $("#task-modal").css("display", "block");
    });

</script>
<div style="clear: both"></div>
@endsection