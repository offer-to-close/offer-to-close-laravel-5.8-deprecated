@php

    $tabs  = [
                'Documents'=>route('transactionSummary.documents', $transactionID),
                'Timeline'=>route('transactionSummary.timeline', $transactionID),
                'Disclosures'=>route('transactionSummary.disclosures', $transactionID),
              ];

    $urls  = [
                'Documents'=>route('transactionSummary.documents', $transactionID),
                'Timeline'=>route('transactionSummary.timeline', $transactionID),
                'Disclosures'=>route('transactionSummary.disclosures', $transactionID),
              ];

   $icons =   [
                'Documents'=>'fas fa-file-alt',
                'Timeline'=>'fas fa-clock',
                'Disclosures'=>'fas fa-file-signature'
              ];

    $notLiveTabsURLs =
              [
                'Mail'=>route('chooseTemplate', $transactionID),
                //'Share'=>route('transactionSummary.shareRoom', $transactionID),
              ];

    $notLiveIcons =
              [
                'Mail'=>'fas fa-mail-bulk',
                //'Share'=>'far fa-share-square',
              ];

    if(!isServerLive())
    {
        foreach($notLiveTabsURLs as $key => $nav)
        {
            $tabs[$key] = $nav;
            $urls[$key] = $nav;
        }
        foreach($notLiveIcons as $key => $icon)
        {
            $icons[$key] = $icon;
        }
    }

@endphp

<style>
    .dashboard-banner .wrap .title{
        color: black;
    }
</style>

<ul class="list-tabs">
    @foreach ($tabs as $label=>$route)
        <li class="tab-button current-tab">
            @php $isLinkActive = ( url()->current() == $route  ) ? 'link-active' : '' @endphp
            @if(isset($notLiveTabsURLs[$label]))
                <a href="{{ $urls[$label] }}" style="color: {{\Illuminate\Support\Facades\Config::get('otc.color.orange')}} !important;"><span class="mobile-view"><i class="{{ $icons[$label] . ' '. $isLinkActive }}"></i></span><span class="desktop-view {{ $isLinkActive }}">{{$label}}</span></a>
            @else
                <a href="{{ $urls[$label] }}"><span class="mobile-view"><i class="{{ $icons[$label] . ' '. $isLinkActive }}"></i></span><span class="desktop-view {{ $isLinkActive }}">{{$label}}</span></a>
            @endif
        </li>
    @endforeach
</ul>
