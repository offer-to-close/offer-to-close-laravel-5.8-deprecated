@extends('2a.layouts.master')
@if(Session::has('successNHD'))
    @include('2a.partials._successMessage', [
        '_Message' => Session::get('successNHD'),
    ])
@endif
@if(Session::has('failNHD'))
    @include('2a.partials._failureMessage', [
        '_Message' => Session::get('failNHD'),
    ])
@endif

@section('content')

@section('custom_css')
    <!-- <div class="clearfix" style="min-height: 1400px">
    <link rel="stylesheet" href="{{asset('css/fontawesome-all.min.css')}}"> -->
    <!-- <link rel="stylesheet" href="{{asset('css/lightcase.css')}}"> -->
    <!-- <link rel="stylesheet" href="{{asset('css/transactionSummary/app.css')}}">
        <style>
            .footer{position:relative!important}
        </style> -->
    <style>
        .timeline-hide-toggle {color:teal!important; cursor: pointer; display: none}
        .timeline-show-completed {cursor: pointer; text-align: center;  font-size: 17px!important;}
        .timeline_completed {display:none}
        .desc-wrap:hover {cursor: pointer}
        .desc-wrap-completed:hover {background-color: #e85c5c!important; cursor: pointer}
        .timeline-past-due {color:red!important; font-weight: 600}
        .completed-milestone-h3{color:#21d7d1 !important;}
        .link-active{ color: #D52A39;}
        .mobile-view{color: #00DED3}
    </style>
    @include ('2a.css.basicTable')
@endsection
@php
    $FirstDay = date("Y-m-d", strtotime('sunday last week'));
    $LastDay = date("Y-m-d", strtotime('sunday this week'));
    $Date =  date("Y-m-d");
@endphp

@include('2a.transactionSummary.subViews._transactionSummaryBanner')

<section class="main-details dashboard clearfix">
    <div class="wrap">
        <div class="top">
            <div class="container-fluid">
                <div class="tb-wrap">
                    <div class="tb-layout">
                        <div class="tb-item left">
                            @include('2a.transactionSummary.subViews._subMenu')
                        </div>
                        <div class="tb-item right"></div>
                    </div>
                </div>
            </div>


            <?php ///////////////////////////////////////////////////////////////////////////////////////////////////// ?>
            <?php // ... Page specific code follows ... ?>
            <?php ///////////////////////////////////////////////////////////////////////////////////////////////////// ?>

            <div class="bottom">
                <div class="container-fluid">

                    <!-- TIMELINE TAB -->
                    <div id="timeline" class="tab-content">
                        <div class="tb-wrap">
                            <div class="tb-layout">
                                <div class="tb-item left">
                                    <div class="content-wrap">
                                        <div class="section-item">
                                            <h3 class="title">Share Room</h3>
                                        </div>

                                        <div class="section-item">
{!! $table !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="tb-item right">
                                    <div class="details-content">
                                        @include('2a.transactionSummary.property_details')
                                    </div>

                                    <div class="details-content">
                                        @include('2a.transactionSummary.key_people')
                                    </div>

                                    @include('2a.transactionSummary.subViews._requestReports')

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  EDIT TIMELINE MODAL WINDOW SECTION START -->

        <div id="timeline-edit-modal" class="modal timeline-modal-class">
            <div class="modal-dialog modal-lg">
                <div id="task-approve-modal" class="modal">
                    <div class="modal-dialog modal-lg">
                        <form class="modal-content animate" method="post" action="{{route('updateStatus')}}">
                            {{ csrf_field() }}
                            <div class="imgcontainer">
                                <span class="close-modals close" title="Close Modal">&times;</span>
                            </div>
                            <div class="container" id="task-approval-container">
                                <input type="hidden" name="transactionID" value="{{$transactionID}}">
                                <input type="hidden" name="id" id="timeline-id-inner">
                                <input type="hidden" name="clientRole" value="">
                                <input id="status" type="hidden" name="status" value="edit">

                            </div>
                        </form>
                    </div>
                </div>
                <form class="modal-content animate" method="post" action="{{route('updateStatus')}}" id="dateForm">
                    {{ csrf_field() }}
                    <div class="imgcontainer">
                        <span class="close close-modals" title="Close Modal">&times;</span>
                    </div>
                    <div class="container">
                        <input type="hidden" name="transactionID" value="{{$transactionID}}">
                        <input type="hidden" name="id" id="timeline-id">
                        <input type="hidden" name="clientRole" value="">

                        <h3 id="timeline-description"></h3>

                        <div class="clearfix"></div>
                        <br>

                        <label for="due"><b>Due Date <span class="important">*</span></b></label>

                        <div class="">
                            <div class="img"></div>
                            <div class="the-input">
                                <input id="edit-date-due" type="date" placeholder="Enter Due Date" name="DateDue" class="timeline-input" value="" required>
                            </div>
                        </div>
                        <br/>
                        <label class="error_input" for="DateDue" style="color: red;"></label>
                    </div>

                    <div class="container btngroup">
                        <button name="saveTask" value="true" class="tasks-save" type="button">Save</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- MODAL WINDOW SECTION END -->
    </div> <!-- End of Wrap -->
</section>

<script>

    var token="{{Session::token()}}";
    var transactionID = '{{$transactionID }}';

    function getMonth(string) {
        var month_names =["0","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

        var res = string.split(" ");
        var res = res[0].split("-");
        return month_names[parseInt(res[1])];
    }

    function getDate(string) {
        var res = string.split(" ");
        var res = res[0].split("-");
        return res[2];
    }

    function getInterval(string) {
        var dateOptions = { year: 'numeric', month: '2-digit', day: '2-digit' };
        var res = string.split(" ");
        var res_date = res[0].split("-");
        var res_time = res[1].split(":");

        var dtCompare = new Date();
        dtCompare.setFullYear(res_date[0], parseInt(res_date[1])-1, res_date[2]);
        dtCompare.setFullYear(res_date[0], parseInt(res_date[1])-1, res_date[2]);
        dtCompare.setHours(res_time[0],res_time[1],res_time[2]);
        var ftCompare = dtCompare.toLocaleDateString("ko-KR", dateOptions);

        var dtToday = new Date();
        var ftToday = dtToday.toLocaleDateString("ko-KR", dateOptions);

        var diff = 7-dtToday.getDay();

        var dtWeek = new Date();
        dtWeek.setDate(dtWeek.getDate() + diff);
        var ftWeek = dtWeek.toLocaleDateString("ko-KR", dateOptions);

        if (ftCompare < ftToday) {
            return 'past';
        } else {
            if (ftCompare > ftWeek) return 'upcoming';
            else return 'week';
        }
    }


    function drawTimeline() {
        var id = $(location).attr('href').match(/\d+/);
        $(".timeline_past_due").html('');
        $(".timeline_upcoming").html('');
        $(".timeline_this_week").html('');
        $(".timeline_completed").html('');

        $.ajax({
            url : "{{route('transaction.posttimeline')}}",
            type: "post",
            tryCount : 0,
            retryLimit : 3,
            data: {'id':id, "_token":token},
            success: function(data){
                var items = data.items;
                $.each(items, function(idx, item) {
                    var month = getMonth(item.MilestoneDate);
                    var date = getDate(item.MilestoneDate);
                    var interval = getInterval(item.MilestoneDate);
                    var item_html =
                        '<div class="date-item">\n' +
                        '    <div class="date"><span class="month" style="text-transform: uppercase">'+month+'</span><span class="day">'+date+'</span></div>\n' +
                        '        <div class="description">\n' +
                        '            <div class="desc-wrap" data-id="'+item.ID+'" >\n' +
                        '                <span class="action"></span>'+item.MilestoneName+'\n' +
                        '                 <ul class="action-list">\n'+
                        '                    <li><a href="#"><i data-id="'+item.ID+'" data-status="'+item.isComplete+'" class="fas fa-check completed"></i></a></li>\n'+
                        '                    <li><a href="#"><i data-id="'+item.ID+'" id="edit-'+item.ID+'" data-status="'+item.MilestoneName+'" class="fas fa-edit edit"></i></a></li>\n'+
                        '                 </ul>\n'+
                        '            </div>\n' +
                        '        </div>\n' +
                        '    </div>\n' +
                        '</div>';

                    var item_html_complete =
                        '<div class="date-item">\n' +
                        '    <div class="date"><span class="month" style="text-transform: uppercase">'+month+'</span><span class="day">'+date+'</span></div>\n' +
                        '        <div class="description">\n' +
                        '            <div class="desc-wrap desc-wrap-completed" data-id="'+item.ID+'" data-status="'+item.isComplete+'">\n' +
                        '                <span class="action"><i class="fas fa-check"></i></span>'+item.MilestoneName+'\n' +
                        '                 <ul class="action-list">\n'+
                        '                    <li><a href="#"><i data-id="'+item.ID+'" data-status="'+item.isComplete+'" class="fas fa-redo completed"></i></a></li>\n'+
                        '                 </ul>\n'+
                        '            </div>\n' +
                        '        </div>\n' +
                        '    </div>\n' +
                        '</div>';

                    if(item.isComplete==1) $(".timeline_completed").append(item_html_complete);
                    else {
                        if(interval=='past') $(".timeline_past_due").append(item_html);
                        else if(interval=='week') $(".timeline_this_week").append(item_html);
                        else $(".timeline_upcoming").append(item_html);
                    }

                    $('#edit-'+item.ID).data('date',item.MilestoneDate);
                });

                var pastHeader = $('.timeline_past_due_header');
                var thisWeekHeader = $('.timeline_this_week_header');
                var upcomingHeader = $('.timeline_upcoming_header');
                var completedHeader = $('.timeline-show-completed');

                pastHeader.text('');
                thisWeekHeader.text('');
                upcomingHeader.text('');
                completedHeader.text('');

                if($('.timeline_past_due').children().length > 0) pastHeader.text('Past Due');
                if($('.timeline_this_week').children().length > 0) thisWeekHeader.text('This Week');
                if($('.timeline_upcoming').children().length > 0) upcomingHeader.text('Upcoming');
                if($('.timeline_completed').children().length > 0) completedHeader.html('Show Completed Milestones <i class="fa fa-caret-down"></i>');

            },
            error : function(xhr, textStatus, errorThrown ) {
                if (textStatus == 'timeout') {
                    this.tryCount++;
                    if (this.tryCount <= this.retryLimit) {
                        $.ajax(this);
                        return;
                    }
                    return;
                }
            },
            timeout: 1000
        });



    }

    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        drawTimeline();

        $('.close-modals').click(function () {
            $('#reviewItems').remove();
            $('#task-approval-container').children('#aButtons').remove();
            var timelineModal       = $('#timeline-edit-modal');
            var taskApproveModal    = $('#task-approve-modal');
            taskApproveModal.toggle();
            taskApproveModal.css('display','none');
            timelineModal.toggle();
            timelineModal.css('display','none');
        });

    });

    $(document).on('click', '.completed', function(e){
        var self=this;
        var id = $(this).data('id');
        var status = $(this).data('status');
        $.ajax({
            url : "{{route('updateStatus')}}",
            type: "post",
            data: {'id':id, 'status':status,"_token":token},
            success: function(data){
                $(self).data('status',data);
                if(data==1)$(self).find('.action').html('<i class="fas fa-check"></i>');
                else $(self).find('.action').html('<i class="fas fa-times"></i>');
                drawTimeline();
            }
        });
    });

    $(document).on('click', '.timeline-show-completed', function(){
        $(".timeline-show-completed").toggle();
        $(".timeline-hide-toggle").toggle();
        $(".timeline_completed").slideToggle();
    });

    $(document).on('click', '.timeline-hide-toggle', function(){

        $(".timeline-show-completed").toggle();
        $(".timeline-hide-toggle").toggle();
        $(".timeline_completed").slideToggle();
    });

    $(document).on('click', '.edit', function(){
        var self    = $(this);
        var id      = self.data('id');
        var desc    = self.data('status');
        var datedue = self.data('date');
        datedue = new Date(datedue);
        datedue = datedue.getFullYear()+'-'+(("0" + (datedue.getMonth() + 1)).slice(-2))+'-'+("0" + datedue.getDate()).slice(-2);
        var edit    = $('#timeline-edit-modal');
        edit.toggle();
        $('#timeline-edit-modal #edit-date-due').val(datedue);
        $('#timeline-edit-modal #timeline-description').text(desc);
        $('#timeline-edit-modal #timeline-id').val(id);

    });

    $(document).on('click', '.tasks-save', function(){
        var self    = $(this);
        var edit    = $('#task-approve-modal');
        var dateInput = $('#edit-date-due');
        var dateDueLabel = $('label[for="DateDue"]');
        var id = $('#task-approve-modal #timeline-id-inner');
        if(!dateInput.val())
        {
            dateDueLabel.text('Please enter a valid date.');
            return false;
        }
        edit.toggle();
        dateDueLabel.fadeOut(1000);
        id.val($('#timeline-edit-modal #timeline-id').val());
        function addOffset(date, offset){
            var cleanOffset = offset.indexOf('|');
            var days = null;

            if(cleanOffset === -1) days = offset;
            else {
                offset = offset.split('|');
                days = offset[0];
            }
            date = date.split(' ');
            date = date[0];
            var newdate = new Date(date);
            newdate.setDate(newdate.getDate()+parseInt(days)+1);
            var someDate = newdate.getFullYear()+'-'+(newdate.getMonth()+1)+'-'+newdate.getDate();
            return someDate;
        }

        var container = $('#task-approval-container');

        function executeTaskChanges()
        {
            $('#reviewItems').remove();
            $.ajax({
                url: "{{route('approveTaskChange')}}",
                type: "post",
                data: {
                    "milestoneID": id.val(),
                    "status": $('#status').val(),
                    "newDate": dateInput.val(),
                },
                success: function (data) {
                    var totalTasks = data.list.length;
                    if (totalTasks === 0 || data.list === false) {
                        drawTimeline();
                        $('#timeline-edit-modal').toggle();
                        $('#task-approve-modal').toggle();
                        return false;
                    }

                    container.append('<div id="reviewItems" style="position:relative;width: 70% !important; padding:15px;height:500px;"><h3>' + data.list[0].TimelineName + ' affects these edited tasks.</h3></div>');


                    $.each(data.list, function (i, obj) {
                        var parent = $('#reviewItems');
                        var TaskDateDue = obj.TaskDateDue.split(' ');
                        TaskDateDue = TaskDateDue[0];
                        var newDate = addOffset(dateInput.val(), obj.DateOffset);
                        var itemHTML =
                            '    <div class="panel panel-info" style="position:absolute; left:0; top: 20%; height:80%; width:80% !important;">\n' +
                            '      <div class="panel-heading">Task: <b>' + obj.Task + '</b></div>\n' +
                            '      <div class="panel-body" style="padding:15px;">\n' +
                            '        <h5 class="panel-title">Changing ' + obj.TimelineName + ' to <b>' + dateInput.val() + '</b>\n' +
                            '        will change this task\'s due date from <b>' + TaskDateDue + '</b> to <b>' + newDate + '</b>.</h5>\n' +
                            '        <p style="padding:15px;">Please approve this change or press cancel to leave this task\'s date as it is.</p>\n' +
                            '        <button type="button" id="approve-' + obj.ID + '" class="btn btn-teal approve-task-change" data-id="' + obj.ID + '" data-status="' + newDate + '">Approve</button>\n' +
                            '        <button type="button" id="skip-' + obj.ID + '" class="btn btn-danger skip-task-change">Cancel</button>\n' +
                            '      </div>\n' +
                            '    </div>';
                        parent.append(itemHTML);
                        $('#approve-' + obj.ID).data("index", i);
                        $('#skip-' + obj.ID).data("index", i);
                    });

                    var tasksPendingChange = [];
                    $('.approve-task-change').click(function () {
                        var self = $(this);
                        var idThis = self.data('id');
                        var date = self.data('status');
                        var index = self.data('index');
                        tasksPendingChange.push({ID: idThis, NewDate: date});
                        self.parents('.panel').remove();
                        if (index === 0) {
                            self.parents('.panel').remove();
                            $('#reviewItems').remove();
                            $('#timeline-edit-modal').toggle();
                            $('#task-approve-modal').toggle();
                            $.ajax({
                                url: "{{route('approveTaskChange')}}",
                                type: "post",
                                data: {'list': tasksPendingChange, 'pendingTasks': true, "_token": token},
                                success: function (data) {
                                    //console.log(data);
                                }
                            });
                            drawTimeline();
                        }

                    });

                    $('.skip-task-change').click(function () {
                        var self = $(this);
                        var index = self.data('index');
                        self.parents('.panel').remove();
                        if (index === 0) {
                            self.parents('.panel').remove();
                            $('#reviewItems').remove();
                            $('#timeline-edit-modal').toggle();
                            $('#task-approve-modal').toggle();
                            drawTimeline();
                        }
                    });
                }
            });//end of the ajax call
        } //end of execute task changes

        $.ajax({
            url: '{{route('listAffectedTasks')}}',
            type: "post",
            data: {
                "milestoneID": id.val(),
            },
            success: function(response){
                var totalTasks = response.list.length;

                if (totalTasks === 0 || response.list === false) {
                    executeTaskChanges();
                    return false;
                }
                container.append('<div id="reviewItems" style="position:relative; width: inherit !important; padding:15px; height: 500px; overflow-y: scroll;"><h3>' + response.list[0].TimelineName + ' impacts these tasks.</h3></div>');
                $.each(response.list, function(i, obj){
                    var itemHTML =
                        '    <div class="" style="margin-top:5px; width: 50% !important;">\n'+
                        '      <p>'+ obj.Task +'</p>\n'+
                        '      <br>\n'+
                        '    </div>';
                    $('#reviewItems').append(itemHTML);
                });

                var buttonHTML =
                    '    <div id="aButtons" style="margin-top: 10px;">\n'+
                    '    <button id="proceedTaskChange" type="button" class="btn btn-teal">Proceed</button>\n'+
                    '    <button id="cancelTaskChange" type="button" class="btn btn-danger">Cancel</button>\n'+
                    '    </div>';

                container.append(buttonHTML);


            }
        });

        $(document).unbind().on('click', '#proceedTaskChange', function(){
            $('#reviewItems').remove();
            $('#aButtons').remove();
            executeTaskChanges();
        });

        $(document).on('click', '#cancelTaskChange', function(){
            $('#reviewItems').remove();
            container.children('#aButtons').remove();
            var timelineModal       = $('#timeline-edit-modal');
            var taskApproveModal    = $('#task-approve-modal');
            taskApproveModal.toggle();
            taskApproveModal.css('display','none');
            timelineModal.toggle();
            timelineModal.css('display','none');
        });

    });



</script>
</div>
<div style="clear: both"></div>
@endsection