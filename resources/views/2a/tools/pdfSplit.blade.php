@extends('2a.layouts.master')

<?php
$title = 'Split That PDF';
$transactionID  = $transaction_id;
?>
@section('content')
    <div class="container-left col-xs-10">
        <div class="row">
            <div class="add-new-transaction-title col-xs-10">
                <div class="col-xs-12">
                    <h1>{!! $title !!}</h1>
                </div>
            </div>
        @if(Session::has('success'))
            @include('2a.partials._successMessage', [
                '_Message' => Session::get('success'),
            ])
        @elseif(Session::has('failMessage'))
            @include('2a.partials._failureMessage', [
                '_Message' => Session::get('failMessage'),
            ])
        @elseif( Session::has('fail') )
            @include('2a.partials._overrideAddress', [
               '_Message' => Session::get('fail'),
               '_Address' => Session::get('address'),
               '_TransactionId' => Session::get('transactionId'),
            ])
        @endif
        </div>

        <div class="container-two col-xs-4"></div>
    </div>

@endsection
        @section('scripts')
@append

