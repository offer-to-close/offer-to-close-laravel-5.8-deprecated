@extends('2a.layouts.master')
@section('content')

    <form name="bob" method="post" action="{{route('sendEmail.string')}}"  enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_mailTemplateID" value="{{$_mailTemplateID}}" />
        <input type="hidden" name="_transactionID" value="{{$_transactionID}}" />
        <input type="hidden" name="_originalMsg" value="{{$email}}" />
        <input type="hidden" name="_view" value="{{$_view}}" />

        <div class="mail_fields">
            <div>
                <label for="_to">To: </label>
                <input class="to_field" type="text" name="_to" value="{{$_to}}" required /><br/>
            </div>
            <div>
                <label for="_from">From: </label>
                <input type="text" name="_from" value="{{$_from}}" required /><br/>
            </div>
            <div>
                <label for="_cc">Cc: </label>
                <input type="text" name="_cc" value="{{$_cc}}" /><br/>
            </div>
            <div>
                <label for="_bcc">Bcc: </label>
                <input type="text" name="_bcc" value="{{$_bcc}}" /><br/>
            </div>
            <div>
                <label for="subject">Subject: </label>
                <input type="text" name="subject" value="{{$_subject}}" required /><br/>
            </div>
        </div>




        <!-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->
        <div class="email-template-container">
            <div class="email-template">
                <br/>
                <button class="btn btn-teal" type="submit">Send Me!</button>
                <br/>
                <br/>
                <textarea  name="message" style="width:600px; height: 600px;" required>
                 {!! $email !!}
                </textarea>
                <br/>
                <button class="btn btn-teal" type="submit">Send Me!</button>
            </div>
        </div>
        <!-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% -->



    </form>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js{{ !is_null(env('TINYMCE_API_KEY')) ? '?apiKey='.env('TINYMCE_API_KEY') : null}}">
    </script>
    <script>
        tinymce.init({
            selector: "textarea",
            plugins: "autoresize",
        });
    </script>

@endsection