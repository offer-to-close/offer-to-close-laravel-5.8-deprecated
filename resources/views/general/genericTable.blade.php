@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left col-lg-12 margin-tb">
                <h2>{{ $title }}</h2>
            </div>
            <div class="pull-left">
                {!!  \App\Combine\BaseCombine::tableHeader($collection) !!}
                {!!  \App\Combine\BaseCombine::tableData($collection) !!}
                {!!  \App\Combine\BaseCombine::tableFooter($collection) !!}
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('dashboard') }}"> Back</a>
            </div>
        </div>
    </div>


@endsection

