<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>OfferToClose</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app_2a_SASS.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{asset('js/prelogin/jquery.js')}}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="{{asset('js/prelogin/sweetAlert.js')}}"></script>

</head>
<body>

<?php ///////////// Display Special Dialog  //////////////// ?>
@if(Session::has('debug'))
    <script>debugInfo('{{Session::get('debug')}}')</script>
@endif
<?php //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^// ?>


<div id="auth-body">
	<div class="auth-wrapper">
		<a class="logo" href="{{ url('/') }}"><img src="{{ asset('images/logo.png') }}"></a>
			@yield('content')
		<div class="auth-others">
			<a class="back-home" href="{{ url('/') }}">Back to Home</a>
		</div>
	</div>
</div>
@yield('scripts')
</body>
</html>
