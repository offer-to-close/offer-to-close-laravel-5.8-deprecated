<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PK9VQPK');</script>
    <!-- End Google Tag Manager -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transaction Timeline</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

</head>
<style>

    ::-webkit-input-placeholder {
        font-style: italic;
        }
        :-moz-placeholder {
        font-style: italic;
        }
        ::-moz-placeholder {
        font-style: italic;
        }
        :-ms-input-placeholder {
        font-style: italic;
        }

        body a, body button, body .btn {
            -webkit-transition: all 0.7s ease;
            /* transition: all 0.7s ease; */
        }

    body{
        margin: 0;
        padding: 0;
    }

     .head_section{
        background: #cb4544;
        width: 100%;
        height: 120px;
        margin: 0;
        padding: 0;
    }
    .logo{
        margin: auto;
        position: absolute;
        left: 50%;
        transform: translate(-50%);
    }

    .nav_section{
        padding-top: 85px;
        color: white;
    }

    .nav_section a{
        color: white;
        text-decoration: none;
    }

    .nav_item{
        margin-right: 50px;
        font-weight: 500;
    }

    .logo-img{
        margin: auto;
        height: 90px;
        width: 250px;
        object-fit: cover;

    }

    .page_detail{
        margin: auto;
        margin-top: 60px;
        margin-bottom: 30px;
    }

    .page_detail p{
        font-size: 14px;
        margin-top: 20px;
        line-height: 1.3;
    }

    .page_detail h1{
        font-size: 25px;
    }

    .address{
        width: 100%;
        border-radius: 5px;
        border-color: none;
        padding-left: 10px;
        padding-top:5px;
        margin-bottom: 30px;
    }

    .address_section{
        margin-bottom: 40px;
    }

    .calculator_section{
        font-size: 23px;
    }

    .my_column h5{
        font-size: 15px;
        font-weight: 600;
        margin-bottom: 15px;
    }

    .my_column h6{
        font-size: 15px;
        font-weight: 600;
        margin-top: 20px;
    }

    .cal_table{
        font-size: 15px;
        font-weight: 600;
        margin-bottom: 30px;
    }

    .cal_table td{
        font-size: 14px;
        font-weight: 550;
    }

    .cal_table thead{
        border-bottom: 1px dotted grey;
    }

    .cal_table thead th{
        font-size: 15px;
        font-weight: 550;
        height: 40px;
    }

    .date_feild{
        border-color: black;
        border-width: 0px;
        margin-bottom: 7px;
        color:black;
        text-align:center;
        padding-left: 45px;
    }

    .origin_date{
        border-color: black;
        border-radius: 5px;
        margin-bottom: 7px;
        color: black;
        text-align: center;
         border-width: 0px;
         padding-left: 45px;
    }

    .days {
        width: 40px;
        border-color: black;
        border-radius: 5px;
        margin-bottom: 7px;
        border-width: 0px;
        color: black;
        text-align: center;
    }

    .input-icon{
        background: white;
        border-color: black;
    }

    .input-group-prepend{

    }

    .email_section{
        margin-bottom: 10px;
    }

    .email_input{
        border-width: 0px;
    }

    .email_label{
        margin-top: 5px;
        margin-bottom: 0px;
        font-weight: 600;
        font-size: 15px;
    }

    .form-control:focus {
        border-color: inherit;
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .submit_button{
        margin-top: 15px;
        color: white;
        font-weight: 600;
        font-size: 14px;
        background: #07c4e2;
    }

    .footer_section{
        background: #28343B;
        min-height: 90px;
        padding-top: 20px;
        padding-bottom: 20px;
        padding-right: 20px;
        padding-left: 20px;
        color: white;
        font-size: 15px;
    }

    .footer_section a:hover{
        color: #D8283A;
    }

    .social_icon_section a{
        margin-left: 10px;
        font-size: 17px;
        color: white;
    }

    .footer_menu a{
        margin-left: 15px;
        color: white;
        font-size: 12px;
        text-decoration: none;
    }

     /* Medida queries------------------------------------------------------------------------- */
    @media only screen and (max-width: 600px) {

            .nav_section{
                display: none;
            }

            .address{
                margin-bottom: 0px;
            }
              .days{
                background: #FBFBFB;
                font-size: 15px;
                height: 30px;
                border-width: 0px;
                margin-left: 90px;
            }

            .date_feild{
                background: #FBFBFB;
                height: 25px;
                margin-bottom: 10px;
                margin-top: 10px;
                border-width: 0px;
            }
                table, thead, tbody, th, td, tr {
                    display: block;
                }

            /* Hide table headers (but not display: none;, for accessibility) */
            thead tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
            }

            tr { border: 1px solid #ccc; margin-bottom: 30px; border-radius: 5px;background-color: #fbfbfb; }

            td {
                /* Behave  like a "row" */
                border: none;
                border-bottom: 1px solid #eee;
                position: relative;
                text-align: center;
            }

            td:before {
                /* Now like a table header */
                position: absolute;
                /* Top/left values mimic padding */
                top: 6px;
                left: 6px;
                width: 45%;
                padding-right: 10px;
                white-space: nowrap;
            }
            td:nth-of-type(3):before {
                color: grey;
                font-size: 12px;
                font-weight: 500;
                text-align: left;
            }
            td:nth-of-type(1):before { content: ""; }
	        td:nth-of-type(2):before { content: ""; }
	        td:nth-of-type(3):before { content: "Days after Acceptance"; }


            .errors{
                font-size: 10px;
            }

            .copyrights_section{
                font-size: 12px;
                text-align: center !important;
                padding-bottom: 20px;
            }

            .social_icon_section{
                margin-top: 20px;
                margin-bottom: 20px;
                text-align: center !important;
            }

            .social_icon_section a{
                font-size: 20px;
                margin-left: 15px;
                margin-right: 15px;
            }


            .footer_menu{
                display: none;
            }




        }

</style>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PK9VQPK"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div class="head_section">
        <div class="logo">
            <img class="img-responsive logo-img" src="{{ asset('images/ofc-logo.png') }}" alt="">
        </div>
        <div class="nav_section text-center">
                <a href=""><span class="nav_item">About us</span></a>
                <a href=""><span class="nav_item">Pricing</span></a>
                <a href=""><span class="nav_item">Get Started</span></a>
                <a href=""><span class="nav_item">Transaction Timeline</span></a>
        </div>
    </div>

    <div class="container">

        <div class="row no-gutters">
            <div class="col-md-7 text-center page_detail">
                <h1>Transaction Timeline from Offer To Close</h1>
                <p class="text-muted">To calculate key dates and deadlines enter the date the official date the contract was accepted. We’re included the default period for each of the key dates but if you or your client have agreed to non-standard terms, you can change the number of days from acceptance to have the dates updated to reflect the terms of the residential purchase agreement.</p>
            </div>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="col-md-7 m-auto">
           <div class="row no-gutters address_section">
                <div class="col-md-4"><h5>Property Address:</h5></div>
                <div class="col-md-8 text-muted">
                <textarea class="form-control address" name="property_address" id="" readonly>{{ $timeline_details->PropertyAddress }}</textarea>
                </div>
           </div>
            <table class="col-md-12 cal_table">
                <thead class="thead-light">
                    <tr>
                        <th class=""style="width: 34%">KEY MILESTONES</th>
                        <th class="text-center"style="width: 33%">DATE</th>
                        <th stylw="width: 33%">DAYS AFTER ACCEPTANCE</th>
                    </tr>
                </thead>
                    <tr>
                        <td>Mutual Acceptance:</td>
                        <td class="text-center"><input class="origin_date" name="mutual_acceptance_date" id="mutual_acceptance_date" type="date" value="{{ $timeline_details->MutualAcceptanceDate }}" readonly>
                        <td></td>
                    </tr>

                    <tr>
                        <td>Deposit Due:</td>
                        <td class="text-center"><input class="date_feild" name="deposit_due_date" value="{{ $timeline_details->DepositDueDate or "--" }}" type="date" readonly></td>
                        <td class="text-center"><input type="text" name="deposit_due_days" class="days"></td>

                    </tr>

                    <tr>
                        <td>Disclosures Due:</td>
                        <td class="text-center"><input class="date_feild" name="disclosures_due_date" type="date" value="{{ $timeline_details->DisclosureDueDate }}" readonly></td>
                        <td class="text-center"><input type="text" name="disclosures_due_days" class="days" readonly></td>

                    </tr>

                    <tr>
                        <td>Inspection Contingency:</td>
                        <td class="text-center"><input class="date_feild" name="inspection_contingency" type="date" value="{{ $timeline_details->InspectionContingency }}" readonly></td>
                        <td class="text-center"><input type="text"  name="inspection_contingency_days"class="days" readonly></td>

                    </tr>

                    <tr>
                        <td>Appraisal Contingency:</td>
                        <td class="text-center"><input class="date_feild" name="appraisal_contingency"  type="date" value="{{ $timeline_details->AppraisalContingency }}" readonly></td>
                        <td class="text-center"><input type="text"  name="appraisal_contingency_days" class="days" readonly></td>

                    </tr>

                    <tr>
                        <td>Loan Contingency:</td>
                        <td class="text-center"><input name="loan_contingency" class="date_feild"  type="date" value="{{ $timeline_details->LoanContingency }}" readonly></td>
                        <td class="text-center"><input name="loan_contingency_days" type="text"  class="days" readonly></td>

                    </tr>

                    <tr>
                        <td>Close of Escrow Date:</td>
                        <td class="text-center"><input class="date_feild" name="close_of_escrow_date" type="date" value="{{ $timeline_details->CloseOfEscrowDate }}" readonly></td>
                        <td class="text-center"><input type="text" name="close_of_escrow_days" class="days" value="{{ $timeline_details->close_od_escrow_date }}" readonly></td>

                    </tr>

                    <tr>
                        <td>Possession on the Property:</td>
                        <td class="text-center"><input class="date_feild" name="possession_on_property" value="{{ $timeline_details->PossessionOnProperty }}" type="date" readonly></td>
                        <td class="text-center"><input type="text"  name="possession_on_property_days" readonly class="days"></td>

                    </tr>

            </table>
            <div class="col-md-12 form-group text-left">
                    <div class="row no-gutters email_section">
                        <div class="input-group">
                           <div class="row col-md-12 no-gutters">
                                <div class="col-md-3">
                                    <label class="email_label" for="">Sender's E-mail:</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" readonly class="form-control email_input"value="{{ $timeline_details->Sender }}" name="reciever_email" required placeholder="Email of Client">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row no-gutters">
                        <div class="input-group">
                            <div class="row col-md-12 no-gutters">
                                <div class="col-md-3">
                                    <label class="email_label" for="">Reciever's E-mail:</label>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" readonly class="form-control email_input"value="{{ $timeline_details->Reciever }}" name="reciever_email" required placeholder="Email of Client">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 text-center">
                            <a style="text-decoration: none" href="{{ route('timeline.edit', [ 'link' => $timeline_details->LinkID ] )  }}"><button class="btn btn-info btn-block submit_button" type="submit" > EDIT TIMELINE </button></a>
                        </div>
                    </div>
            </div>

            </div>
        </div>
    </div>

    <!--Footer section -->
    <div class="row no-gutters footer_section">
        <div class="col-md-4 text-left copyrights_section">
            © 2018 Offer to Close All right reserved
        </div>

        <div class="col-md-4 text-center">
                <img src="{{ asset('images/logo-white.png') }}" alt="">
        </div>

        <div class="col-md-4 text-right">
            <div class="col-12 text-right social_icon_section">
                <a href="https://www.OfferToClose.com/blog/">Blog</a>
                <a href="https://www.facebook.com/offertoclose/"><span><i class="fa fa-facebook"></i></span></a>
                <a href="https://www.twitter.com/offertoclose"><span><i class="fa fa-twitter"></i></span></a>
                <a href="https://www.instagram.com/offertoclose/"><span><i class="fa fa-instagram"></i></span></a>
                <!--      <a href=""><span><i class="fa fa-youtube-play"></i></span></a> -->
            </div>
            <!--
            <div class="col-12 text-right footer_menu">
                <a href=""><span>About Us</span></a>
                <a href=""><span>Contact Us</span></a>
                <a href=""><span>Terms & Conditions</span></a>
                <a href=""><span>Privacy Policy</span></a>
            </div>
            <div class="col-md-12 text-right footer_menu">
                <a href=""><span>Are You An Agent</span></a>
            </div>
            -->
        </div>
    </div>




    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            $(document).on('change input', '.date_feild', function(){
                var el = $(this);
                var end_date = el.val();
                var start_date = $('#mutual_acceptance_date').val();
                var days_input = el.parent().siblings().find('.days')
                var from = moment(start_date, 'YYYY-MM-DD')
                var to = moment(end_date, 'YYYY-MM-DD');
                if( !(start_date == "" && end_date == "") ){
                   var duration = to.diff(from, 'days')
                   days_input.val(duration);
                   if (isNaN(days_input . val())) {
                        days_input . val(" ");
                    }
               }
            });
            $(document).on('change input', '.days', function(){
                var el = $(this);
                var start_date = $('#mutual_acceptance_date').val();
                var origin_date = moment(start_date, 'YYYY-MM-DD');
                var end_date = origin_date.add(el.val(), 'days').format('YYYY-MM-DD');
                var date_input = el.parent().siblings().find('.date_feild');
                date_input.val(end_date);
                if (isNaN(el.val())) {
                    el.val(" ");
                }

            });

            $(document).on('change input', '#mutual_acceptance_date', function(){
                $('.date_feild').each(function() {
                        $(this).change();
                });
            });

            $('.date_feild').each(function(){
                el = $(this);
                if( el.val() == "" ){
                    el.attr('type', 'text');
                    el.val('--');
                    el.css('width', '190px');
                }
            });



            $('#mutual_acceptance_date').trigger('change');
            $('.days').each(function(){
                el = $(this);
                if( el.val() == " " ){
                    el.val('--');
                }
            });

            $('td:nth-of-type(3):first').css('display', 'none')

        });
    </script>
</body>
</html>