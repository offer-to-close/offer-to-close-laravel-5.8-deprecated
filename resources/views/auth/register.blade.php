@extends('layouts.auth')
<?php
$inviteCode = $inviteCode ?? NULL;
$userCanRegister = FALSE;
$requestedRole = $equestedRole ?? '';
$reasonRequested = (strtolower($code)=='beta') ? 'Beta Request from Welcome Page' : '';
if (is_null($inviteCode))
    {
        $header = '<i style="color:white;"></i>'.'Request Early Access';
        $route = 'requestMembership';
    }
else
    {
        $userCanRegister = TRUE;
        $header = '<i style="color:white;"></i> '.'Complete Early Access Registration';
        $route = 'register';
    }
?>
@section('content')

    <script src="https://www.google.com/recaptcha/api.js?render=6LcRbZoUAAAAAO-fOefXKTpajPQ5S0AlBzuXR6Eb"></script>
    <div class="container">
        @if(Session::has('inviteCodeErrors') && !is_null($inviteCode))
            @include('2a.partials._failureMessage', [
                '_Message' => Session::get('inviteCodeErrors'),
            ])

        @endif
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="">{!! $header !!}</h1>

                <div class="panel panel-default">

                    <div class="panel-body">
                        <form id="registrationForm" class="form-horizontal" method="POST" action="{{ route($route) }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="userRole" value="{{$role ?? 'a'}}" >
                            <input type="hidden" name="inviteCode" value="{{$inviteCode ?? '*'}}" >
                            <input type="hidden" name="ReasonRequested" value="{{$reasonRequested ?? '*'}}" >


                            <div class="form-group{{ $errors->has('NameFirst') ? ' has-error' : '' }}">
                                <label for="NameFirst" class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input id="NameFirst" type="text" class="form-control" name="NameFirst" value="{{ !empty($nameFirst) ? $nameFirst : old('NameFirst') }}" autofocus>
                                    @if ($errors->has('NameFirst'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('NameFirst') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('NameLast') ? ' has-error' : '' }}">
                                <label for="NameLast" class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input id="NameLast" type="text" class="form-control" name="NameLast" value="{{ !empty($nameLast) ? $nameLast : old('NameLast') }}">
                                    @if ($errors->has('NameLast'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('NameLast') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ !empty($email) ? $email : old('email') }}">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('RequestedRole') ? ' has-error' : '' }}">
                                <label for="RequestedRole" class="col-md-4 control-label">Choose a role</label>

                                <?php
                                    $roleOptions = [
                                        'Transaction Coordinator'   => 'tc',
                                        'Real Estate Agent'         => 'a',
                                        'Buyer'                     => 'b',
                                        'Seller'                    => 's',
                                    ];
                                ?>
                                <div class="col-md-6">
                                    @foreach($roleOptions as $display => $value)
                                        @if($requestedRole != '')
                                            @if($roleRequested == $value)
                                                <input type="radio" name="RequestedRole" value="{{$value}}" checked>{{$display}}<br/>
                                            @else
                                                <input type="radio" name="RequestedRole" value="{{$value}}">{{$display}}<br/>
                                            @endif
                                        @else
                                            <input type="radio" name="RequestedRole" value="{{$value}}">{{$display}}<br/>
                                        @endif
                                    @endforeach
                                </div>
                                @if($errors->has('RequestedRole'))
                                    <span class="help-block">
                                                Selecting a role is required.
                                    </span>
                                @endif

                            </div>


                        @if($userCanRegister)
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button class="btn btn-primary register-button" type="button">
                                        Register Now
                                    </button>
                                    <button style="visibility: hidden;" class="btn btn-primary" type="submit">
                                    </button>
                                </div>
                            </div>
                            <div class="text-center">
                                <p>Already Have an OTC Account? <a href="{{route('otc.login', ['inviteCode'=>$inviteCode])}}">Log in</a>.</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
            async defer>
    </script>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            function submitRegistration()
            {
                let formElement = $('form#registrationForm *');
                formElement.find('[type="submit"]').trigger('click');
            }

            $(document).on('click', '.register-button', function(){
                var onloadCallback = function () {
                    grecaptcha.ready(function () {
                        grecaptcha.execute('6LcRbZoUAAAAAO-fOefXKTpajPQ5S0AlBzuXR6Eb',
                            {action: 'register'}).then(function(token) {
                                $.ajax({
                                    url: '{{route('verify_reCaptcha')}}',
                                    type: 'POST',
                                    data: {
                                        _token: '{{csrf_token()}}',
                                        token: token,
                                    },
                                    success: function (r) {
                                        if(r.status === 'success')
                                        {
                                            if(r.score > 0.5)
                                            {
                                                submitRegistration();
                                            }
                                            else
                                            {
                                                Swal2({
                                                    title: 'Error',
                                                    text: 'An error has occurred, please try again. Error code: reg=rec-01',
                                                    type: 'error',
                                                });
                                            }
                                        }
                                        else
                                        {
                                            Swal2({
                                                title: 'Error',
                                                text: 'There was an error processing your request, please try again. Error code: reg=rec-02',
                                                type: 'error',
                                            });
                                        }
                                    },
                                    error: function (err) {
                                        Swal2({
                                            title: 'Error',
                                            text: 'An unknown error has occurred. Error code: reg=rec-03',
                                            type: 'error',
                                        });
                                    },
                                });
                            }
                        ); //end grecaptcha execute
                    });
                };
                onloadCallback();
            });
        });
    </script>
@endsection