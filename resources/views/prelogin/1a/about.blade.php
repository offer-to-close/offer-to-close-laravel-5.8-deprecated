@extends('prelogin.1a.layouts.master')

@section('custom_style')
     <style>
        .about-header .row{
            height: 500px;
            position:relative;
            background-image: url('{{ asset("images/backgrounds/otc-team-high-res.png") }}');
            background-size: cover;
        }

        .about-header .row:after{
            content: "";
            position: absolute;
            top:0;
            left:0;
            right:0;
            bottom:0;
            display: block;
            z-index: 1;
            background:rgba(0, 0, 0, 0.7);
        }
        .about-header .row .container-fluid{
            margin-top: 170px;
        }
        .about-header .row .container-fluid img{
            position: relative;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 2;
        }
        .about-header .row .container-fluid h1{
            position: relative;
            color: white;
            font-size: 56px;
            z-index: 2;
        }
        .about-title{
            font-size: 50px;
            line-height: 47px;
            font-weight: 200;
        }
        .description{
            margin-top: 100px;
        }
        .description p{
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }
        .press{
            padding-top: 50px;
            margin-top: 70px;
            margin-bottom: -100px;
        }
        .press-card{
           background-color: white;
           border-radius: 5px;
           padding: 30px;
           box-shadow: 0px 0px 25px 0px rgba(0, 0, 0, 0.15);
           height: 370px;
           margin-top:70px;
        }
        .press-card .logo{
            position: relative;
            height: 100px;
            margin: 20px;
            background-color: white;
            border-radius: 5px;
        }
        .press-card .logo img{
            position: absolute;
            left: 50%;
            transform: translateX(-50%);
        }
        .press-card-desc{
            height: 100px;
        }



        .team{
            padding-top: 170px;
            background-color: #F9F9F9;
        }
        .team-photo{

        }
        .team-photo img{
            border-radius: 50%;
            width: 100%;
            height: 100%;
            height: 232px;
            width: 232px;
            object-fit:cover;
        }
        .member-name{
            padding-right: 0px;
            padding-left: 0px;
        }
        .member-name h2{
            margin-top: 30px;
            font-size: 22px !important;
        }
        .member-name p{
            font-size: 14px;
        }

        .about-desc{
            margin-top: 70px;
        }

        .about-desc p{
            padding-top: 25px;
            font-size:25px;
            line-height: 37px;
            color: rgb(0,0,0);
            font-family: "Montserrat";
            font-weight: 300;
        }

        .mobile-app:before{
            content: '';
            background-image: url('{{ asset("images/backgrounds/wrap-bg.png") }}');
            background-size: 100%;
            background-repeat: no-repeat;
            background-position: top center;
            position: absolute;
            z-index: 5;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
        }
        .content-wrapper:before{
            display: none;
            padding-top: 0px !important;
        }
        .offer-assistant{
            padding-top: 0px;
        }
        .content-wrapper .wrap{
            background-image: none;
            padding-top: 100px;
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }


     </style>

@endsection

@section('content')
    <section class="about-header">
        <div class="row" style="">
            <div class="container-fluid text-center">
                <img class="img-responsive" src="{{ asset('images/banner-logo.png')}}" alt="">
                <h1><strong>ABOUT US</strong></h1>
            </div>
        </div>
    </section>
    <div class="row" style="background-color:#cb4544; height: 5px;"></div>
    <section class="description">
       <div class="container">
        <div class="col-md-6">
            <div class="col-md-12">
                <h2 class="about-title"><span>Our <strong style="color: #333333; font-weight: 500">Mission</strong></span></h2>
                <p>Offer To Close is a service that puts homesumers™ (home buyers and sellers) first by providing the guidance and experience of a trusted guide.</p>
                <p>Our mission is to use our knowledge and experience as leaders in real estate, marketing, and technology to be a guide for homesumers through a transaction, by creating tools and services that make a home-buying process simple, transparent, and affordable.</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <h2 class="about-title"><span>What<strong style="color: #333333; font-weight: 500"> We Do</strong></span></h2>
                <p>In an effort to fulfill the mission to make home buying simple and transparent, Offer To Close has built an easy-to-use transaction management software powered not only by technology but also by the best transaction coordinators. In addition</p>
                <p>Offer To Close will launch Android and iOS native applications allowing California home buyers to quickly and simply submit an offer to buy property in a matter of minutes.</p>
            </div>
        </div>
       </div>
    </section>
    <section class="press" style="background-color: #cf5755">
        <div class="container text-center">
            <h2 class="about-title" style="color: white;"><span><strong >Press Releases </strong>/ In the News</span></h2>
            <div class="row press-card-row">
                <div class="col-md-4">
                    <div class="col-md-12 press-card text-center">
                        <div class="logo">
                            <img src="{{ asset('/images/banner-logo.png') }}" alt="">
                        </div>
                         <p>Transaction Coordinator Service, Offer To Close, Hires Industry Veteran</p>
                         <a href="{{route('press.3')}}"><p><span style="color:#cf5756">Read More</span></p></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12 press-card text-center">
                        <div class="logo">
                            <img src="{{ asset('/images/banner-logo.png') }}" alt="">
                        </div>
                         <p>Real Estate Transaction Service Launches New Transaction Timeline from Offer to Close</p>
                         <a href="{{route('press.2')}}"><p><span style="color:#cf5756">Read More</span></p></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12 press-card text-center">
                        <div class="logo">
                            <img src="{{ asset('/images/banner-logo.png') }}" alt="">
                        </div>
                        <div class="col-md-12 press-card-desc text-center">
                             <p>Los Angeles Based Offer To Close Announces Hire Of Leading Transaction Coordinator</p>
                        </div>
                         <a href="{{route('press.1')}}"><p><span style="color:#cf5756">Read More</span></p></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="team" style="background-image: {{asset('images/backgrounds/paper.png')}}">
        <div class="container text-center">
            <h2 class="about-title"><span>The <strong style="color: #333333; font-weight: 500">Team</strong></span></h2>
            <div class="col-md-3">
                <div class="col-md-12 team-photo">
                 <img class="img-responsive" src="{{ asset('/images/photos/James.png') }}" alt="">
                </div>
                <div class="col-md-12 text-center member-name">
                    <h2>James Green</h2>
                    <p>Founder and CEO <br/>833 OFFER-TC ext. 702 <br/><span style="color: #cf5756">jgreen@offertoclose.com</span><p>

                </div>
            </div>
            <div class="col-md-3">
                <div class="col-md-12 team-photo">
                 <img class="img-responsive" src="{{ asset('/images/photos/David.png') }}" alt="">
                </div>
                <div class="col-md-12 text-center member-name">
                    <h2>David Rodriguez</h2>
                    <p>Transaction Coordinator <br/>833 OFFER-TC ext. 701 <br/><span style="color: #cf5756">drodriguez@offertoclose.com</span><p>

                </div>
            </div>
            <div class="col-md-3">
                <div class="col-md-12 team-photo">
                 <img class="img-responsive" src="{{ asset('/images/photos/Cherie.png') }}" alt="">
                </div>
                <div class="col-md-12 text-center member-name">
                    <h2>Cherie Harris-Brazeal</h2>
                    <p>Transaction Coordinator <br/>833 OFFER-TC ext. 703 <br/><span style="color: #cf5756">charris@offertoclose.com</span><p>

                </div>
            </div>

            <div class="col-md-3">
                <div class="col-md-12 team-photo">
                 <img class="img-responsive" src="{{ asset('/images/photos/Marc.png') }}" alt="">
                </div>
                <div class="col-md-12 text-center member-name">
                    <h2>Marc Zev</h2>
                    <p>Director of Software Development<br/><span style="color: #cf5756">mzev@offertoclose.com</span><p>

                </div>
            </div>
        </div>
        <div class="container">
            <div class="about-desc">
                    <div class="row">
                        <div class="tb-item col-md-2 col-sm-12">
                        <img src="{{asset('images/icons/icon-email.png')}}" alt="icon-email">
                    </div>
                    <div class="tb-item col-md-6">
                        <p>Please email <a href="mailto:info@OfferToClose.com" class="regular"><strong style="color: #cb4544;">(info@OfferToClose.com)</strong></a> or call us at <a href="tel:8336333782" class="regular"><span class="tracknumber"><strong style="color: #cb4544;">(833-633-3782)</strong></span></a> with any questions.</p>
                    </div>
                    </div>
            </div>
        </div>

    </section>
    <section class="mobile-app" style="background-image: url({{asset('images/backgrounds/section-bg.jpg')}});">
        <div class="wrap">
        <div class="container">
            <div class="tb-layout">
            <div class="tb-item left">
                <div class="content">
                <div class="titles">
                    <h3 class="sub-title">Download Our Free</h3>
                    <h2 class="main-title">Mobile App</h2>
                </div>
                <p class="description">COMING SOON! STAY TUNED FOR MORE INFO!</p>
                <ul class="list-links">
                    <li>
                    <img src="{{asset('images/appstore-btn.png')}}" alt="Appstore Btn" class="img-responsive">
                    </li>
                    <li>
                        <img src="{{asset('images/google-play-btn.png')}}" alt="Google Play Btn" class="img-responsive">
                    </li>
                </ul>
                </div>
            </div>
            <div class="tb-item right">
                <div class="img-wrapper">
                    <img src="{{asset('images/hand-phone.png')}}" alt="Phone 03" class="img-responsive">
                </div>
            </div>
            </div>
        </div>
        </div>
    </section>

     <section class="content-wrapper offer-assistant">
        <div class="wrap">
            <div class="container">
                <h2 class="title-bar"><span> Offer <strong style="color: #cb4544;">Assistant</strong></span></h2>
                <p>Please provide as much information as you can and one of our transaction <br/> coordinators that is also a licensed agent will put together an offer on your behalf.</p>
                <a href="#" class="btn blue">Go To Agent Details</a>
            </div>
        </div>
    </section>


@endsection