@extends('prelogin.1a.layouts.master')
@section('page_description')
   <meta name="description" content="Offer To Close is a transaction coordinator service and platform that helps to simplify the process of buying and selling real estate by making it more transparent and driven by our proprietary technology. Each member of our team of transaction coordinators are either licensed by the California Bureau of Real Estate as a real estate salesperson or have been certified by the California Association of Realtors as transaction coordinators." />
@endsection
 @section('custom_style')
  <style>
      @media (min-width: 1440px){
        .fp-banner .content .content-wrap{
            display: block;
        }
      }
  </style>
 @endsection
@section('content')
  <section class="fp-banner">
    <div class="cs-slider">
      <div class="slides"
           data-min="1"
           data-max="1"
           data-cs-height="auto"
           data-cs-width="auto"
           data-item-height="auto"
           data-cs-responsive="true"
           data-item-scroll="1"
           data-auto="true"
           data-effects="crossfade"
           data-duration="10000">
        <div class="slider-item" style="background-image: url({{asset('images/backgrounds/banner-bg.jpg')}});">
          <div class="content">
            <div class="container-fluid">
              <div class="content-wrap">
                <img src="{{asset('images/banner-logo.png')}}" alt="banner-logo">
                <h1><strong>BUYING AND SELLING A HOME</strong></h1>
                <p>(and all the paperwork) Made Simpler and More Transparent</p>
                <ul class="list-buttons">
                  <li><a href="{{ route('preLogin.agents.get-started')}}" class="btn blue">Hire a Transaction Coordinator</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="agent-services">
    <div class="container-fluid">
       <h2 class="title-bar"><span>Agent <strong style="color: #cb4544;">Services</strong></span></h2>
       <div class="box-wrap">
         <div class="top">
           <div class="tb-layout">
            <div class="tb-item left">
              <h3 class="title">Why use OfferToClose.com? </h3>
              <p>Our team has nearly 30 years working as REALTORS and transaction coordinators with companies such as Century 21, Exit Platinum Realty, Keller Williams, Pinnacle, Redfin, Berkshire Hathaway, Park Regency, Coldwell Banker and White House Properties.</p>
              <hr>
              <h3 class="title">Our team will</h3>
              <ul class="list-checked">
                <li>Generate and Review Disclosures</li>
                <li>Order and Review Reports</li>
                <li>Review Escrow and Commission Instructions</li>
                <li>Schedule Appointments</li>
              </ul><br>
              <a href="{{ route('preLogin.agents') }}" class="link">And much more</a>
              <ul class="list-buttons">
                <li><a href="{{ route('preLogin.agents') }}" class="btn">LEARN MORE</a></li>
                <li><a href="{{ route('preLogin.agents.get-started') }}" class="btn blue">GET STARTED</a></li>
              </ul>
            </div>
            <div class="tb-item right">
              <div class="r-wrap">
                <img src="{{asset('images/person.png')}}" alt="person">
              </div>
            </div>
           </div>
         </div>
         <div class="bottom">
           <h3>What we can do for you!</h3>
           <p>Our transaction coordinators, paired with our custom proprietary transaction management software help us to protect you, your time, and your clients. <br/>Let us assist you in each of your transactions so you have more time to do what you do best.</p>
           <ul class="list-icons">
             <li>
               <img src="{{asset('images/offers-icons/icon-save-time.png')}}" alt="icon-save-time">
               <h4>Save your time</h4>
             </li>
             <li>
               <img src="{{asset('images/offers-icons/icon-meet-broker.png')}}" alt="icon-meet-broker">
               <h4>Meet broker <br>requirements</h4>
             </li>
             <li>
               <img src="{{asset('images/offers-icons/icon-get-paid.png')}}" alt="icon-get-paid">
               <h4>Get paid on time</h4>
             </li>
             <li>
               <img src="{{asset('images/offers-icons/icon-reduce-liability.png')}}" alt="icon-reduce-liability">
               <h4>Reduce liablity</h4>
             </li>
             <li>
               <img src="{{asset('images/offers-icons/icon-access-file.png')}}" alt="icon-access-file">
               <h4>access file <br>24/7</h4>
             </li>
             <li>
               <img src="{{asset('images/offers-icons/icon-help-get-offers.png')}}" alt="icon-help-get-offers">
               <h4>help get offers <br>to close</h4>
             </li>
             <li>
               <img src="{{asset('images/offers-icons/icon-monitor-dates.png')}}" alt="icon-monitor-dates">
               <h4>monitor dates and <br>deadlines</h4>
             </li>
             <li>
               <img src="{{asset('images/offers-icons/icon-keep-you.png')}}" alt="icon-keep-you">
               <h4>keep you in <br>the know</h4>
             </li>
           </ul>
         </div>
       </div>
    </div>
  </section>
  <section class="content-wrapper">
    <div class="wrap">
      <div class="container">
       <h2 class="title-bar"><span>For Sale <strong style="color: #cb4544;">By Owner</strong></span></h2>
       <h3 class="title">Selling your home?</h3>
       <p class="caption">You've got this</p>
       <div class="packages-list">
         <div class="tb-layout">
           <div class="tb-item left">
             <div class="box-wrap">
               <div class="box-top">
                 <ul class="list-details">
                   <li class="left"><img src="{{asset('images/icon-three-star.png')}}" alt="three-star"></li>
                   <li class="center">Basic</li>
                   <li class="right">For Sale By Owner</li>
                 </ul>
               </div>
               <div class="box-bottom">
                 <ul class="list-checked">
                    <li>Help with All Paperwork</li>
                    <li>Call Support from Our Team of <br>Licensed Agents</li>
                    <li>Transaction Coordinators</li>
                    <li>Open Escrow and Work with Title</li>
                    <li>Provide Timelines and Manage the Process</li>
                    <li>Loan Contingency Follow Up</li>
                 </ul>
               </div>
             </div>
             <div class="box-wrap">
               <div class="box-top" style="background-color: #818181;">
                 <ul class="list-details">
                   <li class="left"><img src="{{asset('images/icon-three-star.png')}}" alt="three-star"></li>
                   <li class="center">Custom</li>
                   <li class="right">For Sale By Owner</li>
                 </ul>
               </div>
               <div class="box-bottom">
                 <ul class="list-checked">
                   <li>For a customized proposal to meet your specific needs, call our team at (833) OFFER-TC</li>
                 </ul>
               </div>
             </div>
           </div>
           <div class="tb-item right">
             <div class="box-wrap">
               <div class="box-top" style="background-color: #cb4544;">
                 <ul class="list-details">
                   <li class="left five"><img src="{{asset('images/con-five-star.png')}}" alt="five-star"></li>
                   <li class="center">Advanced</li>
                   <li class="right">For Sale By Owner</li>
                 </ul>
               </div>
               <div class="box-bottom">
                 <ul class="list-checked">
                   <li>Help with All Paperwork</li>
                   <li>Call Support from Our Team of Licensed Agents <br>and Transaction Coordinators</li>
                   <li>Open Escrow and Work with Title</li>
                   <li>Provide Timelines and Manage the Process</li>
                   <li>Loan Contingency Follow Up</li>
                   <li>Review Offers and Help Qualify Buyers</li>
                   <li>Help With Open Houses</li>
                   <li>Recommend Service Providers</li>
                   <li>List Your Home on the MLS (including Zillow, <br>Trulia, Redfin.com, and Realtor.com)</li>
                   <li>Comparative Market Analysis</li>
                 </ul>
               </div>
             </div>
           </div>
         </div>
       </div>
       <ul class="list-buttons">
         <li><a href="{{ route('preLogin.fsbo')}}" class="btn">LEARN MORE</a></li>
         <li><a href="{{ route('preLogin.fsbo.get-started')}}" class="btn blue">GET STARTED</a></li>
       </ul>
      </div>
    </div>
  </section>
  <section class="services-listing">
    <div class="container">
      <div class="top">
        <h2 class="title">MLS Listing Service</h2>
        <p class="caption">MLS is a real estate technologies that make the transaction more efficient. Real estate brokers regularly gathered at the offices of their local associations to share information about properties they were trying to sell.</p>
        <ul class="list-logo-box">
          <li>
            <div class="item-wrap">
              <div class="item-top">
               <img src="{{asset('images/mls-logo/logo-realtor.png')}}" alt="logo-realtor">
              </div>
              <div class="item-bottom">
                realtor.com
              </div>
            </div>
          </li>
          <li>
            <div class="item-wrap">
              <div class="item-top">
                <img src="{{asset('images/mls-logo/logo-redfin.png')}}" alt="logo-redfin">
              </div>
              <div class="item-bottom">
                redfin.com
              </div>
            </div>
          </li>
          <li>
            <div class="item-wrap">
              <div class="item-top">
                <img src="{{asset('images/mls-logo/logo-zillow.png')}}" alt="logo-zillow">
              </div>
              <div class="item-bottom">
                zillow.com
              </div>
            </div>
          </li>
          <li>
            <div class="item-wrap">
              <div class="item-top">
                <img src="{{asset('images/mls-logo/logo-trulia.png')}}" alt="logo-trulia">
              </div>
              <div class="item-bottom">
                trulia.com
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div class="bottom">
        <div class="tb-layout">
          <div class="tb-item left">
            <img src="{{asset('images/icons/icon-email.png')}}" alt="icon-email">
          </div>
          <div class="tb-item center">
            <p>Please email <a href="mailto:info@OfferToClose.com" class="regular">(info@OfferToClose.com)</a> or call us at <a href="tel:8336333782" class="regular"><span class="tracknumber">(833-633-3782)</span></a> with any questions.</p>
          </div>
          <div class="tb-item right">
            <a href="{{ route('preLogin.mls') }}" class="btn blue">VIEW MLS LISTING OPTIONS</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="mobile-app" style="background-image: url({{ asset('images/backgrounds/section-bg.jpg') }});">
    <div class="wrap">
      <div class="container">
        <div class="tb-layout">
          <div class="tb-item left">
            <div class="content">
              <div class="titles">
                <h3 class="sub-title">Download Our Free</h3>
                <h2 class="main-title">Mobile App</h2>
              </div>
              <p class="description">COMING SOON! STAY TUNED FOR MORE INFO!</p>
              <ul class="list-links">
                <li>
                  <img src="{{asset('images/appstore-btn.png')}}" alt="Appstore Btn" class="img-responsive">
                </li>
                <li>
                    <img src="{{asset('images/google-play-btn.png')}}" alt="Google Play Btn" class="img-responsive">
                </li>
              </ul>
            </div>
          </div>
          <div class="tb-item right">
            <div class="img-wrapper">
              <img src="{{asset('images/hand-phone.png')}}" alt="Phone 03" class="img-responsive">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="offer-assistant">
    <div class="container">
      <div class="content">
        <h2 class="title-bar"><span>Offer <strong style="color: #cb4544;">Assistant</strong></span></h2>
        <p class="caption">Please provide as much information as you can and one of our transaction coordinators <br>that is also a licensed agent will put together an offer on your behalf.</p>
        <a href="https://www.offertoclose.com/offer-assistant/" class="btn blue">Go To Offer Assistant</a>
      </div>
    </div>
  </div>
@endsection