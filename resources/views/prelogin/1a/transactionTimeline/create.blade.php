@extends('prelogin.1a.layouts.master')
@section('custom_style')
    <style>
        body{
            font-family: "Montserrat";
        }
        ::-webkit-input-placeholder {
            font-style: italic;
        }
        :-moz-placeholder {
            font-style: italic;
        }
        ::-moz-placeholder {
            font-style: italic;
        }
        :-ms-input-placeholder {
            font-style: italic;
        }
        .mt-50{
            margin-top: 50px;
        }

        .cal_table input[type=date]::-webkit-inner-spin-button, .cal_table input[type=date]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            display: none;
            margin: 0;
        }
        .cal_table input[type=date]{
            border: none;
            text-transform: uppercase;
            color: #E46A70;
        }

        .timeline-header .row{
            height: 350px;
            position: relative;
            background-image: url('{{ asset("images/backgrounds/timeline_header_bg.png") }}');
            background-repeat: no-repeat;
            background-size: cover;

        }
        .timeline-header .row:after{
            content: "";
            position: absolute;
            top:0;
            left:0;
            right:0;
            bottom:0;
            display: block;
            z-index: 1;
            background:rgba(0, 0, 0, 0.7);
        }
        .timeline-header .row .container-fluid{
            margin-top: 120px;
        }
        .timeline-header .row .container-fluid img{
            position: relative;
            left: 50%;
            transform: translate(-50%, -50%);
            z-index: 2;
        }
        .timeline-header .row .container-fluid h1{
            position: relative;
            color: white;
            font-size: 35px;
            z-index: 2;
        }
        .timeline-header .row .container-fluid .header_button{
            position: relative;
            color: white;
            z-index: 2;
        }

        .timeline-description{
            padding-top: 50px;
        }
        .timeline-description p{
            font-size: 20px;
            letter-spacing: 0px;
            line-height: 35px;
            color: rgb(26,26,26);
            font-family: "Montserrat";
            font-weight: 300;
            text-align: center;
        }
        .address-box{
            position: relative;
            padding-left: 10%;
            height: 105px;
            border-radius: 5px;
            box-shadow: 0px 2px 9px rgba(0,0,0,0.15);
            background-color: #ffffff;
            border: 1px solid #f1f1f1;
            margin-bottom: 80px;
        }
        .o-box{
            margin-top: 30px;
            position: relative;
            box-shadow: 0px 2px 9px rgba(0,0,0,0.15);
            background-color: #ffffff;
            border: 1px solid #f1f1f1;
            padding-top: 20px;
            padding-bottom: 20px;
            border-radius: 6px;
        }
        .add-milestone{
            background-color: #00C5BA;
            margin-top: 80px;
            height: 80px;
            border-radius: 5px;
            border: 1px solid #f1f1f1;
            margin-bottom: 80px;
        }
        .add-milestone .add-milestone-title{
            padding-left:20px;
            margin-top: auto;
            margin-bottom: auto;
            font-size: 20px;
            line-height: 80px;
            color: rgb(255,255,255);
            font-family: "Montserrat";
            font-weight: 300;
        }
        .add-milestone input{
            width: 50%;
            height: 40px;
            padding-left: 10px;
            margin-right: 10px;
        }
        .add-milestone .add-icon{
            position: absolute;
            top: 55%;
            color: white;
            font-size: 27px;
            transform: translateY(-50%)
        }
        .address{
            width: 100%;
            position: absolute;
            left: 0;
            top: 50%;
            transform: translateY(-50%);
            padding-left: 20%;
            border: none;
            box-shadow: none;
            resize: none;
            font-size: 20px;
            color: rgb(26,26,26);
            font-family: "Montserrat";
            font-weight: 300;
        }
        .address:hover{
            box-shadow: none;
        }
        .loc-icon{
            position: absolute;
            left: 7%;
            top: 50%;
            transform: translate(-50%, -50%);

        }
        .cal_table tr{
            height: 60px;
        }
        .cal_table tr td{
            margin-left: 30px;
        }

        .cal_table tr:not(first-child):hover{
            box-shadow: 0px 2px 9px rgba(0,0,0,0.15);
            border-radius: 5px;
            cursor: pointer;
        }
        .cal_table .title-bar span {
            padding-left: 15px;
            font-size: 18px;
            font-weight: 600;
        }
        .cal_table .title-bar{
            margin-bottom: 0;
        }
        .cal_table thead tr{
            height: 100px;
        }
        .cal_table tr td:first-child {
            padding-left:50px;
            color: #7E7E7E;
        }
        .cal_table thead tr th:first-child {
            padding-left:50px;
        }
        .days{
            width: 60px;
            border-color: #F4F4F4;
            text-align: center;
            color: #A8A8A9;
        }
        .freq{
            padding-left: 10px;
            color: #E46A70;
        }
        .add-email{
            font-size: 20px;
            color: #00CFC3;
            margin-top: 30px;
        }
        .message{
            font-size: 20px;
            font-weight: 300;
            margin-top: auto;
            margin-bottom: auto;
            padding-left: 10%;
            padding-right: 10%;
            width: 100%;
            border: none;
            resize: none;
            height: 120px;
        }


        .offer-assistant .wrap{
            padding-top: 0px;
            background-color: white;
            background-image: none;
            font-size: 18px;
            line-height: 30px;
            color: rgb(69,69,69);
            font-weight: 300;
        }
        .mobile-app{
            margin-top: 100px;
        }



    </style>

@endsection

@section('content')
    <section class="timeline-header">
        <div class="row" style="">
            <div class="container-fluid text-center">
                <img class="img-responsive" src="{{ asset('images/banner-logo.png')}}" alt="">
                <h1><strong>TRANSACTION TIMELINE</strong></h1>
            </div>
        </div>
    </section>
    <div class="row" style="background-color:#cb4544; height: 5px;"></div>

    <section class="timeline-description">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12 col-sm-12 col-md-offset-2 text-left">
                    <p class="text-muted">To calculate key dates and deadlines enter the date the official date the contract was accepted. We’re included the default period for each of the key dates but if you or your client have agreed to non-standard terms, you can change the number of days from acceptance to have the dates updated to reflect the terms of the residential purchase agreement.</p>
                </div>
            </div>
        </div>
    </section>

    <section>
        <form action="{{ route('timeline.store') }}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="container">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <div class="alert alert-{{ $msg }} alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            </button>
                            {{ Session::get('alert-' . $msg) }}
                        </div>
                    @endif
                @endforeach
                @if( $errors->any() )
                    <div class="errors alert alert-danger" >
                        <ul>
                            @foreach( $errors->all() as $error )
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 col-sm-10 col-xs-offset-1 col-xs-10 col-sm-offset-1 address-box">
                        <textarea class="address"  autocomplete="gkrjeot3u90tu30tjoifjpt09409" name="property_address" id="" placeholder="{{ old('property_address') ? old('property_address'): 'Insert Property address' }}">{{ old('property_address') ? old('property_address'):"" }}</textarea>
                        <img  src="{{ asset('images//icons/loc-icon.png')}}" class="loc-icon" alt="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12  col-sm-12">
                        <table class="col-md-12 cal_table">
                            <thead>
                            <tr>
                                <th class=""style="width: 40%"><h3 class="title-bar"><span>KEY <strong style="color: #cb4544;">MILESTONES</strong></span></h3></th>
                                <th class=""style="width: 20%"><h3 class="title-bar"><span>DATE</span></h3></th>
                                <th class=""style="width: 40%"><h3 class="title-bar"><span><strong style="color: #cb4544;">DAYS </strong>AFTER ACCEPTANCE</span></h3></th>
                            </tr>
                            </thead>
                            <tr>
                                <td>Mutual Acceptance:</td>
                                <td class="text-left"><input class="origin_date" name="mutual_acceptance_date"  id="mutual_acceptance_date" type="date" value="{{ old('mutual_acceptance_date') ? old('mutual_acceptance_date') : date("Y-m-d") }}">
                                <td></td>
                            </tr>

                            <tr>
                                <td>Deposit Due:</td>
                                <td class="text-left"><input class="date_feild" name="deposit_due_date" type="date" value="{{ old('deposit_due_date') }}"></td>
                                <td class="text-left"><input type="number" name="deposit_due_days" class="days" value="{{ old('deposit_due_days') }}"><span class="freq">days</span></td>
                            </tr>

                            <tr>
                                <td>Disclosures Due:</td>
                                <td class="text-left"><input class="date_feild" name="disclosures_due_date" type="date" value="{{ old('disclosures_due_date') }}"></td>
                                <td class="text-left"><input type="number" name="disclosures_due_days" class="days" value="{{ old('disclosures_due_days') }}"><span class="freq">days</span></td>

                            </tr>

                            <tr>
                                <td>Inspection Contingency:</td>
                                <td class="text-left"><input class="date_feild" name="inspection_contingency" type="date" value="{{ old('inspection_contingency') }}"></td>
                                <td class="text-left"><input type="number"  name="inspection_contingency_days"class="days" value="{{ old('inspection_contingency_days') }}"><span class="freq">days</span></td>
                            </tr>

                            <tr>
                                <td>Appraisal Contingency:</td>
                                <td class="text-left"><input class="date_feild" name="appraisal_contingency"  type="date" value="{{ old('appraisal_contingency') }}"></td>
                                <td class="text-leftr"><input type="number"  name="appraisal_contingency_days" class="days" value="{{ old('appraisal_contingency_days') }}"><span class="freq">days</span></td>
                            </tr>

                            <tr>
                                <td>Loan Contingency:</td>
                                <td class="text-left"><input name="loan_contingency" class="date_feild"  type="date" value="{{ old('loan_contingency') }}"></td>
                                <td class="text-left"><input type="number" name="loan_contingency_days" type=number"  class="days" value="{{ old('loan_contingency_days') }}"><span class="freq">days</span></td>
                            </tr>

                            <tr>
                                <td>Close of Escrow:</td>
                                <td class="text-left"><input class="date_feild" name="close_of_escrow_date" type="date" value="{{ old('close_of_escrow_date') }}"></td>
                                <td class="text-left"><input type="number" name="close_of_escrow_days" class="days" value="{{ old('close_of_escrow_days') }}"><span class="freq">days</span></td>
                            </tr>

                            <tr>
                                <td>Possession on the Property:</td>
                                <td class="text-left"><input class="date_feild" name="possession_on_property"  type="date" value="{{ old('possession_on_property') }}"></td>
                                <td class="text-cleft"><input type="number"  name="possession_on_property_days" class="days" value="{{ old('possession_on_property_days') }}"><span class="freq">days</span></td>
                            </tr>
                        </table>
                        <!-- <div class="row">
                            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12 text-center add-milestone">
                                <span class="add-milestone-title">Add another Key milestone</h4></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                <input placeholder="Key milestone name" type="text">&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href="#"><span class="add-icon"><i class="fas fa-plus"></i></span></a>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
    </section>

    <section>
        <div class="row">
            <div class="container">
                <div class="col-md-12 o-box">
                    <div class="row no-gutters email_section">
                        <div class="col-md-3"></div>
                        <div class="input-group col-md-6">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-icon text-muted" id="basic-addon1"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                            </div>
                            <input type="email" class="form-control email_input" name="sender_email" required placeholder="Email of Agent" value="{{ old('sender_email') }}">
                        </div>
                    </div>

                    <div class="row no-gutters">
                        <div class="col-md-3"></div>
                        <div class="input-group col-md-6">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-icon text-muted" id="basic-addon1"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                            </div>
                            <input type="email" class="form-control email_input" name="reciever_email" required placeholder="Email of Client" value="">
                            <div class="input-group-prepend">
                                <span class="input-group-text input-icon text-muted" id="basic-addon1"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row no-gutters">
                        <div class="col-md-3"></div>
                        <div class="col-md-6 add-email text-center">
                            <a class="" > Add another email </a>
                        </div>
                    </div> -->
                </div>

                <!-- <div class="col-md-12 text-center o-box" style="margin-top: 50px">
                    <textarea placeholder="Type your message here" class="message"></textarea>
                </div> -->
                <div class="col-md-12 text-center mt-50">
                    <button class="btn btn-info submit_button" type="submit"> SEND AND SAVE </button>
                </div>
                </form>
            </div>
        </div>
    </section>

    <section class="mobile-app" style="background-image: url({{ asset('images/backgrounds/section-bg.jpg') }});">
        <div class="wrap">
            <div class="container">
                <div class="tb-layout">
                    <div class="tb-item left">
                        <div class="content">
                            <div class="titles">
                                <h3 class="sub-title">Download Our Free</h3>
                                <h2 class="main-title">Mobile App</h2>
                            </div>
                            <p class="description">COMING SOON! STAY TUNED FOR MORE INFO!</p>
                            <ul class="list-links">
                                <li>
                                    <img src="{{asset('images/appstore-btn.png')}}" alt="Appstore Btn" class="img-responsive">
                                </li>
                                <li>
                                    <img src="{{asset('images/google-play-btn.png')}}" alt="Google Play Btn" class="img-responsive">
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tb-item right">
                        <div class="img-wrapper">
                            <img src="{{asset('images/hand-phone.png')}}" alt="Phone 03" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content-wrapper offer-assistant">
        <div class="wrap">
            <div class="container">
                <h2 class="title-bar"><span> Offer <strong style="color: #cb4544;">Assistant</strong></span></h2>
                <p>Please provide as much information as you can and one of our transaction <br/> coordinators that is also a licensed agent will put together an offer on your behalf.</p>
                <a href="https://www.offertoclose.com/offer-assistant" class="btn blue">Go To Offer Assistant</a>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js"></script>
    <script>
        $(document).ready(function() {
            $(document).on('change input', '.date_feild', function(){
                var el = $(this);
                var end_date = el.val();
                var start_date = $('#mutual_acceptance_date').val();
                var days_input = el.parent().siblings().find('.days')
                var from = moment(start_date, 'YYYY-MM-DD')
                var to = moment(end_date, 'YYYY-MM-DD');
                if( !(start_date == "" && end_date == "") ){
                    var duration = to.diff(from, 'days')
                    days_input.val(duration);
                    if( isNaN(days_input.val()) ){
                        days_input.val(" ");
                    }

                }
            });

            $(document).on('change input', '.days', function(){
                var el = $(this);
                var start_date = $('#mutual_acceptance_date').val();
                var origin_date = moment(start_date, 'YYYY-MM-DD');
                var end_date = origin_date.add(el.val(), 'days').format('YYYY-MM-DD');
                var date_input = el.parent().siblings().find('.date_feild');
                date_input.val(end_date);
                if( isNaN(el.val() )){
                    el.val(" ");
                }
            });

            $(document).on('change input', '#mutual_acceptance_date', function(){
                $('.date_feild').each(function() {
                    $(this).change();
                });
            });
            $('#mutual_acceptance_date').trigger('change');
            //hiding label for maintainence day field
            $('td:nth-of-type(3):first').css('display', 'none')

        });
    </script>
@endsection