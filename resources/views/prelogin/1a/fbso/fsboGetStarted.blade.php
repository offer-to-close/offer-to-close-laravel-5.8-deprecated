@extends('prelogin.1a.layouts.master');

@section("custom_style");
<style>
.red-line{
    height: 5px;
    background-color:#D15D5C;
    margin-bottom: 70px;
    width: 100%;
}
.form-section{
    background-color: #F9F9F9;
}
.mobile-app{
  margin-top:-23px;
}
.mobile-app:before{
    content: '';
    background-image: url('{{ asset("images/backgrounds/wrap-bg.png") }}');
    background-size: 100%;
    background-repeat: no-repeat;
    background-position: top center;
    position: absolute;
    z-index: 5;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    }
</style>
@endsection

@section("content");
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-sm-push-2 col-sm-pop-3">
            <div class="col-sm-7 ">
                <img src="{{asset('images/Layer_33.png')}}">
            </div>
        </div>
    </div>
</div>
<div class="col-sm-12 red-line"></div>

@include('prelogin.1a.forms._form_fsboGetStarted');

<section class="mobile-app" style="background-image: url({{asset('images/backgrounds/section-bg.jpg')}});">
    <div class="wrap">
      <div class="container">
        <div class="tb-layout">
          <div class="tb-item left">
            <div class="content">
              <div class="titles">
                <h3 class="sub-title">Download Our Free</h3>
                <h2 class="main-title">Mobile App</h2>
              </div>
                <p class="description">COMING SOON! STAY TUNED FOR MORE INFO!</p>
                <ul class="list-links">
                <li>
                  <a href="#"><img src="{{asset('images/appstore-btn.png')}}" alt="Appstore Btn" class="img-responsive"></a>
                </li>
                <li>
                  <a href="#">
                    <img src="{{asset('images/google-play-btn.png')}}" alt="Google Play Btn" class="img-responsive">
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="tb-item right">
            <div class="img-wrapper">
              <img src="{{asset('images/hand-phone.png')}}" alt="Phone 03" class="img-responsive">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="offer-assistant">
        <div class="container">
          <div class="content">
            <h2 class="title-bar"><span>Offer <strong style="color: #cb4544;">Assistant</strong></span></h2>
            <p class="caption">Please provide as much information as you can and one of our transaction coordinators <br>that is also a licensed agent will put together an offer on your behalf.</p>
            <a href="#" class="btn blue">Go To Agent Details</a>
          </div>
        </div>
      </div>
@endsection

@section("scripts");
<script src="https://services.cognitoforms.com/scripts/embed.js"></script>
@endsection
