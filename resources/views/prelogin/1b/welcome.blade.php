@extends('prelogin.1b.layouts.master')
@section('page_title')
Transaction Management Software and Coordinator Service Made By and For Real Estate Agents
@endsection
@section('page_keywords')
Transaction Management Software,Transaction Coordinators,TC,real estate assistant,offer to close,offertoclose.com,real estate transaction software
@endsection
@section('page_description')
Offer To Close is a transaction management solution merging first-class technology with qualified, experienced real estate assistants. We simplify the process of buying and selling real estate by making it more transparent.
@endsection
@section('content')
<div class="container-fluid " id="banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="{{asset('images/prelogin/laptop.png')}}" class="img-responsive">
            </div>
            <div class="col-sm-6" style="padding-left: 40px;">
                <h2>Real Estate Contracts and <br>Disclosures Made Simple</h2>
                <div class="search-container">
                    <a href="{{ route('register') }}?c=beta"><button class="request_beta_submit_button- ladda-button" data-style="expand-right" id="submit"><small style="text-transform: uppercase">Sign up for Beta Release Access</small></button></a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid services" id="welcome">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 service">
                <div class="row-content">
                    <img src="{{asset('images/prelogin/icon1.png')}}" class="img-responsive">
                    <h4>Custom Tasks and Timeline:</h4>
                    <p>No more one-size-fits-all checklists and timeliness. We build a custom list of tasks/timelines and give you full access to add, delete, or assign new tasks to someone else in the transaction.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 service">
                <div class="row-content">
                    <img src="{{asset('images/prelogin/icon2.png')}}" class="img-responsive">
                    <h4>Simplified Disclosures:</h4>
                    <p>We make completing documents such as your Agent Visual Inspection Disclosure (<a class="styled-link" href="https://www.offertoclose.com/blog/agent-visual-inspection-disclosure-avid/">AVID</a>), Transfer Disclosure Statement (<a class="styled-link" href="https://www.offertoclose.com/blog/what-is-the-transfer-disclosure-statement/">TDS</a>), and Seller Property Questionnaire (<a class="styled-link" href="https://www.offertoclose.com/blog/seller-property-questionnaire-spq/">SPQ</a>) simple and straightforward.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 service">
                <div class="row-content">
                    <img src="{{asset('images/prelogin/icon3.png')}}" class="img-responsive">
                    <h4>Document Lists:</h4>
                    <p>As each transaction is opened, our customized system determines which documents you'll need to complete to help you meet your legal disclosure obligations.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 service">
                <div class="row-content">
                    <img src="{{asset('images/prelogin/icon4.png')}}" class="img-responsive">
                    <h4>Experience:</h4>
                    <p>Behind every transaction is a qualified experienced transaction coordinator. If you have questions, you can<mark style="color: #ca4544; background-color: white;"><a class="styled-link" href="tel:833-633-3782">call us</a></mark>and one of our transaction coordinators will help answer your or your client's questions.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container brands">
    <img src="{{asset('images/prelogin/brands.png')}}" class="img-responsive">
</div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click', '.request_beta_submit_button', function(){
                let betaDescription = $('.beta-description-text');
                let l = Ladda.create(this);
                l.start();
                $.ajax({
                    url: '{{route('requestBetaEmail')}}',
                    method: 'POST',
                    data: {
                        _token: '{{csrf_token()}}',
                        email: $('#request_beta_email_input').val(),
                    },
                    success: function(r){
                        setTimeout(function() {l.stop();},1000);
                        if(r.status === 'success')
                        {
                            betaDescription.html(r.message);
                        }
                        else if(r.status === 'fail')
                        {
                            betaDescription.html(r.message);
                        }
                    },
                    error: function(err){
                        setTimeout(function() {l.stop();},1000);
                        betaDescription.html('AN ERROR HAS OCCURRED PLEASE TRY AGAIN.');
                    },
                });
            });
        });
    </script>
@endsection