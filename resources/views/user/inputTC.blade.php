@extends('layouts.app')

@section('content')
    @if(Request::is('ta/*'))
        @include('transaction._menubar')
    @endif

    <?php
        $route = route('assignRole.TC');
    Log::debug([__FILE__=> __LINE__, 'route for pickPerson'=>$route, 'screenMode'=>$_screenMode,]);
    ?>
    @include('user.subForms._listPickPerson',['action'=>$route, '_role'=>$_role, 'callingForm'=>'tc',])

    <div class="mc-wrapper">
        <div class="mc-heading">

            @if (@$action == 'saved' || session('data_saved'))
                @include('user._successMessageIcon')
            @endif

            <h1>
                @if($_screenMode == 'create')
                    Create New
                @elseif($_screenMode == 'edit')
                    Edit
                @else
                    View
                @endif
                {{ $_role['display'] }}
            </h1>
        </div>

        <!-- Show errors if they exist -->
        @if ($errors->any() && !$errors->has('_pickedID'))
            <div class="alert alert-danger alert-dismissable text-center">
                <a href="#" class="close" data-dismiss="alert" alert-label="close">&times;</a>
                There was an error submitting your form. Kindly review the errors below and resubmit your form
                <!-- {!! var_dump($errors) !!} -->
            </div>
        @endif

    <!-- Create new agent form -->
        <?php
            $dsp = str_replace([' ','\''], null, $_role['display']);
            if(empty($data['Transactions_ID'])) $action = 'saveRole.' . $dsp;
            else $action = 'save' . $dsp;

            $action = route($action);
        ?>

        <div class="mc-box-groups user new">
            <form action="{{ $action }}" method="POST">
                {{ csrf_field() }}

                <input type="hidden" name="_screenMode" value="{{ (!empty($_screenMode) ? $_screenMode : '') }}">
                <input type="hidden" name="_roleCode" value="{{ (!empty($_role['code']) ? $_role['code'] : '') }}">
                <input type="hidden" name="_roleDisplay"
                       value="{{ (!empty($_role['display']) ? $_role['display'] : '') }}">

                <input type="hidden" name="Transactions_ID" value="{{ (!empty($data['Transactions_ID']) ? $data['Transactions_ID'] : '') }}">
                <input type="hidden" name="Users_ID" value="{{ (!empty($data['Users_ID']) ? $data['Users_ID'] : '') }}">
                <input type="hidden" name="ID" value="{{ (!empty($data['ID']) ? $data['ID'] : '') }}">

                <div class="col-width-2">
                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-user"></i> PERSONAL DETAILS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._personalDetails', ['nameType' => 'license', 'callingForm'=>'tc'])

                    </div>


                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-comments"></i>CONTACT DETAILS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._contactDetails', ['callingForm'=>'tc',])

                    </div>
                </div>
                <div class="col-width-2">

                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="fas fa-home"></i>ADDRESS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._address', ['callingForm'=>'tc',])

                    </div>

                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-file-alt"></i>NOTES</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._notes', ['callingForm'=>'tc',])

                    </div>

                </div>

                <div class="clearfix"></div>
                <div class="ctrls">
                    <button class="btn red">SAVE</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    @include('scripts.phoneMask')
    @include('scripts.disableAutofill')
@append