@extends('layouts.app')

@section('content')

<div class="mc-wrapper">
     @if(Request::is('ta/*'))
        @include('transaction._menubar')
    @endif
    <div class="mc-heading">
        <h1>Create New User</h1>
       
    </div>

    <div class="mc-box-groups user new">
        <form class="" method="POST" action="{{ route('login') }}">
            <div class="col-width-2">
                <div class="mc-box">
                    <div class="mc-box-heading no-other">
                        <h2><i class="far fa-user"></i>PERSONAL DETAILS</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-groups">
                        <div class="form-group">
                             <input class="form-control first-name" type="text" placeholder="First Name">
                             <div class="errorMsg">Please enter Name</div>
                        </div>
                        <div class="form-group">
                            <input class="form-control last-name" type="text" placeholder="Last Name">
                        </div>
                        <div class="form-group">
                            <input class="form-control full-name" type="text" placeholder="Name to appear on documents">
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Role">
                        </div>
                    </div>

                </div>

                <div class="mc-box">
                     <div class="mc-box-heading no-other">
                        <h2><i class="far fa-comments"></i>CONTACT DETAILS</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="form-groups">
                        <div class="form-group with-label">
                            <label class="label-control">Primary Phone <span class="important">*</span></label>
                            <input class="form-control masked-phone" type="text" placeholder="(###) ###-#####">
                        </div>
                        <div class="form-group with-label">
                            <label class="label-control">Secondary Phone <span class="important">*</span></label>
                            <input class="form-control masked-phone" type="text" placeholder="(###) ###-#####">
                        </div>
                        <div class="form-group with-label">
                            <label class="label-control">Email <span class="important">*</span></label>
                            <input class="form-control" type="email" placeholder="name@email.com">
                        </div>
                         <div class="form-group with-label">
                            <label class="label-control">Email Format <span class="important">*</span></label>
                            <label class="checkbox"><input name="radio-email-format" class="form-control" type="radio" value="html"> <span>HTML</span></label>
                            <label class="checkbox"><input name="radio-email-format" class="form-control" type="radio" value="text"> <span>Text</span></label>
                        </div>
                        <div class="form-group with-label">
                            <label class="label-control">Send Texts To <span class="important">*</span></label>
                            <input class="form-control masked-phone" type="text" placeholder="(###) ###-#####">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-width-2">

                <div class="mc-box">
                    <div class="mc-box-heading no-other">
                        <h2><i class="fas fa-home"></i>ADDRESS</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="form-groups">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Street 1">
                        </div>
                        <div class="form-groups-wrapper">
                            <div class="form-group w70">
                                <input class="form-control" type="text" placeholder="Street 2">
                            </div><div class="form-group w30">
                                <input class="form-control" type="text" placeholder="Unit #">
                            </div>
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="City">
                        </div>
                        <div class="form-groups-wrapper">
                            <div class="form-group w50">
                                <select><option value="us">State</option></select>
                            </div><div class="form-group w50">
                                <input class="form-control" type="text" placeholder="Zip Code">
                            </div>
                        </div>
                       
                    </div>
                </div>

                <div class="mc-box">
                    <div class="mc-box-heading no-other">
                        <h2><i class="far fa-file-alt"></i>NOTES</h2>
                         <div class="clearfix"></div>
                    </div>
                    <div class="form-groups">
                        <div class="form-group no-label">
                            <textarea class="form-control" type="text"></textarea>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="ctrls">
                <a class="btn red">SAVE</a>
            </div>
        </form>
    </div>
   

    </div>
</div>
@endsection


@section('scripts')
<script>
$(document).ready(function(){
    $('.masked-phone').mask('(###)-000-0000');
});
</script>

@endsection