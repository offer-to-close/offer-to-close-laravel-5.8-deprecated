<div class="form-groups">
<?php
$sameSide = $sameSide ?? '';
$textFields = [
    ['col' => 'Street1', 'label' => 'Street 1', 'default' => null, 'required' => $sameSide,],
    ['col' => 'Street2', 'label' => 'Street 2', 'default' => null, 'required' => false,],
    ['col' => 'Unit', 'label' => 'Unit #', 'default' => null, 'required' => false,],
    ['col' => 'City', 'label' => 'City', 'default' => null, 'required' => $sameSide,],
];

foreach($textFields as $field)
{
    $col   = $field['col'];
    if (!array_key_exists($col, $data)) continue;

    $label = $field['label'];
    $ph = $field['placeholder'] ?? $label;
    $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
    $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
    ?>

    <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
        <label for="{{$col}}" class="label-control"> {{$label}} {!! $star !!}</label>
        <input name="{{$col}}" class="form-control full-name" type="text"
               placeholder="{{$ph}}" value="{{ $default }}">
        @if($errors->has($col))
            <span class="help-block">
                <strong>{{ $errors->first($col) }}</strong>
            </span>
        @endif
    </div>
<?php   } ?>

<?php
    $default = old('State') ?? ($data['State'] ?? 'CA');
    $star = ($sameSide) ? '<span class="important">*</span>' : null;
?>
<div class="form-group{{ $errors->has('State') ? ' has-error' : '' }} with-label">
    <label for="State" class="label-control">State {!! $star !!}</label>
    <select name="State">
        {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($default, Config::get('otc.states'), 'index', 'value') !!}
    </select>
    @if($errors->has('State'))
        <span class="help-block"><strong>{{ $errors->first('State') }}</strong></span>
    @endif
</div>


<?php
$textFields = [
['col'=>'Zip', 'label'=>'Zip Code', 'default'=>null, 'required'=>$sameSide,],
];

foreach($textFields as $field)
{
    $col   = $field['col'];
    if (!array_key_exists($col, $data)) continue;

    $label = $field['label'];
    $ph = $field['placeholder'] ?? $label;
    $default = old($col) ?? ($data[$col] ?? ($field['default'] ?? null));
    $star = ($field['required'] ?? false) ? '<span class="important">*</span>' : null;
    ?>

    <div class="form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
        <label for="{{$col}}" class="label-control"> {{$label}} {!! $star !!}</label>
        <input name="{{$col}}" class="form-control full-name" type="text"
               placeholder="{{$ph}}" value="{{ $default }}">
        @if($errors->has($col))
            <span class="help-block">
                <strong>{{ $errors->first($col) }}</strong>
            </span>
        @endif
    </div>
<?php   } ?>

</div>
