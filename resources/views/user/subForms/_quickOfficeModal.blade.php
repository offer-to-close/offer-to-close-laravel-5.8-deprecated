<!-- Quick Entry -->

@section('custom_css')
    <style>
        .error{
            color: #a94442;
            display: none;
            font-size: 12px;
        }

        .spinner{
            display: none;
            font-size: 25px;
            color: #a94442;
            position: absolute;
            top:10px;
            left: 47%;
            transform: translateX(-50%) !important;
        }


    </style>
@append
<?php
$star = '<span class="important">*</span></label>';
?>
<div class="modal" id="add{{str_replace([' ', '_', '-'], null, $targetTable)}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h6 class="modal-title">Add a new {{ strtoupper(substr($targetTable, 0)) }}</h6>
                <span><i class="fas fa-sync fa-spin spinner" style=""></i></span>
                <div class="row message"></div>
            </div>

            <form role="form" class="modal_form" action="{{ route('save.Modal.Office') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="modalModel" value="{{$targetTable}}">
                <input type="hidden" name="Agents_ID" value="{{ (!empty($data['ID']) ? $data['ID'] : '') }}">
                <input type="hidden" name="mainTable" value="{{ $mainTable }}">
                <input type="hidden" name="mainID" value="{{ $mainID }}">
                <input type="hidden" name="updateField" value="{{ $updateField }}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Office Name{!! $star !!}</label>
                            <input type="text" class="form-control" name="Name">
                            <label class="error" for="Name" id="first_name_error"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label for="">Street Address{!! $star !!}</label>
                            <input type="text" class="form-control" name="Street1">
                            <label class="error" for="Street1" id="street1_error"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">City{!! $star !!}</label>
                            <input type="text" class="form-control" name="City">
                            <label class="error" for="City" id="city_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label for="State" class="label-control">State <span class="important">*</span></label>
                            <select name="State" class="form-control">
                                {!! \App\Library\Utilities\FormElementHelpers::buildOptionListFromConstantArray($data['State']??'CA', Config::get('otc.states'), 'index', 'value') !!}
                            </select>
                            <label class="error" for="State" id="state_error"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Zip{!! $star !!}</label>
                            <input type="text" class="form-control" name="Zip">
                            <label class="error" for="Zip" id="zip_error"></label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submit_button" class="btn btn-primary">Add <span><i class="fas fa-sync fa-spin spinner" style=""></i></span> </button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>

</div>

@section('scripts')

    <script>
        $( document ).ready(function() {

            $(document).on('submit', '.modal_form', function(e){
                e.preventDefault();
                $('.error').hide();
                $('.spinner').show();
                $('#submit_button').attr('disabled', true);
                var el      = $(this),
                    url     = el.attr ('action'),
                    method  = el.attr('method'),
                    data    = {};
                el.find(':input').each(function(){
                    var el    = $(this),
                        name  = el.attr('name'),
                        value = el.val();
                    data[name] = value;
                });
                $.ajax({
                    type: method,
                    url: url,
                    data: data,
                    error: function(response){

                        $('.spinner').fadeOut(700, function(){
                            for(x in response['responseJSON']['errors']){
                                el = $('.modal_form').find('input[name = "'+ x +'"]');
                                el.siblings('.error').text(response['responseJSON']['errors'][x]).show();
                            }
                            $('#submit_button').attr('disabled', false);
                        });
                    },
                    success: function(response) {
                        var id        = response['ID'],
                            full_name = response['return']
                        $('.spinner').fadeOut(1000, function(){
                            $('#submit_button').attr('disabled', false);
                            $('.modal_form').trigger('reset');
                            $('.close').trigger('click');
                            $('.success_msg').show();
                            $('.success_msg').html('<div class="alert alert-success alert-dismissable text-center"><a href="#" class="close" data-dismiss="alert" alert-label="close">&times;</a>'+ data["modalModel"] +' '+ full_name + ' added!</div>').fadeOut(10000);
                        });
                        $('#search_office').val(full_name);
                        $('#BrokerageOffices_ID').val(id);
                        $('#BrokerageOffices_ID').trigger('change');

                    }
                });



            });

        });
    </script>

@append