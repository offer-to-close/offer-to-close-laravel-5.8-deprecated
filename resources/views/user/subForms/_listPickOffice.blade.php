@section('custom_css')
    <style>


        #serch_office{
            border-radius: 0px;
            border-color: grey;
        }
        .search_list{
            position: absolute;
            left: 0px;
            list-style: none;
            padding-left: 5px;
            background: white;
            max-height: 150px;
            overflow-y: scroll;
            box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
        }

        .search_list::-webkit-scrollbar{
            width: 5px;
            background-color: white;
        }

        .search_list::-webkit-scrollbar-thumb{
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #D62929;
        }
        .search_list li:hover{
            background: #f2f2f2;
            cursor: pointer;
        }
        .search_list{
            font-weight: 1000;
            color: #090f0f;
            display: none;
        }

        .search_spinner{
            font-size: 20px;
            position: absolute;
            top: 20%;
            left: 87%;
            display: none;
        }
    </style>
@append

<!-- List Pick -->
<?php
$modelDir = '\\App\\Models\\';
$role = $_role['code'];
$roleTable =array_search($role, \Illuminate\Support\Facades\Config::get('constants.USER_ROLE'));
$roleTable = str_replace(['Buyers', 'Sellers', '-', '_',], null, title_case($roleTable));
$roleModel = $modelDir . $roleTable;
if ($roleModel::count()) {
?>
<div class="row text-center">
    <div class="form-group">

        <form role="form" action="{{ $action }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_table" value="{{ $roleTable }}">
            <input type="hidden" name="_model" value="{{ $roleModel }}">
            <input type="hidden" name="_roleCode" value="{{ $role }}">
            <input type="hidden" name="_pickedID" value="">
            <div class="form-groups">
                <?php
                $col = '_pickedID';
                $label = 'Enter information below to create new, or choose from this list: ';
                $default = null;
                ?>
               <div class="row col-md-12 col-xs-12  text-right form-group{{ $errors->has($col) ? ' has-error' : '' }} with-label">
                       <div class="col-md-7">
                             <label for="{{$col}}" class="label-control">{{$label}}</label>
                       </div>

                       <div class="col-md-4 col-xs-12 text-left">
                                <input name="search_query" class="form-control" placeholder="Search for agents"autocomplete="off" type="search" id="serch_office" >
                                <i class="search_spinner fas fa-circle-notch fa-spin"></i>
                                <div class="col-md-12">
                                    <ul class="col-md-12 search_list" id="search_list"> 
                                       
                                    </ul>
                                </div>
                        </div>
                        <div class="col-md-1 text-left">
                            <button type="submit" style="font-size: 14px;"class="btn-sm btn-primary">Select</button>
                        </div>                                             
                </div>
            </div>
        </form>

    </div>
</div>
<?php } ?>

@section('scripts')
    <script>
    $(document).ready(function() {
        var iterator;
        $(document).on('keyup click', '#search_person', function() {
            //stop previous generator function
            iterator.return()
            var el = $(this),
                LENGTH = 0,
                data = {},
                form = el.closest('form');
            APP_URL = '{!! route('search.Office') !!}';

            form.find(':input').each(function() {
                var el = $(this),
                    name = el.attr('name'),
                    value = el.val();
                data[name] = value;
            });
            var query = el.val();
            if (query.length > LENGTH) {
               // $('.search_spinner').fadeIn(100);
                var COUNT = getRowCount(data);
                if(COUNT == '0'){
                    $('#search_list').html(" ");
                    $('#search_list').append('<li class="text-center text-muted">No Data Found</li>');
                    $('#search_list').show();
                }else{
                    $('#search_list').html(" ");
                    var limit = 50;
                    iterator = ajaxRequestToFetchData(COUNT, data, limit);
                    iterator.next();
                }
                
            } else {
                $('#search_list').hide();
                $('#search_list').html(" ");
            }
        });
        $(document).on('click', '#search_list li', function() {
            el = $(this);
            $('#search_person').val(el.text());
            $('#search_list').html("");
            $('input[name="_pickedID"]').val(el.attr("data-id"));
            $('#search_list').hide();
        });
        $(document).on('focusout', '#search_person', function() {
            $('#search_list').fadeOut(500);
            $('.search_spinner').fadeOut(500);
        });

        $.('#search_list').on('scroll', function(){
            var el = $(this);
            var scrollHeight = el.prop('scrollHeight');
            var divHeight = el.outerHeight(true) - 
            el.css("border-top")) - el.css("border-bottom")) ;

            var scrollEndPoint = scrollHeight - divHeight;
            var divScrollerTop = el.scrollTop();
            if(divScrollerTop == scrollerEndPoint){
                iterator . next();
            }
        });
/*
        $('#search_list').on('scroll', function() {
            var el = $(this);
            var scrollTop = el.scrollTop();
            var scrollHeight = this.scrollHeight;
            if(scrollTop + $(this).innerHeight() >= this.scrollHeight) {
                iterator.next();
                $(this).scrollTop(scrollHeight);
            }
        })
        */
        function *ajaxRequestToFetchData(total_rows, data, limit){
            if (total_rows > 50) {
                    $('#search_list').html(" ");
                    for (var offset = 0; offset <= total_rows; offset += limit) {
                        fetchRecords(APP_URL, limit, offset, data);
                        yield;
                    }
            } else fetchRecords(APP_URL, 50, 0, data);
        }

        function getRowCount(request_data) {
            var rows = $.parseJSON($.ajax({
                url: "{{ route('search.Person') }}",
                type: "GET",
                data: request_data,
                success: function(response) {},
                    async: false
                }).responseText);
            return rows;
        }

        function fetchRecords(url, limit, offset, request_data) {
            request_data['limit'] = limit;
            request_data['offset'] = offset;
            $.ajax({
                url: url,
                type: "POST",
                data: request_data,
                success: function(response) {
                    for (i in response) {
                        $('#search_list').append('<li data-id=' + response[i]["ID"] + '> ' + response[i]["NameFull"] + '</li>');
                    }
                    $('#search_list').show();
                    $('#search_list').scrollTop(0);
                    $('.search_spinner').hide();
                },
                error: function(response) {
                    $('.search_spinner').hide();
                }
            });
        }
    }); 
</script>
@append