@extends('layouts.app')

@section('custom_css')
    <style>
        .success_msg{
            position: fixed;
            top: -5px;
            left: 50%;
            z-index: 999999;
            transform: translateX(-50%);
        }
    </style>
@endsection

@section('content')
    @if(Request::is('ta/*'))
        @include('transaction._menubar')
    @endif

    <?php
    $route = route('assignRole.Broker');
    ?>
    @include('user.subForms._listPickPerson',['action'=>$route,  '_role'=>$_role,])

    <div class="mc-wrapper">
        <div class="mc-heading">

            @if (@$_action == 'saved' || session('data_saved'))
               @include('user._successMessageIcon')
            @endif

            <h1>
                @if($_screenMode == 'create')
                    Create New
                @elseif($_screenMode == 'edit')
                    Edit
                @else
                    View
                @endif
                {{ $_role['display'] }}
            </h1>
        </div>

        <!-- Show errors if they exist -->
        @if ($errors->any() && !$errors->has('_pickedID'))
            <div class="alert alert-danger alert-dismissable text-center">
                <a href="#" class="close" data-dismiss="alert" alert-label="close">&times;</a>
                There was an error submitting your form. Kindly review the errors below and resubmit your form
            </div>
        @endif
        <div class="row success_msg"></div>

        <!-- Create new broker form -->
        <?php
        $dsp = str_replace([' ','\''], null, $_role['display']);
        if(empty($data['Transactions_ID'])) $action = route('saveRole.' . $dsp);
        else $action =  route('save' . $dsp);
        $modalAction = route('save.Modal.Person');
        ?>

        <div class="mc-box-groups user new">
            <form action="{{ $action }}" method="POST">
                {{ csrf_field() }}

                <input type="hidden" name="_screenMode" value="{{ (!empty($_screenMode) ? $_screenMode : '') }}">
                <input type="hidden" name="_roleCode" value="{{ (!empty($_role['code']) ? $_role['code'] : '') }}">
                <input type="hidden" name="_roleDisplay"
                       value="{{ (!empty($_role['display']) ? $_role['display'] : '') }}">

                <input type="hidden" name="Transactions_ID" value="{{ (!empty($data['Transactions_ID']) ? $data['Transactions_ID'] : '') }}">
                <input type="hidden" name="Users_ID" value="{{ (!empty($data['Users_ID']) ? $data['Users_ID'] : '') }}">
                <input type="hidden"{{ @$data['ID'] ? '' : 'disabled' }} name="ID" value="{{ (!empty($data['ID']) ? $data['ID'] : '') }}">

                <div class="col-width-2">

                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-user"></i> PERSONAL DETAILS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._personalDetails', ['nameType' => '', 'callingForm'=>'br'])

                    </div>


                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-comments"></i>CONTACT DETAILS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._contactDetails')

                    </div>
                </div>
                <div class="col-width-2">

                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="fas fa-home"></i>ADDRESS</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._address')

                    </div>

                    <div class="mc-box">
                        <div class="mc-box-heading no-other">
                            <h2><i class="far fa-file-alt"></i>NOTES</h2>
                            <div class="clearfix"></div>
                        </div>

                        @include('user.subForms._notes')

                    </div>

                </div>

                <div class="clearfix"></div>
                <div class="ctrls">
                    <button class="btn red">SAVE</button>
                </div>
            </form>comm
        </div>
    </div>

    <!-- Modal Goes Here -->
    @include('user.subForms._quickPersonModal', ['targetTable'=>'Brokerage',
                                             'action'=>$modalAction,
                                             'mainTable'=>'Broker',
                                             'mainID'=>$data['ID'],
                                             'updateField'=>'Brokerage_ID',
                                             ])

@endsection

@section('scripts')
    @include('scripts.phoneMask')
    @include('scripts.disableAutofill')
@append