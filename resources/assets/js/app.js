
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./jquery.mask.js');
require('./jquery.steps.min.js');
require('./upload/core.js');
require('./upload/upload.js');

window.Vue = require('vue');
require('vue-resource');

window.Swal2 = require('sweetalert2');
window.swal  = require('sweetalert2');

window.moment   = require('moment-business-days');
moment.updateLocale('us', {
    workingWeekdays: [1, 2, 3, 4, 5],
});

/**
 * Vuex will be used for a Vue Store, so that we can update and move data around across components.
 */
import Vuex from 'vuex';
Vue.use(Vuex);

import Vuetify, {
    VApp,
    VAlert,
    VDataTable,
    VTextField,
    VExpansionPanel,
    VExpansionPanelContent,
    VCard,
    VCardTitle,
    VCardText,
    VCardActions,
    VDataIterator,
    VContainer,
    VFlex,
    VLayout,
    VImg,
    VBtn,
    VDivider,
    VParallax,

} from 'vuetify/lib';
import {Ripple} from 'vuetify/es5/directives';
Vue.use(Vuetify, {
    components: {
        VApp,
        VAlert,
        VDataTable,
        VTextField,
        VExpansionPanel,
        VExpansionPanelContent,
        VCard,
        VCardTitle,
        VCardText,
        VCardActions,
        VDataIterator,
        VContainer,
        VFlex,
        VLayout,
        VImg,
        VBtn,
        VDivider,
        VParallax
    },
    directives: {
        Ripple
    },
});

/**
 * This list of routes makes it easy for us to use routes inside of vue.js without having
 * to pass in and manage props inside of each component. These are universally accessible from any
 * Vue component.
 * @type {Store}
 */
const store = new Vuex.Store({
    state: {
        routes: [
            { name: 'saveAdHocDocument',            route: '/save/adhoc/document'},
            { name: 'checkDocumentExistence',       route: '/everSign/document/existence'},
            { name: 'cancelEverSignDocument',       route: '/everSign/document/cancel'},
            { name: 'getMergedTasksTimeline',       route: '/get/timeline'},
            { name: 'update.tasks',                 route: '/ts/ajax/updateTask'},
            { name: 'updateStatus',                 route: '/ts/timeline/update-transaction-status'},
            { name: 'timeline.recalculate',         route: '/transaction-timeline/recalculate'},
            { name: 'listAffectedTasks',            route: '/ts/task-list-to-change'},
            { name: 'approveTaskChange',            route: '/ts/task-date-change'},
            { name: 'updateMilestoneLength',        route: '/ts/update/milestone/length'},
            { name: 'transactionSummary.saveTask',  route: '/ts/todos/save'},
            { name: 'answer.questionnaire',         route: '/ts/questionnaire/answer/:questionnaireID/:transactionID/:type?/:key?'},
        ]
    },
    getters: {
       routeName: state => (name) => {
           return state.routes.filter(route => route.name == name);
       }
    }
});

// *
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
 

Vue.component('example-component', require('./components/ExampleComponent.vue'));
/**
 * This component can be accessed by going into components/DocumentPermissionsTable.vue
 * All the logic and scoped styling for this component can be found there.
 */
var documentPermissions = Vue.component('document-permissions',require('./components/DocumentPermissionsTable.vue'));
var accessSquare = Vue.component('access-square', require('./components/AccessSquare.vue'));
var documentBagView = Vue.component('document-bag-view', require('./components/DocumentBagView'));
var individualDocumentView = Vue.component('individual-document-view', require('./components/IndividualDocumentView'));
var documentBagListView = Vue.component('document-bag-list', require('./components/DocumentBagListView'));
var formatDate = Vue.component('date',require('./components/Date'));
var uploadDocument = Vue.component('upload-document', require('./components/UploadDocument'));
var property = Vue.component('property', require('./components/Property'));
var documentBagShare = Vue.component('document-bag-share', require('./components/DocumentBagShareView'));
var documents = Vue.component('documents', require('./components/Documents'));
var prosumerDashboard = Vue.component('prosumer-dashboard', require('./components/ProsumerDashboard'));
var timeline = Vue.component('timeline', require('./components/Timeline'));

/**
 * This is the instance of the component, the instance can only exist once per page, and you must put the id on a wrapper
 * div which will contain the components registered.
 */
const app = new Vue({
    el: '#app',
    store,
    components: {
        'document-permissions'     : documentPermissions,
        'access-square'            : accessSquare,
        'document-bag-view'        : documentBagView,
        'individual-document-view' : individualDocumentView,
        'document-bag-list'        : documentBagListView,
        'date'                     : formatDate,
        'upload-document'          : uploadDocument,
        'property'                 : property,
        'document-bag-share'       : documentBagShare,
        'documents'                : documents,
        'prosumer-dashboard'       : prosumerDashboard,
        'timeline'                 : timeline,
    },
    data: {
        json: {},
    },
    methods: {
        setJSON (payload){
            this.json = payload;
        },
    },
});



