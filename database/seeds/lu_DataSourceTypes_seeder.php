<?php

use Illuminate\Database\Seeder;

class lu_DataSourceTypes_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_DataSourceTypes';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'table',
            'Display' => 'Table',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'listfile',
            'Display' => 'List File',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'constants',
            'Display' => 'Constants',
            'Notes' => '',
            'Order' => 0.,
        ]);
    }
}
