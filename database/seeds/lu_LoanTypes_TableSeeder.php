<?php

use Illuminate\Database\Seeder;

class lu_LoanTypes_TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_LoanTypes';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'conventional',
            'Display' => 'Conventional',
            'Notes' => '',
            'Order' => 1.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'fha',
            'Display' => 'FHA',
            'Notes' => '',
            'Order' => 2.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'va',
            'Display' => 'VA',
            'Notes' => '',
            'Order' => 3.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'cash',
            'Display' => 'Cash Only Offer',
            'Notes' => '',
            'Order' => 4.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'other',
            'Display' => 'Other',
            'Notes' => '',
            'Order' => 100.,
        ]);
    }
}
