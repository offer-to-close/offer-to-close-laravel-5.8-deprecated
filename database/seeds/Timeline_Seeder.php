<?php

use Illuminate\Database\Seeder;

class Timeline_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'Timeline';
        DB::table($tbl)->delete();

        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'aoo',
            'Name'                 => 'Acceptance of Offer',
            'DaysOffset'           => 0,
            'fromEnd'              => false,
            'isRequired'           => true,
            'isStartEvent'         => true,
            'isEndEvent'           => false,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'acpt',
        ]);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'oe',
            'Name'                 => 'Open Escrow',
            'DaysOffset'           => 1,
            'fromEnd'              => false,
            'isRequired'           => true,
            'isStartEvent'         => false,
            'isEndEvent'           => false,
            'mustBeBusinessDay'    => true,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'oe',
        ]);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'coe',
            'Name'                 => 'Close Escrow',
            'DaysOffset'           => 45,
            'fromEnd'              => false,
            'isRequired'           => true,
            'isStartEvent'         => false,
            'isEndEvent'           => true,
            'mustBeBusinessDay'    => true,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'coe',
        ]);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'deposit',
            'Name'                 => 'Delivery of the initial deposit',
            'DaysOffset'           => 3,
            'fromEnd'              => false,
            'isRequired'           => true,
            'isStartEvent'         => false,
            'isEndEvent'           => false,
            'mustBeBusinessDay'    => true,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'dep',
        ]);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'dis-to-b',
            'Name'                 => 'Disclosures to Buyer',
            'DaysOffset'           => 7,
            'fromEnd'              => false,
            'isRequired'           => true,
            'isStartEvent'         => false,
            'isEndEvent'           => false,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'dis',
        ]);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'dis-from-b',
            'Name'                 => 'Fully executed disclosures from buyer',
            'DaysOffset'           => 10,
            'fromEnd'              => false,
            'isRequired'           => true,
            'isStartEvent'         => false,
            'isEndEvent'           => false,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'dis',
        ]);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'ic',
            'Name'                 => 'Inspection Contingency',
            'DaysOffset'           => 17,
            'fromEnd'              => false,
            'isRequired'           => true,
            'isStartEvent'         => false,
            'isEndEvent'           => false,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'insp',
        ]);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'lc',
            'Name'                 => 'Loan Contingency',
            'DaysOffset'           => 21,
            'fromEnd'              => false,
            'isRequired'           => true,
            'isStartEvent'         => false,
            'isEndEvent'           => false,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'lf',
        ]);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'app',
            'Name'                 => 'Appraisal Contingency Completed',
            'DaysOffset'           => 17,
            'fromEnd'              => false,
            'isRequired'           => true,
            'isStartEvent'         => false,
            'isEndEvent'           => false,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'apr',
        ]);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'pp',
            'Name'                 => 'Possession of Property',
            'DaysOffset'           => 0,
            'fromEnd'              => true,
            'isRequired'           => true,
            'isStartEvent'         => false,
            'isEndEvent'           => false,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'coe',
        ]);

        DB::table($tbl)->insert([
            'isTest'               => false,
            'State'                => 'CA',
            'Code'                 => 'la',
            'Name'                 => 'Loan Application',
            'DaysOffset'           => 3,
            'fromEnd'              => false,
            'isRequired'           => true,
            'isStartEvent'         => false,
            'isEndEvent'           => false,
            'DateCreated'          => date('Y-m-d h:i:s'),
            'TaskCategories_Value' => 'lf',
        ]);
    }
}
