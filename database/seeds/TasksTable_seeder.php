<?php

use Illuminate\Database\Seeder;

class TasksTable_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'Tasks';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        $this->builder();
    }


    public function builder()
    {
        $timeStart = time();
        $initMem = ini_get('memory_limit');
 //       ini_set("memory_limit", "2400M");

        $testLength = 0;

        if ($testLength == 0) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $file    = public_path('_imports/tasks/') . 'allTasks.csv';
        $records = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds'  . PHP_EOL . PHP_EOL;

        $tblTasks = DB::table('Tasks');
        $dateCreated = date('Y-m-d');

        foreach ($records as $idx => $rec)
        {
            if (empty($rec['State'])) continue;
            echo implode(', ', $rec) . PHP_EOL;
            if ($testLength > 0 && $idx  == $testLength)
            {
                echo ' ---- Import limit of ' . $testLength . ' has been reached while building task tables.' . PHP_EOL. PHP_EOL;
                break;
            }

            $recTasks = $rec;
            $recTasks['DateCreated'] = $dateCreated;
            $recTasks['isTest'] = false;

            $tasksID = $tblTasks->insertGetId($recTasks);

            unset($records[$idx]);
        }

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds'  . PHP_EOL;
        echo '===================' . PHP_EOL. PHP_EOL;
    }
}
