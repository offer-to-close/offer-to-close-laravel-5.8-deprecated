<?php

use Illuminate\Database\Seeder;

class _UpdateAgentLinks_Seeder extends Seeder
{
    public function run()
    {
        $links     = $brByLic = [];
        $timeStart = time();
        $initMem   = ini_get('memory_limit');
        ini_set("memory_limit", "2400M"); // This routine can be a memory hog, so we bump it up while running then
        // reduce it at the end.

        $testLength = 1000000; // 0000;

        if (!$testLength) echo PHP_EOL . 'All the records will be imported' . PHP_EOL . PHP_EOL;

        $br = $bg = $bo = $a = [];

        $file     = public_path('_imports/181220/') . 'CA-DRE_Agent_Update.csv';
        $records  = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);
        $timeLoad = time();
        echo 'Total records imported: ' . count($records) . ' in ' . ($timeLoad - $timeStart) . ' seconds' . PHP_EOL . PHP_EOL;

        foreach ($records as $idx => $rec)
        {
            if ($idx > $testLength) break;
            if (strtolower($rec['lic_type']) == 'corporation' || strtolower($rec['lic_type']) == 'broker') continue;
            if (empty($rec['rel_lic_type'])) continue;

            $links[] = ['aLic' => $rec['lic_number'], 'boLic' => $rec['rel_lic_number']];
        }

        $timeParse = time();
        echo 'Total BrokerageOffice Agent Connections: ' . count($links) . PHP_EOL;
        echo '   ...  in ' . ($timeParse - $timeLoad) . ' seconds' . PHP_EOL;

        echo '===================' . PHP_EOL . PHP_EOL;

        $a  = DB::table('Agents')->select('ID', 'License', 'BrokerageOffices_ID')->where('Status', '=', 'upload')->get();
        $bo = DB::table('BrokerageOffices')->select('ID', 'License')->where('Status', '=', 'upload')->get();

        foreach ($a as $line)
        {
            $aByLic[$line->License] = ['id' => $line->ID, 'boid' => $line->BrokerageOffices_ID];
        }

        foreach ($bo as $line)
        {
            $boByLic[$line->License] = $line->ID;
        }

        echo PHP_EOL . 'Total Brokerage Office List: ' . count($boByLic) . PHP_EOL;
        echo PHP_EOL . 'Total Agent List: ' . count($aByLic) . PHP_EOL;
        echo 'Link Count: ' . count($links) . PHP_EOL . PHP_EOL;

        $modInc = 1000;

        $linkCount = $errorCount = 0;

        foreach ($links as $idx => $link)
        {
            $alic  = $link['aLic'];
            $bolic = $link['boLic'];

            if (!isset($aByLic[$alic]) || !isset($boByLic[$bolic]))
            {
                $err = [];
                if (!isset($aByLic[$alic])) $err[] = 'Agent Lic#: ' . $alic;
                if (!isset($boByLic[$bolic])) $err[] = 'Brokerage Office Lic#: ' . $bolic;

                ++$errorCount;
                \Illuminate\Support\Facades\Log::error('Not Found: ' . implode(' & ', $err));
                continue;
            }

            if ($aByLic[$alic]['boid'] != $boByLic[$bolic])
            {
                DB::table('Agents')
                  ->where('ID', '=', $aByLic[$alic]['id'])
                  ->update(['BrokerageOffices_ID' => $boByLic[$bolic], 'Notes'=>'linked'.date('Y-m-d_H-i-s')]);

                ++$linkCount;
                $delta = time() - $timeStart;
                if (($linkCount % $modInc) == 0)
                {
                    echo '.... Count = ' . $linkCount . ' out of ' . count($links) . ' - ' .
                         ($delta > 180) ? (round($delta / 60, 2) . ' min') :
                        ($delta . ' sec') . ', ' . PHP_EOL . PHP_EOL;
                }
                if ($linkCount > 5000) $modInc = 5000;
                if ($linkCount > 10000) $modInc = 10000;
                if ($linkCount > 50000) $modInc = 50000;
            }
        }

        $timeLinks = time();
        echo $errorCount . ' link errors were found' . PHP_EOL;
        echo $linkCount . ' links made' . PHP_EOL;
        echo '   ...  in ' . ($timeLinks - $timeParse) . ' seconds' . PHP_EOL;

        ini_set("memory_limit", $initMem);
        $timeEnd = time();
        echo '===================' . PHP_EOL;
        echo ' Total Time ' . ($timeEnd - $timeStart) . ' seconds' . PHP_EOL;
        echo '===================' . PHP_EOL . PHP_EOL;
    }


    public function updateDisplayIndex(array $values, &$brByLic, &$ii)
    {
        //        echo '-+-+-  brByLic = ' . PHP_EOL;
        //        rsort($brByLic);
        //        var_export($brByLic);

        echo ++$ii . '  |-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+' . PHP_EOL;

        $caseString = 'case ID';
        $ids        = '';
        //      ddd(['values'=>var_export($values, true), /* 'brByLic'=>var_export($brByLic, true), */]);
        foreach ($values as $idx => $value)
        {
            $boLic = $value['boLic'];

            if (!isset($brByLic[$boLic]))
            {
                //               echo 'License #'.$boLic. ' not found in Brokers table' . PHP_EOL;
                continue;
            }
            $brID       = $brByLic[$boLic];
            $caseString .= " when $boLic then $brID" . PHP_EOL;
            $ids        .= " $boLic,";
        }
        $ids = trim($ids, ',');
        /*
                $sql = 'update BrokerageOffices set Brokers_ID = ' . $caseString . ' end where License in (' . $ids . ')';
                //var_export($sql);
                if (!strpos($sql, '()'))
                {
                    $rv = DB::update($sql);
                    if ($rv) var_export($sql);
                    echo PHP_EOL;
                }
        */
    }
}

