<?php

use Illuminate\Database\Seeder;
use App\Library\Utilities\_Files;
use Illuminate\Support\Facades\DB;
use App\Models\DocumentTemplate;
use App\Models\Document;
use App\Models\DocumentTemplateField;

class DocumentTemplates extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file       = public_path('_imports/templates/') . 'DocumentTemplates.csv';
        $records    = _Files::readArrayFromCSVFile($file);
        $templates  = [];
        foreach($records as $record)
        {
            $templates[$record['Documents_Code']][] = [
                'Documents_Code'    => $record['Documents_Code'],
                'Type'              => $record['Type'],
                'Page'              => $record['Page'],
                'FriendlyIndex'     => $record['FriendlyIndex'],
                'Identifier'        => $record['Identifier'],
                'X'                 => $record['X'],
                'Y'                 => $record['Y'],
                'Color'             => $record['Color'],
                'FontSize'          => $record['FontSize'],
                'TextStyle'         => $record['TextStyle'],
            ];
        }

        DB::table('DocumentTemplates')->truncate();
        Db::table('DocumentTemplateFields')->truncate();

        foreach ($templates as $documentCode => $template)
        {
            $templateModel  = new DocumentTemplate();
            $document       = Document::where('Code', $documentCode)->limit(1)->get();
            if(!$document->isEmpty())
            {
                $document = $document->first();
                $upsertedTemplate = $templateModel->upsert([
                    'DocumentName'      => $document['Description'],
                    'ShortName'         => $document['ShortName'],
                    'Documents_Code'    => $document['Code'],
                ]);

                foreach($template as $field)
                {
                    $fieldsModel = new DocumentTemplateField();
                    $fieldsModel->upsert([
                        'DocumentTemplates_ID'  => $upsertedTemplate->ID,
                        'Type'                  => $field['Type'],
                        'Page'                  => $field['Page'],
                        'FriendlyIndex'         => $field['FriendlyIndex'],
                        'Identifier'            => $field['Identifier'],
                        'X'                     => $field['X'],
                        'Y'                     => $field['Y'],
                        'Color'                 => $field['Color'],
                        'FontSize'              => $field['FontSize'],
                        'TextStyle'             => $field['TextStyle'],
                    ]);
                }
            }
        }
    }
}
