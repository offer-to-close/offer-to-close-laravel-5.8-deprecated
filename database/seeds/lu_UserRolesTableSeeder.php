<?php

use Illuminate\Database\Seeder;

class lu_UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = \Illuminate\Support\Facades\Config::get('constants.USER_ROLE_INT');
        $roleCodes = \Illuminate\Support\Facades\Config::get('constants.USER_ROLE');
        $loginable = ['h', 'p', 'tc',];
        $isViewable = ['b', 's', 'ba', 'sa',];

        $tbl = 'lu_UserRoles';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        foreach($roles as $dsp=>$intVal)
        {
            DB::table($tbl)->insert([
                'Value'       => $roleCodes[$dsp],
                'Display'     => $dsp,
                'Notes'       => '',
                'Order'       => in_array($roleCodes[$dsp], $isViewable) ? 1 : 0,
                'ValueInt'    => $intVal,
                'isLoginRole' => isset($loginable[$roleCodes[$dsp]]),
            ]);
        }
    }
}
