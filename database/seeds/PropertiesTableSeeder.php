<?php

use Illuminate\Database\Seeder;

class PropertiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Properties')->insert([
            'isTest'       => boolval(true),
            'Street1'      => '13135 Whistler Ave',
            'City'         => 'Granada Hills',
            'State'        => 'CA',
            'Zip'          => '91344',
            'County'       => 'Los Angeles',
            'ParcelNumber' => 'APN000001',
            'Notes'        => 'Test Case',
            'DateCreated'  => date("Y-m-d H:i:s"),
            'DateUpdated'  => date("Y-m-d H:i:s"),
            'YearBuilt'    => 1969,
       ]);
    }
}