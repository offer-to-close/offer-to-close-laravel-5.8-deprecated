<?php

use Illuminate\Database\Seeder;

class Questions_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'Questions';
        DB::table($tbl)->insert([
            'Title' => 'Lead paint disclosure',
            'Text' => 'What year was the home built',
            'AnswerElement' => 'textbox',
            'ChoiceSource' => 'table',
            'SuccessCriteria' => '< 1978',
            'DateCreated' => date('Y-m-d h:i:s'),
            'Notes' => '',
        ]);

        DB::table($tbl)->insert([
            'Title' => 'Mold report or waiver',
            'Text' => 'Is the mold inspection part of the contract',
            'AnswerElement' => 'yesno',
            'ChoiceSource' => '',
            'SuccessCriteria' => 'yes',
            'DateCreated' => date('Y-m-d h:i:s'),
            'Notes' => '',
        ]);
    }
}
