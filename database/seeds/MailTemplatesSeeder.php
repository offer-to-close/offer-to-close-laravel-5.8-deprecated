<?php

use Illuminate\Database\Seeder;

class MailTemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'MailTemplates';
        DB::table($tbl)->truncate();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        /****** Welcome  ******/
        $category = 'welcome';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Welcome to L (STC)',
            'Subject' => '[p]',
            'Message' => '*.emails.fromSTC.welcome_l',
            'Code' => 'otc-2018-w-3',
            'ToRole' => 'l',
            'FromRole' => 'stc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Intro Email to Buyer (BTC)',
            'Subject' => 'Getting Started on Paperwork for [p]',
            'Message' => '*.emails.fromBTC.introEmail_b',
            'Code' => 'otc-2018-w-4',
            'ToRole' => 'b',
            'FromRole' => 'btc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Intro Email to Seller\'s Agent (BTC)',
            'Subject' => 'Introduction for [p]',
            'Message' => '*.emails.fromBTC.introEmail_sa',
            'Code' => 'otc-2018-w-5',
            'ToRole' => 'sa',
            'FromRole' => 'btc',
            'CcRoles' => 'ba',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Intro Email to Escrow (BTC)',
            'Subject' => 'Escrow for [p]',
            'Message' => '*.emails.fromBTC.introEmail_e',
            'Code' => 'otc-2018-w-6',
            'ToRole' => 'e',
            'FromRole' => 'btc',
            'CcRoles' => 'ba',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Intro Email to Lender (BTC)',
            'Subject' => 'Loan Process for {{buyers}}',
            'Message' => '*.emails.fromBTC.introEmail_l',
            'Code' => 'otc-2018-w-7',
            'ToRole' => 'l',
            'FromRole' => 'btc',
            'CcRoles' => 'ba',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Intro Email to Seller (STC)',
            'Subject' => 'Congratulations on accepting an offer! [p]',
            'Message' => '*.emails.fromSTC.introEmail_s',
            'Code' => 'otc-2018-w-8',
            'ToRole' => 's',
            'FromRole' => 'stc',
            'CcRoles' => 'sa',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Intro Email to Buyer\'s Agent (STC)',
            'Subject' => 'Introduction for [p]',
            'Message' => '*.emails.fromSTC.introEmail_ba',
            'Code' => 'otc-2018-w-9',
            'ToRole' => 'ba',
            'FromRole' => 'stc',
            'CcRoles' => 'sa',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Intro to Escrow (STC)',
            'Subject' => 'Escrow for [p]',
            'Message' => '*.emails.fromSTC.introEmail_e',
            'Code' => 'otc-2018-w-10',
            'ToRole' => 'ba',
            'FromRole' => 'stc',
            'CcRoles' => 'sa',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Intro to Escrow (BA)',
            'Subject' => 'Escrow for [p]',
            'Message' => '*.emails.fromBA.introEmail_e',
            'Code' => 'otc-2018-w-11',
            'ToRole' => 'e',
            'FromRole' => 'ba',
            'CcRoles' => 'btc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Intro to Lender (BA)',
            'Subject' => 'Loan Process for {{buyers}}',
            'Message' => '*.emails.fromBA.introEmail_l',
            'Code' => 'otc-2018-w-12',
            'ToRole' => 'l',
            'FromRole' => 'ba',
            'CcRoles' => 'btc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Intro to Escrow (SA)',
            'Subject' => 'Escrow for [p]',
            'Message' => '*.emails.fromSA.introEmail_e',
            'Code' => 'otc-2018-w-13',
            'ToRole' => 'e',
            'FromRole' => 'sa',
            'CcRoles' => 'stc',
        ]);

        /*** Cron Daily Summary **/
        $category = 'Cron';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Cron Daily Summary',
            'Subject' => 'Daily Summary for {{date}}',
            'Message' => '*.emails.cron.dailySummary',
            'Code' => 'otc-2018-c-1',
        ]);

        /*** Snap NHD ***/
        $category = 'NHD';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Order SnapNHD',
            'Subject' => 'An order from Offer To Close',
            'Message' => '*.emails.reports.snapNHDemail',
            'Code' => 'otc-2018-r-1',
            'ToRole' => 'offertoclose@snapnhd.com',
        ]);


        /*** Invitations ***/
        $category = 'invite';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Invite new user to be an SA (BTC)',
            'Subject' => 'You\'re Invited to join Offer To Close',
            'Message' => '*.emails.invitations.newUserFromBtc_sa',
            'Code' => 'otc-2018-i-1',
            'ToRole' => 'sa',
            'FromRole' => 'btc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Invite new user to be a BA (BTC)',
            'Subject' => 'You\'re Invited to join Offer To Close',
            'Message' => '*.emails.invitations.newUserFromBtc_ba',
            'Code' => 'otc-2018-i-2',
            'ToRole' => 'ba',
            'FromRole' => 'btc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Invite new user to be an SA (STC)',
            'Subject' => 'You\'re Invited to join Offer To Close',
            'Message' => '*.emails.invitations.newUserFromStc_sa',
            'Code' => 'otc-2018-i-3',
            'ToRole' => 'sa',
            'FromRole' => 'stc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Invite new user to be a BA (STC)',
            'Subject' => 'You\'re Invited to join Offer To Close',
            'Message' => '*.emails.invitations.newUserFromStc_ba',
            'Code' => 'otc-2018-i-4',
            'ToRole' => 'ba',
            'FromRole' => 'stc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Member request approved',
            'Subject' => 'You\'re Invited to join Offer To Close',
            'Message' => '*.emails.invitations.requestedReply_positive',
            'Code' => 'otc-2019-i-5',
            'ToRole' => 'u',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Request OTC Beta',
            'Subject' => 'GET ME BETA ACCESS',
            'Message' => '*.emails.invitations.requestBeta',
            'Code' => 'otc-2019-i-6',
            'ToRole' => 'tc@offertoclose.com',
        ]);

        /*** Account ***/
        $category = 'account';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Alert staff to new member request',
            'Subject' => 'OTC - New Member Request for Review',
            'Message' => '*.emails.registration.newRequestAlertToStaff',
            'Code' => 'otc-2019-acc-1',
            'ToRole' => 'membership@OfferToClose.com',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Thank New Registration',
            'Subject' => 'Thank You For Registering With Offer To Close!',
            'Message' => '*.emails.registration.registrationThankYou',
            'Code' => 'otc-2019-acc-2',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Forgot Password (System)',
            'Subject' => 'Reset Your Offer To Close Password',
            'Message' => '*.emails.system.forgotPassword',
            'Code' => 'otc-2019-acc-3',
            'ToRole' => 'u',
        ]);

        /*** Documents **/
        $category = 'doc';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Document Shared',
            'Subject' => 'OTC: {{property_address}}, {{name}} has shared a document!',
            'Message' => '*.emails.documents.documentShared',
            'Code' => 'otc-2019-doc-1',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Send Disclosures to Buyer for Review and Signature (BTC)',
            'Subject' => 'Review: Seller Disclosures for [p]',
            'Message' => '*.emails.fromBTC.sendDisclosuresReviewAndSignature_b',
            'Code' => 'otc-2019-doc-002',
            'FromRole' => 'btc',
            'ToRole' => 'b',
            'CcRoles' => 'ba'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Send Disclosures to Seller to Be Completed (STC)',
            'Subject' => 'Disclosures for [p]',
            'Message' => '*.emails.fromSTC.sendDisclosures_ba',
            'Code' => 'otc-2019-doc-003',
            'FromRole' => 'stc',
            'ToRole' => 'ba',
            'CcRoles' => 'sa'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Send Disclosures to Buyer for Review and Signature (BA)',
            'Subject' => 'Review: Seller Disclosures for [p]',
            'Message' => '*.emails.fromBA.sendDisclosuresReviewAndSignature_b',
            'Code' => 'otc-2019-doc-004',
            'FromRole' => 'ba',
            'ToRole' => 'b',
            'CcRoles' => 'btc'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Send Disclosures to Buyer for Review and Signature (SA)',
            'Subject' => 'Disclosures for [p]',
            'Message' => '*.emails.fromSA.sendDisclosuresReviewAndSignature_ba',
            'Code' => 'otc-2019-doc-005',
            'FromRole' => 'sa',
            'ToRole' => 'ba',
            'CcRoles' => 'stc'
        ]);

        /*** Contact **/
        $category = 'contact';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Contact Us',
            'Subject' => 'OTC: New contact from the website',
            'Message' => '*.emails.contact.standard',
            'Code' => 'otc-2019-contact-1',
        ]);

        echo 'The End: ' . __CLASS__ . PHP_EOL;

        /*** Special Offers ***/
        $category = 'specials';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'First Time Special',
            'Subject' => 'FIRST TRANSACTION SPECIAL',
            'Message' => '*.emails.specials.firstTimeOffer',
            'Code' => 'otc-2019-special-1',
        ]);

        /*** Contract **/
        $category = 'contract';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'TC Contract Email (BTC)',
            'Subject' => 'Getting Started - TC Agreement',
            'Message' => '*.emails.fromBTC.tcAgreement_ba',
            'Code' => 'otc-2019-con-001',
            'ToRole' => 'ba',
            'FromRole' => 'btc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'TC Contract Email (STC)',
            'Subject' => 'Getting Started - TC Agreement',
            'Message' => '*.emails.fromSTC.tcAgreement_sa',
            'Code' => 'otc-2019-con-002',
            'ToRole' => 'sa',
            'FromRole' => 'stc',
        ]);

        /*** Requests ***/
        $category = 'request';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Order Home Warranty (BTC)',
            'Subject' => 'Ordering Home Warranty for [p]',
            'Message' => '*.emails.fromBTC.orderHomeWarranty_warrantyProvider',
            'Code' => 'otc-2019-req-001',
            'FromRole' => 'btc',
            'CcRoles' => 'ba',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Check Home Warranty Order Status (BTC)',
            'Subject' => 'Checking Status of Home Warranty for [p]',
            'Message' => '*.emails.fromBTC.checkHomeWarranty_warrantyProvider',
            'Code' => 'otc-2019-req-002',
            'FromRole' => 'btc',
            'CcRoles' => 'ba',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Request for Repairs (BTC)',
            'Subject' => 'Request for Repairs on [p]',
            'Message' => '*.emails.fromBTC.requestRepairs_b',
            'Code' => 'otc-2019-req-003',
            'FromRole' => 'btc',
            'ToRole' => 'b',
            'CcRoles' => 'ba',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Remove Inspection Contingency (BTC)',
            'Subject' => 'Inspection Contingency Removal Reminder',
            'Message' => '*.emails.fromBTC.removeIC_b_ba',
            'Code' => 'otc-2019-req-004',
            'FromRole' => 'btc',
            'ToRole' => 'b,ba',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Request Disclosures from Seller (BTC)',
            'Subject' => 'Status of Seller Disclosures for [p]',
            'Message' => '*.emails.fromBTC.requestDisclosures_sa',
            'Code' => 'otc-2019-req-005',
            'FromRole' => 'btc',
            'ToRole' => 'sa',
            'CcRoles' => 'ba'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Order Home Warranty (STC)',
            'Subject' => 'Ordering Home Warranty for [p]',
            'Message' => '*.emails.fromSTC.orderHomeWarranty_warrantyProvider',
            'Code' => 'otc-2019-req-006',
            'FromRole' => 'stc',
            'CcRoles' => 'sa'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Check Home Warranty Order Status (STC)',
            'Subject' => 'Checking Status of Home Warranty for [p]',
            'Message' => '*.emails.fromSTC.checkHomeWarrantyOrder_warrantyProvider',
            'Code' => 'otc-2019-req-007',
            'FromRole' => 'stc',
            'CcRoles' => 'sa'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Request Disclosures from Seller (STC)',
            'Subject' => 'Disclosures Needing to Be Completed',
            'Message' => '*.emails.fromSTC.requestDisclosures_sa',
            'Code' => 'otc-2019-req-008',
            'ToRole' => 's',
            'FromRole' => 'stc',
            'CcRoles' => 'sa'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Order Home Warranty (BA)',
            'Subject' => 'Ordering Home Warranty for [p]',
            'Message' => '*.emails.fromBA.orderHomeWarranty_warrantyProvider',
            'Code' => 'otc-2019-req-009',
            'FromRole' => 'ba',
            'CcRoles' => 'btc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Check Home Warranty Order Status (BA)',
            'Subject' => 'Checking Status of Home Warranty for [p]',
            'Message' => '*.emails.fromBA.checkHomeWarrantyOrder_warrantyProvider',
            'Code' => 'otc-2019-req-010',
            'FromRole' => 'ba',
            'CcRoles' => 'btc'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Request for Repairs (BA)',
            'Subject' => 'Request for Repairs on [p]',
            'Message' => '*.emails.fromBA.requestRepairs_b',
            'Code' => 'otc-2019-req-011',
            'FromRole' => 'ba',
            'ToRole' => 'b',
            'CcRoles' => 'btc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Remove Inspection Contingency (BA)',
            'Subject' => 'Inspection Contingency Removal Reminder',
            'Message' => '*.emails.fromBA.removeIC_b',
            'Code' => 'otc-2019-req-012',
            'FromRole' => 'ba',
            'ToRole' => 'b',
            'CcRoles' => 'btc',
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Request Disclosures from Seller (BA)',
            'Subject' => 'Status of Seller Disclosures for [p]',
            'Message' => '*.emails.fromBA.requestDisclosures_sa',
            'Code' => 'otc-2019-req-013',
            'ToRole' => 'sa',
            'FromRole' => 'ba',
            'CcRoles' => 'btc'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Order Home Warranty (SA)',
            'Subject' => 'Ordering Home Warranty for [p]',
            'Message' => '*.emails.fromSA.orderHomeWarranty_warrantyProvider',
            'Code' => 'otc-2019-req-014',
            'FromRole' => 'sa',
            'CcRoles' => 'stc'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Check Home Warranty Order Status (SA)',
            'Subject' => 'Checking Status of Home Warranty for [p]',
            'Message' => '*.emails.fromSA.checkHomeWarrantyOrder_warrantyProvider',
            'Code' => 'otc-2019-req-015',
            'FromRole' => 'sa',
            'CcRoles' => 'stc'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Request Disclosures from Seller (SA)',
            'Subject' => 'Disclosures Needing to Be Completed',
            'Message' => '*.emails.fromSA.requestDisclosures_s',
            'Code' => 'otc-2019-req-016',
            'ToRole' => 's',
            'FromRole' => 'sa',
            'CcRoles' => 'stc'
        ]);

        /*** Reminder ***/
        $category = 'reminder';

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Escrow Follow Up on EMD (BTC)',
            'Subject' => 'EMD for [p]',
            'Message' => '*.emails.fromBTC.followUpEMD_e',
            'Code' => 'otc-2019-rem-001',
            'FromRole' => 'btc',
            'ToRole' => 'e',
            'CcRoles' => 'ba'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Remind Buyer to Schedule Inspections (BTC)',
            'Subject' => 'Reminder: Schedule Inspections',
            'Message' => '*.emails.fromBTC.remindScheduleInspections_b',
            'Code' => 'otc-2019-rem-002',
            'FromRole' => 'btc',
            'ToRole' => 'b',
            'CcRoles' => 'ba'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Pre-Closing Email (BTC)',
            'Subject' => 'Closing is Just Around the Corner',
            'Message' => '*.emails.fromBTC.preClosing_b',
            'Code' => 'otc-2019-rem-003',
            'FromRole' => 'btc',
            'ToRole' => 'b',
            'CcRoles' => 'ba'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Escrow Follow Up on EMD (STC)',
            'Subject' => 'EMD for [p]',
            'Message' => '*.emails.fromSTC.followUpEMD_e',
            'Code' => 'otc-2019-rem-004',
            'ToRole' => 'e',
            'FromRole' => 'stc',
            'CcRoles' => 'sa'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Escrow Follow Up on EMD (BA)',
            'Subject' => 'EMD for [p]',
            'Message' => '*.emails.fromBA.followUpEMD_e',
            'Code' => 'otc-2019-rem-005',
            'ToRole' => 'e',
            'FromRole' => 'ba',
            'CcRoles' => 'btc'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Remind Buyer to Schedule Inspections (BA)',
            'Subject' => 'Reminder: Schedule Inspections',
            'Message' => '*.emails.fromBA.remindScheduleInspections_b',
            'Code' => 'otc-2019-rem-006',
            'FromRole' => 'ba',
            'ToRole' => 'b',
            'CcRoles' => 'btc'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Pre-Closing Email (BA)',
            'Subject' => 'Closing is Just Around the Corner',
            'Message' => '*.emails.fromBA.preClosing_b',
            'Code' => 'otc-2019-rem-007',
            'FromRole' => 'ba',
            'ToRole' => 'b',
            'CcRoles' => 'btc'
        ]);

        DB::table($tbl)->insert([
            'Category' => $category,
            'DisplayDescription' => 'Escrow Follow Up on EMD (SA)',
            'Subject' => 'EMD for [p]',
            'Message' => '*.emails.fromSA.followUpEMD_e',
            'Code' => 'otc-2019-rem-008',
            'FromRole' => 'sa',
            'ToRole' => 'e',
            'CcRoles' => 'stc'
        ]);

    }
}
