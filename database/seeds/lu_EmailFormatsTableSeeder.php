<?php

use Illuminate\Database\Seeder;

class lu_EmailFormatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_EmailFormats';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'html',
            'Display' => 'HTML',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'text',
            'Display' => 'Text',
            'Notes' => '',
            'Order' => 0.,
        ]);
    }
}
