<?php

use Illuminate\Database\Seeder;

class lu_ReferralFeeTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tbl = 'lu_ReferralFeeTypes';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'flat',
            'Display' => 'Flat',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'percentage',
            'Display' => 'Percentage',
            'Notes' => '',
            'Order' => 0.,
        ]);

    }
}
