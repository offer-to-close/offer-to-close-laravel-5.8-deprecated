<?php

use Illuminate\Database\Seeder;

class lu_DocumentAccessActions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $docActions = \Illuminate\Support\Facades\Config::get('constants.DocumentAccessActions');
        $values = array_keys($docActions);
        $intValues = array_column($docActions, 'value');
        $display   = array_column($docActions, 'display');
        $isVisible = array_column($docActions, 'isVisible');
        $isViewable = $values;

        $tbl = 'lu_DocumentAccessActions';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        foreach($values as $idx=>$value)
        {
            DB::table($tbl)->insert([
                'Value'       => $values[$idx],
                'Display'     => $display[$idx],
                'Notes'       => '',
                'Order'       => $isVisible[$idx] ? 1 : 0,
                'ValueInt'    => $intValues[$idx],
            ]);
        }

    }
}
