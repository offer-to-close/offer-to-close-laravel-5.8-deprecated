<?php

use Illuminate\Database\Seeder;

class lu_TypeCategories_seeder extends Seeder
{
/**
 * Run the database seeds.
 *
 * @return void
 */
    public function run()
    {
        $tbl = 'lu_TaskCategories';
        DB::table($tbl)->delete();
        $max = DB::table($tbl)->max('id') + 1;
        DB::statement('ALTER TABLE ' . $tbl . ' AUTO_INCREMENT = ' . $max);

        DB::table($tbl)->insert([
            'Value' => 'acpt',
            'Display' => 'Acceptance',
            'Notes' => '',
            'Order' => 1.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'oe',
            'Display' => 'Open Escrow',
            'Notes' => '',
            'Order' => 2.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'dep',
            'Display' => 'Deposit',
            'Notes' => '',
            'Order' => 3.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'dis',
            'Display' => 'Disclosures',
            'Notes' => '',
            'Order' => 4.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'insp',
            'Display' => 'Inspections',
            'Notes' => '',
            'Order' => 5.,
        ]);
        DB::table($tbl)->insert([
            'Value' => 'apr',
            'Display' => 'Appraisal',
            'Notes' => '',
            'Order' => 6.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'roc',
            'Display' => 'Removal of Contingencies',
            'Notes' => '',
            'Order' => 7.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'lf',
            'Display' => 'Loan Funds',
            'Notes' => '',
            'Order' => 8.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'coe',
            'Display' => 'Close of Escrow',
            'Notes' => '',
            'Order' => 9.,
        ]);


        DB::table($tbl)->insert([
            'Value' => 'hl',
            'Display' => 'House Listing',
            'Notes' => '',
            'Order' => 0.,
        ]);


        DB::table($tbl)->insert([
            'Value' => 'adv',
            'Display' => 'Advisories',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'aa',
            'Display' => 'Agency Agreement',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'con',
            'Display' => 'Contracts',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'e/t',
            'Display' => 'Escrow/Title',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'la',
            'Display' => 'Listing Agreement',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'rpt',
            'Display' => 'Reports',
            'Notes' => '',
            'Order' => 0.,
        ]);

        DB::table($tbl)->insert([
            'Value' => 'ah',
            'Display' => 'Other',
            'Notes' => '',
            'Order' => 1000,
        ]);
    }
}
