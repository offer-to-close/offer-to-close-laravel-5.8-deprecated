<?php

use Illuminate\Database\Seeder;

class BreDataInstall extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            _AddBreMloDataSeeder::class,
            _AddBreDataSeeder::class,
            _CreateAgentLinks_Seeder::class,
            _CreateBrokerLinks_Seeder::class,
        ]);
    }
}
