<?php

use Illuminate\Database\Seeder;

class BrokerageOfficesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('BrokerageOffices')->insert([
            'isTest' => true,
            'Name' => 'Brokerage Office ' . str_random(3),
            'Brokers_ID' => 1,
            'Street1' => '123 ' . str_random(8) ,
            'City' => str_random(6),
            'State' => str_random(2),
            'Zip' => '12345',
            'PrimaryPhone' => '',
            'Email' => str_random(4) . '@' . str_random(6) . '.' . str_random(3),
            'EmailFormat' => 'html',
            'DateCreated' => date('Y-m-d h:i:s'),
            'Notes' => '',
        ]);

    }
}
