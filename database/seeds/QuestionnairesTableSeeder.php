<?php

use Illuminate\Database\Seeder;
use App\Models\Questionnaire;
use App\Models\QuestionnaireQuestion;

class QuestionnairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = $questionnaires = $questions = array();

        $file    = public_path('_imports/questionnaires/') . 'Questionnaires.csv';
        $records = \App\Library\Utilities\_Files::readArrayFromCSVFile($file);

        DB::table('Questionnaires')->truncate();

        foreach($records as $rowNum => $record)
        {
            switch ($record['Field'])
            {
                case 'Questionnaire':
                    $data['row']                = $rowNum;
                    $data['Type']               = $record['Type'];
                    $data['State']              = $record['State'];
                    $data['Description']        = $record['Description'];
                    $data['ShortName']          = $record['ShortName'];
                    $data['Version']            = $record['Version'];
                    $data['ResponderRole']      = $record['ResponderRole'];
                    $data['Documents_Code']     = $record['Documents_Code'];
                    $data['IncludeWhen']        = $record['IncludeWhen'];
                    $data['isTest']             = $record['isTest'];
                    $data['Status']             = $record['Status'];
                    $data['DateCreated']        = $record['DateCreated'] == '' ? date('Y-m-d'): $record['DateCreated'];
                    $data['DateUpdated']        = $record['DateUpdated'] == '' ? date('Y-m-d'): $record['DateUpdated'];
                    $data['deleted_at']         = $record['deleted_at'] == '' ? NULL: $record['deleted_at'];
                    $data['Notes']              = $record['Notes'];
                    $questionnaires[] = $data;
                    unset($data);
                    break;

                case 'Q':
                    $data['row']                = $rowNum;
                    $data['FriendlyIndex']      = $record['FriendlyIndex'];
                    $data['ChildIndex']         = $record['ChildIndex'];
                    $data['Section']            = $record['Section'];
                    $data['Question']           = $record['Question'];
                    $data['Hint']               = $record['Hint'];
                    $data['IncludeWhen']        = $record['IncludeWhen'];
                    $data['DisplayOrder']       = $record['DisplayOrder'];
                    $data['PossibleAnswers']    = $record['PossibleAnswers'];
                    $data['QuestionType']       = $record['QuestionType'];
                    $data['isTest']             = $record['isTest'];
                    $data['Status']             = $record['Status'];
                    $data['DateCreated']        = $record['DateCreated'] == '' ? date('Y-m-d'): $record['DateCreated'];
                    $data['DateUpdated']        = $record['DateUpdated'] == '' ? date('Y-m-d'): $record['DateUpdated'];
                    $data['deleted_at']         = $record['deleted_at']  == '' ? NULL: $record['deleted_at'];
                    $data['Notes']              = $record['Notes'];
                    $questions[] = $data;
                    unset($data);
                    break;
            }
        }

        /**
         * Add the questionnaires to the database, save their rows and keys.
         */
        $questionnaireRows = [];
        foreach($questionnaires as $key => $ea_questionnaire)
        {

            $questionnaireRows[] = $ea_questionnaire['row'];

            $id = DB::table('Questionnaires')->insertGetId([
                'Type'              => $ea_questionnaire['Type'],
                'State'             => $ea_questionnaire['State'],
                'Description'       => $ea_questionnaire['Description'],
                'ShortName'         => $ea_questionnaire['ShortName'],
                'IncludeWhen'       => $ea_questionnaire['IncludeWhen'],
                'Version'           => $ea_questionnaire['Version'],
                'ResponderRole'     => $ea_questionnaire['ResponderRole'],
                'Documents_Code'    => $ea_questionnaire['Documents_Code'],
                'isTest'            => $ea_questionnaire['isTest'],
                'Status'            => $ea_questionnaire['Status'],
                'DateCreated'       => $ea_questionnaire['DateCreated'] ?? NULL,
                'DateUpdated'       => $ea_questionnaire['DateUpdated'] ?? NULL,
                'deleted_at'        => $ea_questionnaire['deleted_at'] ?? NULL,
            ]);

            $questionnaireIDs[] = $id;

            echo "\nInserting Questionnaire " . $ea_questionnaire['Description'];
        }

        /*
         * iterate through each row that has a questionnaire
         * give every question after that row the Questionnaires_ID of the questionnaire
         * stop at a new questionnaire and repeat
         *
         */
        for($i = 0; $i < count($questionnaireRows); $i++)
        {
            $dbQuestions = QuestionnaireQuestion::where('Questionnaires_ID', $questionnaireIDs[$i])->get();
            /*
             * Delete items in db that are deleted from the excel file
             */
            if(!$dbQuestions->isEmpty())
            {
                foreach ($dbQuestions as $dbQuestion)
                {
                    $key = array_search($dbQuestion->FriendlyIndex, array_column($questions,'FriendlyIndex'));
                    if($key === FALSE)
                    {
                        DB::table('QuestionnaireQuestions')
                            ->where('ID', $dbQuestion->ID)
                            ->update([
                                'deleted_at' => date('Y-m-d'),
                        ]);
                        echo "\n".'Removed QuestionnaireQuestion with friendly index (soft delete): '.$dbQuestion->FriendlyIndex;
                    }
                }
            }
            foreach($questions as $q)
            {
                if($i + 1 != count($questionnaireRows))
                {
                    if($q['row'] > $questionnaireRows[$i] && $q['row'] < $questionnaireRows[$i+1])
                    {
                        $existingQuestion = QuestionnaireQuestion::where('Questionnaires_ID', $questionnaireIDs[$i])
                            ->where('FriendlyIndex', $q['FriendlyIndex'])
                            ->limit(1)
                            ->get();

                        $values = [
                            'Questionnaires_ID'     => $questionnaireIDs[$i],
                            'FriendlyIndex'         => $q['FriendlyIndex'],
                            'ChildIndex'            => $q['ChildIndex'],
                            'Section'               => $q['Section'],
                            'Question'              => $q['Question'],
                            'Hint'                  => $q['Hint'],
                            'IncludeWhen'           => $q['IncludeWhen'],
                            'DisplayOrder'          => $q['DisplayOrder'],
                            'PossibleAnswers'       => $q['PossibleAnswers'],
                            'QuestionType'          => $q['QuestionType']
                        ];

                        if(!$existingQuestion->isEmpty())
                        {
                            $values['ID'] = $existingQuestion->first()->ID;
                        }

                        $questionnaireQuestion = new QuestionnaireQuestion();
                        $questionnaireQuestion->upsert($values);
                    }
                }
                else{
                    if($q['row'] > $questionnaireRows[$i])
                    {
                        $existingQuestion = QuestionnaireQuestion::where('Questionnaires_ID', $questionnaireIDs[$i])
                            ->where('FriendlyIndex', $q['FriendlyIndex'])
                            ->limit(1)
                            ->get();

                        $values = [
                            'Questionnaires_ID'     => $questionnaireIDs[$i],
                            'FriendlyIndex'         => $q['FriendlyIndex'],
                            'ChildIndex'            => $q['ChildIndex'],
                            'Section'               => $q['Section'],
                            'Question'              => $q['Question'],
                            'Hint'                  => $q['Hint'],
                            'IncludeWhen'           => $q['IncludeWhen'],
                            'DisplayOrder'          => $q['DisplayOrder'],
                            'PossibleAnswers'       => $q['PossibleAnswers'],
                            'QuestionType'          => $q['QuestionType']
                        ];

                        if(!$existingQuestion->isEmpty())
                        {
                            $values['ID'] = $existingQuestion->first()->ID;
                        }

                        $questionnaireQuestion = new QuestionnaireQuestion();
                        $questionnaireQuestion->upsert($values);
                    }
                } //end else statement
            } //end foreach for questions
        } //end the for loop
    } //end of run function
}
