<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameLkTransactionsDisclosuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__.PHP_EOL;
        Schema::rename('lk_Transactions-Disclosures', 'lk_Transactions-Questionnaires');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('lk_Transactions-Questionnaires', 'lk_Transactions-Disclosures');
    }
}
