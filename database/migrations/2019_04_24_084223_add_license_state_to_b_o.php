<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLicenseStateToBO extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('BrokerageOffices', function (Blueprint $table) {
            $table->string('LicenseState', 25)->after('License')->default('')->comment('The state in which this license number is valid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('BrokerageOffices', function (Blueprint $table) {
            $table->dropColumn('LicenseState');
        });
    }
}
