<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMessageColumnLogMail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_Mail', function (Blueprint $table) {
            if(Schema::hasColumn('log_Mail', 'Message')) //check whether log_Mail table has Message column and change it.
            {
                $table->longText('Message')->change();
            }
            else  //if it does not have Message column, create it.
            {
                if(Schema::hasColumn('log_Mail', 'Subject')) //check whether log_Mail table has Subject column.
                {
                    $table->longText('Message')->after('Subject')->nullable()->comment='Email text';
                }
                else
                {
                    $table->longText('Message')->nullable()->comment='Email text';
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('log_Mail', 'Message')) //check whether Log_Mail table has Message column.
        {
            Schema::table('log_Mail', function (Blueprint $table) {
                $table->string('Message')->change();
            });
        }
    }
}
