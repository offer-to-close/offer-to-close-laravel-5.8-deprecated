<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Alerts', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('Type')->nullable()->comment="What type of alerts is this: Email, SMS, etc.";
            $table->string('Model')->nullable(FALSE)->comment="Name of the Model this alert is for.";
            $table->integer('Model_ID')->nullable(FALSE)->comment="ID of Model.";
            $table->boolean('isActive')->default(1)->comment="Whether or not the alert is active. (Showing up to the user)";
            $table->timestamp('DateRead')->nullable()->comment="Date that the alert was marked as read.";
            $table->integer('Priority')->default(5)->comment="A value from 1-10 indicating level of priority, 10 being the highest.";
            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Alerts');
    }
}
