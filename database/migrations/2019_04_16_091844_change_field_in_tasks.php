<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldInTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__   . PHP_EOL . PHP_EOL;

        Schema::table('Tasks', function (Blueprint $table) {
            $table->string('Timeline_Code', 10)->default('')->after('Timeline_ID')->comment('Foreign key to Timeline.Code');
        });
        echo __CLASS__ . ': schema change complete' . PHP_EOL . PHP_EOL;

        $milestones = \App\Models\Timeline::select('ID', 'Code')->get();
        foreach($milestones as $milestone)
        {
            \App\Models\Task::where('Timeline_ID', '=', $milestone->ID)->update(['Timeline_Code'=>$milestone->Code]);
        }
        echo __CLASS__ . ': data change complete' . PHP_EOL . PHP_EOL;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo __METHOD__   . PHP_EOL . PHP_EOL;

        Schema::table('Tasks', function (Blueprint $table) {
            $table->dropColumn('Timeline_Code');
        });
    }
}
