<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionToMailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MailTemplates', function (Blueprint $table) {
            $table->string('DisplayDescription',150)->after('Category')->default(NULL)->nullable()->comment('What gets shown to users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MailTemplates', function (Blueprint $table) {
            $table->dropColumn('DisplayDescription');
        });
    }
}
