<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLkTransactionsDocumentAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_TransactionsDocumentAccess', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('Documents_ID')->comment="Foreign key to Documents table";
            $table->integer('Transactions_ID')->index()->comment="Foreign key to Transactions table";
            $table->string('PovRole')->comment="Point Of View Role. Values come from list of transaction roles";
            $table->integer('TransferSum')->comment="Sum of the User Role values that describe which transaction roles the POV Role can share the document to";

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

            $table->unique(['Transactions_ID', 'Documents_ID', 'PovRole'], 'unique_transaction_document_access');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_TransactionsDocumentAccess');
    }
}
