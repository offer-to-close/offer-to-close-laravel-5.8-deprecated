<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Documents', function (Blueprint $table) {
            $table->increments('ID');

            $table->string('State', 2)->nullable();
            $table->string('Category', 50)->nullable()->comment='The type of document this is e.g. disclosures, contracts, etc. lu_DocumentTypes';
            $table->integer('Order')->nullable()->comment='Part of OTC document code and to order the document in the stack';
            $table->string('ShortName')->nullable()->comment='Short name or abbreviation used by industry';
            $table->string('Description')->nullable()->comment='The full name of the document';
            $table->string('Provider')->nullable()->comment='The member of the transaction party that initializes the document, lu_TranasctionMembers';
            $table->integer('RequiredSignatures')->nullable()->comment='Who in the transaction party must sign the document';
            $table->string('IncludeWhen')->nullable()->comment='TBD';
            $table->integer('Timeline_ID')->nullable()->comment='Foreign key to Timeline Milestone as basis for calculating default date';
            $table->string('DateOffset', 20)->nullable()->comment='The number of days from the Timeline Milestone date. ';

            MigrationHelpers::assignStandardSwahFields($table);

            $table->unique(['State', 'Category', 'Order']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Documents');
    }
}
