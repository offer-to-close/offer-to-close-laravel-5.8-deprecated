<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLicenseAndCompanyToAuxillaryRoleTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Escrows', function (Blueprint $table)
        {
            $table->integer('License')->after('NameLast')->nullable();
            $table->integer('Company')->after('NameLast')->nullable();
        });
        Schema::table('Titles', function (Blueprint $table)
        {
            $table->integer('License')->after('NameLast')->nullable();
            $table->integer('Company')->after('NameLast')->nullable();
        });
        Schema::table('Loans', function (Blueprint $table)
        {
            $table->integer('License')->after('NameLast')->nullable();
            $table->integer('Company')->after('NameLast')->nullable();
        });

        Schema::table('Brokers', function (Blueprint $table)
        {
            $table->integer('License')->after('NameLast')->nullable();
        });
        Schema::table('Brokerages', function (Blueprint $table)
        {
            $table->integer('License')->after('Name')->nullable();
        });
        Schema::table('BrokerageOffices', function (Blueprint $table)
        {
            $table->integer('License')->after('Name')->nullable();
        });

        Schema::table('Agents', function (Blueprint $table)
        {
            $table->renameColumn('StateLicense', 'License');
            $table->renameColumn('Brokers_ID', 'BrokerageOffices_ID');
        });

        Schema::table('BrokerageOffices', function (Blueprint $table)
        {
            $table->renameColumn('Brokerages_ID', 'Brokers_ID');
        });

        Schema::table('Brokers', function (Blueprint $table)
        {
            $table->renameColumn('BrokerageOffices_ID', 'Brokerages_ID');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Escrows', function (Blueprint $table)
        {
            $table->dropColumn('License');
            $table->dropColumn('Company');
        });
        Schema::table('Titles', function (Blueprint $table)
        {
            $table->dropColumn('License');
            $table->dropColumn('Company');
        });
        Schema::table('Loans', function (Blueprint $table)
        {
            $table->dropColumn('License');
            $table->dropColumn('Company');
        });

        Schema::table('Brokers', function (Blueprint $table)
        {
            $table->dropColumn('License');
        });
        Schema::table('Brokerages', function (Blueprint $table)
        {
            $table->dropColumn('License');
        });
        Schema::table('BrokerageOffices', function (Blueprint $table)
        {
            $table->dropColumn('License');
        });

        Schema::table('Agents', function (Blueprint $table)
        {
            $table->renameColumn('License', 'StateLicense');
            $table->renameColumn('BrokerageOffices_ID', 'Brokers_ID');
        });

        Schema::table('Brokers', function (Blueprint $table)
        {
            $table->renameColumn('Brokerages_ID', 'BrokerageOffices_ID');
        });

        Schema::table('BrokerageOffices', function (Blueprint $table)
        {
            $table->renameColumn('Brokers_ID', 'Brokerages_ID');
        });
    }
}
