<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserManagement', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('users_id');
            $table->integer('AccountTypes')->nullable();
            $table->date('DateLastLoggedIn')->nullable();
            $table->boolean('isExpelled')->nullable();
            $table->boolean('isGuest')->nullable();

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UserManagement');
    }
}
