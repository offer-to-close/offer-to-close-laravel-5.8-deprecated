<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateTransactionCoordinatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('TransactionCoordinators', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('Users_ID')->nullable();

            MigrationHelpers::assignStandardNameFields($table);

            MigrationHelpers::assignStandardAddressFields($table);

            MigrationHelpers::assignStandardContactFields($table);

            MigrationHelpers::assignStandardSwahFields($table);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TransactionCoordinators');
    }
}
