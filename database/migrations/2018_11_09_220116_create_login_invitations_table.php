<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LoginInvitations', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('InvitedBy_ID')->nullable()->comment='Foreign Key to users table';
            $table->string('InvitedByRole')->nullable()->comment='The role of user who did the inviting';
            $table->string('InviteCode')->nullable()->comment='Code used to validate the invitee';
            $table->string('InviteeRole')->nullable()->comment='Role invitee will be registered as';
            $table->string('NameFirstInvitee')->nullable()->comment='Invitee\'s first name';
            $table->string('NameLastInvitee')->nullable()->comment='Invitee\'s last name';
            $table->string('EmailInvitee')->nullable()->comment='Invitee\'s Email';
            $table->string('EmailInviter')->nullable()->comment='Inviter\'s Email';
            $table->integer('Transactions_ID')->default(0)->nullable()->comment='Transaction ID from which the invitation was made. Zero if no transaction. Foreign key to Transactions table';
            $table->date('DateInvite')->nullable()->comment='Date the invitation was made';
            $table->date('DateInvitationExpires')->nullable()->comment='Date the invitation expires';
            $table->date('DateRegistered')->nullable()->comment='Date invitee registered';

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

 //           $table->index(['Category', 'DateTransmitted'], 'log_mail_category_date');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('LoginInvitations');
    }
}
