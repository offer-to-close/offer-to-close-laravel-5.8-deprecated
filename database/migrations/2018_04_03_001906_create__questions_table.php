<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Questions', function (Blueprint $table) {
            $table->increments('ID');
            $table->boolean('isTest')->nullable()->comment='Standard table field used for testing';
            $table->string('Status')->nullable();

            $table->string('Title');
            $table->string('Text');
            $table->string('AnswerElement');
            $table->string('ChoiceSource')->nullable(true);
            $table->string('SuccessCriteria')->nullable(true);
            $table->string('Unit')->nullable(true);

            $table->string('Notes')->nullable(true);
            $table->timestamp('DateCreated')->nullable();
            $table->timestamp('DateUpdated')->nullable(true);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Questions');
    }
}
