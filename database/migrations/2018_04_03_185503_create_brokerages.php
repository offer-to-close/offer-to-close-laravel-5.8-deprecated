<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateBrokerages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Brokerages', function (Blueprint $table)
        {
            $table->increments('ID');

            $table->string('Name');
            $table->boolean('isVerified');

            MigrationHelpers::assignStandardSwahFields($table);
        });

        Schema::table('BrokerageOffices', function (Blueprint $table)
        {
            $table->integer('Brokerages_ID')->nullable()->after('Status');
        });

        Schema::table('Agents', function (Blueprint $table)
        {
            $table->integer('Brokers_ID')->nullable()->after('Status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Brokerages');
        Schema::table('BrokerageOffices', function (Blueprint $table) {
            $table->dropColumn('Brokerages_ID');
        });
        Schema::table('Agents', function (Blueprint $table) {
            $table->dropColumn('Brokers_ID');
        });

    }
}
