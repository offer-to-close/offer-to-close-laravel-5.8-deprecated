<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLkTransactionsDocumentTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lk_TransactionsDocumentTransfers', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('Documents_ID')->comment="Foreign key to Documents table";
            $table->integer('Transactions_ID')->index()->comment="Foreign key to Transactions table";
            $table->string('PovRole')->comment="Point Of View Role. Values come from list of transaction roles";
            $table->integer('TransferSum')->comment="Sum of the User Role values that describe which transaction roles the POV Role can send the document to";
            $table->integer('SharingSum')->comment="Sum of the User Role values that describe which transaction roles the POV Role IS sharing with";

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);

            $table->unique(['Transactions_ID', 'Documents_ID', 'PovRole'], 'unique_transaction_document_transfer');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_TransactionsDocumentTransfers');
    }
}
