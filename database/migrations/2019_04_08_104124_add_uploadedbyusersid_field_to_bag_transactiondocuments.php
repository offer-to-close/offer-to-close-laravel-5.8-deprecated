<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUploadedbyusersidFieldToBagTransactiondocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bag_TransactionDocuments', function (Blueprint $table) {
            $table->integer('UploadedByUsers_ID')->nullable(FALSE)->comment('Foreign key to users table.')->after('ApprovedByUsers_ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bag_TransactionDocuments', function (Blueprint $table) {
            $table->dropColumn('UploadedByUsers_ID');
        });
    }
}
