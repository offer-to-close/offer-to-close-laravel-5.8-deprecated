<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisclosureQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DisclosureQuestions', function (Blueprint $table) {
            $table->increments('ID');
            $table->integer('Disclosures_ID')->comment('Foreign key to Disclosures');
            $table->string('Question');
            $table->string('Hint')->nullable()->comment('A learn more segment for this question');
            $table->string('IncludeWhen')->nullable()->comment('Condition for this question, based on other questions');
            $table->float('DisplayOrder')->comment('The order of the question to be displayed');
            $table->text('PossibleAnswers')->comment('List of possible answers to the question.');
            $table->string('QuestionType')->nullable()->comment('What type of question: dropdown, checkbox, radio, etc.');
            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DisclosureQuestions');
    }
}
