<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('MemberRequests', function (Blueprint $table)
        {
            $table->increments('ID');

            $table->string('State')->nullable();
            $table->integer('TransactionID')->default(0)->comment('Hopefully this value is in the Transactions table');
            $table->string('RequestedRole')->comment('This value will be one of the valid transaction roles');
            $table->string('PropertyAddress')->nullable()->comment('Full [single line] address');
            $table->string('TransactionSide')->nullable()->comment('b for buyer or s for seller');
            $table->string('NameFirst');
            $table->string('NameLast');
            $table->string('Email');
            $table->string('Company')->nullable();
            $table->boolean('isApproved')->default(0)->comment('True if membership is approved');
            $table->boolean('isRejected')->default(0)->comment('True if membership is denied');
            $table->string('DecisionReason')->nullable()->comment('UI should require explanation text if membership is rejected');
            $table->integer('DeciderUsers_ID')->default(0)->comment('Foreign key to users table for the person who decided');
            $table->timestamp('DateDecision')->nullable();
            $table->integer('Decision_LogMail_ID')->nullable()->comment('The candidate should get an email telling them their result and this is the Foreign key to the log_Mail table documenting the email.');

            \App\Library\Utilities\MigrationHelpers::assignStandardSwahFields($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MemberRequests');
    }
}
