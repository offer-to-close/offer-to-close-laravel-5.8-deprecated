<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CopyAgentsToStorage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo PHP_EOL . __METHOD__ . PHP_EOL . PHP_EOL;
        if (false)
        { // do nothing
            foreach (['Agents', 'Loans', 'BrokerageOffices'] as $tbl)
            {
                echo $tbl . PHP_EOL;
                $new = 'lot_' . $tbl;
                Schema::dropIfExists($new);
                Schema::rename($tbl, $new);
                DB::statement('CREATE TABLE ' . $tbl . ' LIKE ' . $new . '; ');
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        echo PHP_EOL . __METHOD__ . PHP_EOL . PHP_EOL;
        if (false)
        { // do nothing
            foreach (['Agents', 'Loans', 'BrokerageOffices'] as $tbl)
            {
                echo $tbl . PHP_EOL;
                $old = 'lot_' . $tbl;
                Schema::dropIfExists($tbl);
                Schema::rename($old, $tbl);
            }
        }
    }
}
