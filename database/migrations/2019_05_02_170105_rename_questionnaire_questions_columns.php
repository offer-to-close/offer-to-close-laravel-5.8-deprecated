<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameQuestionnaireQuestionsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__.PHP_EOL;
        Schema::table('QuestionnaireQuestions', function (Blueprint $table) {
            $table->renameColumn('Disclosures_ID','Questionnaires_ID');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('QuestionnaireQuestions', function (Blueprint $table) {
            $table->renameColumn('Questionnaires_ID','Disclosures_ID');
        });
    }
}
