<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpirationToApiAccess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('AccountAccess', function (Blueprint $table) {
            $table->timestamp('DateExpires')->nullable()->after('AccessCode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('AccountAccess', function (Blueprint $table) {
            $table->dropColumn('DateExpires');
        });
    }
}
