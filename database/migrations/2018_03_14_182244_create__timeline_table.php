<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Library\Utilities\MigrationHelpers;

class CreateTimelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Timeline', function (Blueprint $table) {
            $table->increments('ID');
            $table->boolean('isTest')->nullable()->comment='Standard table field used for testing';
            $table->string('Status')->nullable();

            $table->string('MilestoneName', 50)->unique()->comment='The name of this milestone. E.g. open, close, inspection.';
            $table->integer('OffsetDaysMin')->comment='Minimum allowable for offset. The offset is how many days from open this mileston is. Positive days are measured from open and negative days are measured from close';
            $table->integer('OffsetDaysMax')->comment='Maximum allowable for offset. The offset is how many days from open this mileston is. Positive days are measured from open and negative days are measured from close';
            $table->integer('OffsetDaysTypical')->comment='The typical value used. The offset is how many days from open this mileston is. Positive days are measured from open and negative days are measured from close';
            $table->string('WhoseAction', 25)->nullable()->comment='Which transaction party member is responsible for this milestone';

            $table->string('Notes')->nullable();
            $table->timestamp('DateCreated')->nullable();
            $table->timestamp('DateUpdated')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Timeline');
    }
}
