<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLuCommissionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lu_CommissionTypes', function (Blueprint $table) {
            $table->increments('ID');
            $table->boolean('isTest')->nullable()->comment='Standard table field used for testing';

            $table->string('Value', 25)->unique()->comment='The value that gets stored';
            $table->string('Display', 50)->comment='What gets displayed for this value';
            $table->string('Notes')->nullable();
            $table->float('Order')->nullable()->default(1)->comment='optional field that can be used to create an \'arbitrary\' sort order.';
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lu_CommissionTypes');
    }
}
