<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSharingSumToIntegerInBagTransactionDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bag_TransactionDocuments', function (Blueprint $table) {
            $table->integer('SharingSum')->comment('Sum of the User Role values that describe who this document is being shared with.')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bag_TransactionDocuments', function (Blueprint $table) {
            $table->boolean('SharingSum')->change();
        });
    }
}
