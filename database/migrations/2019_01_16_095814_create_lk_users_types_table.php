<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLkUsersTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('UserManagement');
        Schema::create('lk_UsersTypes', function (Blueprint $table) {
            $table->increments('ID');

            $table->integer('Users_ID')->comment='Foreign key to Users';
            $table->string('Type', 5)->comment='Foreign key to UserTypes lookup table value';
            $table->boolean('isActive')->default(true);

            $table->boolean('isTest')->default(0)->comment='Standard table field used for testing';
            $table->string('Status')->nullable();
            $table->timestamp('DateCreated')->nullable();
            $table->timestamp('DateUpdated')->nullable();
            $table->softDeletes();

            $table->unique(['Users_ID', 'Type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lk_UsersTypes');
    }
}
