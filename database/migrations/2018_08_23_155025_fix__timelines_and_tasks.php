<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTimelinesAndTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = 'ALTER TABLE `lk_Transactions-Tasks` 
                   CHANGE Tasks_ID TaskFilters_ID INT NOT NULL 
                    COMMENT \'Foreign key to Tasks\'';

        Schema::table('lk_Transactions-Tasks', function (Blueprint $table) {
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('lk_Transactions-Tasks');
            if ($doctrineTable->hasIndex('lk_tran_tasks_st_tran_id_task_id_unq'))
            {
                $table->dropUnique('lk_tran_tasks_st_tran_id_task_id_unq');
            }
        });

        \Illuminate\Support\Facades\DB::statement($query);

        Schema::table('lk_Transactions-Tasks', function (Blueprint $table) {
            $table->unique(['Transactions_ID', 'TaskFilters_ID'], 'lk_tran_tasks_st_tran_id_task_id_unq');
            $table->string('Description')->after('TaskFilters_ID')->nullable();
        });

        Schema::dropIfExists('Timeline');
        Schema::rename('MilestoneReferences', 'Timeline');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $query = 'ALTER TABLE `lk_Transactions-Tasks` 
                   CHANGE TaskFilters_ID Tasks_ID INT NOT NULL 
                    COMMENT \'Foreign key to Tasks\'';

        Schema::table('lk_Transactions-Tasks', function (Blueprint $table) {
            $sm = Schema::getConnection()->getDoctrineSchemaManager();
            $doctrineTable = $sm->listTableDetails('lk_Transactions-Tasks');
            if ($doctrineTable->hasIndex('lk_tran_tasks_st_tran_id_task_id_unq'))
            {
                $table->dropUnique('lk_tran_tasks_st_tran_id_task_id_unq');
            }
        });

        \Illuminate\Support\Facades\DB::statement($query);

        Schema::table('lk_Transactions-Tasks', function (Blueprint $table) {
            @$table->unique(['Transactions_ID', 'Tasks_ID'], 'lk_tran_tasks_st_tran_id_task_id_unq');
        });

        Schema::rename('Timeline', 'MilestoneReferences');

        Schema::create('Timeline', function (Blueprint $table) {
            $table->increments('ID');
            $table->boolean('isTest')->nullable()->comment='Standard table field used for testing';
            $table->string('Status')->nullable();

            $table->string('MilestoneName', 50)->unique()->comment='The name of this milestone. E.g. open, close, inspection.';
            $table->integer('OffsetDaysMin')->comment='Minimum allowable for offset. The offset is how many days from open this mileston is. Positive days are measured from open and negative days are measured from close';
            $table->integer('OffsetDaysMax')->comment='Maximum allowable for offset. The offset is how many days from open this mileston is. Positive days are measured from open and negative days are measured from close';
            $table->integer('OffsetDaysTypical')->comment='The typical value used. The offset is how many days from open this mileston is. Positive days are measured from open and negative days are measured from close';
            $table->string('WhoseAction', 25)->nullable()->comment='Which transaction party member is responsible for this milestone';

            $table->string('Notes')->nullable();
            $table->timestamp('DateCreated')->nullable();
            $table->timestamp('DateUpdated')->nullable();
            $table->softDeletes();
        });

    }
}
