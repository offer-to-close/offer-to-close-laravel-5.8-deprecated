<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameLkTransactionsQuestionnairesColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        echo __METHOD__;
//        DB::statement('alter table `lk_Transactions-Questionnaires` change column `Disclosures_ID` `Questionnaires_ID` integer(11) null');
        Schema::table('lk_Transaction-Questionnaires', function (Blueprint $table) {
  //          $table->renameColumn('isShared', 'SharingSum');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table `lk_Transactions-Questionnaires` change column `Questionnaires_ID` `Disclosures_ID` integer(11) null');
    }
}
