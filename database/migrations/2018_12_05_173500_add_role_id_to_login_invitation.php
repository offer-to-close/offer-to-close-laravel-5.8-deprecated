<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleIdToLoginInvitation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('LoginInvitations', function (Blueprint $table) {
            $table->integer('InviteeRole_ID')->after('InviteeRole')->nullable()->comment('Foreign key to the invitee\'s role table');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('LoginInvitations', function (Blueprint $table) {
            $table->dropColumn('InviteeRole_ID');
        });
    }
}
