<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeToDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Documents', function (Blueprint $table) {
            $table->string('Code')->default('*')->after('State')->comment('Unique code that identifies this document');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Documents', function (Blueprint $table) {
            $table->dropColumn('Code');
        });
    }
}
