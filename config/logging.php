<?php

return [

    'default' => env('LOG_CHANNEL','daily'),

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['syslog', 'mail', 'daily',],
        ],

        'syslog' => [
            'path' => storage_path('logs/sys.log'),
            'driver' => 'syslog',
            'level' => 'debug',
        ],


        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
        ],

        'daily' => [
            'driver' => 'daily',
            'level' => 'debug',
            'path' => storage_path('logs/laravel.log'),
            ],

        'mail' => [
            'driver' => 'daily',
//            'tap' => [App\Logging\CustomizeFormatter::class],
            'path' => storage_path('logs/mail.log'),
            'level' => 'critical',
        ],
    ],
];