<?php
/*
 * Usage:
 *      Config::get('developers.CONSTANT_NAME')
 *      To share pieces of data across your views:
 *
 *      View::share('my_constant', Config::get('developers.CONSTANT_NAME'));
 *      Put that at the top of your routes.php and the constant will then be accessible
 *      in all your blade views as:
 *        {{ $my_constant }}
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Parameters set to aid developers.
    |--------------------------------------------------------------------------
    |
    */

    'isTest' => env('isTest', false),
];
