<?php

return [


    /*
    |--------------------------------------------------------------------------
    | Handshake
    |--------------------------------------------------------------------------
    |
    | This is the secure password needed to communicate with the otcAdmin service
    |
    */

    'secureAdminHandshake' => env('AdminHandshake', 'No handshake set. Check .env'),
    ];
