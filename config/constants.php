<?php
/*
 * Usage:
 *      Config::get('constants.CONSTANT_NAME')
 *      To share pieces of data across your views:
 *
 *      View::share('my_constant', Config::get('constants.CONSTANT_NAME'));
 *      Put that at the top of your routes.php and the constant will then be accessible
 *      in all your blade views as:
 *        {{ $my_constant }}
 */

return [

    'LOG_ON'   => false,
    'LOG_FILE' => './otc_debug.log',
    'DEBUG_ON' => false,

    /*
      |--------------------------------------------------------------------------
      | system Default values
      |--------------------------------------------------------------------------
      |
      | These values are used as needed.
      |
      */
    'DEFAULT'  => [
        'ESCROW_LENGTH' => 45,
        'DATE_CONTRACT' => date('Y-m-d'),
        'DATE_START'    => date('Y-m-d'),
        'STATE_CODE'    => 'CA',
    ],

    /*
    |--------------------------------------------------------------------------
    | Sort by
    |--------------------------------------------------------------------------
    |
    | These values are used by lookup tables to determine on which field to sort
    | the return values
    |
    */

    'SORT_BY_NONE'    => null,
    'SORT_BY_VALUE'   => 'value',
    'SORT_BY_DISPLAY' => 'display',
    'SORT_BY_ORDER'   => 'order',


    /*
    |--------------------------------------------------------------------------
    | User Types
    |--------------------------------------------------------------------------
    |
    | These values are used by lookup tables to identify different levels of access people
    | have within the system. There are two types of values set; String values
    | and numeric values, the higher the value, the higher access. The way to measure access by
    | comparing the user's type value to the level of access associated with the page/process.
    |
    */

    'UserType' => [
        'z' => ['display' => 'super', 'value' => 1001, 'isVisible'=>false],
        'd' => ['display' => 'dev',   'value' => 400, 'isVisible'=>true],
        'a' => ['display' => 'admin', 'value' => 300, 'isVisible'=>true],
        's' => ['display' => 'staff', 'value' => 200, 'isVisible'=>true],
        'u' => ['display' => 'user',  'value' => 100, 'isVisible'=>true],
        'g' => ['display' => 'guest', 'value' => 1, 'isVisible'=>true],
    ],

    /*
    |--------------------------------------------------------------------------
    | User Roles
    |--------------------------------------------------------------------------
    |
    | These values are used by lookup tables to identify different people
    | associated with the transaction. There are two types of values set; String values
    | and power of 2 integers.
    |
    */

    'USER_ROLE' => [
        'GUEST'                           => 'g',
        'HOMESUMER'                       => 'h',
        'PROSUMER'                        => 'p',
        'BUYER'                           => 'b',
        'SELLER'                          => 's',
        'AGENT'                           => 'a',
        'TRANSACTION_COORDINATOR'         => 'tc',
        'BUYERS_AGENT'                    => 'ba',
        'SELLERS_AGENT'                   => 'sa',
        'BUYERS_TRANSACTION_COORDINATOR'  => 'btc',
        'SELLERS_TRANSACTION_COORDINATOR' => 'stc',
        'BROKER'                          => 'br',
        'BUYERS_BROKER'                   => 'bbr',
        'SELLERS_BROKER'                  => 'sbr',
        'ESCROW'                          => 'e',
        'LOAN'                            => 'l',
        'TITLE'                           => 't',
        'BROKERAGE_OFFICE'                => 'bo',
        'BROKERAGE'                       => 'bg',
        'OWNER'                           => 'o',
        'CREATOR'                         => 'c',
    ],

    'USER_ROLE_INT' => [
        'GUEST'                           => 0,
        'HOMESUMER'                       => 1,
        'PROSUMER'                        => 2,
        'BUYER'                           => 4,
        'SELLER'                          => 8,
        'AGENT'                           => 16,
        'TRANSACTION_COORDINATOR'         => 32,
        'BUYERS_AGENT'                    => 64,
        'SELLERS_AGENT'                   => 128,
        'BUYERS_TRANSACTION_COORDINATOR'  => 256,
        'SELLERS_TRANSACTION_COORDINATOR' => 512,
        'BROKER'                          => 1024,
        'BUYERS_BROKER'                   => 2048,
        'SELLERS_BROKER'                  => 4096,
        'ESCROW'                          => 8192,
        'LOAN'                            => 16384,
        'TITLE'                           => 32768,
        'BROKERAGE_OFFICE'                => 65536,
        'BROKERAGE'                       => 131072,
        'OWNER'                           => 262144,
        'CREATOR'                         => 324288,
    ],

    'RoleModel' => [
        'g'   => 'Guest',
        'h'   => null,
        'p'   => null,
        'b'   => 'Buyer',
        's'   => 'Seller',
        'a'   => 'Agent',
        'tc'  => 'TransactionCoordinator',
        'ba'  => 'Agent',
        'sa'  => 'Agent',
        'btc' => 'TransactionCoordinator',
        'stc' => 'TransactionCoordinator',
        'br'  => 'Broker',
        'bbr' => 'Broker',
        'sbr' => 'Broker',
        'e'   => 'Escrow',
        'l'   => 'Loan',
        't'   => 'Title',
        'bo'  => 'BrokerageOffice',
        'bg'  => 'Brokerage',
        'o'   => 'User',
    ],

    /*
    |--------------------------------------------------------------------------
    | Document Access Options
    |--------------------------------------------------------------------------
    |
    | These values are used by lookup tables to identify how a specific docuement can
    | be used by particular transaction roles There are two types of values set; String values
    | and numeric (power of 2 values).
    |
    */

    'DocumentAccessActions' => [
        'r' => ['display' => 'readonly', 'value' =>  1, 'isVisible'=>true],
        'e' => ['display' => 'edit',     'value' =>  2, 'isVisible'=>true],
        's' => ['display' => 'sign',     'value' =>  4, 'isVisible'=>true],
        'u' => ['display' => 'upload',   'value' =>  8, 'isVisible'=>true],
        'o' => ['display' => 'own',      'value' => 16, 'isVisible'=>true],
    ],

    /*
    |--------------------------------------------------------------------------
    | Document Access Options
    |--------------------------------------------------------------------------
    |
    | These values are used by lookup tables to identify how a specific document can
    | be used by particular transaction roles There are two types of values set; String values
    | and numeric (power of 2 values).
    |
    */

    'DocumentAccessRoles' => [
        'b'   => ['display' => 'Buyer',                             'value' =>     4, 'isVisible'=>true],
        'ba'  => ['display' => 'Buyer\'s Agent',                    'value' =>    64, 'isVisible'=>true],
        'btc' => ['display' => 'Buyer\'s Transaction Coordinator',  'value' =>   256, 'isVisible'=>true],
        's'   => ['display' => 'Seller',                            'value' =>     8, 'isVisible'=>true],
        'sa'  => ['display' => 'Seller\'s Agent',                   'value' =>   128, 'isVisible'=>true],
        'stc' => ['display' => 'Seller\'s Transaction Coordinator', 'value' =>   512, 'isVisible'=>true],
        'e'   => ['display' => 'Escrow',                            'value' =>  8192, 'isVisible'=>true],
        'l'   => ['display' => 'Loan',                              'value' => 16384, 'isVisible'=>true],
        't'   => ['display' => 'Title',                             'value' => 32768, 'isVisible'=>true],
    ],


    /*
    |--------------------------------------------------------------------------
    | Milestones
    |--------------------------------------------------------------------------
    |
    | These values are used by the timeline.
    |
    */
    'MILESTONES'    => ['START_OF_CONTRACT'                     => 0,
                        'OPEN_ESCROW'                           => 1,
                        'CLOSE_ESCROW'                          => 2,
                        'DELIVERY_OF_THE_INITIAL_DEPOSIT'       => 3,
                        'DISCLOSURES_TO_BUYER'                  => 4,
                        'FULLY_EXECUTED_DISCLOSURES_FROM_BUYER' => 5,
                        'INSPECTION_CONTINGENCY'                => 6,
                        'LOAN_CONTINGENCY'                      => 7,
                        'APPRAISAL_COMPLETED'                   => 8,
                        'POSSESSION_OF_PROPERTY'                => 9,
                        'LOAN_APPLICATION'                      => 10,
    ],

    'MILESTONES_DISPLAY_MAP' => [0  => 'Start of Contract',
                                 1  => 'Offer Accepted',
                                 2  => 'Close of Escrow',
                                 3  => 'Deposit Due',
                                 4  => '',
                                 5  => '',
                                 6  => 'Inspection Contingency',
                                 7  => 'Loan Contingency',
                                 8  => 'Appraisal Contingency',
                                 9  => 'Possession of Property',
                                 10 => 'Loan Application',
    ],


    /*
     |--------------------------------------------------------------------------
     | Directories
     |--------------------------------------------------------------------------
     |
     | These values are used as needed.
     |
     */
    'DIRECTORIES'            => [
        'data'      => './dataLists/',
        'help'      => './help/',
        'icons'     => './images/icons/',
        'images'    => './images/',
        'imports'   => './imports/',
        'override'  => './_SystemBoot/override/',
        'root'      => './',
        'uploads'   => './uploads/',
        'userList'  => './lib/php/_UserLists_/',
        'userlists' => './php/_UserLists_/',
        'seeders'   => './database/seeds/',
        'tempCache' => './__tempCache/',
        'templates' => './templates/',

        'transactionDocuments' => public_path('_uploaded_documents/transactions/'),
        'offerDocuments' => public_path('./_uploaded_documents/offers/'),

        'avatars' => 'public/images/avatars/',
        'propertyImages' => 'propertyImages/'
    ],


    /*
     |--------------------------------------------------------------------------
     | Storage URLS
     |--------------------------------------------------------------------------
     |
     | These values are used for storage, it is the value in between http:// and the following forward slash (if any).
     |
     |
     */

    'STORAGE_URLS'  => [
        'filestack' => 'cdn.filestackcontent.com',
        'eversign'  => 'eversign-files.s3.amazonaws.com',
    ],

    /*
     |--------------------------------------------------------------------------
     | API and KEYS
     |--------------------------------------------------------------------------
     |
     | These values are used for various APIs.
     |
     */

    'FILESTACK_SECRET' => env('FILESTACK_SECRET','K4BNWYR2JBBNRBKMDQIPTRO7OE'),

    //DocuSign
    'DOCUSIGN_INTEGRATOR_KEY' => env('INTEGRATOR_KEY','62a70dcd-a5a8-4886-aa27-5f03c8d713ee'),
    'DOCUSIGN_SECRET_KEY' => env('DOCUSIGN_SECRET_KEY', 'ec4a4227-ef0c-4403-bae8-acd2ca724d31'),
    'DOCUSIGN_INTEGRATOR_SECRET_KEY_CONCATENATION' => env('DOCUSIGN_INTEGRATOR_SECRET_KEY_CONCATENATION', 'NjJhNzBkY2QtYTVhOC00ODg2LWFhMjctNWYwM2M4ZDcxM2VlOmVjNGE0MjI3LWVmMGMtNDQwMy1iYWU4LWFjZDJjYTcyNGQzMQ=='),

    //Adobe Sign
    'ADOBE_CLIENT_ID' => env('ADOBE_CLIENT_ID', 'CBJCHBCAABAAblzGjsdG8oZ1od7igtVsgm_qpsXPXjzc'),

    //Google reCaptcha
    'RECAPTCHA_SITE_KEY' => env('RECAPTCHA_SITE_KEY', '6LcRbZoUAAAAAO-fOefXKTpajPQ5S0AlBzuXR6Eb'),
    'RECAPTCHA_SECRET_KEY' => env('RECAPTCHA_SECRET_KEY', '6LcRbZoUAAAAANoPRbNYbQdQtiKpEnXM-pJIg_qk'),

    //EverSign
    'EVERSIGN_BUSINESS_ID' => env('EVERSIGN_BUSINESS_ID', '60195'),
    'EVERSIGN_ACCESS_KEY' => env('EVERSIGN_ACCESS_KEY', '983b9d3c1597f8c1a279923064a9a57d')

    /*
      |--------------------------------------------------------------------------
      | URLs
      |--------------------------------------------------------------------------
      |
      | These values are used as needed.
      |
      */

 /*   'URLS'            => [
        'transactionDocuments' => asset('_uploaded_documents/transactions/'),
        'offerDocuments' => asset('./_uploaded_documents/offers/'),
    ],
*/
];
